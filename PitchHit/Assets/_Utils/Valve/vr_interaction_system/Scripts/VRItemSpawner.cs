﻿//===================== Copyright (c) Valve Corporation. All Rights Reserved. ======================

using UnityEngine;
using System.Collections;
using Valve.VR.InteractionSystem;

[RequireComponent( typeof( Interactable ) )]
public class VRItemSpawner : MonoBehaviour
{
	//Public variables
	public GameObject objectToSpawn;
	public bool requireTriggerPress;
	public string attachmentPoint;

	[EnumFlags]
	public Hand.AttachmentFlags attachmentFlags = Hand.defaultAttachmentFlags;

	//Private variables
	private bool alreadySpawned;

	//-----------------------------------------------------
	void OnHandHoverBegin( Hand hand )
	{
		Debug.Log( "OnHoverBegin" );
		if ( !requireTriggerPress )
		{
			SpawnAndAttachObject( hand );
		}
	}


	//-----------------------------------------------------
	void OnHandHoverEnd( Hand hand )
	{
		Debug.Log( "OnHandHoverEnd" );
		alreadySpawned = false;
	}


	//-----------------------------------------------------
	void HandHoverUpdate( Hand hand )
	{
		if ( requireTriggerPress && hand.GetStandardInteractionButtonDown() )
		{
			SpawnAndAttachObject( hand );
		}
	}


	//-----------------------------------------------------
	private void SpawnAndAttachObject( Hand hand )
	{
		if ( !alreadySpawned )
		{
			GameObject objectToAttach = GameObject.Instantiate( objectToSpawn );
			hand.AttachObject( objectToAttach, attachmentFlags, attachmentPoint );

			alreadySpawned = true;
		}
	}
}

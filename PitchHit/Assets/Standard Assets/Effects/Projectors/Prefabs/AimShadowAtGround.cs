﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimShadowAtGround : MonoBehaviour {

	// Update is called once per frame
	void Update ()
    {
		if(transform.eulerAngles.x != 90f)
        {
            transform.eulerAngles = new Vector3(90f, transform.eulerAngles.y, transform.eulerAngles.z);
        }
	}
}

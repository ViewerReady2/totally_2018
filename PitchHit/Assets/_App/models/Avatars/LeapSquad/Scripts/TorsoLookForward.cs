﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorsoLookForward : MonoBehaviour {

    public bool mirrorMode;

    public Transform torsoContainer;

    public Transform sourceHead;

    // Use this for initialization
    void Start () {

        if(!sourceHead)sourceHead = Camera.main.transform;

    }

    // Update is called once per frame
    void Update()
    {

        if (mirrorMode)
        {
            transform.position = sourceHead.position + Vector3.forward;
            transform.rotation = Quaternion.AngleAxis(180f, Vector3.up) * sourceHead.rotation;
        }
        else
        {
            transform.position = sourceHead.position;
            transform.rotation = sourceHead.rotation;
        }

        if (torsoContainer)
        {
            Vector3 torsoLookDirection = transform.forward;
            if(transform.forward.y < -.5f) //if looking down, use up for forward
            {
                torsoLookDirection = transform.up;
            }
            torsoLookDirection.y = 0f;

            torsoContainer.rotation = Quaternion.LookRotation(torsoLookDirection, Vector3.up);

        }
    }
}

﻿using UnityEngine;
using System.Collections;

public class WordsScaleScript : MonoBehaviour {

    public enum WordProperty { Positive, Negative}
    public WordProperty Property;
    public AudioClip soundClip;

    public float ExpandRate, DestroyTimer;
    private float Timer;
    private Transform Endpoint;
    private float pauseTime = 2.5f;
    private float fadeSpeed = 3f;
    [Range(0f,1f)]
    public float soundVolume = .6f;
    public TMPro.TextMeshPro tmp;
    public TMPro.TextMeshPro childTMP;
    private static CatchSoundController soundController;
    private Color originalColor;
	// Use this for initialization
	void Start ()
    {
        //tmp = GetComponent<TMPro.TextMeshPro>();
        if (tmp)
        {
            originalColor = tmp.color;
        }
        //if(!soundController)
        //{
        //    soundController = FindObjectOfType<SoundController>();
        //}

        StartCoroutine(SoundRoutine());
        


        /*
        if (Property == WordProperty.Negative)
        {
            InitialScale = new Vector3(transform.localScale.x, transform.localScale.y, transform.localScale.z);
            Debug.Log(InitialScale);
            //transform.localScale = new Vector3(0, 0, 0);
        }
        else
        {
            Endpoint = GameObject.Find("WordEndPoint").transform;
        }
        */
	}

    public static void SetSoundController(CatchSoundController controller)
    {
        if(!soundController)
        {
            soundController = controller;
        }
    }

    IEnumerator SoundRoutine()
    {
        yield return null;
        //soundController.PlaySoundAt(soundClip, soundController.transform.position,soundVolume); 
    }
	
	// Update is called once per frame
	void Update ()
    {
        Timer += 1 * Time.deltaTime;

        if(tmp.color.a==0f)
        {
            Destroy(gameObject);
        }

        
        if(Property == WordProperty.Positive)
        {
            transform.position += Vector3.up * Time.deltaTime * tmp.color.a;
        }
        else
        {
            transform.position -= Vector3.up * Time.deltaTime * tmp.color.a;
        }

        if (Timer > pauseTime)
        {
            tmp.color = new Color(originalColor.r, originalColor.g, originalColor.b, Mathf.MoveTowards(tmp.color.a, 0f, Time.deltaTime*fadeSpeed));
            if(childTMP)
            {
                childTMP.color = tmp.color;
            }
        }


        /*

        if (Property == WordProperty.Negative)
        {
            if (Timer < 0.5)
            {
                transform.localScale += InitialScale * (ExpandRate * Time.deltaTime);
            }
            else
            {
                transform.localScale -= InitialScale * (ExpandRate * Time.deltaTime);
            }
        }
        else
        {
            transform.position = Vector3.Lerp(transform.position, Endpoint.position, Time.deltaTime);
        }

        Destroy(gameObject, DestroyTimer);
        */
	}
}

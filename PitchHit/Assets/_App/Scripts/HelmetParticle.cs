﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelmetParticle : MonoBehaviour {

	void Start ()
    {
        Material mat = FieldManager.currentOffensiveTeam.helmetMaterial;
        gameObject.GetComponent<ParticleSystemRenderer>().material = mat;
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.UI;

public class RealButton : MonoBehaviour {
    public static int buttonsOnAlert;
    public enum ButtonType
    {
        simple,radio, clickClick, pushHold
    }
    public bool requirePushHold;
    public bool keepOculusSimple;
    public bool isLocked;
    public bool oneTimeUse;
    public GameObject lockedMessage;
    public ButtonType buttonType;
    public Transform theButton;
    public Transform theBase;
    public RealButton[] otherRadioButtons;
    public bool controlButtonColor;
    public bool controlBaseColor;
    public Material buttonMaterialOnUp;
    public Material buttonMaterialOnActive;
    public Material buttonMaterialOnHover;
    public Material buttonMaterialOnPressed;
    public Color buttonColorOnUp = Color.red;
    public Color buttonColorOnActive = Color.green;
    public Color buttonColorOnHover = Color.yellow;
    public Color buttonColorOnPressed = Color.white;
    public Color baseColorOnUp = Color.black;
    public Color baseColorOnActive = Color.black;
    public Color baseColorOnPressed = Color.black;
    public Color buttonColorOnLocked = Color.grey;
    public Color baseColorOnLocked = Color.black;
    public AudioClip soundOnActivate;
    public AudioClip soundOnDeactivate;
    public float delayForPushHold;
    public float buttonPushDepth = .07f;
    [Range(.1f,.9f)]
    public float buttonSensitivity = .5f;
    public float timeToHold = 1f;

    public UnityEvent onButtonActivated;
    public UnityEvent onButtonDeactivated;
    private const float lookAtAngle = 45f;
    private Vector3 deepestButtonPosition;

    public bool isActivated;
    private bool canUpdate = true;
    public Image holdingProgressImage;
    public Renderer buttonSkin;
    public Renderer baseSkin;
    //[HideInInspector]
    public bool onAlert;
    private bool alreadyHeld;
    private float holdCountdown;
    private IEnumerator currentHoldRoutine;
    private static LayerMask batLayer = -1;
    private static LayerMask pusherLayer = -1;
    //private AudioSource sound;
    public GameObject destroyAfterSoundPrefab;
    private Vector3 initialPosition;
    private Vector3 initialButtonLocalPosition;
    private bool usingPointer = false;
    private bool buttonIsTouched;
    private bool hasBeenPressed;
    private bool isBeingPointed;
    private float pressedDepth;

    [SerializeField]
    //AppearingMenu owningMenu;
	// Use this for initialization
	void Awake () {
        
        if(batLayer<0)
        {
            //~((1 << layer1) | (1 << layer2))
            batLayer = LayerMask.NameToLayer("Bat");
            pusherLayer = LayerMask.NameToLayer("ButtonPusher");
            //batLayer = (1<<(LayerMask.NameToLayer("Bat")));// | (1<<LayerMask.NameToLayer("ButtonPusher")));
            //batLayer = (LayerMask.NameToLayer("Bat") | LayerMask.NameToLayer("ButtonPusher"));
            Debug.Log("================================================================================ "+ (batLayer));
        }
        //sound = GetComponentInChildren<AudioSource>();

        //buttonSkin = theButton.GetComponent<Renderer>();
        //baseSkin = theBase.GetComponent<Renderer>();
        //holdingProgressImage = GetComponentInChildren<Image>();
        holdCountdown = timeToHold;
        holdingProgressImage.fillAmount = 0f;
        initialButtonLocalPosition = theButton.localPosition;

        SetupButton();
    }

    void Start()
    {
        RefreshColor();
    }

    private void SetupButton()
    {
        theButton.localPosition = initialButtonLocalPosition;
        initialPosition = transform.position;
        deepestButtonPosition = theButton.position;

        if (isActivated)
        {
            buttonSkin.material.color = buttonColorOnActive;
            baseSkin.material.color = baseColorOnActive;

            switch (buttonType)
            {
                case ButtonType.radio:
                    theButton.position = deepestButtonPosition;
                    break;
                case ButtonType.clickClick:
                    theButton.position = deepestButtonPosition + (theButton.forward * buttonPushDepth * buttonSensitivity);
                    break;
            }
        }
        else
        {
            if (isLocked)
            {
                buttonSkin.material.color = buttonColorOnLocked;
                baseSkin.material.color = baseColorOnLocked;
            }
            else
            {
                buttonSkin.material.color = buttonColorOnUp;
                baseSkin.material.color = baseColorOnUp;
            }
            theButton.position = deepestButtonPosition + (theButton.forward * buttonPushDepth);
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        /*
        if(onAlert)
        {
            Debug.DrawRay(transform.position, transform.forward, Color.red);
        }
        else
        {
            Debug.DrawRay(transform.position, transform.forward, Color.green);
        }
        */

        if(transform.position != initialPosition)
        {
            SetupButton();
        }
        if (Camera.main && Vector3.Angle(Camera.main.transform.forward, transform.position - Camera.main.transform.position) < lookAtAngle)
        {
            if (onAlert)
            {
                if (buttonType != ButtonType.radio || !isActivated)
                {
                    RaycastHit hit = new RaycastHit();
                    buttonIsTouched = false;
                    if (Physics.OverlapBox(deepestButtonPosition, theButton.lossyScale * .5f, theButton.rotation, (1 << batLayer) | (1 << pusherLayer)).Length > 0)
                    {

                        buttonIsTouched = true;
                        //Debug.Log("overlapped "+Time.time);
                        hit.distance = 0f;
                    }
                    else
                    {
                        buttonIsTouched = Physics.BoxCast(deepestButtonPosition, theButton.lossyScale * .5f, theBase.forward, out hit, theButton.rotation, buttonPushDepth, (1 << batLayer) | (1 << pusherLayer));
                        //if (buttonIsTouched)
                        //{
                            //Debug.Log("swept "+Time.time);
                        //}
                    }

                    if (buttonIsTouched)
                    //if (theButton.SweepTest(theButton.forward, out hit, buttonPushDepth,QueryTriggerInteraction.Collide))
                    {
                        if (isLocked)
                        {
                            theButton.position = deepestButtonPosition + (theButton.forward * hit.distance);
                            if (hit.distance < buttonPushDepth * buttonSensitivity)
                            {
                                if (!alreadyHeld)
                                {
                                    alreadyHeld = true;
                                    SetLockedMessageVisible(true);
                                }
                            }
                            else
                            {
                                if (alreadyHeld)
                                {
                                    //disappear lock message
                                    Debug.Log("A");
                                    alreadyHeld = false;
                                    SetLockedMessageVisible(false);
                                }
                            }
                        }
                        else
                        {
                            if (requirePushHold || (!keepOculusSimple && (GameController.isTrueOculus || GameController.isOculusInSteam)))
                            {
                                theButton.position = deepestButtonPosition + (theButton.forward * hit.distance);
                                if (hit.distance < buttonPushDepth * buttonSensitivity)
                                {
                                    SetHeld(true);
                                }
                                else
                                {
                                    SetHeld(false);
                                }
                            }
                            else
                            {
                                //Debug.Log("button being hit to"+hit.distance + " max"+buttonPushDepth);
                                switch (buttonType)
                                {
                                    case ButtonType.simple:
                                        theButton.position = deepestButtonPosition + (theButton.forward * hit.distance);
                                        if (hit.distance < buttonPushDepth * buttonSensitivity)
                                        {
                                            if (!alreadyHeld)
                                            {
                                                buttonSkin.material.color = buttonColorOnActive;
                                                baseSkin.material.color = baseColorOnActive;
                                                alreadyHeld = true;
                                                SetActivation(true);
                                            }
                                        }
                                        else
                                        {
                                            buttonSkin.material.color = buttonColorOnUp;
                                            baseSkin.material.color = baseColorOnUp;
                                            Debug.Log("A");
                                            alreadyHeld = false;
                                            SetActivation(false);
                                        }
                                        break;
                                    case ButtonType.pushHold:
                                        theButton.position = deepestButtonPosition + (theButton.forward * hit.distance);
                                        if (hit.distance < buttonPushDepth * buttonSensitivity)
                                        {
                                            SetHeld(true);
                                        }
                                        else
                                        {
                                            SetHeld(false);
                                        }
                                        break;
                                    case ButtonType.radio:
                                        if (hit.distance < buttonPushDepth * buttonSensitivity)
                                        {
                                            SetActivation(true);
                                            theButton.position = deepestButtonPosition;
                                            buttonSkin.material.color = buttonColorOnActive;
                                            baseSkin.material.color = baseColorOnActive;
                                            pressedDepth = buttonPushDepth;
                                        }
                                        else
                                        {
                                            theButton.position = deepestButtonPosition + (theButton.forward * hit.distance);
                                        }
                                        break;
                                    case ButtonType.clickClick:

                                        if (isActivated)
                                        {
                                            if (hit.distance > buttonPushDepth * buttonSensitivity)
                                            {
                                                alreadyHeld = false;
                                                hit.distance = buttonPushDepth * buttonSensitivity;
                                            }

                                            if (!alreadyHeld && hit.distance < buttonPushDepth * buttonSensitivity * buttonSensitivity)
                                            {
                                                alreadyHeld = true;
                                                SetActivation(false);
                                                buttonSkin.material.color = buttonColorOnUp;
                                                baseSkin.material.color = baseColorOnUp;
                                            }
                                        }
                                        else
                                        {
                                            if (!alreadyHeld && hit.distance < buttonPushDepth * buttonSensitivity)
                                            {
                                                alreadyHeld = true;
                                                SetActivation(true);
                                                buttonSkin.material.color = buttonColorOnActive;
                                                baseSkin.material.color = baseColorOnActive;
                                            }
                                        }
                                        theButton.position = deepestButtonPosition + (theButton.forward * hit.distance);

                                        break;
                                }
                            }
                        }
                        pressedDepth = buttonPushDepth - hit.distance;
                    }
                    else
                    {
                        if (!isBeingPointed)
                        {
                            OnNotTouched();
                        }
                    }
                }
            }
            else//not onAlert
            {
                if (!isBeingPointed)
                {
                    OnNotTouched();
                }
            }
        }
        
	}

    public void PointerHover()
    {
        if(buttonType== ButtonType.radio && isActivated)
        { return; }

        if (!onAlert && !buttonIsTouched)
        {
            OnNotTouched();
        }
        //theButton.position = deepestButtonPosition + (theButton.forward * (buttonPushDepth*buttonSensitivity));
        
        buttonSkin.material.color = buttonColorOnHover;
        baseSkin.material.color = baseColorOnUp;

        switch (buttonType)
        {
            case ButtonType.simple:
                //if (alreadyHeld)
                //{
                //    alreadyHeld = false;
                //    SetActivation(false);
                //}
                break;
            case ButtonType.pushHold:
                if (!onAlert)
                {
                    SetHeld(false);
                }
                break;
            case ButtonType.radio:
                
                break;
            case ButtonType.clickClick:
                break;
        }
        isBeingPointed = true;
    }

    public void PointerPress()
    {
        if (buttonType == ButtonType.radio && isActivated)
        { return; }

        theButton.position = deepestButtonPosition;

        if(isLocked)
        {
            SetLockedMessageVisible(true);
            return;
        }

        switch (buttonType)
        {
            case ButtonType.simple:
                buttonSkin.material.color = buttonColorOnActive;
                baseSkin.material.color = baseColorOnActive;
                alreadyHeld = true;
                SetActivation(true);

                break;
            case ButtonType.pushHold:
                SetHeld(true);

                break;
            case ButtonType.radio:
                SetActivation(true);
                theButton.position = deepestButtonPosition;
                buttonSkin.material.color = buttonColorOnActive;
                baseSkin.material.color = baseColorOnActive;
                pressedDepth = buttonPushDepth;

                break;
            case ButtonType.clickClick:
                if (!alreadyHeld)
                {
                    if (isActivated)
                    {
                        alreadyHeld = true;
                        SetActivation(false);
                        buttonSkin.material.color = buttonColorOnUp;
                        baseSkin.material.color = baseColorOnUp;
                    }
                    else
                    {
                        alreadyHeld = true;
                        SetActivation(true);
                        buttonSkin.material.color = buttonColorOnActive;
                        baseSkin.material.color = baseColorOnActive;
                    }
                }
                break;
        }
        isBeingPointed = true;
    }

    public float GetPressedDepth()
    {
        return pressedDepth;
    }

    public void RadioDeactivate()
    {
        SetActivation(false);
        theButton.position = deepestButtonPosition + (theButton.forward * buttonPushDepth);
        if (isLocked)
        {
            buttonSkin.material.color = buttonColorOnLocked;
            baseSkin.material.color = baseColorOnLocked;
        }
        else
        {
            buttonSkin.material.color = buttonColorOnUp;
            baseSkin.material.color = baseColorOnUp;
        }
    }

    public void SetIsLocked(bool locked)
    {
        //Debug.Log("SET LOOOCKED................. "+locked);
        if(!buttonSkin)
        {
            buttonSkin = theButton.GetComponent<Renderer>();
            baseSkin = theBase.GetComponent<Renderer>();
        }
        if(locked!=isLocked)
        {
           // Debug.Log("1");
            isLocked = locked;
            if(locked)
            {
               // Debug.Log("2");
                buttonSkin.material.color = buttonColorOnLocked;
                baseSkin.material.color = baseColorOnLocked;
               // Debug.Log("2a");
            }
            else
            {
               // Debug.Log("3");
                OnNotTouched();
                if(isActivated)
                {
                    buttonSkin.material.color = buttonColorOnActive;
                    baseSkin.material.color = baseColorOnActive;
                }
                else
                {
                    buttonSkin.material.color = buttonColorOnUp;
                    baseSkin.material.color = baseColorOnUp;
                }
            }
        }
    }

    public void RefreshColor()
    {

        if (!buttonSkin)
        {
            buttonSkin = theButton.GetComponent<Renderer>();
        }
        if (!baseSkin)
        {
            baseSkin = theBase.GetComponent<Renderer>();
        }

        if (isLocked)
        {
            buttonSkin.material.color = buttonColorOnLocked;
            baseSkin.material.color = baseColorOnLocked;
        }
        else
        {
            if (isActivated)
            {
                buttonSkin.material.color = buttonColorOnActive;
                baseSkin.material.color = baseColorOnActive;
            }
            else
            {
                buttonSkin.material.color = buttonColorOnUp;
                baseSkin.material.color = baseColorOnUp;
            }
        }
        OnNotTouched();
    }

    public void SetLockedMessageVisible(bool vis)
    {
        if(lockedMessage)
        {
            lockedMessage.SetActive(vis);
        }
    }

    private void SetHeld(bool holding)
    {
        if(alreadyHeld !=holding)
        {
            alreadyHeld = holding;
            if(holding)
            {
                if (!isActivated)
                {
                    buttonSkin.material.color = buttonColorOnPressed;
                    baseSkin.material.color = baseColorOnPressed;
                }
                currentHoldRoutine = HoldingRoutine();
                StartCoroutine(currentHoldRoutine);
            }
            else
            {
                if (!isActivated)
                {
                    buttonSkin.material.color = buttonColorOnUp;
                    baseSkin.material.color = baseColorOnUp;
                }
                holdingProgressImage.fillAmount = 0f;
                if (currentHoldRoutine != null)
                {
                    StopCoroutine(currentHoldRoutine);
                }
                holdCountdown = timeToHold;
            }
        }
    }

    public void SetActivation(bool a) 
    {
        if (isLocked)
        {
            return;
        }
        if (isActivated!=a)
        {
            isActivated = a;
            if(a)
            {
                if (buttonType == ButtonType.radio)
                {
                    buttonSkin.material.color = buttonColorOnActive;

                    foreach (RealButton rb in otherRadioButtons)
                    {
                        if (rb)
                        {
                            rb.RadioDeactivate();
                        }
                    }
                }
                else if(buttonType == ButtonType.pushHold && !oneTimeUse)
                {
                    isActivated = false;
                    holdingProgressImage.color = buttonColorOnActive;
                }

                if(destroyAfterSoundPrefab && soundOnActivate)
                {
                    //DestroyAfterSound temp = Instantiate(destroyAfterSoundPrefab, transform.position, transform.rotation).GetComponent<DestroyAfterSound>();
                    //temp.AssignSound(soundOnActivate);
                }
                /*
                if(sound && soundOnActivate)
                {
                    sound.clip = soundOnActivate;
                    sound.Play();
                }
                */
                onButtonActivated.Invoke();
                hasBeenPressed = true;

                /*
                if (buttonType != ButtonType.simple)
                {
                    
                    Collider[] cols = Physics.OverlapBox(transform.position)
                    if(onAlert)
                    {
                        buttonsOnAlert--;
                    }
                    onAlert = false;
                   
                }
                */
            }
            else
            {
                if (destroyAfterSoundPrefab && soundOnDeactivate)
                {
                    //DestroyAfterSound temp = Instantiate(destroyAfterSoundPrefab, transform.position, transform.rotation).GetComponent<DestroyAfterSound>();
                    //temp.AssignSound(soundOnDeactivate);
                }
                /*
                if (sound && soundOnDeactivate)
                {
                    sound.clip = soundOnDeactivate;
                    sound.Play();
                }
                */
                onButtonDeactivated.Invoke();
            }
        }
    }

    public void OnNotTouched()
    {
        buttonIsTouched = false;
        isBeingPointed = false;
        if(isLocked)
        {
            alreadyHeld = false;
            theButton.position = deepestButtonPosition + (theButton.forward * buttonPushDepth);
            SetLockedMessageVisible(false);
            return;
        }
        //onAlert = false;

        if (!keepOculusSimple)
        {
            if (requirePushHold || (buttonType != ButtonType.pushHold && (GameController.isTrueOculus || GameController.isOculusInSteam)))
            {
                SetHeld(false);
            }
        }
        else
        {
            if(requirePushHold || buttonType!=ButtonType.pushHold)
            {
                SetHeld(false);
            }
        }
        

        switch (buttonType)
        {
            case ButtonType.simple:
                pressedDepth = 0;
                alreadyHeld = false;
                SetActivation(false);
                buttonSkin.material.color = buttonColorOnUp;
                baseSkin.material.color = baseColorOnUp;

                theButton.position = deepestButtonPosition + (theButton.forward * buttonPushDepth);
                break;
            case ButtonType.pushHold:
                pressedDepth = 0;
                SetHeld(false);
                theButton.position = deepestButtonPosition + (theButton.forward * buttonPushDepth);
                break;
            case ButtonType.radio:
                if (isActivated)
                {
                    theButton.position = deepestButtonPosition;
                    pressedDepth = buttonPushDepth;
                }
                else
                {
                    theButton.position = deepestButtonPosition + (theButton.forward * buttonPushDepth);
                    pressedDepth = 0;
                }
                break;
            case ButtonType.clickClick:
                pressedDepth = 0;
                alreadyHeld = false;
                if (isActivated)
                {
                    theButton.position = deepestButtonPosition + (theButton.forward * buttonPushDepth * buttonSensitivity);
                }
                else
                {
                    theButton.position = deepestButtonPosition + (theButton.forward * buttonPushDepth);
                }
                break;
        }
        
    }


    IEnumerator HoldingRoutine()
    {
        if (!isActivated)
        {
            holdingProgressImage.color = buttonColorOnActive;
        }
        else
        {
            holdingProgressImage.color = buttonColorOnPressed;
        }
        while (holdCountdown > 0)
        {
            yield return null;
            if (Vector3.Angle(Camera.main.transform.forward, transform.position - Camera.main.transform.position) < lookAtAngle)
            {
                holdCountdown -= Time.deltaTime;
            }
            holdingProgressImage.fillAmount = 1f-(holdCountdown/timeToHold);
        }
        yield return null;
        holdingProgressImage.fillAmount = 0f;

        if(!isActivated && oneTimeUse)
        {
            buttonSkin.material.color = buttonColorOnActive;
            baseSkin.material.color = baseColorOnActive;
        }
        else
        {
            buttonSkin.material.color = buttonColorOnPressed;
            baseSkin.material.color = baseColorOnPressed;
        }

        SetActivation(!isActivated);
        
    }

    void OnTriggerEnter(Collider c)
    {
        //Debug.Log("in button");

        if (c.name.Contains("Pointer")){
            usingPointer = true;
        }
        else
        {
            usingPointer = false;
        }

        if (c.gameObject.layer == batLayer && !(oneTimeUse && hasBeenPressed))
        {
           // Debug.Log("yep");
           if(!onAlert)
           {
               buttonsOnAlert++;
           }
            onAlert = true;
        }
        else
        {
            if(onAlert)
            {
                buttonsOnAlert--;
            }
            onAlert = false;
        }
    }

    void OnTriggerExit(Collider c)
    {
        //Debug.Log("out button");
        if (c.gameObject.layer == batLayer)
        {
            //Debug.Log("yep");
            OnNotTouched();

            if (onAlert)
            {
                buttonsOnAlert--;
            }
            onAlert = false;
            alreadyHeld = false;

            if (c.name.Contains("Pointer"))
            {
                usingPointer = false;
            }
            
        }
    }
    void OnDestroy()
    {
        if (onAlert)
        {
            buttonsOnAlert--;
        }
    }

    void OnDisable()
    {
        if (onAlert)
        {
            onAlert = false;
            buttonsOnAlert--;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MonitorViewToggle : MonoBehaviour
{
    [SerializeField] Camera renderTextureCam;
    [SerializeField] GameObject pressBText;
    [SerializeField] GameObject fpsWarningText;
    [SerializeField] Cinemachine.CinemachineBrain cinemachineBrain;

    void Start()
    {
        if (pressBText) pressBText.SetActive(!renderTextureCam.gameObject.activeSelf);
        if (fpsWarningText) fpsWarningText.SetActive(renderTextureCam.gameObject.activeSelf);
    }
    
    // Update is called once per frame
    void Update ()
    {
        if (Input.GetKeyDown(KeyCode.B))
        {
            ToggleMonitorView();
        }
    }

    public void ToggleMonitorView()
    {
        renderTextureCam.gameObject.SetActive(!renderTextureCam.gameObject.activeSelf);
        if (pressBText) pressBText.SetActive(!renderTextureCam.gameObject.activeSelf);
        if (fpsWarningText) fpsWarningText.SetActive(renderTextureCam.gameObject.activeSelf);
        if (cinemachineBrain != null)
        {
            cinemachineBrain.GetComponent<Camera>().enabled = !cinemachineBrain.GetComponent<Camera>().enabled;
        }
    }

    public void MonitorViewOn()
    {
        renderTextureCam.gameObject.SetActive(true);
        if (pressBText) pressBText.SetActive(!renderTextureCam.gameObject.activeSelf);
        if (fpsWarningText) fpsWarningText.SetActive(renderTextureCam.gameObject.activeSelf);
        if (cinemachineBrain != null)
        {
            cinemachineBrain.GetComponent<Camera>().enabled = true;
        }
    }

    public void MonitorViewOff()
    {
        renderTextureCam.gameObject.SetActive(false);
        if (pressBText) pressBText.SetActive(!renderTextureCam.gameObject.activeSelf);
        if (fpsWarningText) fpsWarningText.SetActive(renderTextureCam.gameObject.activeSelf);
        if (cinemachineBrain != null)
        {
            cinemachineBrain.GetComponent<Camera>().enabled = false;
        }
    }
}

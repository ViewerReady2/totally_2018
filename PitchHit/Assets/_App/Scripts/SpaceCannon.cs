﻿using UnityEngine;
using System.Collections;

public class SpaceCannon : MonoBehaviour {
    //[HideInInspector]
    public Transform target;
    public GameObject projectile;
    public float minProjectileSpeed = 20f;
    public float maxProjectileSpeed = 40f;
    public float increaseProjectileSpeedPerMinute = 20f;
    private float targetDeviation = 0f;
    public GameObject[] chargingParticles;
    public AudioClip[] chargingSounds;
    public GameObject firingParticle;
    public AudioClip firingSound;
    public float chargeStationDuration = 1.2f;
    private AudioSource sound;
    private float timeOfCreation;

    private static float timeOfAnyLastCannonFire;
    private static float minTimeBetweenAnyCannonFire = 1f;
	// Use this for initialization

    public static void ResetAllCannons()
    {
        timeOfAnyLastCannonFire = 0f;
    }


	void Awake () {
        if(timeOfAnyLastCannonFire<Time.timeSinceLevelLoad)
        {
            timeOfAnyLastCannonFire = 0f;
        }
        timeOfCreation = Time.timeSinceLevelLoad;
        sound = GetComponent<AudioSource>();
        if (!sound)
        {
            sound = gameObject.AddComponent<AudioSource>();
            sound.spatialBlend = 1f;
            sound.spatialize = true;
        }
       
	}

    public void Fire()
    {
        StartCoroutine(FiringSequence());
    }

    private IEnumerator FiringSequence()
    {
        bool firing = false;
        while(!firing)
        {
            if (Time.timeSinceLevelLoad > timeOfAnyLastCannonFire + minTimeBetweenAnyCannonFire)
            {
                firing = true;
                timeOfAnyLastCannonFire = Time.timeSinceLevelLoad;
            }
            else
            {
                yield return null;

            }
        }
        for(int c=0; c<chargingParticles.Length;c++)
        {
            if (chargingParticles[c])
            {
                Instantiate(chargingParticles[c], transform.position, transform.rotation, transform);
                
                yield return new WaitForSeconds(chargeStationDuration);
            }
			if (chargingSounds [c]) 
			{
				if(c<chargingSounds.Length && chargingSounds[c])
				{
					sound.clip = chargingSounds[c];
				}
				if (sound.clip)
				{
					sound.Play();
				}
			}
        }

        GameObject tempProjectile = Instantiate(projectile, transform.position, transform.rotation) as GameObject;
        Vector3 aimPosition = (Random.insideUnitSphere * targetDeviation) + target.position;

        float tempProjectileSpeed = Mathf.Min(minProjectileSpeed + (((Time.timeSinceLevelLoad-timeOfCreation)/60f) * increaseProjectileSpeedPerMinute),maxProjectileSpeed);
        tempProjectile.GetComponent<Rigidbody>().velocity = (aimPosition - transform.position).normalized *tempProjectileSpeed;
        hittable hitComponent = tempProjectile.GetComponent<hittable>();
        hitComponent.SetOrigin(ballController.ballServiceType.none,transform);

        //Debug.Log("fire after alive: "+ (Time.timeSinceLevelLoad - timeOfCreation) + " "+tempProjectileSpeed+" percent:"+ (tempProjectileSpeed - minProjectileSpeed) / (maxProjectileSpeed - minProjectileSpeed) );
        tempProjectile.GetComponent<EnemyProjectile>().SetTarget(target,(tempProjectileSpeed-minProjectileSpeed)/(maxProjectileSpeed-minProjectileSpeed));

        if(firingSound)
        {
            sound.clip = firingSound;
        }

        if (sound.clip)
        {
            sound.pitch = (tempProjectileSpeed - minProjectileSpeed) / (maxProjectileSpeed - minProjectileSpeed) + 1f;
            sound.Play();
        }
        if(firingParticle)
        {
            Instantiate(firingParticle, transform.position, transform.rotation, transform);
        }
    }
}

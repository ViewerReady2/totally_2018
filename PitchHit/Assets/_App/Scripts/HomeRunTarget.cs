﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomeRunTarget : MonoBehaviour {
    public Transform body;
    public Renderer targetRend;
    public GameObject screamingPeople;
    public float movementDistance = 32f;
    public float movementSpeed= 60f; 

    private Vector3 standingUpPosition;
    private Vector3 standingDownPosition;
	// Use this for initialization
	void Start () {
        standingUpPosition = body.position;
        standingDownPosition = standingUpPosition - (Vector3.up * movementDistance);
        body.position = standingDownPosition;
        Debug.Log("standing up pos: "+standingUpPosition);
        Debug.Log("standing down pos: "+standingDownPosition);
        Debug.Log("current pos: "+body.position);
	}
	
    public void GoUp()
    {
        Debug.Log("Go Up now! "+name);
        StartCoroutine(GoUpRoutine());
        if (screamingPeople)
        {
            StartCoroutine(LaunchPeople());
        }
    }

    IEnumerator LaunchPeople()
    {
        yield return new WaitForSeconds(.2f);
        screamingPeople.SetActive(true);
        yield return new WaitForSeconds(4f);
        screamingPeople.SetActive(false);
    }

    IEnumerator GoUpRoutine()
    {
        yield return null;
        float currentSpeed = 0;
        while(body.position.y<standingUpPosition.y)
        {

            yield return null;
            currentSpeed = movementSpeed;

            body.position += (Vector3.up * currentSpeed * Time.deltaTime);
        }
        while (body.position.y > standingUpPosition.y)
        {
            yield return null;
            currentSpeed -= movementSpeed * 4f * Time.deltaTime;

            body.position += (Vector3.up * currentSpeed * Time.deltaTime);
        }
        body.position = standingUpPosition;
    }

    public void GoDown(float delay)
    {
        StopAllCoroutines();
    }

    IEnumerator GoDownRoutine(float delay)
    {
        yield return new WaitForSeconds(delay);
    }

    public void StartBlinking()
    {
        StartCoroutine(BlinkingRoutine());
    }

    IEnumerator BlinkingRoutine()
    {
        int remainingBlinks = 7;

        while(remainingBlinks>0)
        {
            remainingBlinks--;
            yield return new WaitForSeconds(.125f);
            targetRend.material.color = Color.green;
            yield return new WaitForSeconds(.125f);
            targetRend.material.color = Color.white;
        }
    }
}

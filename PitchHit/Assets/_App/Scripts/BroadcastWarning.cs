﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class BroadcastWarning : MonoBehaviour {

    RectTransform trans;

    void Awake()
    {
        trans = gameObject.GetComponent<RectTransform>();
    }

	// Use this for initialization
	void Start ()
    {
        StartCoroutine(removeWarning());
	}

    IEnumerator removeWarning()
    {
        float timer = 0f;

        while (timer < 5f)
        {
            timer += Time.deltaTime;
            yield return null;
        }

        timer = 0f;

        Vector3 startPos = trans.localPosition;
        Vector3 endPos = new Vector3(trans.localPosition.x + trans.sizeDelta.x, trans.localPosition.y, trans.localPosition.z);

        while(timer < 1f)
        {
            timer += Time.deltaTime;
            trans.localPosition = Vector3.Lerp(startPos, endPos, timer);
            yield return null;
        }

        trans.localPosition = endPos;
        yield return null;
        Destroy(gameObject);
    }
}

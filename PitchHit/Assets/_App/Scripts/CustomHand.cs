﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Valve.VR.InteractionSystem;

#if RIFT
using OVRTouchSample;
#elif STEAM_VR
using Valve.VR;
#endif

public class CustomHand : MonoBehaviour
{
    public enum HandState { None, Empty, Ball, LargeBall, Pointing };
    public HandState currentHandState;

    private Renderer debugSkin;

    [HideInInspector]
    public bool renderModelsLoaded;

    public bool isOccupied;
    public bool isOculusRight;
    public Text leftDisplayText;
    public Text rightDisplayText;
    public CustomHand otherHand;
    public GameObject compass;
    public Animator handAnimator;
    private Hand.HandType _handType = Hand.HandType.Any;
    //public Transform grippedBallLocation;
    [HideInInspector]
    public bool joystickPressed = false;
    public bool showRenderModel
    {
        get
        { return _showRenderModel; }
        set
        {
            //Debug.Log(this.name + " set render model "+ value);
            _showRenderModel = value;

            if (GameController.mixedRealityMode)
            {
                value = false;
            }
#if RIFT
            if (oculusRenderModel)
            {
                oculusRenderModel.SetActive(value);
                touchAnimator.enabled = value;

            }
#elif STEAM_VR
            if (modelSpawner && !renderModel)
            {
                renderModel = modelSpawner.GetRenderModels()[0];

                /*
                SpawnRenderModel renderModelSpawner = GetComponentInChildren<SpawnRenderModel>();
                if (renderModelSpawner)
                {
                    renderModel = renderModelSpawner.gameObject;
                }
                *
                /*
                SpawnRenderModel renderModelSpawner = GetComponentInChildren<SpawnRenderModel>();
                if (renderModelSpawner)
                {
                    Debug.Log("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx found it.");
                    renderModel = renderModelSpawner.GetRenderModels()[0];
                }
                */
            }
            if (renderModel)
            {
                //renderModel.OnHideRenderModels(!value);
                renderModel.gameObject.SetActive(value);
            }
#endif
        }
    }
    [SerializeField]
    private bool _showRenderModel = true;

    SteamVR_Events.Action renderModelLoadedAction;

    private TrajTester1 traj;
    private VelocityEstimator estimator;
    private float[] flickSamples;
    private Vector3 previousForward;
    private int sampleCount;
    //private bool controllerLoaded;
    private GameObject oculusRenderModel;
    private bool turnAroundHintShowing;
    private bool gripBatHintShowing;
    private bool spawnBallHintShowing;
    private bool grabHintShowing;
    private bool returnToBatHintShowing;
    private bool menuButtonHintShowing;

    private Grabber grabber;


    public float GripValue
    {
        get; private set;
    }

#if RIFT
    private NewCustomTouchAnimator touchAnimator;
    [HideInInspector]

    //  public TrackedController trackedOculusTrackedController;
#elif GEAR_VR
    [HideInInspector]
    public Vector3 initialLocalPosition;
    //private Vector2 previousTouchPosition;
    private float timeSpentTouching;
    private float touchingTimeUntilSlide = 0;
    private bool sliding;
    
    public Transform teeForPivot;
#elif STEAM_VR
    [HideInInspector]
    public Hand actualHand;
    //private ControllerButtonHints steamVRButtonHints;
    //[HideInInspector]
    //public SteamVR_RenderModel renderModel;
    public SpawnRenderModel modelSpawner;
    protected SteamVR_RenderModel renderModel;

#endif

    void Awake()
    {
#if STEAM_VR
        actualHand = GetComponent<Hand>();
        Debug.Log("actual hand " + actualHand + " found for " + name);
        renderModelLoadedAction = SteamVR_Events.RenderModelLoadedAction(OnRenderModelLoaded);
        //showRenderModel = true;
#endif


    }

    void Start()
    {
        if (compass)
        {
            compass.SetActive(false);
        }
        debugSkin = GetComponentInChildren<Renderer>();

#if RIFT
       // trackedOculusTrackedController = GetComponent<TrackedController>();
        touchAnimator = GetComponentInChildren<NewCustomTouchAnimator>();
        renderModelsLoaded = touchAnimator;

        if (isOculusRight) {
            oculusRenderModel = GameObject.Find("right_touch_controller_model_skel");
        }
        else
        {
            oculusRenderModel = GameObject.Find("left_touch_controller_model_skel");
        }

        showRenderModel = false;
        grabber = GetComponentInChildren<Grabber>();

        SetHandState(HandState.Empty);
#elif GEAR_VR
        if(hitThings.pivotAroundTee)
        {
            if(!teeForPivot)
            {
                if (ballController.instance && ballController.instance.teeController)
                {
                    teeForPivot = ballController.instance.teeController.transform;
                }
            }

        }
        initialLocalPosition = transform.localPosition;
#endif

#if STEAM_VR || RIFT
        estimator = GetComponentInChildren<VelocityEstimator>();

        if (estimator)
        {
            flickSamples = new float[estimator.angularVelocityAverageFrames];
            previousForward = transform.forward;
        }
        else
        {
            Debug.Log("No velocity estimator present!");
        }
#endif
    }
#if STEAM_VR
    void OnEnable()
    {
        renderModelLoadedAction.enabled = true;
    }

    void OnDisable()
    {
        renderModelLoadedAction.enabled = false;
    }
#endif

    private bool previousGrabbingOn;
    private bool grabbingOn;

    private bool grabAvailable; //MARC: needed as a way to not trigger grab the instant isOccupied is set to false if trigger is down

    private bool previousSqueezingOn;
    private bool squeezingOn;

    private bool previousUsingOn;
    private bool usingOn;

    private bool previousTouchpadOn;
    private bool touchpadOn;
    private bool finishedTurning = false;
    private Vector2 touchPosition;
    private Vector2 previousTouchPosition;

    private float gripStrength;
    private float indexGripStrength;
    private float deepestCurrentGripStrength;
    private float minimumGripStrength = .5f;
    private float gripStrengthMargin = .001f;
    private bool grabIsCancelled;
    //[HideInInspector]
    public bool gripWithWholeHand = true;

    // Update is called once per frame
    void Update()
    {
#if STEAM_VR || RIFT
        if (estimator)
        {
            flickSamples[sampleCount % flickSamples.Length] = Vector3.Angle(previousForward, transform.forward);
            previousForward = transform.forward;
            sampleCount++;
        }
        else
        {
            Debug.Log("No velocity estimator present!");
        }
#endif

#if RIFT
       
        if(isOculusRight)
        {
            bool connected = OVRInput.IsControllerConnected(OVRInput.Controller.RTouch);
            bool tracking = OVRInput.GetControllerPositionTracked(OVRInput.Controller.RTouch);
            Vector3 position = OVRInput.GetLocalControllerPosition(OVRInput.Controller.RTouch);
//            Debug.Log("OVR RTouch " + "connected: " + connected + " tracking: " + tracking + " position: " + position);
            transform.localPosition = OVRInput.GetLocalControllerPosition(OVRInput.Controller.RTouch);
            transform.localRotation = OVRInput.GetLocalControllerRotation(OVRInput.Controller.RTouch);

            //if (OVRInput.GetDown(OVRInput.Button.Two, OVRInput.Controller.RTouch))
            ///{
            //    OVRManager.instance.transform.Rotate(Vector3.up, 180f);
            //}
            
        }
        else
        {
            //Debug.Log(OVRInput.GetConnectedControllers().ToString());
            transform.localPosition = OVRInput.GetLocalControllerPosition(OVRInput.Controller.LTouch);
            transform.localRotation = OVRInput.GetLocalControllerRotation(OVRInput.Controller.LTouch);

            //if (OVRInput.GetDown(OVRInput.Button.Two, OVRInput.Controller.LTouch))
            //{
            //    OVRManager.instance.transform.Rotate(Vector3.up, 180f);
            //}
        }

        if (GameController.canTurnAround)
        {
            if (touchPosition.x > .8f && previousTouchPosition.x <= .8f)
            {
                OVRManager.instance.transform.Rotate(Vector3.up, 90f);
            }
            else
            if (touchPosition.x < -.8f && previousTouchPosition.x >= -.8f)
            {
                OVRManager.instance.transform.Rotate(Vector3.up, -90f);
            }
            else
            if (touchPosition.y < -.8f && previousTouchPosition.y >= -.8f)
            {
                OVRManager.instance.transform.Rotate(Vector3.up, 180f);
            }
        }

#elif STEAM_VR
        if (GameController.isOculusInSteam)
        {
            if (actualHand && actualHand.controller != null)
            {
                if (GameController.canTurnAround)
                {
                    touchPosition = actualHand.controller.GetAxis(EVRButtonId.k_EButton_SteamVR_Touchpad);

                    if (finishedTurning)
                    {
                        if (Mathf.Abs(touchPosition.x) < .5f && Mathf.Abs(touchPosition.y) < .5f)
                        {
                            finishedTurning = false;
                        }
                    }
                    else
                    {

                        if (touchPosition.x > .8f)
                        {
                            Player.instance.transform.Rotate(Vector3.up, 90f);
                            finishedTurning = true;
                        }
                        else
                        if (touchPosition.x < -.8f)
                        {
                            Player.instance.transform.Rotate(Vector3.up, -90f);
                            finishedTurning = true;
                        }
                        else
                        if (touchPosition.y < -.8f)
                        {
                            Player.instance.transform.Rotate(Vector3.up, 180f);
                            finishedTurning = true;
                        }
                    }
                    previousTouchPosition = touchPosition;

                    //if (actualHand.controller.GetPressDown(EVRButtonId.k_EButton_ApplicationMenu))
                    //{
                    //    Player.instance.transform.Rotate(Vector3.up, 180f);
                    //}
                }

            }
            else
            {
                //Debug.Log("no actual hand in "+name);
            }
        }

#endif

        previousGrabbingOn = grabbingOn;
        previousSqueezingOn = squeezingOn;
        previousTouchpadOn = touchpadOn;
        previousUsingOn = usingOn;

#if RIFT

 
        if(isOculusRight)
        {
            squeezingOn  = OVRInput.Get(OVRInput.Button.PrimaryHandTrigger, OVRInput.Controller.RTouch);
            if (gripWithWholeHand)
            {
                gripStrength = Mathf.Max(OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, OVRInput.Controller.RTouch), OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, OVRInput.Controller.RTouch));
            }
            else
            {
                gripStrength = OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, OVRInput.Controller.RTouch);
            }
            joystickPressed = OVRInput.Get(OVRInput.Button.PrimaryThumbstick, OVRInput.Controller.RTouch);
            indexGripStrength = OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, OVRInput.Controller.RTouch);

            //Debug.Log("right oculus grip:"+gripStrength +" squeeze:"+squeezingOn);
        }
        else
        {
            squeezingOn = OVRInput.Get(OVRInput.Button.PrimaryHandTrigger, OVRInput.Controller.LTouch);
            if (gripWithWholeHand)
            {
                gripStrength = Mathf.Max(OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, OVRInput.Controller.LTouch), OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, OVRInput.Controller.LTouch));
            }
            else
            {
                gripStrength = OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, OVRInput.Controller.LTouch);
            }
            joystickPressed = OVRInput.Get(OVRInput.Button.PrimaryThumbstick, OVRInput.Controller.LTouch);
            indexGripStrength = OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, OVRInput.Controller.LTouch);
        }

        touchpadOn = joystickPressed;
        previousTouchPosition = touchPosition;
        touchPosition = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick, isOculusRight? OVRInput.Controller.RTouch: OVRInput.Controller.LTouch);
        //squeezingOn = OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger) > 0; ;


#elif GEAR_VR
        if (Application.isEditor || (OVRInput.GetConnectedControllers() & OVRInput.Controller.RTrackedRemote) != 0)
        {
            if (GameController.isAtBat && !GameController.isMenuPointing)
            {
                if (hitThings.pivotAroundTee)
                {
                    if (hitThings.doublePivot)
                    {
                        squeezingOn = Input.GetKey("k") || OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.RTrackedRemote);
                    }
                    if (hitThings.doublePivot && squeezingOn)
                    {
                        if (debugSkin && squeezingOn)
                        {
                            debugSkin.material.color *= .5f;
                        }
                        if (Application.isEditor)
                        {
                            transform.localRotation = Quaternion.AngleAxis(20f * Time.deltaTime, Vector3.up) * transform.localRotation;
                        }
                        else
                        {
                            transform.localRotation = OVRInput.GetLocalControllerRotation(OVRInput.Controller.RTrackedRemote);
                        }
                        if (!squeezingOn)
                        {
                            transform.position = teeForPivot.position - (transform.forward * .7f);
                        }

                    }
                    else
                    {
                        //this is where the pivoting around the tee happens.
                        transform.localRotation = OVRInput.GetLocalControllerRotation(OVRInput.Controller.RTrackedRemote);
                        transform.position = teeForPivot.position - (transform.forward * .7f);

                        if (OVRInput.Get(OVRInput.Touch.PrimaryTouchpad, OVRInput.Controller.RTrackedRemote))
                        {
                            squeezingOn = true;
                            Vector2 touchPosition = OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad, OVRInput.Controller.RTrackedRemote);
                            transform.Rotate(Vector3.up, touchPosition.x * 30f);
                        }
                    }
                }
                else
                {
                    if (OVRInput.GetDown(OVRInput.Button.Back, OVRInput.Controller.RTrackedRemote))
                    {
                        transform.localPosition = initialLocalPosition;
                        if (debugSkin)
                        {
                            debugSkin.material.color = Color.cyan;
                        }
                    }
                    else
                    {
                        if (debugSkin)
                        {
                            debugSkin.material.color = Color.green;
                        }
                    }
                    transform.localRotation = OVRInput.GetLocalControllerRotation(OVRInput.Controller.RTrackedRemote);
                    //print("BOO " + OVRInput.GetLocalControllerRotation(OVRInput.Controller.RTrackedRemote));
                    if (OVRInput.Get(OVRInput.Touch.PrimaryTouchpad, OVRInput.Controller.RTrackedRemote))
                    {
                        if (!sliding && timeSpentTouching >= 0)
                        {
                            timeSpentTouching -= Time.deltaTime;
                        }
                        else
                        {
                            if (!sliding)
                            {
                                if (compass)
                                {
                                    compass.SetActive(true);
                                }
                                sliding = true;
                                previousTouchPosition = OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad, OVRInput.Controller.RTrackedRemote);
                            }
                            else
                            {
                                Vector2 touchPosition = OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad, OVRInput.Controller.RTrackedRemote);

                                transform.position += transform.forward * (touchPosition.y - previousTouchPosition.y) * .25f;
                                transform.position += transform.right * (touchPosition.x - previousTouchPosition.x) * .25f;

                                previousTouchPosition = touchPosition;
                            }
                        }

                        if (debugSkin)
                        {
                            debugSkin.material.color = Color.blue;
                        }
                    }
                    else
                    {
                        if (compass)
                        {
                            compass.SetActive(false);
                        }
                        sliding = false;
                        timeSpentTouching = touchingTimeUntilSlide;
                    }

                    squeezingOn = OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.RTrackedRemote);
                    if (debugSkin && squeezingOn)
                    {
                        debugSkin.material.color *= .5f;
                    }
                }
            }
            else
            {
                transform.localPosition = (Vector3.forward+Vector3.up).normalized*.5f;
                squeezingOn = true;
                if (!Application.isEditor)
                {
                    transform.localRotation = OVRInput.GetLocalControllerRotation(OVRInput.Controller.RTrackedRemote);
                }
                else
                {
                    transform.localRotation = Quaternion.AngleAxis(Input.GetAxis("Mouse X"), Vector3.up) * transform.localRotation;
                    transform.localRotation = Quaternion.AngleAxis(Input.GetAxis("Mouse Y"), Vector3.Cross(Vector3.up,transform.forward)) * transform.localRotation;
                }
            }
            
        }
        else
        {
            if (debugSkin)
            {
                debugSkin.material.color = Color.black;
            }
        }
       
        grabbingOn = false;
#elif STEAM_VR
        float squeeze = 0;
        if (actualHand && actualHand.controller != null)
        {
            touchpadOn = actualHand.controller.GetPress(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad);
            gripStrength = actualHand.controller.GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger).x;
            indexGripStrength = actualHand.controller.GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger).x;
            squeezingOn = actualHand.controller.GetPress(Valve.VR.EVRButtonId.k_EButton_Grip);

            if (GameController.isOculusInSteam)
            {
                squeeze = actualHand.controller.GetAxis(EVRButtonId.k_EButton_Axis2).x;
            }
            else
            {
                squeeze = actualHand.controller.GetPress(EVRButtonId.k_EButton_Grip) ? 1f : 0f;
            }

        }
        if (squeeze >= gripStrength) SetHandGripValue(squeeze, true);
        else SetHandGripValue(gripStrength, false);
#endif

#if STEAM_VR || RIFT


        // if (gripStrength > 0) grabbingOn = true;
        //  else grabbingOn = false;

        bool grabbingDeeper = false;
        if (gripStrength > deepestCurrentGripStrength + gripStrengthMargin)
        {
            grabbingDeeper = true;
            deepestCurrentGripStrength = gripStrength - gripStrengthMargin;
        }

        if (grabbingOn || grabIsCancelled)
        {
            if (gripStrength < deepestCurrentGripStrength - gripStrengthMargin)
            {
                grabIsCancelled = false;
                grabbingOn = false;
            }
        }
        else
        {
            if (!grabIsCancelled && gripStrength > minimumGripStrength && grabbingDeeper)
            {
                grabbingOn = true;
            }
        }

        if (gripStrength < minimumGripStrength)
        {
            deepestCurrentGripStrength = minimumGripStrength;


        }
        usingOn = (indexGripStrength > .5);

        //MARC: Noticed that setting isOccupied to false would immediately trigger GrabDown(). Ideal behavior is to wait until grips are released, then make grabbing possible again
        if (isOccupied) //hand is occupied, grab is not available
        {
            grabAvailable = false;
        }
        else if (!grabbingOn) //hand is not occupied and not grabbing, grab is available
        {
            grabAvailable = true;
        }

        grabbingOn = grabbingOn && grabAvailable;

#if RIFT

        if (grabber.GetCurrentGrabbable() == null)
        {
            
            if (grabber)
            {
                if(squeezingOn) SetHandGripValue(gripStrength / minimumGripStrength, true);
                else SetHandGripValue(gripStrength / minimumGripStrength, false);
            }

        }
#endif


        //Debug.Log(triggerIsPressed);

#endif
    }

    public Vector3 GetCurrentBatGripPosition()
    {
#if RIFT
        if(!grabber)
        {
            grabber = GetComponentInChildren<Grabber>();
        }
        if(grabber)
        {
            return grabber.transform.position;
        }
#endif
        return transform.position;
    }

    public void StartPointing()
    {
        if (!grabber)
        {
            grabber = GetComponentInChildren<Grabber>();
        }
        if (GetCurrentGrabbable() == null)
        {
            SetHandState(HandState.Pointing);
        }
    }

    public void StopPointing()
    {
        if (currentHandState != HandState.Pointing)
        {
            return;
        }

        //if (hitThings.instance && hitThings.instance.attached && hitThings.instance.attachedCustomHand == this)
        if (isOccupied)
        {
            SetHandState(HandState.None);
        }
        else
        {
            CustomGrabbable grabbed = GetCurrentGrabbable();
            if (grabbed)
            {
                if (grabbed.GetComponent<BallForDucks>())
                {
                    SetHandState(HandState.LargeBall);
                }
                else
                {
                    SetHandState(HandState.Ball);
                }
            }
            else
            {
                SetHandState(HandState.Empty);
            }
        }

    }

    public bool GetGrip()
    {
        return grabbingOn;
    }

    public bool GetGripDown()
    {
        //Debug.Log(name+" get grip down? "+grabbingOn+" "+previousGrabbingOn);
        return grabbingOn && !previousGrabbingOn;
    }


    public bool GetGripUp()
    {
        //return false;
        return !grabbingOn && previousGrabbingOn;
    }

    public bool GetSqueezeDown()
    {
        return squeezingOn && !previousSqueezingOn;
    }

    public bool GetSqueezeUp()
    {
        return !squeezingOn && previousSqueezingOn;
    }
    public bool GetSqueezing()
    {
        return squeezingOn;
    }


    public bool GetTouchpadDown()
    {
        return touchpadOn && !previousTouchpadOn;
    }

    public bool GetTouchpadUp()
    {
        return !touchpadOn && previousTouchpadOn;
    }
    public bool GetTouchpad()
    {
        return touchpadOn;
    }

    public bool GetUseDown()
    {
        return usingOn && !previousUsingOn;
    }

    public bool GetUseUp()
    {
        return !usingOn && previousUsingOn;
    }

    public bool GetUsing()
    {
        return usingOn;
    }

    public Vector2 GetTouchPosition()
    {
        return touchPosition;
    }

    public bool GetButtonOne()
    {
#if STEAM_VR
        return actualHand && actualHand.controller != null && actualHand.controller.GetPressDown(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad);
#elif RIFT
        //if (isOculusRight) Debug.Log("OVR+++++++++++++++++++++++++++++++++++++++++++++ "+ OVRInput.GetDown(OVRInput.Button.One, OVRInput.Controller.RTouch));
        if (isOculusRight) return OVRInput.Get(OVRInput.Button.One, OVRInput.Controller.RTouch);
        return OVRInput.Get(OVRInput.Button.One, OVRInput.Controller.LTouch);
#elif GEAR_VR
        return false;
#endif
    }

    public bool GetMenuButtonDown()
    {
#if STEAM_VR
        return actualHand && actualHand.controller != null && actualHand.controller.GetPressDown(Valve.VR.EVRButtonId.k_EButton_ApplicationMenu);
#elif RIFT
        return OVRInput.GetDown(OVRInput.Button.Start,OVRInput.Controller.LTouch);
#elif GEAR_VR
        return false;
#else
        return false;
#endif
    }



    public void OnPitched(Rigidbody body)
    {
#if RIFT || STEAM_VR
        //Debug.Log("on pitched customhand");
        if (!body)
        {
            Debug.Log("trying to pitch nothing.");
            return;
        }
        // Pitchable pitchComponent = body.GetComponent<Pitchable>();

        //if(pitchComponent)
        //{
        //    Debug.Log("not for the pitching component");
        //    pitchComponent.OnPitched(this);
        //    StartCoroutine(ThrowingRoutine(body));
        //}
        StartCoroutine(ThrowingRoutine(body));
#endif
    }

#if RIFT || STEAM_VR
    public Transform grippedBallLocation;
    private float handFlickPushFactor = 250f;
    private float handFlickSpinFactor = 200f;
    private float maxGripSlide = .3f;
    private float gripRange = .75f;
    private float minimumHandFlick = 5f;
    private VelocityEstimator velocity;
    IEnumerator ThrowingRoutine(Rigidbody thrownBody)
    {
        Pitchable temp = thrownBody.GetComponent<Pitchable>();
        // if (temp)
        //{
        //    temp.DebugColor(true);
        //}

        if (!velocity)
        {
            velocity = GetComponentInChildren<VelocityEstimator>();
        }

        bool stillThrowing = true;
        Vector3 initialGripLocalPosition = (grippedBallLocation ? grippedBallLocation.localPosition : Vector3.zero);

        while (stillThrowing && thrownBody)
        {
            yield return new WaitForFixedUpdate();

            float handFlick = transform.InverseTransformVector(velocity.GetAngularVelocityEstimate()).x;

            if (handFlick > 0)
            {
                //thrownBody.AddTorque(transform.right * handFlick * handFlickSpinFactor * Time.deltaTime, ForceMode.Acceleration);
                thrownBody.AddForce(thrownBody.velocity.normalized * handFlick * handFlickPushFactor * Time.fixedDeltaTime, ForceMode.Acceleration);
                //ballInFocus.rb.AddForce(transform.forward * handFlick * 50f * Time.deltaTime, ForceMode.Acceleration);
            }

            //grippedBallLocation.localPosition = Vector3.Lerp(initialGripLocalPosition, initialGripLocalPosition + (Vector3.up * maxGripSlide), handFlick / 20f);
            float gripDistanceToBall = 0;

            if (grippedBallLocation)
            {
                gripDistanceToBall = Vector3.SqrMagnitude(grippedBallLocation.position - thrownBody.transform.position);
            }
            else
            {
                gripDistanceToBall = Vector3.SqrMagnitude(transform.position - thrownBody.transform.position);
            }

            Debug.Log("------------------------------------------ CUSTOM HAND STILL THROWING??.");
            if (gripDistanceToBall > (gripRange * gripRange) || handFlick < minimumHandFlick)
            {
                //Debug.Log("nope because "+gripDistanceToBall+"    "+handFlick);
                stillThrowing = false;
            }

            RefreshHandState();
        }

        if (grippedBallLocation)
        {
            grippedBallLocation.localPosition = initialGripLocalPosition;
        }
        //Debug.Log("end of just released.");

        //if (temp)
        //{
        //    temp.DebugColor(false);
        //}
    }

    public float GetVelocityFrameCount()
    {
        if (estimator)
        {
            return estimator.velocityAverageFrames;
        }
        else
        {
            Debug.Log("No velocity estimator present!");
        }
        return 0f;
    }

    public int GetAngularVelocityFrameCount()
    {
        if (estimator)
        {
            return estimator.angularVelocityAverageFrames;
        }
        else
        {
            Debug.Log("No velocity estimator present!");
        }
        return 0;
    }

    public Vector3 GetVelocity()
    {
        if (estimator)
        {
            return estimator.GetVelocityEstimate() * (GameController.isOculusInSteam? GameController.oculusInSteamHandMovementMultipier:1f);
        }
        else
        {
            Debug.Log("No velocity estimator present!");
        }
        return Vector3.zero;
    }

    public float GetFlickDeltaEstimate()
    {
        if (estimator)
        {
            int loopLength = Mathf.Min(sampleCount, flickSamples.Length);
            float temp = 0;
            for (int x = 0; x < loopLength; x++)
            {
                temp += flickSamples[x];
            }
            return temp / loopLength;
        }
        else
        {
            Debug.Log("No velocity estimator present!");
        }
        return 0f;
    }

    public Vector3 GetAngularVelocity()
    {
        if (estimator)
        {
            return estimator.GetAngularVelocityEstimate();
        }
        else
        {
            Debug.Log("No velocity estimator present!");
        }
        return Vector3.zero;
    }
#endif
    public void DetachObject(GameObject obj)
    {
#if RIFT

#elif STEAMVR

#endif
    }

    public void HoverLock(Interactable interact)
    {
#if STEAM_VR
        if (actualHand)
        {
            actualHand.HoverLock(interact);
        }
#endif
    }

    public void HoverUnlock(Interactable interact)
    {
#if STEAM_VR
        if (actualHand)
        {
            actualHand.HoverUnlock(interact);
        }
#endif
    }

    private void OnRenderModelLoaded(SteamVR_RenderModel renderModel, bool success)
    {
        if (!renderModelsLoaded)
        {
            //Debug.Log("++++++++++++++++++++++++++++++++ OnRenderModelLoaded. " + name + " rm" + renderModel.name);
            StartCoroutine(AssignRenderModel());

            renderModelsLoaded = success;
        }

    }

    IEnumerator AssignRenderModel()
    {
        yield return null;
#if STEAM_VR

        modelSpawner = GetComponentInChildren<SpawnRenderModel>();
        //Debug.Log("--------------------------------rendermodel spawner "+modelSpawner);

        if (modelSpawner)
        {
            //steamVRButtonHints = GetComponentInChildren<ControllerButtonHints>();

            //renderModel = renderModelSpawner.GetRenderModels()[0];
            //modelSpawner = renderModelSpawner.gameObject;
            //Debug.Log("found render model " + modelSpawner.name);

        }
        else
        {
            //Debug.Log("didn't find one in  " + name);
        }


        if (GameController.mixedRealityMode)
        {
            showRenderModel = showRenderModel;
        }

        //Debug.Log("///////////////////////////////////////////////////////////////////////CUSTOM HAND IS AWAKE " + GameController.mixedRealityMode + " " + showRenderModel);
#endif

    }

    public void SetGripBatHint(bool on)
    {
        gripBatHintShowing = on;
#if RIFT
        if (touchAnimator)
        {
            Debug.Log("oooooooooooooooooooooooooooooooooooooooooooooo activate the touch animator");
           touchAnimator.SetButtonGripHighlight(on);

            if(grabber)
            {
                SetHandState(HandState.None);
                showRenderModel = true;
            }
        }
        else
        {
            Debug.Log("SORRY NO TOUCH ANIMATOR!");
        }
#elif STEAM_VR
        if (on)
        {
            //ControllerButtonHints.ShowButtonHint(GetComponent<Hand>(), EVRButtonId.k_EButton_SteamVR_Trigger);
            Debug.Log("turn grip hint on. " + actualHand.name);
            ControllerButtonHints.ShowButtonHint(GetComponent<Hand>(), EVRButtonId.k_EButton_Grip);
        }
        else
        {
            Debug.Log("Turn Grip hint offfffff.");
            ControllerButtonHints.HideButtonHint(GetComponent<Hand>(), EVRButtonId.k_EButton_Grip);
        }
#endif

        RefreshHandState();
    }

    public void SetBallSpawnHint(bool on)
    {
        spawnBallHintShowing = on;
#if RIFT
        if (touchAnimator)
        {
            touchAnimator.SetButtonOneHighlight(on);
            if (grabber)
            {
                SetHandState(HandState.None);
                showRenderModel = true;
            }
        }
#elif STEAM_VR
        if (!actualHand || actualHand.controller == null)
        {
            return;
        }

        if (on)
        {
            if (GameController.isOculusInSteam)
            {
                ControllerButtonHints.ShowButtonHint(actualHand, EVRButtonId.k_EButton_A);
            }
            else
            {
                ControllerButtonHints.ShowButtonHint(actualHand, EVRButtonId.k_EButton_SteamVR_Touchpad);
            }

        }
        else
        {
            if (GameController.isOculusInSteam)
            {
                ControllerButtonHints.HideButtonHint(actualHand, EVRButtonId.k_EButton_A);
            }
            else
            {
                ControllerButtonHints.HideButtonHint(actualHand, EVRButtonId.k_EButton_SteamVR_Touchpad);
            }

        }
#endif
    }

    public void SetGrabHint(bool on)
    {
        grabHintShowing = on;
#if RIFT
        if(touchAnimator)
        {
            touchAnimator.SetButtonTriggerHighlight(on);
            if (grabber)
            {
                SetHandState(HandState.None);
                showRenderModel = true;
            }
        }
#elif STEAM_VR
        if (on)
        {
            ControllerButtonHints.ShowButtonHint(actualHand, EVRButtonId.k_EButton_SteamVR_Trigger);
        }
        else
        {
            ControllerButtonHints.HideButtonHint(actualHand, EVRButtonId.k_EButton_SteamVR_Trigger);
        }
#endif
    }

    public void SetReturnToBatHint(bool on)
    {
        returnToBatHintShowing = on;
#if RIFT
        if (touchAnimator)
        {
            touchAnimator.SetJosystickHighlight(on);
            if (grabber)
            {
                SetHandState(HandState.None);
                showRenderModel = true;
            }
        }
#elif STEAM_VR
        if (on)
        {
            if (GameController.isOculusInSteam)
            {
                ControllerButtonHints.ShowButtonHint(actualHand, EVRButtonId.k_EButton_Axis0);
            }
            else
            {
                ControllerButtonHints.ShowButtonHint(actualHand, EVRButtonId.k_EButton_ApplicationMenu);
            }
        }
        else
        {
            if (GameController.isOculusInSteam)
            {
                ControllerButtonHints.HideButtonHint(actualHand, EVRButtonId.k_EButton_Axis0);
            }
            else
            {
                ControllerButtonHints.HideButtonHint(actualHand, EVRButtonId.k_EButton_ApplicationMenu);
            }

        }
#endif
    }

    public void SetMenuButtonHint(bool on)
    {

#if RIFT
        if(isOculusRight)
        {
            return;
        }
        menuButtonHintShowing = on;
        if (touchAnimator)
        {
            touchAnimator.SetButtonThreeHighlight(on);
            if (grabber)
            {
                SetHandState(HandState.None);
                if (on)
                {
                    showRenderModel = true;
                }
            }
        }
#elif STEAM_VR
        menuButtonHintShowing = on;
        if (on)
        {
            if (GameController.isOculusInSteam)
            {
                ControllerButtonHints.ShowButtonHint(actualHand, EVRButtonId.k_EButton_Axis0);
            }
            else
            {
                ControllerButtonHints.ShowButtonHint(actualHand, EVRButtonId.k_EButton_ApplicationMenu);
            }
        }
        else
        {
            if (GameController.isOculusInSteam)
            {
                ControllerButtonHints.HideButtonHint(actualHand, EVRButtonId.k_EButton_Axis0);
            }
            else
            {
                ControllerButtonHints.HideButtonHint(actualHand, EVRButtonId.k_EButton_ApplicationMenu);
            }

        }
        if (GameController.isOculusInSteam && isOculusRight)
        {
            return;
        }
#endif
    }

    public void SetTurnAroundHint(bool on)
    {
        turnAroundHintShowing = on;
#if RIFT
        if(touchAnimator)
        {
            touchAnimator.SetJosystickHighlight(on);
            //touchAnimator.SetButtonTwoHighlight(on);
            if (grabber)
            {
                SetHandState(HandState.None);
                showRenderModel = true;
            }
        }
#elif STEAM_VR
        if (GameController.isOculusInSteam)
        {
            if (on)
            {
                ControllerButtonHints.ShowButtonHint(actualHand, EVRButtonId.k_EButton_Axis0);
            }
            else
            {
                ControllerButtonHints.HideButtonHint(actualHand, EVRButtonId.k_EButton_Axis0);
            }
        }
        else
        {
            if (on)
            {
                ControllerButtonHints.ShowButtonHint(actualHand, EVRButtonId.k_EButton_ApplicationMenu);
            }
            else
            {
                ControllerButtonHints.HideButtonHint(actualHand, EVRButtonId.k_EButton_ApplicationMenu);
            }
        }
#endif
    }

    public void HideHints()
    {
#if STEAM_VR
        ControllerButtonHints.HideAllButtonHints(actualHand);
#endif
        turnAroundHintShowing = false;
        gripBatHintShowing = false;
        spawnBallHintShowing = false;
        grabHintShowing = false;
        returnToBatHintShowing = false;
        menuButtonHintShowing = false;
        RefreshHandState();
    }

    public void DisplayText(string words)
    {

        if (HandManager.currentLeftCustomHand == this)
        {
            leftDisplayText.gameObject.SetActive(true);
            leftDisplayText.text = words;
            if (rightDisplayText)
            {
                rightDisplayText.gameObject.SetActive(false);
            }
        }
        else if (HandManager.currentRightCustomHand == this)
        {
            rightDisplayText.gameObject.SetActive(true);
            rightDisplayText.text = words;
            if (leftDisplayText)
            {
                leftDisplayText.gameObject.SetActive(false);
            }
        }

    }

    public void DisableText()
    {

        if (leftDisplayText)
        {
            leftDisplayText.gameObject.SetActive(false);
        }

        if (rightDisplayText)
        {
            rightDisplayText.gameObject.SetActive(false);
        }

        //displayText.enabled = false;
        //displayText.gameObject.SetActive(false); 

    }

    public void RefreshHandState(bool forceShowRenderModel = false)
    {
        if (AnyHintsShowing())
        {

            showRenderModel = true;
            SetHandState(HandState.None);

            /*
            if (turnAroundHintShowing)
            {
                SetTurnAroundHint(true);
            }

            if(gripBatHintShowing)
            {
                SetGripBatHint(true);
            }

            if(spawnBallHintShowing)
            {
                SetBallSpawnHint(true);
            }

            if(grabHintShowing)
            {
                SetGrabHint(true);
            }

            if(returnToBatHintShowing)
            {
                SetReturnToBatHint(true);
            }

            if(menuButtonHintShowing)
            {
                SetMenuButtonHint(true);
            }
            */

            return;
        }

        if (forceShowRenderModel == false)
        {
            showRenderModel = false;
        }
        else
        {
            showRenderModel = true;
            SetHandState(HandState.None);
            return;
        }

        //if (grabber)
        {
            if (GetCurrentGrabbable())
            {
                if (GetCurrentGrabbable().GetComponent<BallForDucks>())
                {
                    SetHandState(HandState.LargeBall);
                }
                else
                {
                    SetHandState(HandState.Ball);
                }
            }
            else
            {


                //bool occupied = ballController.instance.glove && ballController.instance.glove.attached && ((ballController.instance.glove.isOnRightHand && isOculusRight) || (!ballController.instance.glove.isOnRightHand && !isOculusRight));
                //if (!occupied && (hitThings.instance == null || hitThings.instance.attached == false || hitThings.instance.attachedCustomHand != this))

                //Debug.Log("_____________________________________________________ REFRESHING HAND "+name+" is occupied? "+IsOccupied());
                if (!IsOccupied())
                {
                    SetHandState(HandState.Empty);
                }
                else
                {
                    SetHandState(HandState.None);
                }
            }
        }
    }

    public bool IsOccupied()
    {
        if (GetCurrentGrabbable())
        {
            return true;
        }

        if (HandManager.instance.glove && HandManager.instance.glove.GetAttachedHand() == this)
        {
            return true;
        }

        if (HandManager.instance.cannon && HandManager.instance.cannon.GetAttachedHand() == this)
        {
            return true;
        }

        if (HandManager.instance.bat && HandManager.instance.bat.GetAttachedHand() == this)
        {
            return true;
        }

        return false;
    }

    public CustomGrabbable GetCurrentGrabbable()
    {
#if RIFT
        if(grabber)
        {
            return grabber.GetCurrentGrabbable();
        }
#elif STEAM_VR
        if (actualHand)
        {
            if (actualHand.currentAttachedObject)
                return actualHand.currentAttachedObject.GetComponent<CustomGrabbable>();
        }
#endif
        return null;
        //return m_grabbedObj;
    }

    public void MakeHandInvisible(bool permanent = false)
    {
        //Debug.Log("MAKE HAND INVISIBLE!!!!! ");
        //if (grabber)
        {
            //Debug.Log("and it has a grabber.");
            SetHandState(HandState.None);
            if (permanent && handAnimator)
            {
                Destroy(handAnimator.gameObject);
            }
        }
    }


    private bool AnyHintsShowing()
    {
        return turnAroundHintShowing || gripBatHintShowing || spawnBallHintShowing || grabHintShowing || returnToBatHintShowing || menuButtonHintShowing;
    }

    public void StartVibration(AnimationCurve curve = null, float duration = 1f)
    {
        StartCoroutine(VibrationRoutine(curve, duration));
    }

    public void Vibrate(ushort durationMicroSec)
    {
#if STEAM_VR
        actualHand.controller.TriggerHapticPulse(durationMicroSec);
#endif
    }



    IEnumerator VibrationRoutine(AnimationCurve curve, float duration = 1f)
    {
        for (float i = 0; i < 1; i += Time.deltaTime / duration)
        {
#if RIFT
            //OVRInput.SetControllerVibration(vibrateFade.Evaluate(i) * 1f, vibrateFade.Evaluate(i) * 1f, HandManager.OculusHand(attachedCustomHand));
#elif STEAM_VR
            if (curve != null)
            {
                actualHand.controller.TriggerHapticPulse((ushort)(900 - 900 * curve.Evaluate(i)));
            }
            else
            {
                actualHand.controller.TriggerHapticPulse(500);
            }
#endif
            yield return null;
        }
    }

    public void SetHandGripValue(float value, bool isSqueeze)
    {
        //isSqueeze specifies that we're using the grip buttons, rather than the trigger.

        GripValue = value;
        if (handAnimator && handAnimator.gameObject.activeInHierarchy)
        {
            if (FieldManager.currentHumanPlayerRole == FieldManager.HumanPlayerRole.BROADCAST)
            {
                if(isSqueeze) handAnimator.SetFloat("GripBlend", value);
                else handAnimator.SetFloat("GripBlend", -value);
            }
            else handAnimator.SetFloat("GripBlend", value);
        }
//        Debug.Log("Set grip val " + value);
    }

    public void SetHandType(Hand.HandType type)
    {
        if (_handType == type) return;
        _handType = type;
        if (_handType == Hand.HandType.Left)
        {
            grippedBallLocation.localScale = new Vector3(-1, 1, 1);
            handAnimator.name = "LeftAnim";
        }
        if (_handType == Hand.HandType.Right)
        {
            grippedBallLocation.localScale = Vector3.one;
            handAnimator.name = "RightAnim";
        }
    }

    public void SetHandState(HandState state)
    {

        //Debug.Log(name+" okay change hand state to " + state+"?? ");
        if (!handAnimator)
        {
            //Debug.Log("it's already invisible so nevermind.");
            return;
        }

        if (state == currentHandState && state != HandState.None)
        {
            //Debug.Log("it's already in that state..");
            return;
        }


        if (state != HandState.None)
        {
            if (currentHandState == HandState.None)
            {
                handAnimator.gameObject.SetActive(true);
            }

            if (handAnimator)
            {
                switch (state)
                {
                    case HandState.Empty:
                        handAnimator.SetTrigger("BecomeEmpty");
                        break;
                    case HandState.Ball:
                        handAnimator.SetTrigger("HoldBall");
                        break;
                    case HandState.LargeBall:
                        handAnimator.SetTrigger("HoldLargeBall");
                        break;
                    case HandState.Pointing:
                        handAnimator.SetTrigger("BeginPointing");
                        break;
                }
            }
        }
        else
        {
            if (handAnimator)
            {
                handAnimator.gameObject.SetActive(false);

            }
        }

        currentHandState = state;
    }

    private void OnHandInitialized(int deviceIndex)
    {

       // Debug.Log("_____________________________________________CUSTOM HAND ON HAND INITIALIZED " + deviceIndex + " show render model " + showRenderModel);
        RefreshHandState(true);

        //SetHandState(HandState.None);
        //showRenderModel = true;

        StartCoroutine(RefreshHandsAfterDelay(2f));
    }

    IEnumerator RefreshHandsAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        RefreshHandState();
    }

}

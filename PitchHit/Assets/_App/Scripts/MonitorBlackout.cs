﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class MonitorBlackout : MonoBehaviour {
    public RawImage blackout;
    public float fadeTime = 1f;
	// Use this for initialization
	void Awake () {
        DontDestroyOnLoad(gameObject);
        StartCoroutine(FadingRoutine());
    }

	// Update is called once per frame
	IEnumerator FadingRoutine () {
        float countdown = fadeTime;

        while(countdown>0)
        {
            countdown -= Time.deltaTime;
            blackout.color = new Color(0, 0, 0, 1f-(countdown / fadeTime));
            yield return null;
        }
        blackout.color = Color.black;
        float previousLevelTime = Time.timeSinceLevelLoad;
        yield return null;
        while(Time.timeSinceLevelLoad>previousLevelTime)
        {
            yield return null;
        }
        blackout.color = Color.black;

        countdown = fadeTime;

        while (countdown > 0)
        {
            countdown -= Time.deltaTime;
            blackout.color = new Color(0, 0, 0, countdown / fadeTime);
            yield return null;
        }

        Destroy(gameObject);
    }
}

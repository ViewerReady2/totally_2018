﻿//Hikmat
using UnityEngine;
using System.Collections;

public class DistanceCounter : MonoBehaviour
{

    public float distanceTravelled = 0;
    Vector3 lastPosition;
    private bool compute = false;
    private DistanceScoreController distance;
    // Use this for initialization
    void Start()
    {
        distance = FindObjectOfType<DistanceScoreController>();
        //distance = GameObject.FindGameObjectWithTag("score_board").GetComponent<DistanceModeController>();
    }
    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "bat")
        {
            lastPosition = transform.position;
            compute = true;
            this.gameObject.tag = "ball_in_use";
            //Invoke("ForObjectInvoke", 0);
        }
        if (col.gameObject.tag == "ground")
        {
            compute = false;
            this.gameObject.tag = "ball";
            //Invoke("ForInvoke", 0);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (compute == true)
        {
            distanceTravelled += Vector3.Distance(transform.position, lastPosition);
            lastPosition = transform.position;
        }
    }
    /*
    void ForInvoke()
    {
        if (distance)
        {
            distance.Compute();
        }
    }
    void ForObjectInvoke()
    {
        if (distance)
        {
            distance.ObjectReference();
        }
    }
    */
}

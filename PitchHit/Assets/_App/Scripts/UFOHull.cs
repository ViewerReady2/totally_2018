﻿using UnityEngine;
using System.Collections;

public class UFOHull : MonoBehaviour {
    private Rigidbody shipRigid;
    public EnemyUFO thisUFO;
    public Joint joint;
    public GameObject deathExplosion;
    public AudioClip deathSound;

    public float deathElevation = 60f;

    private AudioSource sound;
    private bool crashing;
    private bool crashed;
	// Use this for initialization
	void Awake () {
        shipRigid = GetComponent<Rigidbody>();
        sound = GetComponent<AudioSource>();
	}
	
	public void BeginDestruction()
    {
        transform.SetParent(null);
        Destroy(joint);
        StartCoroutine(SpacingRoutine());
    }

    private float additionalForceStrength = 2.5f;
    private Collider groundCollider;
    void OnCollisionEnter(Collision c)
    {
        ContactPoint firstContact = c.contacts[0];

        if (crashing)
        {
            if(c.gameObject.tag == "ground")
            {
                groundCollider = c.collider;
                crashing = false;
            }

        }
        else
        {
            hittable ball = c.collider.GetComponent<hittable>();
            //Debug.Log("impact happened " + c.gameObject.name);
            if (ball && ball.GetWasHitByBat())
            {
                //Debug.Log("impacted by ball");
                Vector3 impactForce = -1f * c.rigidbody.mass * c.rigidbody.velocity * additionalForceStrength;
                shipRigid.AddForceAtPosition(impactForce, firstContact.point, ForceMode.Impulse);

                thisUFO.SufferDamage(impactForce.magnitude, c);
            }
        }
    }


    IEnumerator SpacingRoutine()
    {
        //crashing = true;
        while (transform.position.y<deathElevation)
        {
            shipRigid.AddForce(10f * Vector3.up);

            yield return null;
        }

        if (deathExplosion)
        {
            Destroy(Instantiate(deathExplosion, transform.position, transform.rotation),10f);
        }

        if(deathSound && sound)
        {
            sound.clip = deathSound;
            sound.Play();
        }

        Destroy(gameObject);
    }

        IEnumerator CrashingRoutine()
    {
        crashing = true;
        while(crashing)
        {
            shipRigid.AddForce(-5f*Vector3.up);

            yield return null;
        }
        float countDown = 3f;

        while(countDown>0)
        {
            countDown -= Time.deltaTime;
            shipRigid.AddForce(-Vector3.up);
            yield return null;
        }
        crashed = true;

        while(!shipRigid.IsSleeping())
        {
            yield return new WaitForSeconds(1f);
        }
        Physics.IgnoreCollision(GetComponent<Collider>(), groundCollider);
        shipRigid.isKinematic = true;

        while(transform.position.y>-15f)
        {
            transform.position -= Vector3.up * Time.deltaTime;
            yield return null;
        }

        while (transform.localScale.y > .1f)
        {
            transform.localScale = Vector3.Lerp(transform.localScale,Vector3.zero,Time.deltaTime);
            yield return null;
        }

        Destroy(gameObject);
    }

    IEnumerator CrashedRoutine()
    {
        while (crashing)
        {
            shipRigid.AddForce(-Vector3.up);

            yield return null;
        }
    }
}

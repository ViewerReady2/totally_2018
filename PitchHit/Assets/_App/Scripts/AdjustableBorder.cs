﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdjustableBorder : MonoBehaviour {
    public Transform borderLeft;
    public Transform borderRight;
    public Transform borderTop;
    public Transform borderBottom;

    public Transform topLeft;
    public Transform topRight;
    public Transform bottomLeft;
    public Transform bottomRight;

    // Use this for initialization
    void Start () {
        //StartCoroutine(TestSizesRoutine());
	}
	
	IEnumerator TestSizesRoutine()
    {
        while(true)
        {
            SetWidthHeight(Random.value, Random.value);
            yield return null;
        }
    }

    public void SetWidthHeight(float w, float h)
    {
        transform.localScale = new Vector3(w, h, transform.localScale.z);
     
        //borderLeft.localPosition   = new Vector3(-w - halfThickness, 0f, -halfThickness);
        //borderRight.localPosition  = new Vector3(w + halfThickness, 0f, -halfThickness);
        //borderBottom.localPosition = new Vector3(0,-h + halfThickness, -halfThickness);
        //borderTop.localPosition    = new Vector3(0,h - halfThickness, -halfThickness);

        borderLeft.localScale   = new Vector3(1f / transform.localScale.x, 1f, borderLeft.localScale.z);
        borderRight.localScale  = new Vector3(1f / transform.localScale.x, 1f, borderLeft.localScale.z);
        borderTop.localScale    = new Vector3(1f,1f / transform.localScale.y, borderLeft.localScale.z);
        borderBottom.localScale = new Vector3(1f,1f / transform.localScale.y, borderLeft.localScale.z);

        topLeft.localScale     = new Vector3(1f / transform.localScale.x, 1f / transform.localScale.y, borderLeft.localScale.z);
        topRight.localScale    = new Vector3(1f / transform.localScale.x, 1f / transform.localScale.y, borderLeft.localScale.z);
        bottomLeft.localScale  = new Vector3(1f / transform.localScale.x, 1f / transform.localScale.y, borderLeft.localScale.z);
        bottomRight.localScale = new Vector3(1f / transform.localScale.x, 1f / transform.localScale.y, borderLeft.localScale.z);
    }
}

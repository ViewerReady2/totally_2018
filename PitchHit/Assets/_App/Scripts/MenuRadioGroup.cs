﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuRadioGroup : MonoBehaviour {
    [SerializeField]
    MenuButton[] buttons;
	// Use this for initialization
	void Start () {
		foreach (MenuButton button in buttons)
        {
            foreach (MenuButton innerButton in buttons)
            {
                if (button != innerButton)
                {
                    button.otherRadioButtons.Add(innerButton);
                }
            }

            button.buttonType = MenuButton.ButtonType.Radio;
        }
	}
}

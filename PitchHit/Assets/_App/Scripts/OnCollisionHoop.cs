﻿//This script was written by Mukhammadali

using UnityEngine;
using System.Collections;

public class OnCollisionHoop : MonoBehaviour {
    
    //Random colors of the hoops
    Color[] colors = new Color[10];

    //score components
    public int minusPoints = -5;

    //speed variables
    //private float speedX = 2.5f;
    //private float speedY = 2.5f;
    //private float speedZ = 2.5f;
    private float speed;

    //rotation variables
    private int choice;

    //audios
    private AudioSource audioSource;
    public AudioClip audioClip;
    


	void Start () 
    {
        //speedX = Random.Range(0.5f, 5.0f);
        //speedY = Random.Range(0.5f, 5.0f);
        //speedZ = Random.Range(0.5f, 5.0f);

        audioSource = gameObject.AddComponent<AudioSource>();

        //colors to be changed
        colors[0] = Color.cyan;
        colors[1] = Color.red;
        colors[2] = Color.green;
        colors[3] = new Color(1f, .65f, 0);
        colors[4] = Color.clear;
        colors[5] = Color.gray;
        colors[6] = Color.magenta;
        colors[7] = new Color(Random.Range(0f, 1f),
            Random.Range(0f, 1f),
            Random.Range(0f, 1f));
        colors[8] = new Color(Random.Range(0f, 1f),
            Random.Range(0f, 1f),
            Random.Range(0f, 1f));
        colors[9] = new Color(Random.Range(0f, 1f),
            Random.Range(0f, 1f),
            Random.Range(0f, 1f));
            
      
	}

    public void SetRandomRotationSpeed(float min, float max)
    {
        choice = Random.Range(1, 4);
        speed = Random.Range(min, max);
        
    }

    void Update()
    {
        //Randomly choses the on what axis to rotate
        switch (choice)
        {
            case 1:
                //Debug.Log("First Case is entered");
                RandomRotator_X_axis();
                break;
            case 2:
                //Debug.Log("Second Case is entered");
                RandomRotator_Y_axis();
                break;
            case 3:
               // Debug.Log("Third Case is entered");
                RandomRotator_Z_axis();
                break;
            case 4:
                //Debug.Log("Forth Case is entered");
                RandomRotator_X_axis();
                RandomRotator_Y_axis();
                RandomRotator_Z_axis();
                break;
            default:
                //print("Default method is the RandomRotator_Z_axis");
                RandomRotator_Z_axis();
                break;

        }
       
    }

    private void Collided()
    {
        gameObject.GetComponent<Renderer>().material.color = colors[Random.Range(0, colors.Length)];

        audioSource.clip = audioClip;
        audioSource.Play();
      
        if (ScoreboardController.instance.myScore > 0)
        {
            //ScoreboardController.instance.scorePopup(minusPoints, transform.position);
            ScoreboardController.instance.AddScore(minusPoints);

            if (ScoreboardController.instance.myScore < 0)
            {
                ScoreboardController.instance.SetScore(0);
            }
       }
       
    }

    /*
    //when the ball collides the hoops metal
    void OnCollisionEnter(Collision other)
    {
        //Debug.Log("ONCOLLISION ENTER IS Working properly");
        //changes the color each time when the ball hits the hoop
        Collided();
    }
    */

    void OnCollisionEnter(Collision collision)
    {

        hittable hitEventController = collision.gameObject.GetComponent<hittable>();
        if (hitEventController && hitEventController.GetWasHitByBat())
        {
            // Debug.Log("enter bat");
            if (collision.impulse.sqrMagnitude > 0f /*|| hitEventController.OnBatEnter()*/)
            {
                hitEventController.alreadyBeenHit = true;
                Collided();
            }
        }
    }

    // Text debugText;
    void OnCollisionStay(Collision collision)
    {
       // if (collision.relativeVelocity.magnitude > 1f && collision.rigidbody != null && collision.rigidbody.gameObject.tag == "ball")
       // {

            //if(collision.frictionForceSum)
            hittable hitEventController = collision.rigidbody.gameObject.GetComponent<hittable>();
            if (!hitEventController.alreadyBeenHit && hitEventController.GetWasHitByBat())
            {
                //Debug.Log("hit??????? vel mag: "+ collision.rigidbody.velocity.magnitude +"stay? "+ hitEventController.OnBatStay());

                //if (collision.contacts[0].separation > .1f)
                //{ 
                    if (collision.impulse.sqrMagnitude > 0f || ((collision.contacts[0].separation < .05f || collision.rigidbody.velocity.magnitude > 1f) /*&& hitEventController.OnBatStay()*/))
                    {
                        Collided();
                        hitEventController.alreadyBeenHit = true;
                    }
               // }
            }
        //}

    }
    
    void OnCollisionExit(Collision collision)
    {
        hittable hitEventController = collision.gameObject.GetComponent<hittable>();
        if (hitEventController && hitEventController.GetWasHitByBat())
        {
            if (!hitEventController.alreadyBeenHit)
            {
                //if (hitEventController.OnBatExit())
               // {
                //    Collided();
               // }
            }
            else
            {
                hitEventController.alreadyBeenHit = false;
            }
        }
    }
    

    //void OnColliderEnter(Collider other)
    //{
    //    Debug.Log("Is inside the Hoops Collider");
    //    if(other.tag == "Hoop")
    //    {
    //        Debug.Log("Hoops are colliding");
    //        Destroy(other.gameObject);
    //    }
    //}
    //rotates through X axis
    void RandomRotator_X_axis()
    {
        transform.Rotate(Random.Range(20f, 50f) * Time.deltaTime * speed,
             0.0f, 0.0f);
    }
    //rotates through Y axis
    void RandomRotator_Y_axis()
    {
        transform.Rotate(0.0f, Random.Range(20f, 50f) * Time.deltaTime * speed, 0.0f);
    }
   
    //rotates through Z axis
     void RandomRotator_Z_axis()
    {
        transform.Rotate(0.0f, 0.0f, Random.Range(20f, 50f) * Time.deltaTime * speed);
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayerObjectFinder : MonoBehaviour {

    [ContextMenu("FindObjectsInLayer")]
    void FindGameObjectsWithLayer()
    {
        GameObject[] goArray = FindObjectsOfType<GameObject>();
        List<GameObject> goList = new List<GameObject>();
        for(int i = 0; i < goArray.Length; i++)
        {
            if(goArray[i].layer == 20)
            {
                goList.Add(goArray[i]);
            }
        }

        if(goList.Count == 0)
        {
            Debug.Log("No objects in layer");
        }
        else
        {
            foreach(GameObject go in goList)
            {
                Debug.Log("Layer contains: " + go.name);
            }
        }
    }
 }
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIScoreboard : MonoBehaviour {

    public TextMeshProUGUI strikesText;

    public TextMeshProUGUI ballsText;
    public TextMeshProUGUI outsText;

    public TextMeshProUGUI[] runs_0;
    public TextMeshProUGUI[] runs_1;

    public TextMeshProUGUI totalRuns0;
    public TextMeshProUGUI totalRuns1;

    public Image frameHighlight;


    void Awake()
    {
        FieldManager.OnStrikesChange += SetStrikes;
        FieldManager.OnFoulsChange += SetFouls;
        FieldManager.OnOutsChange += SetOuts;
        FieldManager.OnBallsChange += SetBalls;
        FieldManager.OnScoreChange += SetRuns;

        for(int i = 0; i<runs_0.Length; i++)
        {
            runs_0[i].text = "";
            runs_1[i].text = "";
        }
    }
	
	public void SetRuns(int team, int inning, int runs)
    {
        TextMeshProUGUI runText = GetRunText(team, inning);
        if(runText != null)
        {
            runText.text = runs.ToString();
            HighlightFrame(team, inning);
        }
        if (team % 2 == 0)
        {
            if(FieldManager.awayTeamRuns < 10) totalRuns0.text = "0" + FieldManager.awayTeamRuns.ToString();
            else totalRuns0.text = FieldManager.awayTeamRuns.ToString();
        }
        if (team % 2 == 1)
        {
            if (FieldManager.homeTeamRuns < 10) totalRuns1.text = "0" + FieldManager.homeTeamRuns.ToString();
            else totalRuns1.text = FieldManager.homeTeamRuns.ToString();
        }
    }

    private TextMeshProUGUI GetRunText(int team, int inning)
    {
        if(team == 0)
        {
            if (inning < runs_0.Length) return runs_0[inning];
        }
        if(team == 1)
        {
            if (inning < runs_1.Length) return runs_1[inning];
        }
        return null;
    }

    private void HighlightFrame(int team, int inning)
    {
        if (team == 0 && inning == 0) return;
        TextMeshProUGUI runText = GetRunText(team, inning);
        if (runText != null)
        {
            frameHighlight.transform.position = runText.transform.position;
//            Debug.Log(runText.transform.position);
        }
    }

    public void SetTime(int time) {
        //timeText.text = time.ToString();
    }

    public void SetStrikes(int strikes) {
        strikesText.text = strikes.ToString();
    }

    public void SetFouls(int fouls)
    {
        //foulsText.text = fouls.ToString();
    }

    public void SetBalls(int balls) {
        ballsText.text = balls.ToString();
    }

    public void SetOuts(int outs) {
        outsText.text = outs.ToString();
    }

    void OnDestroy()
    {
        FieldManager.OnStrikesChange -= SetStrikes;
        FieldManager.OnFoulsChange   -= SetFouls;
        FieldManager.OnOutsChange    -= SetOuts;
        FieldManager.OnBallsChange   -= SetBalls;
        FieldManager.OnScoreChange   -= SetRuns;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldBaseFence : MonoBehaviour {

    public float scrollSpeed = 1;

    public Material mat;
	// Use this for initialization
	void Start () {
        Color c = mat.color;
        c.a = .5f;
        mat.color = c;
		foreach(Renderer rend in GetComponentsInChildren<Renderer>())
        {
            rend.material = mat;
        }
	}
	
	// Update is called once per frame
	void Update () {
        mat.mainTextureOffset = Vector2.right * Time.time * scrollSpeed;
    }
}

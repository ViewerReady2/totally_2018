﻿using UnityEngine;
using System.Collections;

public class DuckController : MonoBehaviour {
    // private static float scoreDistanceMultiplier = 1f;
    // private static float scoreForceMultiplier = 1f;
	public bool isBadDuck;
    public float maxtiltAllowed =45f;
    public float selfRightingStrength = 5f;
    public GameObject scoreDisplayPrefab;
    public Collider[] colliders;
    private static float rangeToActivateColliders = 16f;
    private static LayerMask activeBallLayer = -1;
    private AudioSource quacker;
    private Rigidbody rb;

    private  DuckLevelController duckLevelController;

	// Use this for initialization
	void Awake () {
        if(!duckLevelController)
        {
            duckLevelController = FindObjectOfType<DuckLevelController>();
        }
	    foreach(Collider c in colliders)
        {
            c.enabled = false;
        }

        if(activeBallLayer < 0 )
        {
            activeBallLayer = 1 << LayerMask.NameToLayer("ActiveBalls");
        }

        quacker = GetComponentInChildren<AudioSource>();

        rb = GetComponent<Rigidbody>();
	}

    void Start()
    {
        //this collider needs to be enabled before start, but immediately afterwards, it can be disabled.
        Collider waterCollider = GetComponent<Collider>();
        if(waterCollider)
        {
            waterCollider.enabled = false;
        }
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        bool mightBeHit = false;
        float castRadius = 6f;
        Ray caster = new Ray(transform.position, Vector3.up);
        //Debug.DrawRay(transform.position + (transform.forward * castRadius), Vector3.up, Color.green);
        //Debug.DrawRay(transform.position + (transform.right * castRadius), Vector3.up, Color.green);
        //Debug.DrawRay(transform.position + (transform.up * castRadius), Vector3.up, Color.green);
        
        Collider[] hit = Physics.OverlapSphere(transform.position, castRadius, activeBallLayer);
        if (hit != null)
        {
           // Debug.Log("ball in here. " + hit.Length);
            if (hit.Length > 0)
            {
                //Debug.Log("ball in here. "+hit.Length+" "+hit[0].name);
                mightBeHit = true;
            }

        }
        SetCollidersActive(mightBeHit);

        float angleToUpright = Vector3.Angle(transform.up,Vector3.up);
        if(angleToUpright>maxtiltAllowed)
        {
            Vector3 axis = Vector3.Cross(transform.up, Vector3.up);
            rb.AddTorque(axis * (angleToUpright-maxtiltAllowed)*selfRightingStrength);
        }

    }

    private void SetCollidersActive(bool a)
    {
        foreach(Collider c in colliders)
        {
            c.enabled = a;
        }
    }

    private float additionalForceStrength = 5f;
    void OnCollisionEnter(Collision c)
    {
        hittable ball = c.collider.GetComponent<hittable>();
        if(ball && !ball.isScored && ball.GetWasHitByBat())
        {
            ContactPoint firstContact = c.contacts[0];
            ball.isScored = true;
            float distance = Vector3.Distance(firstContact.point, ball.positionWhenHit);
            float force = 1f;
            //float force = Mathf.Cos(Vector3.Angle(c.rigidbody.velocity, firstContact.normal) * Mathf.Deg2Rad * 2f) * .5f + .5f;
            //force = c.rigidbody.velocity.magnitude * force;
            //force = Mathf.Max(1f, force);
           
            int score = (int)(distance * force);
            quacker.Play();

            GameObject temp = Instantiate(scoreDisplayPrefab, c.transform.position + (Vector3.up * 1f), Quaternion.identity) as GameObject;
            ScoreDisplay disp = temp.GetComponent<ScoreDisplay>();
            
			if (isBadDuck) {
                //disp.BeginDisplay ("ZERO");
                disp.JustAppear();
			} else {
                disp.BeginDisplay(score);
				//disp.BeginDisplay (score + "pts");
			}

            if(ScoreboardController.instance.scorable)
            {
				if (isBadDuck) {
                    ScoreboardController.instance.SetScore(0);
				} else {
					ScoreboardController.instance.AddScore(score);
					if (duckLevelController) {
						duckLevelController.CheckIfChallengePassed ();
					}
				}
            }

            rb.AddForceAtPosition(c.rigidbody.mass * c.rigidbody.velocity * additionalForceStrength, firstContact.point, ForceMode.Impulse);
        }
    }
}

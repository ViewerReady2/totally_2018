﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSoundTimer : MonoBehaviour {
    public float minimumTimeBetweenSounds = 15f;
    public float maximumTimeBetweenSounds = 35f;

    private AudioSource audioSource;

	// Use this for initialization
	void Start () {
        audioSource = GetComponent<AudioSource>();
        if (audioSource && audioSource.clip)
        {
            StartCoroutine(SoundRoutine());
        }
	}
	
    IEnumerator SoundRoutine()
    {
        while(true)
        {
            float countdown = Random.Range(minimumTimeBetweenSounds, maximumTimeBetweenSounds);
            audioSource.PlayDelayed(countdown);
            yield return new WaitForSeconds(countdown);
        }
    }
}

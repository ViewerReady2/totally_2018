﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinimapCamera : MonoBehaviour {

    Vector3 lastPosition;
    Quaternion lastRotation;
    public static MinimapCamera Instance;
    [SerializeField] Transform minimapTransform;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
        lastPosition = transform.position;
        lastRotation = transform.rotation;
    }

    void Update()
    {
        Debug.DrawLine(transform.position, transform.position + transform.forward * 0.3f, Color.blue, Time.deltaTime);
        //Debug.Log((transform.position - lastPosition).ToString("f3"));
        /*if(Vector3.Distance(transform.position, lastPosition) > 0.004f)
        {
            lastPosition = transform.position;
        }
        if (Quaternion.Angle(transform.rotation, lastRotation) > 3f)
        {
            lastRotation = transform.rotation;
        }*/
    }

    public Vector3 GetRelativePosition()
    {
        if(minimapTransform != null)
        {
            return minimapTransform.InverseTransformPoint(transform.position);
        }
        else
        {
            return transform.localPosition;
        }
    }

    public Quaternion GetRelativeRotation()
    {
        if (minimapTransform != null)
        {
            return (Quaternion.Inverse(minimapTransform.transform.rotation) * transform.rotation);
        }
        else
        {
            return transform.localRotation;
        }
    }
}

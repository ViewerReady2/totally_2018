﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
#if RIFT || GEAR_VR
using Oculus.Platform;
using Oculus.Platform.Models;
#elif STEAM_VR
using Steamworks;
#endif

public class LeaderboardManager : MonoBehaviour {
    private struct LocalListing
    {
        public string initials;
        public int score;
    }
    private enum letters
    {
        A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z
    }

    public int minimumForHighScore;
    public string leaderboardName = "Top Scores";
    public GameObject localLeaderboardContainer;
    public GameObject globalLeaderboardContainer;
    public GameObject globalLeaderboardTitle;
    public GameObject localLeaderboardTitle;
    public Text globalPosition;
    public Text localPosition;
    public int boardLength = 10;
    //public Text topScores;

    public GameObject initialsUI;
    public RealButton initialsUIButton;
    public RealButton initialsEnterButton;
    public Text firstLetter;
    public Text secondLetter;
    public Text thirdLetter;
    public Text pendingScoreText;
    public AudioSource highScoreAudio;
    public AudioClip newHighScoreSound;
    public AudioClip highScoreSubmitSound;
    public Transform initialsUIParent;
    public GameObject backboard;
    public Transform fullBatterBackboardPosition;
    private letters[] allLetters;
    private int currentFirstLetter;
    private int currentSecondLetter;
    private int currentThirdLetter;
    private int pendingScoreToRank;
    //public RealButton beginButton;
#if RIFT
    SortedDictionary<int, LeaderboardEntry> leaderboardDictionary;
#elif STEAM_VR
    private SteamLeaderboard_t m_SteamLeaderboard;
	private SteamLeaderboardEntries_t m_SteamLeaderboardEntries;
    // Our GameID
	private CGameID m_GameID;
    
    protected Callback<UserStatsReceived_t> m_UserStatsReceived;
	protected Callback<UserStatsStored_t> m_UserStatsStored;
	protected Callback<UserAchievementIconFetched_t> m_UserAchievementIconFetched;

	//private CallResult<UserStatsReceived_t> UserStatsReceived;
	private CallResult<LeaderboardFindResult_t> LeaderboardFindResult;
	private CallResult<LeaderboardScoresDownloaded_t> LeaderboardScoresDownloaded;
	private CallResult<LeaderboardScoresDownloaded_t> LeaderboardScoresSelfDownloaded;
	private CallResult<LeaderboardScoreUploaded_t> LeaderboardScoreUploaded;
	private CallResult<LeaderboardUGCSet_t> LeaderboardUGCSet;

#endif

    public bool loadAutomatically = true;
    private bool isEnteringInitials;
    private LocalListing[] localListings;

	

	// Values
	//private int m_PlayerScore;

	// Did we get the stats from Steam?
	//private bool m_bRequestedStats;
	//private bool m_bStatsValid;

	// Should we store stats this frame?
	//private bool m_bStoreStats;

	



	// Use this for initialization
	void Start()
	{
        if (initialsUI)
        {
            initialsUI.SetActive(false);
            if(initialsUIButton)
            {
                initialsUIButton.gameObject.SetActive(false);
            }
            initialsUI.transform.parent.SetParent(initialsUIParent);
            initialsUI.transform.parent.localPosition = Vector3.zero;
            initialsUI.transform.parent.localRotation = Quaternion.identity;
        }
        
        if(backboard)
        {
            backboard.SetActive(false);
        }

        if(!GameController.appearingMenuMode || GameController.fullBatterMode)
        {
            if (backboard && fullBatterBackboardPosition)
            {
                backboard.transform.position = fullBatterBackboardPosition.position;
                backboard.transform.rotation = fullBatterBackboardPosition.rotation;
            }
        }

        Debug.Log("turn off all the text in the leaderboards!");

        localLeaderboardContainer.SetActive(false);
        globalLeaderboardContainer.SetActive(false);
        globalLeaderboardTitle.SetActive(false);
        globalPosition.gameObject.SetActive(false);
        localLeaderboardTitle.SetActive(false);
        localPosition.gameObject.SetActive(false);
        if(LockerRoomLevelController.isDemo)
        {
            DisplayDemo();
            return;
        }

#if RIFT
        //   https://developer3.oculus.com/documentation/platform/1.9/concepts/pgsg-2-ps-platform-setup/
        //  https://developer3.oculus.com/documentation/platform/1.9/concepts/dg-core-content/
        //Core.Initialize(PlatformSettings.AppID);
        Core.AsyncInitialize(PlatformSettings.AppID).OnComplete(ConnectToLeaderboard);
#elif GEAR_VR
        Core.Initialize(PlatformSettings.MobileAppID);
#elif STEAM_VR
        if (SteamManager.Initialized)
		{ 
           
			string name = SteamFriends.GetPersonaName();
			CSteamID userId = SteamUser.GetSteamID();
			Debug.Log("initialized "+name);

			//m_PlayerScore = 500;

			// Cache the GameID for use in the Callbacks
			m_GameID = new CGameID(SteamUtils.GetAppID());


			//m_UserStatsReceived = Callback<UserStatsReceived_t>.Create(OnUserStatsReceived);
			//m_UserStatsStored = Callback<UserStatsStored_t>.Create(OnUserStatsStored);
			//m_UserAchievementIconFetched = Callback<UserAchievementIconFetched_t>.Create(OnUserAchievementIconFetched);

			// These need to be reset to get the stats upon an Assembly reload in the Editor.
			//m_bRequestedStats = false;
			//m_bStatsValid = false;

			//if (loadAutomatically) {
				//UserStatsReceived = CallResult<UserStatsReceived_t>.Create(OnUserStatsReceived);
				LeaderboardFindResult = CallResult<LeaderboardFindResult_t>.Create (OnLeaderboardFindResult);
				LeaderboardScoresDownloaded = CallResult<LeaderboardScoresDownloaded_t>.Create (OnLeaderboardScoresDownloaded);
				// TODO: Identify player in leaderboards
				// LeaderboardScoresSelfDownloaded = CallResult<LeaderboardScoresDownloaded_t>.Create(OnLeaderboardScoresSelfDownloaded);
				LeaderboardScoreUploaded = CallResult<LeaderboardScoreUploaded_t>.Create (OnLeaderboardScoreUploaded);

				ConnectToLeaderboard ();
			//}
			/*
            CSteamID[] selfUser = new CSteamID[1];
            selfUser[0] = SteamUser.GetSteamID();
            SteamAPICall_t leaderboardSelfHandle = SteamUserStats.DownloadLeaderboardEntriesForUsers(m_SteamLeaderboard, selfUser, 1);
            LeaderboardScoresSelfDownloaded.Set(leaderboardSelfHandle);
            */
            
		}
        else
        {
            Debug.Log("steam is not initialized!");
        }
#endif

        if (loadAutomatically)
        {
            InitializeLocalScores();
        }
	}

    


    private void InitializeLocalScores()
    {
        if (!initialsUI || !initialsUIButton || !initialsEnterButton)
        {
            Debug.Log("0Something is missing");
            return;
        }

        if (allLetters != null)
        {
            Debug.Log("all letters already exists.");
            return;
        }
        Debug.Log("initialize");
        if (localPosition)
        {
            Debug.Log("yep.");
            localListings = new LocalListing[boardLength];

            string temp = PlayerPrefs.GetString(leaderboardName, "");
            string[] temps = temp.Split(',');
            for (int x = 0; x < localListings.Length; x++)
            {
                string resultInitials = "";
                int resultScore = 0;
                if (x < temps.Length)
                {
                    //temps[x] should be "GJT-123"
                    string[] parts = temps[x].Split('-');
                    if (parts.Length > 1)
                    {
                        resultInitials = parts[0];
                        Int32.TryParse(parts[1], out resultScore);
                    }
                }
                localListings[x].initials = resultInitials;
                localListings[x].score = resultScore;
            }

            DisplayLocalScores();
        }

        allLetters = (letters[])Enum.GetValues(typeof(letters));

        currentFirstLetter = PlayerPrefs.GetInt("FIRSTLETTER", 0);
        currentSecondLetter = PlayerPrefs.GetInt("SECONDLETTER", 0);
        currentThirdLetter = PlayerPrefs.GetInt("THIRDLETTER", 0);

        firstLetter.text = allLetters[currentFirstLetter].ToString();
        secondLetter.text = allLetters[currentSecondLetter].ToString();
        thirdLetter.text = allLetters[currentThirdLetter].ToString();
        //Enum.TryParse<letters>(firstLetter.text,)
    }

    public void DisplayLocalScores()
    {
        if(localListings==null)
        {
            InitializeLocalScores();
        }

        if(localListings.Length<1 || localListings[0].initials.Length<2)
        {
            return;
        }
        localLeaderboardContainer.SetActive(true);
        localLeaderboardTitle.SetActive(true);
        localPosition.gameObject.SetActive(true);
        if(backboard)
        {
            backboard.SetActive(true);
        }

        localPosition.text = "";
        int position = 0;
        foreach(LocalListing l in localListings)
        {
            position++;
            if (l.initials.Length > 1)
            {
                localPosition.text += (position + ". " + l.initials + " - " + l.score + "\n");
            }
            else
            {
                localPosition.text += (position + ".\n");
            }
        }

    }

    public void SaveLocalScores()
    {
        string temp = "";
        for(int x=0;x<localListings.Length;x++)
        {
            temp += (localListings[x].initials+"-"+localListings[x].score.ToString());
            if(x<localListings.Length-1)
            {
                temp += ",";
            }
        }
        PlayerPrefs.SetString(leaderboardName, temp);
    }

    public void EnterLocalScore(string initials, int score)
    {
        Debug.Log("about to enter local score "+initials+" "+score);
        int targetSlot = localListings.Length - 1;
        while (targetSlot>=0 && localListings[targetSlot].score < score)
        {
            if(targetSlot < localListings.Length-1)
            {
                localListings[targetSlot + 1] = localListings[targetSlot];
            }
            targetSlot--;
        }
        if (targetSlot < localListings.Length - 1)
        {
            targetSlot++;
        }

        localListings[targetSlot].initials = initials;
        localListings[targetSlot].score = score;

        DisplayLocalScores();

        SaveLocalScores();
    }

    public bool WouldScoreMakeLocalBoard(int score)
    {
        if (!initialsUI || !initialsUIButton || !initialsEnterButton)
        {
            return false;
        }

        if(score < minimumForHighScore)
        {
            return false;
        }

        InitializeLocalScores();
        Debug.Log("would " + score + " make it up on the scoreboard. " + pendingScoreToRank);
        Debug.Log(localListings);
        Debug.Log(localListings[localListings.Length - 1].score);

        if (localListings!=null && localListings[localListings.Length - 1].score < score)
        {
            Debug.Log("yep");
            SetInitialsUIActive(false);
            pendingScoreToRank = score;
            initialsUIButton.gameObject.SetActive(true);
            initialsUIButton.SetActivation(false);
            if (pendingScoreText)
            {
                pendingScoreText.text = pendingScoreToRank.ToString();
            }
            initialsUIButton.RefreshColor();

            if (highScoreAudio && newHighScoreSound)
            {
                highScoreAudio.clip = newHighScoreSound;
                highScoreAudio.Play();
            }

            return true;
        }
        
        return false;
    }

   // private bool initialsUIShouldBeVisible;
    private bool appearingMenuIsInTheWay;
    public  void SetInitialsUIActive(bool a)
    {
        if(!initialsUI || !initialsUIButton || !initialsEnterButton)
        {
            return;
        }

        //initialsUIShouldBeVisible = a;

        if(appearingMenuIsInTheWay)
        {
            return;
        }

        //Debug.Log("set initial thing active "+a+ " "+initialsUI.name);
        initialsUI.SetActive(a);
        
        initialsEnterButton.SetActivation(false);
        initialsEnterButton.RefreshColor();
        if(a)
        {
            pendingScoreText.text = pendingScoreToRank.ToString();
            InitializeLocalScores();
        }
    }

    public void OnAppearingMenu(bool appearing, Vector3 appearingPosition)
    {
        if(appearing)
        {
            Vector3 perspective = TButt.TBCameraRig.instance.GetActiveCameraObject().transform.position;
            float visualAngle = Mathf.Abs(GameController.AngleAroundAxis(appearingPosition - perspective, initialsUIParent.position - perspective, Vector3.up));
            if(visualAngle<65)
            {
                appearingMenuIsInTheWay = true;
                if(initialsUIParent.gameObject.activeSelf)
                {
                    initialsUIParent.gameObject.SetActive(false);
                }
            }
        }
        else
        {

            //if(initialsUIShouldBeVisible)
            //{
                initialsUIParent.gameObject.SetActive(true);
                appearingMenuIsInTheWay = false;
            //}
        }

    }

    public void SubmitInitials()
    {
        EnterLocalScore(firstLetter.text + secondLetter.text + thirdLetter.text, pendingScoreToRank);
        initialsUIButton.gameObject.SetActive(false);
        SetInitialsUIActive(false);

        if (highScoreAudio && highScoreSubmitSound)
        {
            highScoreAudio.clip = highScoreSubmitSound;
            highScoreAudio.Play();
        }
    }

    public void LetterUp(int which)
    {
        switch(which)
        {
            case 1:
                currentFirstLetter++;
                break;
            case 2:
                currentSecondLetter++;
                break;
            case 3:
                currentThirdLetter++;
                break;
        }
        UpdateLetter(which);
    }

    public void LetterDown(int which)
    {
        switch (which)
        {
            case 1:
                currentFirstLetter--;
                break;
            case 2:
                currentSecondLetter--;
                break;
            case 3:
                currentThirdLetter--;
                break;
        }
        UpdateLetter(which);
    }

    private void UpdateLetter(int which)
    {
        switch (which)
        {
            case 1:
                if (currentFirstLetter >= allLetters.Length)
                {
                    currentFirstLetter = 0;
                }
                else if(currentFirstLetter <0)
                {
                    currentFirstLetter = allLetters.Length - 1;
                }
                firstLetter.text = allLetters[currentFirstLetter].ToString();
                PlayerPrefs.SetInt("FIRSTLETTER", currentFirstLetter);
                break;
            case 2:
                if (currentSecondLetter >= allLetters.Length)
                {
                    currentSecondLetter = 0;
                }
                else if (currentSecondLetter < 0)
                {
                    currentSecondLetter = allLetters.Length - 1;
                }
                secondLetter.text = allLetters[currentSecondLetter].ToString();
                PlayerPrefs.SetInt("SECONDLETTER", currentSecondLetter);
                break;
            case 3:
                if (currentThirdLetter >= allLetters.Length)
                {
                    currentThirdLetter = 0;
                }
                else if (currentThirdLetter < 0)
                {
                    currentThirdLetter = allLetters.Length - 1;
                }
                thirdLetter.text = allLetters[currentThirdLetter].ToString();
                PlayerPrefs.SetInt("THIRDLETTER", currentThirdLetter);
                break;
        }

    }





#if RIFT
    public void ConnectToLeaderboard(Message<PlatformInitialize> msg)
    {
        Debug.Log("finished initializing the oculus platform core.");
        if (!msg.IsError)
        {
            Debug.Log("connect the rift to the leaderboard.............................");

            Leaderboards.GetEntries(leaderboardName, boardLength, LeaderboardFilterType.None, LeaderboardStartAt.CenteredOnViewerOrTop).OnComplete(OnLeaderboardScoresDownloaded);
        }
    }
#elif STEAM_VR
    public void ConnectToLeaderboard()
	{
		// Get leaderboard
		SteamAPICall_t leaderboardHandle = SteamUserStats.FindLeaderboard(leaderboardName);

		LeaderboardFindResult.Set(leaderboardHandle);
		//print("FindLeaderboard(\""+leaderboardName+"\") - " + leaderboardHandle);
    }
#endif
   

#if RIFT
#elif STEAM_VR
	private void OnLeaderboardFindResult(LeaderboardFindResult_t pCallback, bool bIOFailure)
	{

		Debug.Log("OnLeaderboardFindResult - " + pCallback.m_hSteamLeaderboard + " -- " + pCallback.m_bLeaderboardFound);

		if (pCallback.m_bLeaderboardFound != 0)
		{
			m_SteamLeaderboard = pCallback.m_hSteamLeaderboard;
		}
        if (loadAutomatically)
        {
            BeginRefreshLeaderboardDisplay();
        }
	}
#endif




    public void BeginRefreshLeaderboardDisplay()
	{
#if RIFT
#elif STEAM_VR
		if (m_SteamLeaderboard != null && LeaderboardScoresDownloaded !=null) {
			SteamAPICall_t leaderboardEntriesHandle = SteamUserStats.DownloadLeaderboardEntries(m_SteamLeaderboard, ELeaderboardDataRequest.k_ELeaderboardDataRequestGlobalAroundUser, -3, 3);
			LeaderboardScoresDownloaded.Set(leaderboardEntriesHandle);
			//print("DownloadLeaderboardEntries(" + m_SteamLeaderboard + ", ELeaderboardDataRequest.k_ELeaderboardDataRequestGlobalAroundUser, -10, 9) - " + leaderboardEntriesHandle);
		}
#endif
	}

#if RIFT
    private void OnLeaderboardScoresDownloaded(Message<LeaderboardEntryList> msg)
    {
        Debug.Log("ONLEADERBOARDSCORESDOWNLOADED");
        if (!msg.IsError)
        {
            if(leaderboardDictionary==null)
            {
                leaderboardDictionary = new SortedDictionary<int, LeaderboardEntry>();
            }
            string leaderboardText = "";

            foreach (LeaderboardEntry entry in msg.Data)
            {
                leaderboardDictionary[entry.Rank] = entry;

                //if (entry.User.ID == PlatformManager.MyID)
                // {
                //     m_foundLocalUserMostWinsEntry = true; 
                //     m_numWins = entry.Score;
                // }

                leaderboardText += string.Format("{0}: {1} - {2}\n", entry.Rank, entry.User.OculusID, entry.Score);
            }

            // results might be paged for large requests
            //if (msg.Data.HasNextPage)
            //{
            //    Leaderboards.GetNextEntries(msg.Data).OnComplete(MostWinsGetEntriesCallback);
            //    return;
            //}

            // if local user not in the top, get their position specifically
            //if (!m_foundLocalUserMostWinsEntry)
            //{
            //    Leaderboards.GetEntries(MOST_MATCHES_WON, 1, LeaderboardFilterType.None,
            //        LeaderboardStartAt.CenteredOnViewer).OnComplete(MostWinsGetEntriesCallback);
            //    return;
            //}

            if (msg.Data.Count > 0)
            {
                globalLeaderboardTitle.SetActive(true);
                globalPosition.gameObject.SetActive(true);
                if (backboard)
                {
                    backboard.SetActive(true);
                }
                SetYourPositionText(leaderboardText);
            }
        }
        // else an error is returned if the local player isn't ranked - we can ignore that

        //if (m_mostWinsCallback != null)
        //{
        //    m_mostWinsCallback(m_mostWins);
        ///}
        //m_mostWins = null;
    }
#elif STEAM_VR
	private void OnLeaderboardScoresDownloaded(LeaderboardScoresDownloaded_t pCallback, bool bIOFailure)
	{
		Debug.Log (pCallback.m_hSteamLeaderboard.ToString());
		Debug.Log("failure?:"+bIOFailure+"  [" + LeaderboardScoresDownloaded_t.k_iCallback + " - LeaderboardScoresDownloaded] - " + pCallback.m_hSteamLeaderboard + " -- " + pCallback.m_hSteamLeaderboardEntries + " -- " + pCallback.m_cEntryCount);
		m_SteamLeaderboardEntries = pCallback.m_hSteamLeaderboardEntries;

		string leaderboardText = "";


		for (int i=0; i < pCallback.m_cEntryCount; i++)
		{
			LeaderboardEntry_t entry = new LeaderboardEntry_t();
			int[] details = new int[1];
			SteamUserStats.GetDownloadedLeaderboardEntry(m_SteamLeaderboardEntries, i, out entry, details, 1);

			//print(string.Format("Leaderboard entry {0}: {1} - {2}", entry.m_nGlobalRank, SteamFriends.GetFriendPersonaName(entry.m_steamIDUser), entry.m_nScore));
			leaderboardText += string.Format("{0}: {1} - {2}\n", entry.m_nGlobalRank, SteamFriends.GetFriendPersonaName(entry.m_steamIDUser), entry.m_nScore);
		}

        if (pCallback.m_cEntryCount > 0)
        {
            if (backboard)
            {
                backboard.SetActive(true);
            }
            globalLeaderboardContainer.SetActive(true);
            globalLeaderboardTitle.SetActive(true);
            globalPosition.gameObject.SetActive(true);
            SetYourPositionText(leaderboardText);
        }

	}
#endif


    public void CommitScoreToLeaderboard(int value)
    {
        if (WouldScoreMakeLocalBoard(value))
        {
#if RIFT
            Leaderboards.WriteEntry(leaderboardName, value).OnComplete(OnLeaderboardScoreUploaded);
#elif STEAM_VR
		//Debug.Log ("Begin sending score to leaderboard "+value);
		if (!SteamManager.Initialized || LeaderboardScoreUploaded==null)
		{
			//print("Steamworks not initialized!");
			return;
		}
        //title.gameObject.SetActive(true);
       // title.text = "commit:" + value + " " + Time.time;
        SteamAPICall_t leaderboardUploadHandle = SteamUserStats.UploadLeaderboardScore(m_SteamLeaderboard, ELeaderboardUploadScoreMethod.k_ELeaderboardUploadScoreMethodKeepBest, value, null, 0);
		LeaderboardScoreUploaded.Set(leaderboardUploadHandle);
		//print("UploadLeaderboardScore(" + m_SteamLeaderboard + ", ELeaderboardUploadScoreMethod.k_ELeaderboardUploadScoreMethodKeepBest, " + value + ", null, 0) - " + leaderboardUploadHandle);
#endif
        }
    }
#if RIFT
    private void OnLeaderboardScoreUploaded(Message<bool> msg)
    {

    }
#elif STEAM_VR
	private void OnLeaderboardScoreUploaded(LeaderboardScoreUploaded_t pCallback, bool bIOFailure)
	{
		//Debug.Log ("failure: "+bIOFailure);
		//Debug.Log ("pCallback success: "+pCallback.m_bSuccess);
		//Debug.Log ("pCallback score: "+pCallback.m_nScore);
		//if (pCallback.m_bSuccess) {
			BeginRefreshLeaderboardDisplay ();
		//}
		Debug.Log("[" + LeaderboardScoreUploaded_t.k_iCallback + " - LeaderboardScoreUploaded] - " + pCallback.m_bSuccess + " -- " + pCallback.m_hSteamLeaderboard + " -- " + pCallback.m_nScore + " -- " + pCallback.m_bScoreChanged + " -- " + pCallback.m_nGlobalRankNew + " -- " + pCallback.m_nGlobalRankPrevious);
	}
#endif
        //public void ClearAllStats()
        //{
        //	SteamUserStats.ResetAllStats(true);
        //}






        /*
        // Update is called once per frame
        void Update () {
            if (Input.GetKeyDown ("e")) {
                CommitScoreToLeaderboard (111);
            }
            if(Input.GetKeyDown("t"))
            {
            BeginRefreshLeaderboardDisplay();
            }
        }
        */
    public void SetYourPositionText(string text)
    {
        //GUILayout.Label("GetLeaderboardName(m_SteamLeaderboard) : " + SteamUserStats.GetLeaderboardName(m_SteamLeaderboard));
        //GUILayout.Label("GetLeaderboardEntryCount(m_SteamLeaderboard) : " + SteamUserStats.GetLeaderboardEntryCount(m_SteamLeaderboard));
        //GUILayout.Label("GetLeaderboardSortMethod(m_SteamLeaderboard) : " + SteamUserStats.GetLeaderboardSortMethod(m_SteamLeaderboard));
        //GUILayout.Label("GetLeaderboardDisplayType(m_SteamLeaderboard) : " + SteamUserStats.GetLeaderboardDisplayType(m_SteamLeaderboard));

        if (!LockerRoomLevelController.isDemo)
        {
            globalPosition.text = text;
        }
        else
        {
            DisplayDemo();
        }
    }

    private void DisplayDemo()
    {
        globalLeaderboardTitle.SetActive(true);
        if(backboard)
        {
            backboard.SetActive(true);
        }
        globalPosition.gameObject.SetActive(true);
        globalPosition.text = "...available now in the full version!";
    }

    //public void SetTopScoresText(string text)
    //{
    //    topScores.text = text;
    //}
}

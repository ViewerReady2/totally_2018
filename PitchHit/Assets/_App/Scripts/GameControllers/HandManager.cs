﻿using UnityEngine;
using System.Collections;
#if STEAM_VR
using Valve.VR.InteractionSystem;
#endif

public class HandManager : MonoBehaviour
{
    private static HandManager _instance;
    public static HandManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<HandManager>();
            }
            return _instance;
        }
        private set
        {
            _instance = value;
        }
    }


#if RIFT
    public static OVRInput.Controller dominantOculusHand = OVRInput.Controller.RTouch;
    public static OVRInput.Controller secondaryOculusHand = OVRInput.Controller.LTouch;

    private static CustomHand oculusRightCustomHand;
    private static CustomHand oculusLeftCustomHand;
#elif STEAM_VR
    public static Hand currentLeftSteamVRHand
    {
        get
        {
            if (!Player.instance) return null;
            return Player.instance.leftHand;
        }
    }

    public static Hand currentRightSteamVRHand
    {
        get
        {
            if (!Player.instance) return null;
            return Player.instance.rightHand;
        }
    }
#endif

    public enum HandType
    {
        right, left, both
    }

    public static HandType dominantHand = HandType.right;
    public static CustomHand currentLeftCustomHand
    {
        get
        {

#if STEAM_VR
            if (!currentLeftSteamVRHand) return null;
            return currentLeftSteamVRHand.customHand;
#elif RIFT
            return oculusLeftCustomHand;
#endif
            
        }
    }

    public static CustomHand currentRightCustomHand
    {
        get
        {

#if STEAM_VR
            if (!currentRightSteamVRHand) return null;
            return currentRightSteamVRHand.customHand;
#elif RIFT
            return oculusRightCustomHand;
#endif
        

        }
    }

    public static MixedRealityBat genericTracker;

    public bool humanHoldingBall
    {
        get
        {
            return glove.hasBall;
        }
    }

    public HumanGloveController glovePrefab;
    public HumanGloveController glove { get
        {
            if (_glove == null) { _glove = GameObject.Instantiate(glovePrefab, Vector3.down, Quaternion.identity); _glove.gameObject.SetActive(false); }
            return _glove;
        }
    }
    private HumanGloveController _glove;

    public BatController batPrefab;
    public BatController bat
    {
        get
        {
            if (_bat == null) { _bat = GameObject.Instantiate(batPrefab, Vector3.down, Quaternion.identity); _bat.gameObject.SetActive(false); }
            return _bat;
        }
    }
    private BatController _bat;

    public HandCannonController cannonPrefab;
    public HandCannonController cannon
    {
        get
        {
            if (_cannon == null)
            {
                _cannon = GameObject.Instantiate(cannonPrefab, Vector3.down, Quaternion.identity);
                _cannon.gameObject.SetActive(true);
                _cannon.Disappear_Immediate();
            }
            return _cannon;
        }
    }
    private HandCannonController _cannon;

    public enum HandGear
    {
        BAT,
        GLOVE,
        CANNON,
        HAND,
        CONTROLLER,
        NONE,
    }

    public enum ThrowingVariant
    {
        CANNON,
        MANUAL
    }

    private static ThrowingVariant throwingType
    {
        get { return (ThrowingVariant)PlayerPrefs.GetInt("THROW_VAR"); }
        set { PlayerPrefs.SetInt("THROW_VAR", (int)value);
            useCannon = (value == ThrowingVariant.CANNON);
            useCannon_setFromPrefs = true;
        }
    }
    public static bool useCannon
    {
        get
        {
            if (!useCannon_setFromPrefs)
            {
                _useCannon = (throwingType == ThrowingVariant.CANNON);
                useCannon_setFromPrefs = true;
            }
            return _useCannon;
        }
        private set
        {
            _useCannon = value;
        }
    }
    private static bool _useCannon;
    private static bool useCannon_setFromPrefs;


    void Start()
    {
        instance = this;

#if RIFT || GEAR_VR
        //Debug.Log("----------------------------------------------------------------------- HANDMANAGER STARTING");
        if (OVRManager.instance)
        {
//            Debug.Log("ovr instance found.");
            Transform oculusParent = OVRManager.instance.transform.Find("TrackingSpace");
            CustomHand[] ch = oculusParent.gameObject.GetComponentsInChildren<CustomHand>();
//            Debug.Log("ovr instance found." +ch.Length);
            foreach (CustomHand c in ch)
            {

                if (c.isOculusRight)
                {
                    oculusRightCustomHand = c;
//                    Debug.Log("ovr right hand found");
                }
                else
                {
                    oculusLeftCustomHand = c;
//                    Debug.Log("ovr left hand found");
                }

            }
        }
#elif STEAM_VR

#endif


    }

    void Update()
    {
        UpdateHands();
        RefreshThrowingEquipment();
        //print("dominantOculusHand " + dominantOculusHand);
        if (currentLeftCustomHand)
        {
            Debug.DrawRay(currentLeftCustomHand.transform.position, Vector3.up, Color.red);
        }

        if (currentRightCustomHand)
        {
            Debug.DrawRay(currentRightCustomHand.transform.position + (Vector3.forward * .02f), Vector3.up, Color.blue);
        }


            
        
    }

    public void DetermineDominantHandByGrab(GameObject grabbed)
    {
        int attempts = 0;
        while (attempts < 10)
        {
            hitThings.SelectNextGripType();
            if (hitThings.GetCurrentGripType() == hitThings.gripType.always)
            {
                attempts = 11;
            }
            else
            {
                attempts++;
            }
        }
#if STEAM_VR
        if (currentLeftCustomHand && currentLeftCustomHand.actualHand)
        {
            foreach (Hand.AttachedObject a in currentLeftCustomHand.actualHand.AttachedObjects)
            {
                if (a.attachedObject == grabbed)
                {
                    SetDominantHand(HandType.left);
                    return;
                }
            }
        }

        if (currentRightCustomHand && currentRightCustomHand.actualHand)
        {
            foreach (Hand.AttachedObject a in currentRightCustomHand.actualHand.AttachedObjects)
            {
                if (a.attachedObject == grabbed)
                {
                    SetDominantHand(HandType.right);
                    return;
                }
            }
        }
#elif RIFT
        if (currentLeftCustomHand)
        {
            Grabber leftGrab = currentLeftCustomHand.GetComponentInChildren<Grabber>();
            if (leftGrab && leftGrab.GetCurrentGrabbable())
            {
                if (leftGrab.GetCurrentGrabbable().gameObject == grabbed)
                {
                    SetDominantHand(HandType.left);
                    return;
                }
            }
        }

        if (currentRightCustomHand)
        {
            Grabber rightGrab = currentLeftCustomHand.GetComponentInChildren<Grabber>();
            if (rightGrab && rightGrab.GetCurrentGrabbable())
            {
                if (rightGrab.GetCurrentGrabbable().gameObject == grabbed)
                {
                    SetDominantHand(HandType.right);
                    return;
                }
            }
        }

#endif
    }

    public static void SetDominantHand(HandType dom)
    {
#if RIFT
        if(dom == HandType.right)
            {
                dominantOculusHand = OVRInput.Controller.RTouch;
                secondaryOculusHand = OVRInput.Controller.LTouch;
            }
            else
            {
                dominantOculusHand = OVRInput.Controller.LTouch;
                secondaryOculusHand = OVRInput.Controller.RTouch;
            }
#endif


        dominantHand = dom;

        hitThings currentBat = FindObjectOfType<hitThings>();
        if (currentBat != null)
        {
            currentBat.RefreshOnHandChange();
        }

        //save to userprefs.

        string temp = "BOTH";
        if (dom == HandType.right)
        {
            temp = "RIGHT";
        }
        else if (dom == HandType.left)
        {
            temp = "LEFT";
        }

        PlayerPrefs.SetString("HAND", temp);
    }

    public static OVRInput.Controller OculusHand(CustomHand customHand)
    {
        if (customHand == currentLeftCustomHand)
        {
            return OVRInput.Controller.LTouch;
        }
        if (customHand == currentRightCustomHand)
        {
            return OVRInput.Controller.RTouch;
        }
        return OVRInput.Controller.RTouch;
    }

    private static float timeWithAnyHands = 0;
    private static int previousNumberOfHands = 0;
    public static void UpdateHands()
    {

#if RIFT
        //only keep hands assigned if that controller is active?

#elif GEAR_VR
        //Debug.Log("current right hand. "+currentRightCustomHand);
        //if((OVRInput.GetConnectedControllers() & OVRInput.Controller.Touchpad) != 0)
        //{

        //}

#elif STEAM_VR

        

        /*
        if (Player.instance)
        {
            if (Player.instance.hands.Length > 0 && Player.instance.hands.Length == previousNumberOfHands)
            {
                timeWithAnyHands += Time.deltaTime;
            }
            else
            {
                timeWithAnyHands = 0;
                previousNumberOfHands = Player.instance.hands.Length;
            }
            if (true || timeWithAnyHands > 1f)
            {
                if (GameController.isOculusInSteam)
                {
                    if (currentLeftSteamVRHand == null)
                    {
                        if (currentRightSteamVRHand == null)
                        {
                            //this is the first controller.
                            if (Player.instance.hands.Length > 0)
                            {
                                foreach (Hand h in Player.instance.hands)
                                {
                                    if (h.controller != null && h.controller.valid)
                                    {
                                        SteamVR_RenderModel temp = h.GetComponentInChildren<SteamVR_RenderModel>();
                                        if (temp && (temp.transform.Find("x_button")))
                                        {
                                            currentLeftSteamVRHand = h;
                                            currentLeftCustomHand = currentLeftSteamVRHand.GetComponent<CustomHand>();
                                            leftHandDeviceIndex = (int)currentLeftSteamVRHand.controller.index;

                                            currentLeftCustomHand.name = "LeftCustomHand_1st";
                                            if (currentLeftSteamVRHand.startingHandType == Hand.HandType.Right)
                                            {
                                                SwapHandVisuals(currentLeftCustomHand, currentLeftCustomHand.otherHand);
                                            }

                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            Debug.Log(".....................................assign left hand................................. second");

                            //right hand already exists
                            //currentRightSteamVRHand = Player.instance.rightHand;
                            //currentLeftSteamVRHand = Player.instance.leftHand;

                            currentLeftSteamVRHand = currentRightSteamVRHand.otherHand;


                            //currentLeftSteamVRHand = currentRightSteamVRHand.otherHand;
                            if (currentLeftSteamVRHand)
                            {
                                if (currentLeftSteamVRHand.controller != null && currentLeftSteamVRHand.controller.valid)
                                {
                                    currentLeftCustomHand.name = "LeftCustomHand_2nd";
                                    currentLeftCustomHand = currentLeftSteamVRHand.GetComponent<CustomHand>();
                                    leftHandDeviceIndex = (int)currentLeftSteamVRHand.controller.index;
                                }
                                else
                                {
                                    currentLeftSteamVRHand = null;
                                    leftHandDeviceIndex = -1;
                                }
                            }
                        }
                    }

                    if (currentRightSteamVRHand == null)
                    {
                        if (currentLeftSteamVRHand == null)
                        {
                            //this is the first controller.
                            if (Player.instance.hands.Length > 0)
                            {
                                foreach (Hand h in Player.instance.hands)
                                {
                                    if (h.controller != null && h.controller.valid)
                                    {
                                        SteamVR_RenderModel temp = h.GetComponentInChildren<SteamVR_RenderModel>();
                                        if (temp && (temp.transform.Find("a_button")))
                                        {
                                            currentRightSteamVRHand = h;
                                            currentRightCustomHand = currentRightSteamVRHand.GetComponent<CustomHand>();
                                            rightHandDeviceIndex = (int)currentRightSteamVRHand.controller.index;
                                            currentRightCustomHand.name = "RightCustomHand_1st";

                                            if(currentRightSteamVRHand.startingHandType == Hand.HandType.Left)
                                            {
                                                SwapHandVisuals(currentRightCustomHand, currentRightCustomHand.otherHand);
                                            }

                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            Debug.Log(".....................................assign right hand.................................second");

                            //right hand already exists
                            //currentLeftSteamVRHand = Player.instance.leftHand;
                            //currentRightSteamVRHand = Player.instance.rightHand;
                            //currentRightSteamVRHand = currentLeftSteamVRHand.otherHand;

                            currentRightSteamVRHand = currentLeftSteamVRHand.otherHand;

                            if (currentRightSteamVRHand)
                            {
                                if (currentRightSteamVRHand.controller != null && currentRightSteamVRHand.controller.valid)
                                {
                                    currentRightCustomHand = currentRightSteamVRHand.GetComponent<CustomHand>();
                                    rightHandDeviceIndex = (int)currentRightSteamVRHand.controller.index;
                                    currentRightCustomHand.name = "RightCustomHand_2nd";

                                }
                                else
                                {
                                    currentRightSteamVRHand = null;
                                    rightHandDeviceIndex = -1;
                                }
                            }
                        }
                    }
                
                }
                else
                {//VIVE CONTROLLER VERSION

                    if(currentLeftSteamVRHand == null)//look for left hand.
                    {
                        if(leftHandDeviceIndex != -1)//refresh device index based on previous level.
                        {
                            if (Player.instance.leftController != null && Player.instance.leftController.index == (uint)leftHandDeviceIndex)
                            {
                                currentLeftSteamVRHand = Player.instance.leftHand;
                            }
                            else if (Player.instance.rightController != null && Player.instance.rightController.index == (uint)leftHandDeviceIndex)
                            {
                                currentLeftSteamVRHand = Player.instance.rightHand;
                            }
                        }
                        else
                        {
                            if (dominantHand == HandType.left || currentRightSteamVRHand)//dominant hand goes first or other hand is already done
                            {
                                if (currentRightSteamVRHand)//right hand already done
                                {
                                    if(Player.instance.leftHand && Player.instance.rightHand)//when both hands are newly visible. they should be reassessed.
                                    {
                                        currentRightSteamVRHand = Player.instance.rightHand;
                                        currentLeftSteamVRHand = Player.instance.leftHand;

                                        if (currentRightSteamVRHand)//you also have to update the other hand in this case.
                                        {
                                            currentRightCustomHand = currentRightSteamVRHand.GetComponent<CustomHand>();
                                            rightHandDeviceIndex = (int)currentRightSteamVRHand.controller.index;
                                        }
                                    }
                                }
                                else
                                {
                                    //no right hand visible. this one must be first.
                                    currentLeftSteamVRHand = Player.instance.leftHand;
                                    if(!currentLeftSteamVRHand)
                                    {
                                        currentLeftSteamVRHand = Player.instance.rightHand;
                                    }
                                }
                            }
                        }
                        if (currentLeftSteamVRHand)
                        {
                            currentLeftCustomHand = currentLeftSteamVRHand.GetComponent<CustomHand>();
                            leftHandDeviceIndex = (int)currentLeftSteamVRHand.controller.index;
                        }
                    }

                    if (currentRightSteamVRHand == null)//look for right hand
                    {
                        if (rightHandDeviceIndex != -1)//refresh device index based on previous level.
                        {
                            if (Player.instance.rightController != null && Player.instance.rightController.index == (uint)rightHandDeviceIndex)
                            {
                                currentRightSteamVRHand = Player.instance.rightHand;
                            }
                            else if (Player.instance.leftController != null && Player.instance.leftController.index == (uint)rightHandDeviceIndex)
                            {
                                currentRightSteamVRHand = Player.instance.leftHand;
                            }
                        }
                        else
                        {
                            if (dominantHand == HandType.right || currentLeftSteamVRHand)//dominant hand goes first or other hand is already done
                            {
                                if (currentLeftSteamVRHand)//left hand already done
                                {
                                    if (Player.instance.leftHand && Player.instance.rightHand)//when both hands are newly visible. they should be reassessed.
                                    {
                                        currentRightSteamVRHand = Player.instance.rightHand;
                                        currentLeftSteamVRHand = Player.instance.leftHand;

                                        if (currentLeftSteamVRHand)//you also have to update the other hand in this case
                                        {
                                            currentLeftCustomHand = currentLeftSteamVRHand.GetComponent<CustomHand>();
                                            leftHandDeviceIndex = (int)currentLeftSteamVRHand.controller.index;
                                        }
                                    }
                                }
                                else
                                {
                                    //no lef hand visible. this one must be first.
                                    currentRightSteamVRHand = Player.instance.rightHand;
                                    if (!currentRightSteamVRHand)
                                    {
                                        currentRightSteamVRHand = Player.instance.leftHand;
                                    }
                                }
                            }
                        }
                        if (currentRightSteamVRHand)
                        {
                            if (currentRightSteamVRHand.controller != null)
                            {
                                currentRightCustomHand = currentRightSteamVRHand.GetComponent<CustomHand>();
                                rightHandDeviceIndex = (int)currentRightSteamVRHand.controller.index;
                            }
                            else
                            {
                                currentRightSteamVRHand = null;
                            }
                        }
                    }

                    if (currentLeftCustomHand)
                    {
                        if (currentLeftSteamVRHand == null || currentLeftSteamVRHand.controller == null || currentLeftSteamVRHand.controller.valid == false)
                        {
                            currentLeftCustomHand = null;
                            currentLeftSteamVRHand = null;
                            leftHandDeviceIndex = -1;
                        }
                    }

                    if (currentRightCustomHand)
                    {
                        if (currentRightSteamVRHand == null || currentRightSteamVRHand.controller == null || currentRightSteamVRHand.controller.valid == false)
                        {
                            currentRightCustomHand = null;
                            currentRightSteamVRHand = null;
                            rightHandDeviceIndex = -1;
                        }
                    }
                }
                return;
            }
            if (currentLeftSteamVRHand != Player.instance.leftHand)
            {
                currentLeftSteamVRHand = Player.instance.leftHand;
                if (currentLeftSteamVRHand)
                {
                    currentLeftCustomHand = currentLeftSteamVRHand.GetComponent<CustomHand>();
                }
                else
                {
                    currentLeftCustomHand = null;
                }
            }

            if (currentRightSteamVRHand != Player.instance.rightHand)
            {
                currentRightSteamVRHand = Player.instance.rightHand;
                if (currentRightSteamVRHand)
                {
                    currentRightCustomHand = currentRightSteamVRHand.GetComponent<CustomHand>();
                }
                else
                {
                    currentRightCustomHand = null;
                }
            }
        }
        */
#endif
    }

    public static bool IsHandValid(HandType type)
    {
#if RIFT
        OVRInput.Controller controller = OVRInput.GetConnectedControllers();
        //Debug.Log("active? "+ ((controller & OVRInput.Controller.LTouch) !=0));
        if(type == HandType.left)
        {
            if ((controller & OVRInput.Controller.LTouch) !=0)
            {
                return true;
            }
        }
        else if(type == HandType.right)
        {
            if((controller & OVRInput.Controller.RTouch) != 0)
            {
                return true;
            }
        }
#elif GEAR_VR
        if(Application.isEditor)
        {
            if(type == HandType.left)
            {
                return currentLeftCustomHand != null;
            }
            if(type == HandType.right)
            {
                return currentRightCustomHand != null;
            }
        }

        OVRInput.Controller controller = OVRInput.GetConnectedControllers();
        //Debug.Log("active? "+ ((controller & OVRInput.Controller.LTouch) !=0));
        if (type == HandType.left)
        {
            if ((controller & OVRInput.Controller.LTrackedRemote) != 0)
            {
                return true;
            }
        }
        else if (type == HandType.right)
        {
            if ((controller & OVRInput.Controller.RTrackedRemote) != 0)
            {
                return true;
            }
        }
#elif STEAM_VR

        if (type == HandType.left)
        {
            if (currentLeftSteamVRHand && currentLeftSteamVRHand.controller != null && currentLeftSteamVRHand.controller.valid)
            {
                return true;
            }
        }
        else if (type == HandType.right)
        {
            if (currentRightSteamVRHand && currentRightSteamVRHand.controller != null && currentRightSteamVRHand.controller.valid)
            {
                return true;
            }
        }
#endif
        return false;
    }

    public static CustomHand GetAvailableDominantHand()
    {


        if (dominantHand == HandType.left)
        {
            if (IsHandValid(HandType.left))
            {

                return currentLeftCustomHand;
            }
            else if (IsHandValid(HandType.right))
            {
                return currentRightCustomHand;
            }
        }
        else
        {
            if (IsHandValid(HandType.right))
            {
                return currentRightCustomHand;
            }
            else if (IsHandValid(HandType.left))
            {
                return currentLeftCustomHand;
            }
        }

        return null;
    }

    //This is for Oculus in steam. It's the best way I could think as to deal with the hands being judged wrongly at the start.
    private static void SwapHandVisuals(CustomHand handOne, CustomHand handTwo)
    {
        Animator tempHandAnimator = handOne.handAnimator;
        Transform tempHandOneVisual = tempHandAnimator.transform.parent;
        Transform tempHandOneVisualParent = tempHandOneVisual.parent;

        Vector3 tempHandVisualLocalPosition = tempHandOneVisual.localPosition;
        Quaternion tempHandVisualLocalRotation = tempHandOneVisual.localRotation;
        Vector3 tempHandVisualLocalScale = tempHandOneVisual.localScale;

        Transform tempHandTwoVisual = handTwo.handAnimator.transform.parent;

        //move hand one over to hand two's spot.
        handOne.handAnimator = handTwo.handAnimator;
        tempHandOneVisual.SetParent(tempHandTwoVisual.parent);
        //reinstate hand one's old local orientation
        tempHandOneVisual.localPosition = tempHandVisualLocalPosition;
        tempHandOneVisual.localRotation = tempHandVisualLocalRotation;

        //save hand two's local orientation
        tempHandVisualLocalPosition = tempHandTwoVisual.localPosition;
        tempHandVisualLocalRotation = tempHandTwoVisual.localRotation;

        //move hand two over to hand one's spot.
        handTwo.handAnimator = tempHandAnimator;
        tempHandTwoVisual.SetParent(tempHandOneVisualParent);
        //reinstate hand two's old local orientation
        tempHandTwoVisual.localPosition = tempHandVisualLocalPosition;
        tempHandTwoVisual.localRotation = tempHandVisualLocalRotation;
    }

    public void RemoveAllBaseballEquipment()
    {
        SetHandEquipmentActive(HandGear.BAT, false);
        SetHandEquipmentActive(HandGear.GLOVE, false);
        //SetHandEquipmentActive(HandGear.CANNON, false);
        /*
        for (int i = 0; i < System.Enum.GetValues(typeof(HandGear)).Length; i++)
        {
            SetHandEquipmentActive((HandGear)i, false);
        }
        */
    }

    public void SetHandEquipmentActive(HandGear equip, bool isOn)
    {
        //Debug.Log("SetPlayer Equipment " + equip + " " + isOn);
        switch (equip)
        {
            case HandGear.BAT:
                bat.gameObject.SetActive(isOn);
                bat.SetHoldable(isOn);
                bat.enabled = isOn;
                break;
            case HandGear.CANNON:
                //MARC: cannon is always active, use Prepare and Disappear to control
                /*
                isOn = isOn && useCannon;
                cannon.gameObject.SetActive(isOn);
                cannon.enabled = isOn;
                */
                break;
            case HandGear.GLOVE:

                if (isOn)
                {
                    glove.shouldBeOnLeft = (dominantHand == HandType.right);
                    glove.gameObject.SetActive(true);
                    glove.attachToHandAutomatically = true;
                    glove.SetAttached(true);
                    glove.enabled = true;
                }
                else
                {
                    glove.attachToHandAutomatically = false;
                    glove.enabled = false;
                    glove.SetAttached(false);
                    glove.gameObject.SetActive(false);
                }
                break;
            case HandGear.CONTROLLER:
                break;
            case HandGear.HAND:
                break;
            case HandGear.NONE:
                break;
        }
        if(currentLeftCustomHand) currentLeftCustomHand.RefreshHandState();
        if(currentRightCustomHand) currentRightCustomHand.RefreshHandState();

        UpdateHands();
    }

    void RefreshThrowingEquipment()
    {
        if (humanHoldingBall)
        {
            if (useCannon && !cannon.isActivated)
            {
                    cannon.PrepareForUse();
            }
            else if (!useCannon && cannon.isActivated)
            {
                cannon.Disappear_Immediate();
            }

        }
        else
        {
            if (cannon.isActivated)
            {
                if (useCannon)
                {
                    cannon.Disappear();
                }
                else
                {
                    cannon.Disappear_Immediate();
                }
            }
        }
    }

    public static void ToggleThrowingType()
    {

        if (throwingType == ThrowingVariant.MANUAL) throwingType = ThrowingVariant.CANNON;
        else if (throwingType == ThrowingVariant.CANNON) throwingType = ThrowingVariant.MANUAL;


        Debug.Log("Toggle Throwing Type " + throwingType);
    }

    public static void SetThrowingType(ThrowingVariant type)
    {
        throwingType = type;
    }

    public void HideCannonImmediately()
    {
        cannon.Disappear_Immediate();
    }


    #region Vibration
    public static void VibrateController(float duration, float intensity, CustomHand hand)
    {
        if (!instance) return;
        instance.StartCoroutine(instance.VibrationRoutine(duration, intensity, hand));
    }

    public static void VibrateController(float duration, AnimationCurve vibeCurve, CustomHand hand)
    {
        if (!instance) return;
        instance.StartCoroutine(instance.VibrationRoutine(duration, vibeCurve, hand));

    }

    private IEnumerator VibrationRoutine(float duration, float intensity, CustomHand hand)
    {
#if RIFT
        OVRInput.SetControllerVibration(intensity, intensity, HandManager.OculusHand(hand));
        yield return new WaitForSeconds(duration);
        OVRInput.SetControllerVibration(0, 0, HandManager.OculusHand(hand));

#elif STEAM_VR
        if(hand != null)
        {
            if (hand.actualHand.controller != null) hand.actualHand.controller.TriggerHapticPulse(ushort.MaxValue);
        }
        yield return null;
#endif
    }

    private IEnumerator VibrationRoutine(float duration, AnimationCurve curve, CustomHand hand)
    {

#if RIFT
        
        for (float i = 0; i < duration; i += Time.deltaTime)
        {
            float vibe = curve.Evaluate(i/duration);
            OVRInput.SetControllerVibration(vibe, vibe, HandManager.OculusHand(hand));

            yield return null;
        }

        OVRInput.SetControllerVibration(0, 0, HandManager.OculusHand(hand));

#elif STEAM_VR
        if (hand.actualHand.controller != null)
            hand.actualHand.controller.TriggerHapticPulse(ushort.MaxValue);
        yield return null;
#endif

    }


    #endregion
}

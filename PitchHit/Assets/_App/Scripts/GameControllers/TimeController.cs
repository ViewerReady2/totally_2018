﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.Events;


public class TimeController : MonoBehaviour
{
    public UnityEvent OnTimeExpired;
    public GameObject timeDisplay;
    public Number3DController hundredsNumber3D;
    public Number3DController tensNumber3D;
    public Number3DController singlesNumber3D;
    public bool forceDemo;
    public Text timeText;

    public GameObject countdownObj;
    private AudioSource countdownController;

    //private levelSequenceManager levelSequenceMgr;

    private IEnumerator timerCoroutine;
#if STEAM_VR
    private SteamworksClientManager steamClientMgr;
#endif
    public bool startAtFirstBallActive = true;
    public int totalTimeSec;
    public int demoTimeSec = 60;
    public int currentTimeSec;
    public int warningLeftSec;
    public string endGameText;
    public GameObject endGameObject;
    private bool warned;

    // Use this for initialization
    void Awake()
    {
        if (timeDisplay)
        {
            //timeText = timeDisplay.GetComponentInChildren<Text>();

            // if(timeText && timeText.canvas && dominantHandManager.dominantHand == dominantHandManager.Hand.left)
            // {
            //    Transform temp = timeText.canvas.transform;
            //    temp.position = new Vector3(-temp.position.x, temp.position.y, temp.position.z);
            //    temp.rotation = Quaternion.Euler(temp.eulerAngles.x, -temp.eulerAngles.y, temp.eulerAngles.z);
            //}
            timeDisplay.SetActive(false);
        }
        //levelSequenceMgr = GetComponent<levelSequenceManager>();
        if (countdownObj)
        {
            countdownController = countdownObj.GetComponent<AudioSource>();
        }
#if STEAM_VR
        steamClientMgr = FindObjectOfType<SteamworksClientManager>();
#endif
    }

    public void ResetTimer()
    {
        timeDisplay.SetActive(true);

        Debug.Log("reset timer++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        // Stop old timer
        if (timerCoroutine != null)
        {
            StopCoroutine(timerCoroutine);
        }

        warned = false;
        //ClearTimeText();
        if (LockerRoomLevelController.isDemo)
        {
            currentTimeSec = demoTimeSec;
        }
        else
        {
            currentTimeSec = totalTimeSec;
        }
        //Debug.Log("time?"+ currentTimeSec);
        if (currentTimeSec > 0)
        {
            // Debug.Log("there is time to reset.");
            if (timeDisplay)
            {
                timeDisplay.SetActive(true);
            }

            timerCoroutine = TimerCoroutine();
            StartCoroutine(timerCoroutine);
        }
    }

    private void ClearTimeText()
    {

        //if (!timeText && timeDisplay)
        //{
        //    timeText = timeDisplay.GetComponentInChildren<Text>();
        //}

        if (hundredsNumber3D)
        {
            hundredsNumber3D.gameObject.SetActive(false);
        }

        if(tensNumber3D)
        {
            tensNumber3D.gameObject.SetActive(false);
        }

        if(singlesNumber3D)
        {
            singlesNumber3D.gameObject.SetActive(false);
        }


        if (timeText)
        {
            timeText.text = "";
        }

    }

    private void SetTimeTextSec(int seconds)
    {
        if (!timeText)
        {
            int temp;
            if (seconds < 100)
            {
                hundredsNumber3D.DisplayNumber(0);
                hundredsNumber3D.gameObject.SetActive(false);
            }
            else
            {
                hundredsNumber3D.gameObject.SetActive(true);
                temp = seconds / 100;
                hundredsNumber3D.DisplayNumber(temp);
                seconds -= (temp*100);
            }

            if (seconds < 10)
            {
                tensNumber3D.DisplayNumber(0);
                //tensNumber3D.gameObject.SetActive(false);
            }
            else
            {
                //tensNumber3D.gameObject.SetActive(true);
                temp = seconds / 10;
                tensNumber3D.DisplayNumber(temp);
                seconds -= (temp*10);
            }

            singlesNumber3D.DisplayNumber(seconds);

            return;
        }

        if (SceneManager.GetActiveScene().name == "Level_Space")
        {
            timeText.text = "TIME\n" + seconds.ToString();
        }
        else
        {
            timeText.text = "Time: " + seconds.ToString();
        }
    }

    private void SetThankYouText()
    {
        if (timeText)
        {
            timeText.text = endGameText;
            //disable "time: and numbers" 3d text
            for (int i = 0; i < timeDisplay.transform.childCount; i++)
            {
                var child = timeDisplay.transform.GetChild(i).gameObject;
                if (child != null)
                    child.SetActive(false);
            }
            //enable "Thanks for playing!" 3d text
            endGameObject.SetActive(true);
        }
        else
        {
            hundredsNumber3D.transform.parent.gameObject.SetActive(false);
            endGameObject.SetActive(true);
        }
    }

    public IEnumerator TimerCoroutine()
    {
        //int countdown = currentTimeSec;
        SetTimeTextSec(currentTimeSec);
        while (currentTimeSec > 0)
        {
            yield return new WaitForSeconds(1.0f);
            //Debug.Log("Counting " + countdown);
            currentTimeSec--;
            SetTimeTextSec(currentTimeSec);
            //if (levelSequenceMgr != null)
            //   levelSequenceMgr.Tick(totalTimeSec - countdown);

            if (currentTimeSec < warningLeftSec + 1 && !warned)
            {
                warned = true;
                if (countdownController && countdownController.clip)
                {
                    countdownController.PlayOneShot(countdownController.clip);
                }
            }
        }
        OnTimeExpired.Invoke();
        if (LockerRoomLevelController.isDemo || forceDemo)
        {
            StartCoroutine(EndDemo());
        }

        // demo is over, player can no longer score.
        ScoreboardController.instance.FinalizeScore();
#if STEAM_VR
        //if (steamClientMgr)
        //{
        //    steamClientMgr.EndGame(ScoreboardController.instance.myScore, ScoreboardController.instance.ballHitCount);
        //}
#endif
        SetThankYouText();
    }

    public GameObject endDemoMessage;
    public RealButton theExitButton;
    IEnumerator EndDemo()
    {
        endDemoMessage.SetActive(true);
        Debug.Log("end demo");
        yield return new WaitForSeconds(3.5f);
        Debug.Log("now!");
        theExitButton.SetActivation(true);
    }
}

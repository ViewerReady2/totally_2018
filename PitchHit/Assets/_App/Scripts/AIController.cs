﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AIController : MonoBehaviour
{
    #region Public Variables
    public Transform head;
    public Transform leftHand;
    public Transform rightHand;
    public Transform leftElbow;
    public Transform rightElbow;

    public enum LogicState { Idle, Searching, Caught, Throw }

    public float headRotationSpeed = .1f;
    public float handRotationSpeed = .5f;
    //public float focusPointRange = 5f;

    /// <summary>
    /// Determines where the AI's head should be looking at.
    /// </summary>
    //[Tooltip("Determines where the AI's head should be looking at.")]
    //public Transform focusPoint;

    /// <summary>
    /// Objects within these layers can be focused (looked at) by the AI.
    /// </summary>
    public LayerMask focusLayers;

    public Transform debugTransformRed;
    public Transform debugTransformBlue;
    #endregion

    #region Private Variables
    private AIHand m_rightHand;
    private AIHand m_leftHand;

    private AIHand m_gloveHand;
    private Transform gloveElbow;

    private float m_handSpeed = 50f;

    private List<hittable> ballsToTrack;
    #endregion

    #region Unity Methods
    private void Awake()
    {
        ballsToTrack = new List<hittable>();
        DetermineWhichHandIsTheGlove();
        initialPosition = transform.position;
        initialLocalGlovePosition = m_gloveHand.transform.localPosition;
    }

    void OnEnable()
    {
        //m_gloveHand.glove.onHumanBallCatch.AddListener(OnCaughtBall);
        //HumanGloveController.onHumanBallCatch.AddListener(OnCaughtBall);//Greg: I commented this out because it was causing the ball to disappear as it's spawned.
    }

    void OnDisable()
    {
        //m_gloveHand.glove.onHumanBallCatch.RemoveListener(OnCaughtBall);
        //HumanGloveController.onHumanBallCatch.RemoveListener(OnCaughtBall);//Greg: I commented this out because it was causing the ball to disappear as it's spawned.

    }

    public void StartTrackingBall(hittable ball)
    {
        StartCoroutine(StartTrackingAfterDelay(ball, .5f));
    }

    IEnumerator StartTrackingAfterDelay(hittable ball, float delay)
    {
        yield return new WaitForSeconds(.25f);
        ballsToTrack.Add(ball);
    }

    private bool tryToCatchBall;
    private bool ballHasBeenJudged;
    private Vector3 catchingSpot;

    private void LateUpdate()
    {
        PerformJudgementOnBall();

        PerformMovement();

    }
    #endregion

    #region Private Methods


    private float gloveOutFrontDistance = .15f;
    private void PerformJudgementOnBall()
    {
        if (ballsToTrack.Count > 0)
        {
            hittable ball = ballsToTrack[0];
            TrajTester1 ballTrajectory = ball.GetComponent<TrajTester1>();

            if (!ballHasBeenJudged)
            {
                float timeUntilPassing = 0f;
                bool looksGood = Trajectory.GetPointOfPassingTarget(ball.GetComponent<Rigidbody>(), ball.spotThrownFrom, initialPosition-((initialPosition-ball.spotThrownFrom).normalized*gloveOutFrontDistance), out catchingSpot, out timeUntilPassing);
                if (catchingSpot.y < 0)
                {
                    catchingSpot += Vector3.up * catchingSpot.y * -1f;
                }

                if(looksGood && Vector3.Distance(catchingSpot, initialPosition + (Vector3.up * head.position.y)) < 3.5f && transform.InverseTransformPoint(ball.transform.position).z >= 0 /*timeUntilPassing>= 0f && timeUntilPassing < 4*/)
                {
                    tryToCatchBall = true;
                }
                else
                {
                    tryToCatchBall = false;
                }
               
            }
            else
            {
                //debugTransformBlue.position = ball.transform.position;
            }

            if (!tryToCatchBall)
            {
                bool success = ballsToTrack.Remove(ball);
                return;
            }

        }
        else
        {
            tryToCatchBall = false;
            ballHasBeenJudged = false;        
        }
    }

    public void OnCaughtBall(hittable ball)
    {
        Debug.Log("remove and destroy ball caught by "+gameObject.name);
        ballsToTrack.Remove(ball);
        Destroy(ball.gameObject);
    }

    private Vector3 initialPosition;
    private Vector3 initialLocalGlovePosition;
    private void PerformMovement()
    {
        Vector3 directionToLook = transform.forward;
        if (tryToCatchBall)
        {
            Vector3 standingTarget = new Vector3(catchingSpot.x, 0f, catchingSpot.z) - (Vector3.forward* gloveOutFrontDistance*5f);

            transform.position = Vector3.MoveTowards(transform.position, standingTarget, Time.deltaTime * 5f);
            directionToLook = (ballsToTrack[0].transform.position - head.transform.position).normalized;
            debugTransformBlue.position = catchingSpot;
            debugTransformRed.position = catchingSpot;
            debugTransformRed.localScale = new Vector3(2f, .2f, .2f);

            m_gloveHand.transform.position = catchingSpot;
            Vector3 fromHandToHead = m_gloveHand.transform.position - head.position;
            float maxReach = 1.3f;
            if (fromHandToHead.sqrMagnitude > (maxReach * maxReach))
            {
                m_gloveHand.transform.position = head.position + (fromHandToHead.normalized * maxReach);
            }

            m_gloveHand.transform.LookAt(ballsToTrack[0].transform.position,m_gloveHand.transform.position-gloveElbow.position);
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, initialPosition, Time.deltaTime * 2f);
            debugTransformBlue.position = transform.position + Vector3.up * 3f;
            debugTransformRed.position = transform.position + Vector3.up * 3f;
            debugTransformRed.localScale = new Vector3(3f, .3f, .3f);

            m_gloveHand.transform.localPosition = initialLocalGlovePosition;
        }

        head.rotation = Quaternion.LookRotation(directionToLook, Vector3.up);
        float angleToFacingForward = Vector3.Angle(head.forward, transform.forward);
        if (angleToFacingForward >80f)
        {
            head.Rotate(Vector3.Cross(head.forward, transform.forward), 80f - angleToFacingForward,Space.World);
        }
        
    }

    private void DetermineWhichHandIsTheGlove()
    {
        m_leftHand = leftHand.GetComponent<AIHand>();
        m_rightHand = rightHand.GetComponent<AIHand>();

        if (m_leftHand.isGlove == true)
        {
            m_gloveHand = m_leftHand;
            m_rightHand.isGlove = false;
            gloveElbow = leftElbow;
        }
        else if (m_rightHand.isGlove == true)
        {
            m_gloveHand = m_rightHand;
            m_leftHand.isGlove = false;
            gloveElbow = rightElbow;
        }
    }
    #endregion
}

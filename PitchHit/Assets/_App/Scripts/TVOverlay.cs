﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TVOverlay : MonoBehaviour
{
    [SerializeField] float textScrollSpeed = 1f;

    [SerializeField] Image ballOneIndicator;
    [SerializeField] Image ballTwoIndicator;
    [SerializeField] Image ballThreeIndicator;
    [SerializeField] Image ballFourIndicator;

    [SerializeField] Image strikeOneIndicator;
    [SerializeField] Image strikeTwoIndicator;
    [SerializeField] Image strikeThreeIndicator;

    [SerializeField] Image outOneIndicator;
    [SerializeField] Image outTwoIndicator;
    [SerializeField] Image outThreeIndicator;

    [SerializeField] RectTransform tickerPanel;
    [SerializeField] Text[] tickerTextObjects;
    Queue<Text> tickerObjectQueue;
    [SerializeField] GameObject tickerTextPrefab;
    Queue<string> tickerWords;
    bool readyToSpawnNewTicker;

    [SerializeField] TextMeshProUGUI teamOneName;
    [SerializeField] TextMeshProUGUI teamTwoName;
    [SerializeField] TextMeshProUGUI teamOneScore;
    [SerializeField] TextMeshProUGUI teamTwoScore;
    int teamOneRuns;
    int teamTwoRuns;

    Dictionary<OffensePlayer, RectTransform> offensePlayers;
    [SerializeField] RectTransform runnersPanel;
    [SerializeField] GameObject runnerHelmetPrefab;
    [SerializeField] float runnerMapScaleFactor;

    [SerializeField] RectTransform batterNamePanel;
    [SerializeField] TextMeshProUGUI batterNameText;

    void Awake()
    {
        readyToSpawnNewTicker = true;
        tickerWords = new Queue<string>();
        tickerObjectQueue = new Queue<Text>(tickerTextObjects);
        teamOneRuns = 0;
        teamTwoRuns = 0;
        offensePlayers = new Dictionary<OffensePlayer, RectTransform>();
    }

    void Start()
    {
        ballOneIndicator.gameObject.SetActive(false);
        ballTwoIndicator.gameObject.SetActive(false);
        ballThreeIndicator.gameObject.SetActive(false);
        ballFourIndicator.gameObject.SetActive(false);
        strikeThreeIndicator.gameObject.SetActive(false);
        strikeTwoIndicator.gameObject.SetActive(false);
        strikeOneIndicator.gameObject.SetActive(false);
        outOneIndicator.gameObject.SetActive(false);
        outTwoIndicator.gameObject.SetActive(false);
        outThreeIndicator.gameObject.SetActive(false);

        teamOneName.text = FieldManager.homeTeam.teamName.Substring(0, 3).ToUpper();
        teamTwoName.text = FieldManager.awayTeam.teamName.Substring(0, 3).ToUpper();
        teamOneScore.text = "0";
        teamTwoScore.text = "0";

        QueueWordsForTicker("THANK YOU FOR JOINING US FOR ANOTHER GREAT DAY OF BASEBALL!  TODAY WE WATCH AS THE LOCAL HOMETOWN HEROES ‘THE FIGHTING FOXES' GO UP AGAINST ‘THE SAN DIEGO SLOTHS’");
        QueueWordsForTicker("TIME 4:32 PM");
        QueueWordsForTicker("WEATHER 88° AND SUNNY");
        QueueWordsForTicker("RETURNING TO THE MOUND PITCHING FOR THE FOXES IN TODAY’S GAME, THE SPECTACULAR JOSE JONES.  EXPECT A FEW INNINGS FULL OF FAST BALLS AND CHANGE UPS FROM JONES WHO HAS A REPUTATION FOR KEEPING THINGS INTERESTING!");
        QueueWordsForTicker("TIME 4:32 PM");
        QueueWordsForTicker("WEATHER 88° AND SUNNY");
        QueueWordsForTicker("TODAY’S GAME IS BROUGHT TO YOU BY OUR SPONSORS AT WILLY’S CHILLY DOGS!  THE ONLY HOTDOG THAT IS COOL AND SPICY HOT!");
        QueueWordsForTicker("SAN 2  PHX 4");
        QueueWordsForTicker("DET 0  BOS 3");
        QueueWordsForTicker("CLE 4  DAL 4");
        QueueWordsForTicker("LA 3  NY 3");
        QueueWordsForTicker("WE THANK YOU AGAIN FOR WATCHING ALL YOUR FAVORITE VIRTUAL REALITY SPORTS AND CONTENT HERE ON THE VIRTUAL REALITY NETWORK, VRN YOUR ONE SOURCE FOR ALL THINGS VIRTUAL!");
        QueueWordsForTicker("BE SURE TO STAY TUNED FOR THE POST-GAME SHOW WITH OUR HOSTS RYAN NOLAN AND PETE DANDELION!");
        QueueWordsForTicker("TIME 4:33 PM");
        QueueWordsForTicker("WEATHER 88° AND SUNNY");
        QueueWordsForTicker("BREAKING NEWS: BALTIMORE BOBCATS PITCHER TODD BONZALEZ HAS BEEN TRADED TO THE DALLAS DINOS, STAY TUNED FOR MORE INFORMATION WITHIN THE HOUR.");
        QueueWordsForTicker("THANK YOU FOR JOINING US FOR ANOTHER GREAT DAY OF BASEBALL!  TODAY WE WATCH AS THE LOCAL HOMETOWN HEROES ‘THE FIGHTING FOXES’ GO UP AGAINST ‘THE SAN DIEGO SLOTHS’");
        QueueWordsForTicker("TIME 4:33 PM");
        QueueWordsForTicker("WEATHER 89° AND SUNNY");
        QueueWordsForTicker("SAN 2  PHX 5");
        QueueWordsForTicker("DET 0  BOS 4");
        QueueWordsForTicker("CLE 4  DAL 4");
        QueueWordsForTicker("LA 5  NY 3");
        QueueWordsForTicker("TODAY’S GAME IS BROUGHT TO YOU BY OUR SPONSORS ROCKET SODA!  STAY COOL THIS SUMMER AND BLAST OFF TO A REFRESHING PLANET OF TASTE WITH ROCKET SODA!");
        QueueWordsForTicker("WE THANK YOU AGAIN FOR WATCHING ALL YOUR FAVORITE VIRTUAL REALITY SPORTS AND CONTENT HERE ON THE VIRTUAL REALITY NETWORK, VRN YOUR ONE SOURCE FOR ALL THINGS VIRTUAL!");
        QueueWordsForTicker("BE SURE TO STAY TUNED FOR THE POST-GAME SHOW WITH OUR HOSTS RYAN NOLAN AND PETE DANDELION!");
        QueueWordsForTicker("SAN 2  PHX 5");
        QueueWordsForTicker("DET 0  BOS 4");
        QueueWordsForTicker("CLE 4  DAL 4");
        QueueWordsForTicker("LA 5  NY 3");
    }

    void OnEnable()
    {
        FieldManager.OnBallsChange += UpdateBallIndicators;
        FieldManager.OnStrikesChange += UpdateStrikeIndicators;
        FieldManager.OnOutsChange += UpdateOutIndicators;
        FieldManager.OnScoreChange += SetRuns;
        FieldManager.OnRegisterRunner += CreateMiniRunner;
        FieldManager.OnUnRegisterRunner += DestroyMiniRunner;
        FieldManager.OnShowBatter += NewBatter;
    }

    void OnDisable()
    {
        FieldManager.OnBallsChange -= UpdateBallIndicators;
        FieldManager.OnStrikesChange -= UpdateStrikeIndicators;
        FieldManager.OnOutsChange -= UpdateOutIndicators;
        FieldManager.OnScoreChange -= SetRuns;
        FieldManager.OnRegisterRunner -= CreateMiniRunner;
        FieldManager.OnUnRegisterRunner -= DestroyMiniRunner;
        FieldManager.OnShowBatter -= NewBatter;
    }

    void Update()
    {
        if (readyToSpawnNewTicker)
        {
            if(tickerWords.Count > 0 && tickerObjectQueue.Count > 0)
            {
                StartCoroutine(ScrollTickerText(tickerObjectQueue.Dequeue(), tickerWords.Dequeue()));
                readyToSpawnNewTicker = false;
            }
        }

        foreach (KeyValuePair<OffensePlayer, RectTransform> player in offensePlayers)
        {
            player.Value.localPosition = new Vector3(player.Key.position.x * runnerMapScaleFactor, player.Key.position.z * runnerMapScaleFactor, 0f);
        }
    }

    void SetRuns(int team, int inning, int runs)
    {
        if (team % 2 == 0)
        {
            //teamOneScore.text = FieldManager.homeTeamRuns.ToString();
        }
        else
        {
            teamTwoScore.text = FieldManager.awayTeamRuns.ToString();
        }
    }

    void UpdateBallIndicators(int balls)
    {
        if (balls >= 4) ballFourIndicator.gameObject.SetActive(true);
        else ballFourIndicator.gameObject.SetActive(false);

        if (balls >= 3) ballThreeIndicator.gameObject.SetActive(true);
        else ballThreeIndicator.gameObject.SetActive(false);

        if (balls >= 2) ballTwoIndicator.gameObject.SetActive(true);
        else ballTwoIndicator.gameObject.SetActive(false);

        if (balls >= 1) ballOneIndicator.gameObject.SetActive(true);
        else ballOneIndicator.gameObject.SetActive(false);
    }

    void UpdateStrikeIndicators(int strikes)
    {
        if (strikes >= 3) strikeThreeIndicator.gameObject.SetActive(true);
        else strikeThreeIndicator.gameObject.SetActive(false);

        if (strikes >= 2) strikeTwoIndicator.gameObject.SetActive(true);
        else strikeTwoIndicator.gameObject.SetActive(false);

        if (strikes >= 1) strikeOneIndicator.gameObject.SetActive(true);
        else strikeOneIndicator.gameObject.SetActive(false);
    }

    void UpdateOutIndicators(int outs)
    {
        if (outs >= 3) outThreeIndicator.gameObject.SetActive(true);
        else outThreeIndicator.gameObject.SetActive(false);

        if (outs >= 2) outTwoIndicator.gameObject.SetActive(true);
        else outTwoIndicator.gameObject.SetActive(false);

        if (outs >= 1) outOneIndicator.gameObject.SetActive(true);
        else outOneIndicator.gameObject.SetActive(false);
    }

    public void QueueWordsForTicker(string text)
    {
        tickerWords.Enqueue(text);
    }

    IEnumerator ScrollTickerText(Text tickerText, string text)
    {
        tickerText.text = ("  -  " + text);
        RectTransform tickerTextTransform = tickerText.gameObject.GetComponent<RectTransform>();
        RectTransform TVOverlayTransform = gameObject.GetComponent<RectTransform>();

        tickerTextTransform.sizeDelta = new Vector2(tickerText.preferredWidth, tickerPanel.sizeDelta.y);
        float startXPos = ((tickerTextTransform.sizeDelta.x / 2f) + (TVOverlayTransform.sizeDelta.x / 2f));
        float endXPos = -((tickerTextTransform.sizeDelta.x / 2f) + (TVOverlayTransform.sizeDelta.x / 2f));
        float newSpawnXPos = (TVOverlayTransform.sizeDelta.x / 2f) - (tickerTextTransform.sizeDelta.x / 2f);
        bool reachedSpawnThreshold = false;
        float timer = 0f;
        float endTime = ((startXPos - endXPos) / 1000f) / textScrollSpeed;
        tickerTextTransform.transform.localPosition = new Vector3(startXPos, -(tickerPanel.rect.height / 2f), 0f);

        while (timer < endTime)
        {
            timer += Time.deltaTime;
            float xPos = Mathf.Lerp(startXPos, endXPos, timer / endTime);
            tickerTextTransform.localPosition = new Vector3(xPos, tickerTextTransform.localPosition.y, tickerTextTransform.localPosition.z);
            if (!reachedSpawnThreshold && xPos <= newSpawnXPos)
            {
                reachedSpawnThreshold = true;
                readyToSpawnNewTicker = true;
            }
            yield return null;
        }
        tickerTextTransform.localPosition = new Vector3(endXPos, tickerTextTransform.localPosition.y - (tickerTextTransform.sizeDelta.y / 2), tickerTextTransform.localPosition.z);
        tickerObjectQueue.Enqueue(tickerText);

        //This is temporary because JJ wanted the text to loop forever.
        QueueWordsForTicker(text);

        yield return null;
    }

    void CreateMiniRunner(OffensePlayer player)
    {
        RectTransform miniTransform = Instantiate(runnerHelmetPrefab, runnersPanel).GetComponent<RectTransform>();
        offensePlayers.Add(player, miniTransform);
    }

    void DestroyMiniRunner(OffensePlayer player)
    {
        if (offensePlayers.ContainsKey(player))
        {
            //Destroy(offensePlayers[player].gameObject);
            StartCoroutine(shrinkRunner(offensePlayers[player]));
            offensePlayers.Remove(player);
        }
    }

    IEnumerator shrinkRunner(RectTransform trans)
    {
        float timer = 0f;
        float timeToComplete = 0.5f;
        Vector2 startsize = trans.sizeDelta;
        Vector2 originalPivot = trans.pivot;

        while (timer < timeToComplete)
        {
            timer += Time.deltaTime;
            trans.sizeDelta = Vector2.Lerp(startsize, Vector2.zero, (timer / timeToComplete));
            float scaleFactor = (trans.sizeDelta.y / startsize.y);
            trans.pivot = new Vector2(0.5f, (originalPivot.y / scaleFactor));
            yield return null;
        }
        trans.sizeDelta = Vector2.zero;
        yield return null;
        Destroy(trans.gameObject);
        yield return null;
    }

    void NewBatter(Transform newBatter, PlayerInfo batterInfo)
    {
        batterNameText.text = batterInfo.fullName + " #" + batterInfo.jerseyNumber;
        StartCoroutine(BatterNameScrollRoutine());
    }

    IEnumerator BatterNameScrollRoutine()
    {
        RectTransform TVOverlayTransform = gameObject.GetComponent<RectTransform>();
        float startXPos = ((batterNamePanel.sizeDelta.x / 2f) + (TVOverlayTransform.sizeDelta.x / 2f));
        float endXPos = -((TVOverlayTransform.sizeDelta.x / 2f) - (batterNamePanel.sizeDelta.x / 2f));
        float timer = 0f;
        batterNamePanel.transform.localPosition = new Vector3(startXPos, batterNamePanel.transform.localPosition.y, batterNamePanel.transform.localPosition.z);

        while(timer < 1f)
        {
            timer += Time.deltaTime;
            float newXPos = Mathf.SmoothStep(startXPos, endXPos, timer);
            batterNamePanel.transform.localPosition = new Vector3(newXPos, batterNamePanel.transform.localPosition.y, batterNamePanel.transform.localPosition.z);
            yield return null;
        }
        batterNamePanel.transform.localPosition = new Vector3(endXPos, batterNamePanel.transform.localPosition.y, batterNamePanel.transform.localPosition.z);
        yield return null;
    }
}

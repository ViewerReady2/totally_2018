﻿//Hikmat
using UnityEngine;
using System.Collections;

public class JuggleCount : MonoBehaviour
{

    public bool beingJuggled;
    public int numOfJuggling = 0;
    private static JuggleCounter counter;
    // Use this for initialization
    void Start()
    {
        if (!counter)
        {
            counter = FindObjectOfType<JuggleCounter>();
        }
    }

    public void OnJuggled()
    {
        if(counter.currentJuggleBall != this)
        {
            beingJuggled = true;
            //this.gameObject.tag = "ball_in_use";
            numOfJuggling = 0;
            counter.BeginJuggling(this);
        }

        numOfJuggling++;
        counter.ShowJuggle();
    }

    private void OnDropped()
    {
        this.gameObject.tag = "ball";
        beingJuggled = false;
        if (counter.currentJuggleBall == this)
        {
            counter.FinalizeJuggle();
        }

        numOfJuggling = 0;
    }

   
    // Update is called once per frame
    void OnCollisionStay(Collision col)
    {
        /*
        if (col.gameObject.tag == "bat")
        {
            numOfJuggling++;
            this.gameObject.tag = "ball_in_use";
            if (numOfJuggling == 1)
            {
                //Invoke("ForObjectInvoke", 0);
                counter.ObjectReference();
            }
            //Invoke("ForShowInvoke", 0);
            counter.ShowJuggle();
        }
        */
        if (beingJuggled)
        {
            if (col.gameObject.tag == "ground" || col.gameObject.tag == "Prop")
            {
                OnDropped();
                //this.gameObject.tag = "ball";
                //Invoke("ForInvoke", 0);
            }
            else
            {
                //Debug.Log("ewll? "+col.gameObject.tag);
            }
        }
    }
   
    //void Update()
    //{

    //}
    //void ForInvoke()
    //{
    //    counter.Calculate();
    //}
    //void ForObjectInvoke()
    //{
    //    counter.ObjectReference();
    //}
    //void ForShowInvoke()
    //{
    //    counter.ShowJuggle();
    //}
}

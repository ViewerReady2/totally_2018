﻿#if REFERENCE
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SideController : MonoBehaviour {

    public static int playerNumber
	{
		get{ return PlayerPrefs.GetInt ("PLAYERNUMBER", 1); }
		set{ PlayerPrefs.SetInt ("PLAYERNUMBER", value);}
	}
    public Text leftText;
    public Text rightText;
    public Toggle useLocalServerToggle;
    public Text statusText;
    public Text currentIPText;
    public InputField inputIPAddress;
    public InputField inputPort;
    public InputField inputRatio;
    public static float scaleRatio
    {
        get { return PlayerPrefs.GetFloat("SCALERATIO", 0.417f); }
        set { PlayerPrefs.SetFloat("SCALERATIO", value); }
    }


    private SceneChanger sceneChanger;

    void Start()
    {
        if(playerNumber==1)
        {
            rightText.color = Color.gray;
            leftText.color = Color.white;
        }
        else
        {
            if(playerNumber==2)
            {
                leftText.color = Color.gray;
                rightText.color = Color.white;
            }
        }

        currentIPText.text = "Current IP: " + Network.player.ipAddress;

        inputIPAddress.text = MultiplayerManager.localIPAddress;
        inputPort.text      = MultiplayerManager.localServerPort.ToString();
        inputRatio.text     = scaleRatio.ToString();
    }

    void Update()
    {
        scaleRatio = float.Parse(inputRatio.text);
        statusText.text = "Status: " + (PhotonNetwork.connected? "Connected" : "No Connection") + (MultiplayerManager.useLocalServer? " via LAN" : " via Cloud");
    }


    public void leftButtonClicked()
    {
        playerNumber = 1;
        rightText.color = Color.gray;
        leftText.color = Color.white;

        SoundController.instance.SetToPlayerTwoMode(false);

        sceneChanger = FindObjectOfType<SceneChanger>();
    }

    public void rightButtonClicked()
    {
        playerNumber = 2;
        leftText.color = Color.gray;
        rightText.color = Color.white;
        SoundController.instance.SetToPlayerTwoMode(true);

        sceneChanger = FindObjectOfType<SceneChanger>();
    }

    public void SaveAndRestart()
    {
        MultiplayerManager.useLocalServer = useLocalServerToggle.isOn;
        MultiplayerManager.localIPAddress = inputIPAddress.text;
        int port;
        if (int.TryParse(inputPort.text, out port))
        {
            MultiplayerManager.localServerPort = port;
        }

        if (sceneChanger)
        {
            sceneChanger.ReloadScene();
        }
    }

}
#endif
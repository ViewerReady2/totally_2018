﻿#if REFERENCE
using UnityEngine;
using System.Collections;

public class HoopTracker : Photon.MonoBehaviour, IPunObservable {
	public short currentScore;
	public MultiplayerManager theMultiplayerManager;
	public bool isMine;
	public ScoreKeeper theScoreKeeper;
	public Collider[] scoringColliders;

	// Use this for initialization
	void Start () {
		isMine = GetComponent<PhotonView> ().isMine;
        theMultiplayerManager = FindObjectOfType<MultiplayerManager>();

		if (!isMine) {
			foreach (Collider c in scoringColliders) {
				c.enabled = false;
			}
		}
	}

	void Update()
	{
		if (theScoreKeeper && theScoreKeeper.Score!=currentScore) {
			currentScore = (short)theScoreKeeper.Score;
			//if (PhotonNetwork.playerList.Length<2) {
			//	theMultiplayerManager.OnScoreUpdated (this);
			//}
		}
	}

	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting) {
			//Debug.Log ("+++++++++++++hoop tracker is writing serialization at time:"+Time.time+" with score "+currentScore);
			stream.SendNext(currentScore);
		} else {
			currentScore = (short)stream.ReceiveNext ();
			//Debug.Log ("-------------hoop tracker is reading serialization at time:"+Time.time+" with score "+currentScore);
			if (theScoreKeeper) {
				theScoreKeeper.SetScore (currentScore);
			}
		}
		if (theMultiplayerManager) {
			theMultiplayerManager.OnScoreUpdated (this);
		}
	}
}
#endif
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraDualRender : MonoBehaviour {

    Camera cam;
    [SerializeField] RenderTexture targetTexture;
    [SerializeField] bool writeToScreen;

    void Start()
    {
        cam = GetComponent<Camera>();
    }

    void OnPreRender()
    {
        //cam.targetTexture = targetTexture;
        cam.targetTexture = targetTexture;

        RenderTexture.active = targetTexture;
        cam.Render();
        //screenTexture.ReadPixels(new Rect(0, 0, width, height), 0, 0);
        //screenTexture.Apply(false);

        
    }

    void OnPostRender()
    {
        /*if (writeToScreen)
        {
            cam.targetTexture = null;
            //Graphics.Blit(targetTexture, null as RenderTexture);
            cam.Render();
        }*/
        RenderTexture.active = null;
        cam.targetTexture = null;

    }
}

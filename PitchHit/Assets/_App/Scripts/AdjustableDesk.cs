﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

[RequireComponent(typeof(BoxCollider))]
[RequireComponent(typeof(Interactable))]
public class AdjustableDesk : MonoBehaviour
{
    private bool beingHeldSteamVR;
    private bool beingHeldOculus;
    private Hand controllingHand;
    private Grabber controllingGrabber;
    private Vector3 originalPosition;
    private float previousHandPositionY;

    void Update()
    {
        if (beingHeldSteamVR)
        {
            float yDifference = previousHandPositionY - controllingHand.transform.position.y;
            transform.position = new Vector3(originalPosition.x, transform.position.y - yDifference, originalPosition.z);
            previousHandPositionY = controllingHand.transform.position.y;
            if (!controllingHand.GetStandardInteractionButton() && ((controllingHand.controller != null) && !controllingHand.controller.GetPress(Valve.VR.EVRButtonId.k_EButton_Grip)))
            {
                beingHeldSteamVR = false;
                controllingHand.ShowHand();
            }
        }
        else if (beingHeldOculus)
        {
            float yDifference = previousHandPositionY - controllingGrabber.transform.position.y;
            transform.position = new Vector3(originalPosition.x, transform.position.y - yDifference, originalPosition.z);
            previousHandPositionY = controllingGrabber.transform.position.y;
        }
    }

    //-------------------------------------------------
    // Called every Update() while a Hand is hovering over this object
    //-------------------------------------------------
    private void HandHoverUpdate(Hand hand)
    {
        if (hand.GetStandardInteractionButtonDown() || ((hand.controller != null) && hand.controller.GetPressDown(Valve.VR.EVRButtonId.k_EButton_Grip)))
        {
            if (!beingHeldSteamVR)
            {
                beingHeldSteamVR = true;
                controllingHand = hand;
                controllingHand.HideHand();
                previousHandPositionY = controllingHand.transform.position.y;
                originalPosition = transform.position;
            }
        }
    }

    public void CustomGrabbableGrabBegin(Grabber grabber)
    {
        if (!beingHeldOculus)
        {
            beingHeldOculus = true;
            controllingGrabber = grabber;
            //controllingGrabber.HideHand();
            previousHandPositionY = controllingGrabber.transform.position.y;
            originalPosition = transform.position;
        }
    }

    public void CustomGrabbableGrabEnd(Grabber grabber)
    {
        beingHeldOculus = false;
    }
}

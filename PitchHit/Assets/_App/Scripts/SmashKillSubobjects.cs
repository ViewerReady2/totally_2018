﻿using UnityEngine;
using System.Collections;

/**
 * This script only exists to destroy child objects
 * when SmashKill() is sent to its game object.
 * Created to clean up exhaust and audio from bulbs.
 **/
public class SmashKillSubobjects : MonoBehaviour {

    public string[] ObjectsToKill;
    bool killed = false;

	public void SmashKill()
    {
        if (!killed)
        {
            int killCount = 0;
            foreach (string name in ObjectsToKill)
            {
                Destroy(transform.Find(name).gameObject);
                killCount++;
            }
            killed = true;
            Debug.Log("Killed " + killCount + " subobjects.");
        }
    }
}

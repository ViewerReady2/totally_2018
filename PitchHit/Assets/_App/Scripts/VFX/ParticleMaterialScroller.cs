﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Reflection;

[RequireComponent(typeof(ParticleSystemRenderer))]
public class ParticleMaterialScroller : MonoBehaviour
{

    private ParticleSystemRenderer rend;
    public float scrollSpeedX;
    public float scrollSpeedY;


    void Awake()
    {
        rend = GetComponent<ParticleSystemRenderer>();
    }


    void Update()
    {

        float offsetX = (Time.time * scrollSpeedX) % 1f;
        float offsetY = (Time.time * scrollSpeedY) % 1f;
        rend.material.mainTextureOffset = new Vector2(-offsetX, -offsetY);

    }



}


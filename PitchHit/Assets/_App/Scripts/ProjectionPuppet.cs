﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectionPuppet : FieldPlayerBodyControl {


    public Transform ringTransform;
    Transform cam;

    void Start()
    {
       cam = Camera.main.transform;

    }

    void OnEnable()
    {
        acceptDeviceInput = true;

    }

    void OnDisable()
    {
        acceptDeviceInput = false;
    }

    public override void LockFeetAtCenter(bool locked)
    {
        base.LockFeetAtCenter(locked);
        if (lockFeet)
        {
            ringTransform.localPosition = Vector3.zero;

        }
    }

    protected override void MatchPositionToFollow()
    {

        if (!lockFeet)
        {
            Vector3 camGround = cam.localPosition; camGround.y = 0;
            ringTransform.localPosition = camGround;
            headRoot.transform.localPosition = cam.localPosition;
        }
        else
        {
            Vector3 camHover = cam.localPosition; camHover.x = 0; camHover.z = 0;
            headRoot.transform.localPosition = camHover;
        }


        headRoot.transform.localRotation = cam.localRotation;

    }

    protected override void MatchHandsToControllers()
    {

        if (HandManager.currentLeftCustomHand)
        {
            leftHand = (HandManager.dominantHand == HandManager.HandType.right ? HandManager.currentLeftCustomHand : HandManager.currentRightCustomHand);
            Vector3 tempOffset = HandManager.dominantHand == HandManager.HandType.right ? leftHandOffset : rightHandOffset;

            if (lockFeet)
            {
                leftHandRoot.position = headRoot.TransformPoint(cam.InverseTransformPoint(leftHand.transform.position));
                leftHandRoot.rotation = leftHand.transform.rotation;
            }
            else
            {
                leftHandRoot.localPosition = leftHand.transform.localPosition;
                leftHandRoot.localRotation = leftHand.transform.localRotation;
            }
        }
        if (HandManager.currentRightCustomHand)
        {
            rightHand = (HandManager.dominantHand == HandManager.HandType.right ? HandManager.currentRightCustomHand : HandManager.currentLeftCustomHand);
            Vector3 tempOffset = HandManager.dominantHand == HandManager.HandType.right ? rightHandOffset : leftHandOffset;
            if (lockFeet)
            {
                rightHandRoot.position = headRoot.TransformPoint(cam.InverseTransformPoint(rightHand.transform.position));
                rightHandRoot.rotation = rightHand.transform.rotation;
            }
            else
            {
                rightHandRoot.localPosition = rightHand.transform.localPosition;
                rightHandRoot.localRotation = rightHand.transform.localRotation;
            }

        }

    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableInBuilds : MonoBehaviour {

	// Use this for initialization
	void Awake () {
        if (!Application.isEditor)
        {
            GameObject.Destroy(this.gameObject);
        }
	}
	
}

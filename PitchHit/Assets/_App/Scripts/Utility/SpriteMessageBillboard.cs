﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[ExecuteInEditMode]
public class SpriteMessageBillboard : MonoBehaviour {
    
    public bool flip_x = false;
    public bool lock_z = true;
    private Vector3 initialPosition;
    private Vector3 floorPosition;
    private Animator anim;

    void OnEnable()
    {
        anim = GetComponent<Animator>();
        SetPosition(transform.position);
    }

    public void SetPosition (Vector3 pos)
    {
        transform.position = pos;

        initialPosition = transform.position;

        floorPosition = initialPosition;
        floorPosition.y = .1f;
    }

    public void SetAnimationTrigger(string triggerName)
    {
        if(anim)
        {
            anim.SetTrigger(triggerName);
        }
    }

    void LateUpdate () {

        if (Camera.main != null)
        {
            if (Vector3.Distance(Camera.main.transform.position, transform.position) < 2f)
            {
                Debug.DrawLine(floorPosition, initialPosition, Color.blue);
                transform.position = floorPosition;
                transform.LookAt(floorPosition-Vector3.up, floorPosition - Camera.main.transform.position);
            }
            else
            {
                transform.position = initialPosition;

                Vector3 lookPos = Camera.main.transform.position;

                this.transform.LookAt(lookPos, lock_z ? Vector3.up : Camera.current.transform.up);
                if (flip_x) this.transform.Rotate(Vector3.up * 180, Space.Self);
            }
        }
	}
}

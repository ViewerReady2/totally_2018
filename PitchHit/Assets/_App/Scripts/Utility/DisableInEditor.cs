﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableInEditor : MonoBehaviour {

	// Use this for initialization
	void Start () {
        if (Application.isEditor)
        {
            GameObject.Destroy(this.gameObject);
        }
	}
	
}

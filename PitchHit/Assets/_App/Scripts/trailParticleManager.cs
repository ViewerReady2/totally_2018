﻿using UnityEngine;
using System.Collections;

public class trailParticleManager : MonoBehaviour {

    public ParticleSystem Glow;
    private Rigidbody RB;
    private hittable ball;
    private float minSpeedSqrd = 20f;

	// Use this for initialization
	void Start ()
    {
        ball = GetComponent<hittable>();
        Glow.GetComponent<ParticleSystem>();
        RB = GetComponent<Rigidbody>();
	}
	

    public void OnHit()
    {
        if(!RB)
        {
            RB = GetComponent<Rigidbody>();
        }

        Glow.gameObject.SetActive(false);
        Glow.Clear();
        StopAllCoroutines();
        StartCoroutine(TrailRoutine());
    }


	// Update is called once per frame
	IEnumerator TrailRoutine ()
    {
        float timePassed = .1f;

        while(timePassed>0 && RB.velocity.sqrMagnitude<minSpeedSqrd)
        {
            yield return null;
            timePassed -= Time.deltaTime;
        }

        if (timePassed <= 0)
        {
            yield break;
        }

        if (RB.velocity.sqrMagnitude > minSpeedSqrd)
        {
            Glow.gameObject.SetActive(true);
            var em = Glow.emission;
            em.enabled = true;


            while (Glow.emission.enabled)
            {
                if (ball)
                {
                    if (ball.isHeld)
                    {
//                        Debug.Log("traj is caught");
                        em.rate = Mathf.Lerp(em.rate.constantMax, 0f, (3f * Time.deltaTime));
                        //if (em.rate.constantMax <= .01f)
                        {
                            StartCoroutine(EndGlow());
                            break;
                        }
                    }
                    else if (ball.hitGround)
                    {
                        //Destroy(gameObject);
                        //Glow.startLifetime = 3f;
                        StartCoroutine(EndGlow());
                        break;
                    }
                }

                yield return null;
            }
        }
    }

   /* void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "ground" && HasBeenLaunched == true)
        {
            Glow.startLifetime = 3f;
            StartCoroutine(EndGlow());
        }
    }*/

    IEnumerator EndGlow()
    {
        //yield return new WaitForSeconds(.5f);
        var em = Glow.emission;
        em.enabled = false;
//        Debug.Log("end glow");
        yield return null;
    }

    public void StopEmission()
    {
        var em = Glow.emission;
        em.enabled = false;
    }
}

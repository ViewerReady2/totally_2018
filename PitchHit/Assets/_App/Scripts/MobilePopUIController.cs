﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobilePopUIController : MonoBehaviour {
    public string lockerRoomSceneName = "FULL_LOCKERS";
    public RealButton musicButton;
    private GameController gameController;
    private SceneChanger sceneChanger;
    private ballController balls;

	// Use this for initialization
	void Start () {
        gameController = FindObjectOfType<GameController>();

        balls = gameController.GetComponent<ballController>();
        sceneChanger = gameController.GetComponent<SceneChanger>();

        if (musicButton)
        {
            musicButton.SetActivation(GameController.musicOn);
            if (GameController.musicOn)
            {
                musicButton.RefreshColor();
            }
        }
    }
	
	public void OnTeeButtonActivated()
    {
        if(balls)
        {
            balls.SetServiceToTee();
        }
    }

    public void OnPitcherButtonActivated()
    {
        if(balls)
        {
            balls.SetServiceToPitcher();
        }
    }

    public void OnRestartButtonActivated()
    {
        if(sceneChanger)
        {
            sceneChanger.ReloadScene();
        }
    }

    public void OnExitButtonActivated()
    {
        if(sceneChanger)
        {
            sceneChanger.GoToScene(lockerRoomSceneName);
        }
    }

    public void OnMusicButton(bool activated)
    {
        if(gameController)
        {
            gameController.SetMusicActive(activated);
        }
    }
}

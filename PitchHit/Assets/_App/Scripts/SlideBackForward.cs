﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlideBackForward : MonoBehaviour {
    [HideInInspector]
    public Vector3 initialLocalPosition;
    private float slideDistance = .3f;
    private float slideSpeed = 1.2f;
    private bool shouldStartBack;
    private bool initialPositionSet;
	// Use this for initialization
	void Awake () {
        if (!initialPositionSet)
        {
            initialLocalPosition = transform.localPosition;
        }
        if(shouldStartBack)
        {
            transform.localPosition = initialLocalPosition + (Vector3.up * slideDistance);
        }
	}

    void OnDisable()
    {

    }

    public void SnapBack()
    {
        if(!initialPositionSet)
        {
            initialLocalPosition = transform.localPosition;
            initialPositionSet = true;
        }

        transform.localPosition = initialLocalPosition + (Vector3.up * slideDistance);

    }

    public void BeginSlideBack()
    {
        if(gameObject.activeInHierarchy)
        {
            StartCoroutine(SlideBack());
        }
        else
        {
            shouldStartBack = true;
        }
    }

    public void BeginSlideForward()
    {
        if (gameObject.activeInHierarchy)
        {
            StartCoroutine(SlideForward());
        }
        else
        {
            shouldStartBack = false;
        }
    }

    IEnumerator SlideBack()
    {
        float distanceSlid = 0f;
        while (distanceSlid < slideDistance)
        {
            float thisMuch = slideSpeed * Time.deltaTime;
            if (distanceSlid + thisMuch > slideDistance)
            {
                thisMuch = slideDistance - distanceSlid;
                distanceSlid = slideDistance;
            }
            else
            {
                distanceSlid += thisMuch;
            }
            transform.localPosition += Vector3.up * thisMuch;

            yield return null;
        }

        transform.localPosition = initialLocalPosition + (Vector3.up * slideDistance);
    }

    IEnumerator SlideForward()
    {
        transform.localPosition = initialLocalPosition + (Vector3.up * slideDistance);

        float distanceSlid = 0f;
        while (distanceSlid < slideDistance)
        {
            float thisMuch = slideSpeed * Time.deltaTime;
            if (distanceSlid + thisMuch > slideDistance)
            {
                thisMuch = slideDistance - distanceSlid;
                distanceSlid = slideDistance;
            }
            else
            {
                distanceSlid += thisMuch;
            }
            transform.localPosition -= Vector3.up * thisMuch;

            yield return null;
        }

        transform.localPosition = initialLocalPosition;
    }

}

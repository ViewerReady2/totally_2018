﻿using UnityEngine;
using System.Collections;

public class randomSoundEffect : MonoBehaviour {

	public AudioClip[] clipsToPlay;
    public DestroyAfterSound soundPrefab;
	//private AudioSource source;

	public void playClipAt(Vector3 position){
		//if (!source.isPlaying) {
			//transform.position = position;
        soundPrefab.AssignSound(clipsToPlay[Random.Range(0, clipsToPlay.Length)]);
        Instantiate(soundPrefab.gameObject, position, Quaternion.identity);
			//source.clip = clipsToPlay [Random.Range (0, clipsToPlay.Length)];
			//source.Play ();
		//}

	}

	// Use this for initialization
	//void Start () {
	//	source = GetComponent<AudioSource> ();
	//}
	
	// Update is called once per frame
	//void Update () {
	
	//}
}

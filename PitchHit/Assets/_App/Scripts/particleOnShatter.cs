﻿using UnityEngine;
using System.Collections;

public class particleOnShatter : MonoBehaviour {

	public ParticleSystem particleEffect;

	//ShatterToolkit.ShatterTool shatterTool;

	// Use this for initialization
	//void Awake() {
		//shatterTool = GetComponent<ShatterToolkit.ShatterTool> ();
		//prewarm the particles
	//}

	//public void Shatter(Vector3 point){
		//if (!particleEffect.isPlaying) {
		//	particleEffect.transform.position = point;
		//}
	//}

	public void PostSplit(){
		if (!particleEffect.isPlaying) {
			particleEffect.Play ();
		}
	}

	// Update is called once per frame
	//void Update () {
	
	//}
}

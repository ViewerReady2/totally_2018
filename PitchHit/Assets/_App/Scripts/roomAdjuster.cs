﻿using UnityEngine;
using System.Collections;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class roomAdjuster : MonoBehaviour {

	// Use this for initialization

	HmdQuad_t playBounds = new HmdQuad_t();
    public Transform theBucketToSwitchSides;
	public Transform[] wallContainers = new Transform[4];
    //public Player thePlayer;
    private TButt.TBCameraRig playerRig;
    public bool offsetWhenRoomSmall;
    public float tinyAreaWidth = 2.5f;
    public bool shouldAdjustFrontCanvas;
    public bool shouldAdjustBackCanvas;
    public bool shouldAdjustRightCanvas;
    public bool shouldAdjustLeftCanvas;
    public bool shouldFlipWallsForOculus;
    public bool shouldFlipWallsForSteamVR;
	//public int anchorOnWall = -1;

	public float minWidth = 0.5f;

	private float x = 0;
	private float z = 0;

	void Start () {
        playerRig = FindObjectOfType<TButt.TBCameraRig>();

#if RIFT
        Debug.Log("ovr boundary "+OVRManager.boundary);
            Vector3 oculusWalls = OVRManager.boundary.GetDimensions(OVRBoundary.BoundaryType.PlayArea);
            x = oculusWalls.x * (shouldFlipWallsForOculus ? -1f : 1f);
            z = oculusWalls.z * (shouldFlipWallsForOculus ? -1f : 1f);

            //x = (Mathf.Abs(x) - .1f) * Mathf.Sign(x);
            //z = (Mathf.Abs(z) - .1f) * Mathf.Sign(z);
            x /= 2f;
            z /= 2f;


            //Debug.Log("+++++++++++++++++++++++++++++++ ovr boundary "+oculusWalls);
#elif STEAM_VR
            //Debug.Log("room adjuster is not Oculus");
            SteamVR_PlayArea.GetBounds(SteamVR_PlayArea.Size.Calibrated, ref playBounds);

            //Debug.Log("playBounds "+playBounds);

            if (playBounds.vCorners0.v0 > x)
            {
                x = playBounds.vCorners0.v0;
            }
            if (playBounds.vCorners0.v2 > z)
            {
                z = playBounds.vCorners0.v2;
            }

            if (playBounds.vCorners1.v0 > x)
            {
                x = playBounds.vCorners1.v0;
            }
            if (playBounds.vCorners1.v2 > z)
            {
                z = playBounds.vCorners1.v2;
            }

            if (playBounds.vCorners2.v0 > x)
            {
                x = playBounds.vCorners2.v0;
            }
            if (playBounds.vCorners2.v2 > z)
            {
                z = playBounds.vCorners2.v2;
            }

            if (playBounds.vCorners3.v0 > x)
            {
                x = playBounds.vCorners3.v0;
            }
            if (playBounds.vCorners3.v2 > z)
            {
                z = playBounds.vCorners3.v2;
            }
#elif GEAR_VR
        return;
        //x = 2f;
        //z = 2f;
#endif

        if (Mathf.Abs(x) < minWidth || Mathf.Abs(z) < minWidth) {
			//Debug.Log ("Play area is not yet ready!");

			StartCoroutine (sizeWallsWhenReady ());

		} else {
            //Debug.Log("arrange things with x"+x+"   z"+z);
			ArrangeThings ();
		}
	
	}

	public IEnumerator sizeWallsWhenReady(){
        Debug.Log("+++++++++++++++++++++++size walls when ready.");
		//float x = 0;
		//float z = 0;

		while (x < minWidth || z < minWidth)
        {

			Debug.LogWarning (x+" "+z+" Play Area still not ready... Make sure you have tracking. Waiting for 2 seconds"); //TODO: Display this on primary camera
				
			yield return new WaitForSeconds (2);

            if (GameController.isTrueOculus)
            {
                Vector3 oculusWalls = OVRManager.boundary.GetDimensions(OVRBoundary.BoundaryType.OuterBoundary);
               // foreach (Vector3 v in oculusWalls)
                //{
                    //Debug.Log("oculus walls "+ oculusWalls);
                    x = Mathf.Max(x, Mathf.Abs(oculusWalls.x));
                    z = Mathf.Max(x, Mathf.Abs(oculusWalls.z));
                //}
            }
            else
            {
                if (playBounds.vCorners0.v0 > x)
                {
                    x = playBounds.vCorners0.v0;
                }
                if (playBounds.vCorners0.v2 > z)
                {
                    z = playBounds.vCorners0.v2;
                }
                if (playBounds.vCorners1.v0 > x)
                {
                    x = playBounds.vCorners1.v0;
                }
                if (playBounds.vCorners1.v2 > z)
                {
                    z = playBounds.vCorners1.v2;
                }
                if (playBounds.vCorners2.v0 > x)
                {
                    x = playBounds.vCorners2.v0;
                }
                if (playBounds.vCorners2.v2 > z)
                {
                    z = playBounds.vCorners2.v2;
                }
                if (playBounds.vCorners3.v0 > x)
                {
                    x = playBounds.vCorners3.v0;
                }
                if (playBounds.vCorners3.v2 > z)
                {
                    z = playBounds.vCorners3.v2;
                }
            }
		}
			

		ArrangeThings ();

		for (int i = 0; i < wallContainers.Length; i++) {
			wallContainers [i].gameObject.SetActive (true);
		}

	}


	void ArrangeThings(){
        //goes front-back-right-left
        if(theBucketToSwitchSides && HandManager.dominantHand== HandManager.HandType.left)
        {
            Vector3 oldLocalPos = theBucketToSwitchSides.localPosition;
            Quaternion oldLocalRot = theBucketToSwitchSides.localRotation;
            if (theBucketToSwitchSides.parent == wallContainers[2])
            {
                theBucketToSwitchSides.SetParent(wallContainers[3]);
            }
            else if (theBucketToSwitchSides.parent == wallContainers[3])
            {
                theBucketToSwitchSides.SetParent(wallContainers[2]);
            }


            theBucketToSwitchSides.localPosition = new Vector3(oldLocalPos.x*-1f, oldLocalPos.y,oldLocalPos.z);
            theBucketToSwitchSides.localRotation = oldLocalRot;
        }

        //Vector3 newPosition;

#if STEAM_VR
        if(shouldFlipWallsForSteamVR)
        {
            x *= -1f;
            z *= -1f;
        }
#endif
        Debug.Log ("z "+z);
		Debug.Log ("x "+x);

        //front
		wallContainers [0].localPosition = new Vector3(wallContainers[0].localPosition.x, wallContainers[0].localPosition.y, z);
        if (shouldAdjustFrontCanvas)
        {
            CheckForCanvasToAdjust(wallContainers[0].GetComponentInChildren<Canvas>(), x * 2f);
        }
        //back
        wallContainers [1].localPosition = new Vector3(wallContainers [1].localPosition.x, wallContainers [1].localPosition.y, -z);

        //right
        wallContainers [2].localPosition = new Vector3(x, wallContainers [2].localPosition.y, wallContainers [2].localPosition.z);
        if (shouldAdjustRightCanvas)
        {
            CheckForCanvasToAdjust(wallContainers[2].GetComponentInChildren<Canvas>(), z * 2f);
        }

        //left
        wallContainers [3].localPosition = new Vector3(-x, wallContainers [3].localPosition.y, wallContainers [3].localPosition.z);
        if (shouldAdjustLeftCanvas)
        {
            CheckForCanvasToAdjust(wallContainers[3].GetComponentInChildren<Canvas>(), z * 2f);
        }


        if (offsetWhenRoomSmall)
        {
            Debug.Log("----------------------------------------adjust for small room? "+x+"  "+z);
            float width = x;
            /*
            if (GameController.isTrueOculus)
            {
                Vector3 oculusWalls = OVRManager.boundary.GetDimensions(OVRBoundary.BoundaryType.PlayArea);
                width = Mathf.Max(width, Mathf.Abs(oculusWalls.x));
            }
            else
            {
                if (SteamVR_PlayArea.GetBounds(SteamVR_PlayArea.Size.Calibrated, ref playBounds)) 
                {
                    width = playBounds.vCorners0.v0;
                    width = Mathf.Max(width, playBounds.vCorners1.v0);
                    width = Mathf.Max(width, playBounds.vCorners2.v0);
                    width = Mathf.Max(width, playBounds.vCorners3.v0);
                }
            }
            */
            if (width != 0)
            {
                if (width < tinyAreaWidth / 2f)
                {
                    Debug.Log("yes it's too small.");
                    if (HandManager.dominantHand == HandManager.HandType.right)
                    {
                        transform.transform.position -= (transform.right * width * .25f);
                        playerRig.transform.position -= (transform.right * width * .25f);
                    }
                    else
                    {
                        if (HandManager.dominantHand == HandManager.HandType.left)
                        {
                            transform.transform.position += (transform.right * width * .25f);
                            playerRig.transform.position += (transform.right * width * .25f);
                        }
                    }
                }
            }
        }
    }

    public void Disappear()
    {
        transform.localScale = Vector3.zero;
        Destroy(gameObject, 3f);
    }

    private void CheckForCanvasToAdjust(Canvas c, float width)
    {
        if (c)
        {
            RectTransform rectTran = c.GetComponent<RectTransform>();
            float goodElevation = rectTran.rect.height * c.transform.lossyScale.y * .5f;
            c.transform.localPosition = new Vector3(c.transform.localPosition.x,goodElevation, c.transform.localPosition.z);
            rectTran.sizeDelta = new Vector2(width/rectTran.lossyScale.x, rectTran.sizeDelta.y);
        }
    }
	
	// Update is called once per frame
	//void Update () {
	
	//}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
[ExecuteInEditMode]
#endif
public class LockOrientation : MonoBehaviour {
    Quaternion initialLocalRotation;
    void Start()
    {
        initialLocalRotation = transform.localRotation;
    }

    void Update()
    {
        transform.localRotation = initialLocalRotation;
    }

    void LateUpdate()
    {
        Vector3 lookDirection = transform.forward;
        lookDirection.y = 0;
        transform.rotation = Quaternion.LookRotation(lookDirection);
        //transform.rotation = Quaternion.FromToRotation(Vector3.up, transform.up);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAnimation : MonoBehaviour {
    private Animator anim;
    public string triggerName;
	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
        anim.SetTrigger(triggerName);
	}
	
}

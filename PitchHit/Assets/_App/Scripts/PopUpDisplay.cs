﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopUpDisplay : MonoBehaviour {
    public Text text;

    private bool appearing = true;
    private float localStartElevation = -25f;
    private float localFinishElevation = -25f;
    private float pauseTime = 2f;
    private float movementSpeed = 35f;

    private bool finishedStaring;
    private Vector3 initialLocalPosition;
    private Vector3 finishingPosition;
    private Transform playerHead;

	// Use this for initialization
	void Start () {
        Debug.Log("made pop up text.");
        initialLocalPosition = transform.localPosition;
        finishingPosition = transform.localPosition + (Vector3.up*localFinishElevation);
        transform.localPosition += (Vector3.up * localStartElevation);

#if RIFT
        playerHead = Camera.main.transform;
#elif STEAM_VR
            playerHead = Valve.VR.InteractionSystem.Player.instance.hmdTransform;
#endif
    }

    // Update is called once per frame
    void Update () {
        if (appearing)
        {
            transform.localPosition = Vector3.MoveTowards(transform.localPosition,initialLocalPosition, movementSpeed * Time.deltaTime);
            //Debug.Log("appearing " + transform.localPosition + " " + initialLocalPosition);

            if (transform.localPosition == initialLocalPosition)
            {
                appearing = false;
            }
        }
        else
        {
            if(pauseTime>0)
            {
                pauseTime -= Time.deltaTime;
            }
            else
            {
                transform.localPosition = Vector3.MoveTowards(transform.localPosition, finishingPosition, movementSpeed * Time.deltaTime);
                if(transform.localPosition == finishingPosition)
                {
                    Destroy(gameObject,.1f);
                }
            }
        }

        transform.LookAt(playerHead, Vector3.up);
    }

    public void SetText(string words)
    {
        text.text = words;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AppearingMenu : MonoBehaviour {
    public static bool isVisible;

    public RealButton teeRealButton;
    public RealButton pitcherRealButton;
    public RealButton selfRealButton;
    public RealButton musicRealButton;
    public RealButton refillRealButton;
    public GameObject fullBatterMenu;
    public AudioSource soundMaker;
    public AudioClip challengePassedSound;
    public Text whichHandText;
    public Text locomotionTypeText;
    public Text throwingStyleText;
    public bool isAlwaysAvailable;

    protected DebugPanel debugPanel;

    [SerializeField]
    float menuDistance = 1f;
    Camera cam;
    float defaultY;
    //[SerializeField]
    //RealButton[] realButtons;
    [SerializeField]
    ButtonPointer buttonPointerPrefab;
    [SerializeField]
    ButtonPointer buttonPointer;
    SceneChanger sceneChanger;
    GameController gameController;
    public string lockerSceneName = "FULL_LOCKERS";
    public string fullSceneName = "AI_LAB";

    private string rightHandedWord = "Righty";
    private string leftHandedWord = "Lefty";

    //public MenuButton refillButton;
    public enum State
    {
        Invisible,
        Appearing,
        Visible
    }
    State _state = State.Invisible;
    public State state
    {
        get
        { return _state; }
    }

    // Use this for initialization
    void Start() {
        debugPanel = GetComponentInChildren<DebugPanel>();

        if (fullBatterMenu && (!GameController.appearingMenuMode || GameController.fullBatterMode))
        {
            if (fullBatterMenu)
            {
                fullBatterMenu.gameObject.SetActive(true);
            }
            Destroy(gameObject);
            //if (leaderboardManager)
            //{
            //    Destroy(leaderboardManager.gameObject);
            //}

            return;
        }

        if (fullBatterMenu)
        {
            fullBatterMenu.gameObject.SetActive(false);
        }

        if (whichHandText)
        {
            if (HandManager.dominantHand == HandManager.HandType.right)
            {
                whichHandText.text = rightHandedWord;
            }
            else if (HandManager.dominantHand == HandManager.HandType.left)
            {
                whichHandText.text = leftHandedWord;
            }
            else
            {
                whichHandText.text = "Ambidextrous";
            }
        }

        gameController = FindObjectOfType<GameController>();
        sceneChanger = gameController.GetComponent<SceneChanger>();
        //ballController.instance.ballsMenuButton = refillButton; 
        ballController.instance.ballsButton = refillRealButton;

        defaultY = transform.position.y;
        transform.position = new Vector3(0, -10000, 0);
        cam = Camera.main;

        switch (ballController.instance.currentServiceType)
        {
            case ballController.ballServiceType.pitcher:
                if (pitcherRealButton)
                {
                    pitcherRealButton.isActivated = true;
                    pitcherRealButton.RefreshColor();
                }
                break;
            case ballController.ballServiceType.self:
                if (selfRealButton)
                {
                    selfRealButton.isActivated = true;
                    selfRealButton.RefreshColor();
                }
                break;
            case ballController.ballServiceType.tee:
                if (teeRealButton)
                {
                    teeRealButton.isActivated = true;
                    teeRealButton.RefreshColor();
                }
                break;
        }

        if (musicRealButton && GameController.musicOn)
        {
            musicRealButton.isActivated = true;
            musicRealButton.RefreshColor();
        }
    }

    // Update is called once per frame
    void Update() {
        if (GameController.isAtBat || isAlwaysAvailable)
        {
#if RIFT
            if (HandManager.currentLeftCustomHand && HandManager.currentLeftCustomHand.GetMenuButtonDown())
#else
            if ((HandManager.currentLeftCustomHand && HandManager.currentLeftCustomHand.GetMenuButtonDown()) || (HandManager.currentRightCustomHand && HandManager.currentRightCustomHand.GetMenuButtonDown()))
#endif
            {
                if (_state == State.Invisible)
                {
                    TurnOn();
                }
                else
                {
                    TurnOff();
                }

            }
        }
    }

    public LeaderboardManager leaderboardManager;
    void TurnOn()
    {
        isVisible = true;
        //        Debug.Log("appearing menu turn on");
        //if (!leaderboardManager)
        //{
        //    leaderboardManager = FindObjectOfType<LeaderboardManager>();
        //}
        Vector3 cameraForward = cam.transform.forward;
        Vector3 cameraForwardRotated = Vector3.Normalize(new Vector3(cameraForward.x, 0.0f, cameraForward.z));
        Vector3 cameraScale = cam.transform.lossyScale;
        Vector3 newMenuPosition = cam.transform.position + menuDistance * cameraScale.z * cameraForwardRotated;
        transform.forward = -cameraForwardRotated;

        if (HumanPosessionManager.playArea)
        {
            transform.position = new Vector3(newMenuPosition.x, (defaultY * cameraScale.y) + HumanPosessionManager.playArea.position.y, newMenuPosition.z);
        }
        else
        {
            transform.position = new Vector3(newMenuPosition.x, defaultY * cameraScale.y, newMenuPosition.z);
        }
        transform.localScale = cameraScale;

        TutorialController.AppearingMenuHasAppeared();
        _state = State.Appearing;
        AppearingFinished();
        //Invoke("AppearingFinished", 2);
        //TODO: move to AppearingFinished
        if (buttonPointerPrefab)
        {
            buttonPointer = (ButtonPointer)Instantiate(buttonPointerPrefab);
        }
        if (leaderboardManager)
        {
            leaderboardManager.OnAppearingMenu(true, transform.position);
        }

        RefreshLocoText();
        RefreshThrowingStyleText();
    }

    public void TurnOff()
    {
        isVisible = false;
        //if (!leaderboardManager)
        // {
        //    leaderboardManager = FindObjectOfType<LeaderboardManager>();
        // }

        CancelInvoke();
        transform.position = new Vector3(0, -1000, 0);
        _state = State.Invisible;
        /*
        foreach (RealButton button in realButtons)
        {
            button.canUpdate = false;
        }
        */

        if (leaderboardManager)
        {
            leaderboardManager.OnAppearingMenu(false, transform.position);
        }
        if (buttonPointer)
        {
            Destroy(buttonPointer);
        }
    }

    void AppearingFinished()
    {
        //        Debug.Log("Appearing Finished");
        _state = State.Visible;
        /*
        foreach (RealButton button in realButtons)
        {
            button.canUpdate = true;
        }
        */
    }

    public void OnChallengedPassed()
    {
        if (soundMaker && challengePassedSound)
        {
            soundMaker.transform.position = Camera.main.transform.position;
            soundMaker.clip = challengePassedSound;
            soundMaker.Play();
        }
    }

    public void OnPitcherButtonActivated()
    {
        ballController.instance.SetServiceToPitcher();
    }

    public void OnTeeButtonActivated()
    {
        ballController.instance.SetServiceToTee();
    }

    public void OnSelfServeButtonActivated()
    {
        ballController.instance.SetServiceToSelf();
    }

    public void OnRestartButtonActivated()
    {
        sceneChanger.GoToScene(fullSceneName);
    }

    public void OnExitButtonActivated()
    {
        sceneChanger.GoToScene(lockerSceneName);
    }

    public void OnMusicButtonToggled(MenuButton butt)
    {
        gameController.SetMusicActive(butt.isActivated);
    }

    public void OnMusicButtonActivated()
    {
        gameController.SetMusicActive(true);
    }

    public void OnMusicButtonDeactivated()
    {
        gameController.SetMusicActive(false);
    }

    public void OnRefillButtonActivated()
    {
        ballController.instance.ResetBalls();
    }

    public void OnHandednessButtonActivated()
    {
        ToggleHandedness();
    }

    public void ToggleDebug()
    {
        if (debugPanel) debugPanel._showDebug = !debugPanel._showDebug;
    }



    public void OnLocomotionTypeChange()
    {
        HumanPosessionManager.ToggleLocomotionType();
        RefreshLocoText();
    }

    public void ToggleHandCannonUse()
    {
        HandManager.ToggleThrowingType();
        //FieldManager.PrepForFullGame(FieldManager.Instance.defense.useCannon);

        RefreshThrowingStyleText();
    }

    void RefreshLocoText()
    {
        if (HumanPosessionManager.locomotionType_saved == HumanPosessionManager.LocomotionVariant.STANDARD)
        {
            locomotionTypeText.text = "A";
        }
        else
        {
            locomotionTypeText.text = "B";

        }
    }

    void RefreshThrowingStyleText()
    {
        if(throwingStyleText)
        {
            if(HandManager.useCannon)
            {
                throwingStyleText.text = "Cannon";
            }
            else
            {
                throwingStyleText.text = "Manual";
            }
        }
    }

    public void SetLeftHanded(bool lefty)
    {
        if (lefty)
        {
            Debug.Log("SetLefty " + Time.time);
            HandManager.SetDominantHand(HandManager.HandType.left);
            if (whichHandText)
            {
                whichHandText.text = leftHandedWord;
            }
            //HandManager.currentRightCustomHand.GetComponent<CustomHand>().showRenderModel = true;
        }
        else
        {
            Debug.Log("SetLefty " + Time.time);

            HandManager.SetDominantHand(HandManager.HandType.right);
            if (whichHandText)
            {
                whichHandText.text = rightHandedWord;
            }
            //HandManager.currentLeftCustomHand.GetComponent<CustomHand>().showRenderModel = true;
        }

        if (HandManager.currentLeftCustomHand)
        {
            HandManager.currentLeftCustomHand.RefreshHandState();
        }

        if (HandManager.currentRightCustomHand)
        {
            HandManager.currentRightCustomHand.RefreshHandState();
        }
    }

    public void ToggleHandedness()
    {
        SetLeftHanded(HandManager.dominantHand != HandManager.HandType.left);
#if RIFT
        /*
        OVRInput.SetControllerVibration(0f, 0f, HandManager.secondaryOculusHand);
        StartCoroutine(OculusHapticOverride(.25f));
        */
#else

        if (HandManager.currentLeftCustomHand)
        {
            HandManager.currentLeftCustomHand.showRenderModel = true;
        }

        if (HandManager.currentRightCustomHand)
        {
            HandManager.currentRightCustomHand.showRenderModel = true;
        }

        if (hitThings.instance && hitThings.instance.attached)
        {
            hitThings.instance.attachedCustomHand.showRenderModel = false;
        }
#endif

        HumanGloveController tempGlove = FindObjectOfType<HumanGloveController>();
        if(tempGlove && tempGlove.attached)
        {
            if(HandManager.dominantHand == HandManager.HandType.right)
            {
                tempGlove.shouldBeOnLeft = true;
            }
            else
            {
                tempGlove.shouldBeOnLeft = false;
            }
            tempGlove.SetAttached(true);
        }
        HandCannonController pitcherCannon = FindObjectOfType<HandCannonController>();
        /*if(pitcherCannon)
        {
            pitcherCannon.RefreshHand();
        }
        */
    }

    public void DetermineGame_Full()
    {
        FieldManager.PrepForFullGame();
    }
    public void DetermineGame_OffenseOnly()
    {
        FieldManager.PrepForOffenseOnly();
    }
    public void DetermineGame_DefenseOnly()
    {
        FieldManager.PrepForDefenseOnly();
    }
    public void DetermineGame_Broadcast()
    {
        FieldManager.PrepForBroadcast();
    }


    /*
    public void MaintainHumanControls()
    {
        if(FieldManager.Instance)
        {
            if(FieldManager.Instance.offense)
            {
                FieldManager.Instance.offense.DetermineHumanControllNotStatic(FieldManager.Instance.offense.humanControlled);
            }

            if(FieldManager.Instance.defense)
            {
                FieldManager.Instance.defense.DetermineHumanControllNotStatic(FieldManager.Instance.defense.humanControlled);
            }

            if (FieldManager.Instance.spectator)
            {
                SpectatorCoordinator.DetermineHumanControl(FieldManager.Instance.spectator.humanControlled);
            }
        }
    }
    */

    void OnDisable()
    {
        isVisible = false;
    }

    void OnDestroy()
    {
        isVisible = false;
    }
}

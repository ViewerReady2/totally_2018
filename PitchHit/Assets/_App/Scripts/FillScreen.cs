﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FillScreen : MonoBehaviour
{
    [SerializeField] Camera cam;

    void Update()
    {
        if(cam != null)
        {
            transform.LookAt(2 * transform.position - cam.transform.position);
            Vector3 camPos = cam.transform.position;
            transform.position = new Vector3(camPos.x, camPos.y, camPos.z + 1f);
            float quadHeight = cam.orthographicSize * 2f;
            float quadWidth = quadHeight * Screen.width / Screen.height;
            transform.localScale = new Vector3(quadWidth, quadHeight, 1);

            /*if (Input.GetKeyDown(KeyCode.B))
            {
                cam.gameObject.SetActive(!cam.gameObject.activeSelf);
            }*/
        }
    }
}

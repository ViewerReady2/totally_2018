﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if STEAM_VR
using Valve.VR.InteractionSystem;
#endif
using System;
using UnityEngine.Playables;
using System.Linq;
using Valve.VR;

[Serializable] public class TimelineDictionary : SerializableDictionary<float, string> { }

public class TotallyTutorial : AIFieldPlayer
{
    public AIFieldPlayer tutorialPlayer;
    public PlayerInfo tutorialJJProfile;
    public Transform positionA;
    public Transform positionB;

    public bool hasTaughtHitting;
    public bool hasTaughtRunning;
    public bool hasTaughtCatching;
    public bool hasTaughtThrowing;
    public bool hasTaughtManualThrowing;

    public bool isTutorialReady;

    public AudioSource voiceOver;
    private float audioStartTime = 0f;

    //public Animator anim;
    public Animator mouthAnim
    {
        get
        {
            if (bodyControl)
            {
                return bodyControl.mouthAnimator;
            }
            else
            {
                return null;
            }
        }
        set
        {
            if (bodyControl)
            {
                bodyControl.mouthAnimator = value;
            }
        }
    }

    [SerializeField] protected RuntimeAnimatorController replacementMouthControl;




    public List<GameObject> viveControllers;
    public List<GameObject> oculusControllers;

    [SerializeField] protected Transform leftHandRoot;
    [SerializeField] protected Transform rightHandRoot;
    [SerializeField] protected float armLength = 1;


    protected BatController bat
    {
        get { return HandManager.instance.bat; }
    }
    public Transform ballSpawnPosition;
    public Transform ballHitPosition;
    public hittable currentBall
    {
        get { return FieldManager.Instance.ballInPlay; }
        set { FieldManager.Instance.ballInPlay = value; }
    }

    protected HumanGloveController glove
    {
        get { return HandManager.instance.glove; }
    }

    protected HandCannonController cannon
    {
        get { return HandManager.instance.cannon; }
    }
    public FieldBase sampleBase;

    void Awake()
    {
        FieldManager.PrepForTutorial();
    }

    // Use this for initialization
    public override void Initialize()
    {
        Debug.LogWarning("TUT INIT");
        base.Initialize();
        TutorialEventHandles handles = bodyControl.gameObject.AddComponent<TutorialEventHandles>();
        handles.Initialize(this);
        bodyControl.SetHandVisibility(HandManager.HandType.left, false);
        bodyControl.SetHandVisibility(HandManager.HandType.right, false);
        bodyControl.ParentToHand(leftHandRoot, HandManager.HandType.left);
        bodyControl.ParentToHand(rightHandRoot, HandManager.HandType.right);
        mouthAnim.runtimeAnimatorController = replacementMouthControl;

        bodyControl.DressAsPlayer(tutorialJJProfile);
#if RIFT
        foreach (GameObject controllerObj in viveControllers)
        {
            controllerObj.SetActive(false);
        }
        foreach(GameObject controllerObj in oculusControllers)
        {
            controllerObj.SetActive(true);
        }
#elif STEAM_VR
        bool oculusInSteam = (GameController.isOculusInSteam);

        foreach(GameObject controllerObj in viveControllers)
        {
            controllerObj.SetActive(!oculusInSteam);
        }
        foreach(GameObject controllerObj in oculusControllers)
        {
            controllerObj.SetActive(oculusInSteam);
        }
#endif

        HumanPosessionManager.SetInitialPositionRotation(Vector3.zero, Quaternion.identity);
        HumanPosessionManager.PosessPlayer(tutorialPlayer, resetPlayArea: true);

        HandManager.instance.SetHandEquipmentActive(HandManager.HandGear.GLOVE, false);
        HandManager.instance.SetHandEquipmentActive(HandManager.HandGear.BAT, false);

        //HandManager.instance.SetHandEquipmentActive(HandManager.HandGear.CANNON, false);


        //StartCoroutine(HandsOnTutorialRoutine());
        //StartCoroutine(TimelineEventsRoutine());

        //MARC: need this to initialize Hint models. will not actually highlight buttons this first time. Super janky
        //HighlightGrips();
        //HideAllButtonHighlights();

        StartCoroutine(DisableBat());

        this.BroadcastMessage("OnHandInitialized", 0, SendMessageOptions.DontRequireReceiver); // let child objects know we've initialized
        position = positionA.position;
        DitherFade(0, 1, .5f);
        SetLookMode(LookMode.TRANSFORM);
        SetLookTarget(tutorialPlayer.transform);

    }

    void FixedUpdate()
    {

        if (voiceOver.isPlaying) mouthAnim.SetFloat("Speed", 1f);
        else
        {
            mouthAnim.SetFloat("Speed", 0f);
            mouthAnim.Play("AnnouncerMouth", 0, 0);
        }
    }

    void MoveToTransform(Transform pos)
    {
        if ((transform.position - pos.position).sqrMagnitude < 1) return;
        Teleport(pos.position);
        //transform.SetPositionAndRotation(pos.position, pos.rotation);
    }

    #region EVENT HANDLES
    public void SetAudioStartTime(float time)
    {
        audioStartTime = time;
    }

    public void StartAudioTrack(AudioClip clip)
    {
        voiceOver.clip = clip;
        voiceOver.time = audioStartTime;
        voiceOver.Play();
        audioStartTime = 0;
    }

    public void SetTutorialReady()
    {
        isTutorialReady = true;
    }

    public void ShowBat()
    {
        hitThings.currentGripType = hitThings.gripType.always;
        HandManager.instance.SetHandEquipmentActive(HandManager.HandGear.BAT, true);
    }
    public void HideBat()
    {
        HandManager.instance.SetHandEquipmentActive(HandManager.HandGear.BAT, false);
    }

    public void LoadBallIntoTutorialGuideHand()
    {
        LoadBall();
        PutBallInTutorialGuideHand();
    }

    #endregion

    IEnumerator DisableBat()
    {
        yield return null;
        bat.enabled = false;

    }

    public void TeachHitting()
    {
        Debug.Log("Teach Hitting");
        StartCoroutine(TeachHittingRoutine());
    }

    public void TeachRunning()
    {

        Debug.Log("Teach Running");
        StartCoroutine(TeachRunningRoutine());
    }

    public void TeachCatching()
    {
        Debug.Log("Teach Catching");
        StartCoroutine(TeachCatchingRoutine());
    }

    public void TeachThrowing()
    {
        Debug.Log("Teach Throwing");
        StartCoroutine(TeachThrowingRoutine());
    }

    public void TeachThrowingStrike()
    {
        Debug.Log("TeachThrowing Strike.");
        StartCoroutine(TeachThrowingStrikeRoutine());
    }


    public void TeachManualThrowing()
    {
        Debug.Log("TeachThrowing Manual.");
        StartCoroutine(TeachManualThrowingeRoutine());
    }

    IEnumerator TeachHittingRoutine()
    {
        MoveToTransform(positionB);
        bodyControl.SetAnimationBool("Success", false);
        currentBall = null;

        yield return null;
        bool success = false;
        while (!success)
        {
            //wait until it's hit.
            while (currentBall && !currentBall.GetWasHitByBat() && !currentBall.hitGround)
            {
                yield return null;
            }

            if (currentBall)
            {
                //ball was hit by bat and has reached ground
                if (currentBall.GetWasHitByBat())
                {
                    success = true;
                    bodyControl.SetAnimationBool("Success", true);
                    currentBall = null;
                }

                //ball hit the ground without being hit
                else
                {
                    FieldManager.Instance.ExplodeBall();
                }
            }
            yield return null;
        }

        hasTaughtHitting = true;
    }

    IEnumerator TeachRunningRoutine()
    {
        isTutorialReady = false;
        CustomHand batHand = bat.GetComponent<hitThings>().attachedCustomHand;
        yield return null;
        batHand.RefreshHandState();
        //anim.SetBool("Success", false);
        bodyControl.SetAnimationBool("Success", false);
        MoveToTransform(positionA);

        while (!isTutorialReady)
        {
            yield return null;
        }

        //highlight running grips
#if RIFT || GEAR_VR
        Transform playArea = OVRManager.instance.transform;
#elif STEAM_VR
        Transform playArea = Player.instance.transform;
#endif

        HumanPosessionManager.SetArmLocomotionEnabled(true);

        //spawn a target to run to.
        sampleBase.safeParticles.Play();

        Vector3 flatCameraPosition = Camera.main.transform.position;
        flatCameraPosition.y = sampleBase.transform.position.y;
        //wait player runs there.


        while (sampleBase.defensivePlayers.Count == 0)
        {
            yield return null;
        }

        sampleBase.safeParticles.Stop();

        //play safe sound.
        sampleBase.PlaySafeOnBaseSound();

        yield return new WaitForSeconds(2f);

        bodyControl.SetAnimationBool("Success", true);

        //anim.SetBool("Success", true);

        HumanPosessionManager.PosessPlayer(tutorialPlayer, resetPlayArea: true, fade: true, cancelIfAlreadyTethered: false);
        HumanPosessionManager.SetArmLocomotionEnabled(false);

        HideAllButtonHighlights();

        //start good-job animation and VO.
        hasTaughtRunning = true;
    }

    IEnumerator TeachCatchingRoutine()
    {
        bodyControl.SetAnimationBool("Success", false);
        MoveToTransform(positionB);

        //anim.SetBool("Success", false);
        HandManager.instance.SetHandEquipmentActive(HandManager.HandGear.GLOVE, true);
        HandManager.instance.SetHandEquipmentActive(HandManager.HandGear.BAT, false);
        //Prevent the cannon from spawning in when they player catches a ball;
        HandManager.SetThrowingType(HandManager.ThrowingVariant.MANUAL);

        HighlightTrigger_NonDominant();
        bool success = false;
        while (!success)
        {
            while (currentBall && currentBall.hasBeenCaught == false && currentBall.hitGround == false)
            {
                yield return null;
            }

            if (currentBall)
            {
                if (currentBall.hasBeenCaught)
                {
                    success = true;
                    //anim.SetBool("Success", true);
                    bodyControl.SetAnimationBool("Success", true);

                }
                else
                {
                    FieldManager.Instance.ExplodeBall();

                }
            }
            yield return null;
        }

        hasTaughtCatching = true;

        HideAllButtonHighlights();
        yield return new WaitForSeconds(2f);

        FieldManager.Instance.ForceDestroyBallInPlay();

    }

    IEnumerator TeachThrowingRoutine()
    {
        isTutorialReady = false;
        //anim.SetBool("Success", false);
        bodyControl.SetAnimationBool("Success", false);


#if RIFT || GEAR_VR
        Transform playArea = OVRManager.instance.transform;
#elif STEAM_VR
        Transform playArea = Player.instance.transform;
#endif

        MoveToTransform(positionB);
        HumanPosessionManager.SetInitialPositionRotation(FieldManager.Instance.GetPitchersMoundPosition(), Quaternion.LookRotation(Vector3.back));
        HumanPosessionManager.PosessPlayer(tutorialPlayer, resetPlayArea: true, fade: true, cancelIfAlreadyTethered: false);
        //start Running explanation animation and VO.

        //form cannon around player's hand
        //HandManager.instance.SetHandEquipmentActive(HandManager.HandGear.CANNON, true);
        HandManager.instance.SetHandEquipmentActive(HandManager.HandGear.GLOVE, true);

        HandManager.SetThrowingType(HandManager.ThrowingVariant.CANNON);

        HighlightTrigger_Dominant();

        cannon.PrepareForUse();
        while (!isTutorialReady)
        {
            yield return null;
        }

        LoadBall();
        glove.CatchBall(currentBall);
        //cannon.ForceLoadBall(currentBall);
        FieldManager.Instance.strikeGate.Reset();

        //wait for user to pass.
        while (currentBall.rb.isKinematic)
        {
            yield return null;
        }

        HideAllButtonHighlights();
        bodyControl.SetAnimationBool("Success", true);

        //anim.SetBool("Success", true);
    }

    IEnumerator TeachThrowingStrikeRoutine()
    {
        bodyControl.SetAnimationBool("Success", false);

        //anim.SetBool("Success", false);
        HandManager.instance.SetHandEquipmentActive(HandManager.HandGear.GLOVE, true);

        FieldManager.Instance.ShowHideStrikeGate(true);
        FieldManager.Instance.ShowHideStrikeMarker(true);
        LoadBall();
        glove.CatchBall(currentBall);
        //cannon.ForceLoadBall(currentBall);
        FieldManager.Instance.strikeGate.Reset();
        //cannon.PrepareForUse();
        HighlightTrigger_Dominant();

        bool strikeSuccess = false;

        while (!strikeSuccess)
        {
            if (currentBall)
            {
                if (FieldManager.Instance.strikeGate.IsPassed)
                {
                    if (FieldManager.Instance.strikeGate.strikeDetected)
                    {
                        bodyControl.SetAnimationBool("Success", true);
                        strikeSuccess = true;
                        FieldManager.Instance.CalloutStrike(currentBall);
                    }
                    else
                    {
                        FieldManager.Instance.ExplodeBall();
                    }
                }

                if (currentBall.hitGround ||
                    ((HomeRunLevelController.instance.isOutsideFence(FieldManager.WorldToLocalPosition(currentBall.transform.position)))))
                {
                    FieldManager.Instance.ExplodeBall();
                }
            }
            else
            {
                LoadBall();
                glove.CatchBall(currentBall);

                FieldManager.Instance.strikeGate.Reset();
            }
            yield return null;
        }

        hasTaughtThrowing = true;
        HideAllButtonHighlights();

    }

    IEnumerator TeachManualThrowingeRoutine()
    {
        bodyControl.SetAnimationBool("Success", false);

//        anim.SetBool("Success", false);
        HandManager.instance.SetHandEquipmentActive(HandManager.HandGear.GLOVE, true);

        HandManager.SetThrowingType(HandManager.ThrowingVariant.MANUAL);
        //FieldManager.Instance.strikeGate.Reset();

        //FieldManager.Instance.ShowHideStrikeGate(true);
        //FieldManager.Instance.ShowHideStrikeMarker(true);

        Debug.Log("========== TEACH MANUAL++++++++ " + FieldManager.Instance.strikeGate.IsPassed);
        LoadBall();
        glove.CatchBall(currentBall);

        HighlightTrigger_Dominant();

        float minDist = 10f;
        bool reachedMinDist = false;

        while (!reachedMinDist)
        {
            if (currentBall)
            {
                if (Vector3.Distance(currentBall.transform.position, tutorialPlayer.transform.position) >= minDist)
                {
                    bodyControl.SetAnimationBool("Success", true);

                    //anim.SetBool("Success", true);
                        reachedMinDist = true;
                }

                if (currentBall.hitGround ||
                    ((HomeRunLevelController.instance.isOutsideFence(FieldManager.WorldToLocalPosition(currentBall.transform.position)))))

                {
                    FieldManager.Instance.ExplodeBall();
                }

            }
            else
            {
                //cannon.PrepareForUse();
                LoadBall();
                //cannon.ForceLoadBall(currentBall);
                glove.CatchBall(currentBall);

                FieldManager.Instance.strikeGate.Reset();
            }
            yield return null;
        }

        hasTaughtManualThrowing = true;
        HideAllButtonHighlights();

    }

    public void LoadBall()
    {
        FieldManager.Instance.SpawnFreshBall(destroyExisting: true);
        currentBall.GetComponent<BallCastAhead>().fastCastThreshold = 1;
    }

    

    protected void PutBallInTutorialGuideHand()
    {
        if (ballHitPosition)
        {
            currentBall.rb.isKinematic = true;
            currentBall.transform.SetParent(ballSpawnPosition);
            currentBall.transform.localPosition = Vector3.zero;
        }
    }

    public void ServeBall()
    {
        Debug.Log("serve ball!");
        if (currentBall == null)
        {
            return;
        }

        currentBall.rb.isKinematic = false;
        currentBall.transform.SetParent(null);
        currentBall.rb.velocity = Trajectory.InitialVelocityNeededToHitPoint(currentBall.rb, ballHitPosition.position, 2);
            //(Trajectory.GetSlowestVelocityByLanding(currentBall.transform.position, ballHitPosition.position));
    }


    


    public void HideAllButtonHighlights()
    {
        HandManager.currentLeftCustomHand.HideHints();
        HandManager.currentRightCustomHand.HideHints();
       
    }


    public void HighlightGrips()
    {

        HandManager.currentRightCustomHand.SetGripBatHint(true);
        HandManager.currentLeftCustomHand.SetGripBatHint(true);
       
    }

    public void HighlightMenuButton()
    {
        HandManager.currentRightCustomHand.SetMenuButtonHint(true);
        HandManager.currentLeftCustomHand.SetMenuButtonHint(true);
        
    }

    public void HighlightTrigger_Dominant()
    {
        HighlightTrigger(HandManager.dominantHand == HandManager.HandType.left? HandManager.HandType.left : HandManager.HandType.right);
    }

    public void HighlightTrigger_NonDominant()
    {
        HighlightTrigger(HandManager.dominantHand == HandManager.HandType.left ? HandManager.HandType.right : HandManager.HandType.left);
    }

    public void HighlightTrigger(HandManager.HandType hand = HandManager.HandType.both)
    {
        if (hand == HandManager.HandType.left || hand == HandManager.HandType.both)
        {
            HandManager.currentLeftCustomHand.SetGrabHint(true);
        }
        if (hand == HandManager.HandType.right || hand== HandManager.HandType.both)
        {
            HandManager.currentRightCustomHand.SetGrabHint(true);

        }

    }

  

    public override void InstantReset()
    {
    
    }
}

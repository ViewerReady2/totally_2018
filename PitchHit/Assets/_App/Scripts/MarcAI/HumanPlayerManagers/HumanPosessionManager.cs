﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System;
#if STEAM_VR
using Valve.VR.InteractionSystem;
#endif

/// <summary>
/// There must be an Instance to run coroutines, but otherwise it can function as a static class
/// </summary>
public class HumanPosessionManager : MonoBehaviour
{
    public enum LocomotionVariant
    {
        STANDARD,
        PROJECTION,
    }

    public static HumanPosessionManager Instance;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        else GameObject.Destroy(this);
    }

    void Start()
    {
        if (!initialized)
        {
            Debug.Log("CALLING IT FROM START");
            Initialize();
        }
    }

    public static Transform playArea
    {
        get
        {
            if (_playArea == null)
            {
#if STEAM_VR
                _playArea = Player.instance.transform;
#elif RIFT
                _playArea = OVRManager.instance.transform;
#endif
            }
            return _playArea;
        }
        set
        {
            _playArea = value;
        }
    }
    private static Transform _playArea;
    protected static Vector3 initialVRPosition;
    protected static Quaternion initialVRRotation;
    protected static ArmSwinger playerLocomotion;
    public static LocomotionVariant locomotionType_saved
    {
        get { return (LocomotionVariant)PlayerPrefs.GetInt("LOCOMOTION"); }
        private set { PlayerPrefs.SetInt("LOCOMOTION", (int)value); }
    }
    private static LocomotionVariant locomotionType_current;

    public static Action<AIFieldPlayer> OnPlayerPosessed;
    public static Action<AIFieldPlayer> OnPlayerUnposessed;

    private AIFieldPlayer currentlyTetheredPlayer_editor; //this field only exists to make currentlyTetheredPlayer visible in the inspector for debugging

    public FieldPlayerBodyControl projectionPrefab;
    private FieldPlayerBodyControl projectionPuppet;

    private bool currentlyProjectingTeleportLocation;

    public static AIFieldPlayer currentlyTetheredPlayer
    {
        get { return _currentlyTetheredPlayer; }
        private set { if (Instance) Instance.currentlyTetheredPlayer_editor = value; _currentlyTetheredPlayer = value; }
    }
    private static AIFieldPlayer _currentlyTetheredPlayer;
    private static Coroutine currentPossesRoutine;

    private static bool fadedOut = false;
    private static AIFieldPlayer waitingToPosess;

    private static bool projectOnRail = false;
    private static Vector3 railStart = Vector3.zero;
    private static Vector3 railEnd = Vector3.forward;

    private const float normalizedScale = 1.3f;

    private bool initialized = false;

    public static bool transferRoutineInProgress{
        get;
        private set;
    }


    void Initialize()
    {
        StartCoroutine(InitializeRoutine());
    }

    IEnumerator InitializeRoutine()
    {
        if (initialized)
        {
            yield break;
        }

        playerLocomotion = playArea.GetComponentInChildren<ArmSwinger>();
        playerLocomotion.OnBeginLocomotion += OnBeginLocomotion;
        playerLocomotion.OnEndLocomotion += OnEndLocomotion;
        projectionPuppet = GameObject.Instantiate<FieldPlayerBodyControl>(projectionPrefab);// locomotionProjection.GetComponentInChildren<ProjectionPuppet>();
        projectionPuppet.gameObject.SetActive(false);

        CameraScaler.instance.scale = normalizedScale;
        initialized = true;

        while (fadedOut)
        {
            yield return null;
        }
        initialVRPosition = playArea.position;
        initialVRRotation = playArea.rotation;
        SetupForLocomotionType(locomotionType_saved);
    }

#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        if (projectOnRail)
        {
            Handles.color = Color.black;
            Handles.DrawLine(railStart, railEnd);
        }
        if(currentlyTetheredPlayer != null)
        {
            Handles.color = Color.yellow;
            Handles.DrawWireDisc(currentlyTetheredPlayer.transform.position, Vector3.up, 1.25f);
        }
    }
#endif


    public static void ResetPlayAreaToInitialPositionRotation()
    {
        playArea.transform.position = initialVRPosition;
        playArea.transform.rotation = initialVRRotation;
    }

    private static bool AlmostEqualsVector3(Vector3 a, Vector3 b, float precision)
    {
        if ((a - b).magnitude <= precision) return true;
        else return false;
    }

    public static bool PlayAreaIsAtInitialPositionRotation()
    {
        //return playArea.transform.position.AlmostEquals(initialVRPosition, .01f) && playArea.transform.rotation.AlmostEquals(initialVRRotation, .01f);
        return AlmostEqualsVector3(playArea.transform.position, initialVRPosition, 0.01f) && (Quaternion.Angle(playArea.transform.rotation, initialVRRotation) < 0.01f);
    }

    public static void SetInitialPositionRotation(Vector3 position, Quaternion rotation)
    {
        initialVRPosition = position;
        initialVRRotation = rotation;
        //ResetPlayAreaToInitialPositionRotation();
    }

    Coroutine locomotionDelay;
    public static void SetArmLocomotionEnabled(bool isOn, float delay = 0)
    {
        if (playerLocomotion)
        {
            if(Instance.locomotionDelay != null)
            {
                Instance.StopCoroutine(Instance.locomotionDelay);
            }

            if (delay == 0)
            {
                if (Instance && locomotionType_saved != locomotionType_current)
                {
                    Instance.SetupForLocomotionType(locomotionType_saved);
                }
                playerLocomotion.latestArtificialMovement = 0f;
                playerLocomotion.armSwingNavigation = isOn;
                playerLocomotion.enabled = isOn;

                playerLocomotion.CleanHistory();
            }
            else
            {
                Instance.StartCoroutine(Instance.DelayedLocomotionActivation(isOn, delay));
            }
        }
    }

    IEnumerator DelayedLocomotionActivation(bool isOn,float delay)
    {
        float countdown = delay;
        while(countdown > 0 && (playerLocomotion.rightButtonPressed || playerLocomotion.leftButtonPressed))
        {
            yield return null;
            countdown -= Time.deltaTime;
        }
        if (Instance && locomotionType_saved != locomotionType_current)
        {
            Instance.SetupForLocomotionType(locomotionType_saved);
        }
        playerLocomotion.latestArtificialMovement = 0f;
        playerLocomotion.armSwingNavigation = isOn;
        playerLocomotion.enabled = isOn;
        playerLocomotion.CleanHistory();
    }

    public static void ConstrainProjectionToRail(bool onRail)
    {
        projectOnRail = onRail;
    }

    public static void SetProjectionRailPoints(Vector3 start, Vector3 end, bool snapToStart = true)
    {
        railStart = start; railStart.y = 0;
        railEnd = end; railEnd.y = 0;
        //as long as useRotationOverride == false, it doesn't matter if we set the override rotation when not using rails
        playerLocomotion.overrideMoveRotation = Quaternion.LookRotation((end - start), Vector3.up);

        if (projectOnRail)
        {
            if (snapToStart)
            {
                Instance.projectionPuppet.transform.position = railStart;
            }
        }
    }

    public static void SetReverseRailLocomotionEnabled(bool reverse)
    {
        if(projectOnRail && locomotionType_current == LocomotionVariant.PROJECTION)
            ArmSwinger.canReverse = reverse;
        else
        {
            ArmSwinger.canReverse = true;
        }
       //Debug.Log("<color=red>REverse on rail " + reverse  + "  on rail now? " + projectOnRail + "</color>");
    }

    protected void SetupForLocomotionType(LocomotionVariant var)
    {
        if (!initialized)
        {
            Debug.Log("Set up for locomotion");
            Initialize();
        }

        locomotionType_current = var;
        switch (var)
        {
            case LocomotionVariant.PROJECTION:
                if (projectOnRail)
                {
                    SetProjectionRailPoints(railStart, railEnd);
                }
                projectionPuppet.LockFeetAtCenter(projectOnRail);
                playerLocomotion.useRotationOverride = projectOnRail;


                playerLocomotion.overrideMoveTransform = projectionPuppet.transform;
                if (FieldManager.currentHumanPlayerRole == FieldManager.HumanPlayerRole.OFFENSE)
                {
                    ArmSwinger.SetSpeedMultiplier(.75f);//Greg: This magic number abomination must be dealt with later.
                }
                else
                {
                    ArmSwinger.SetSpeedMultiplier(1f);//Greg: This magic number abomination must be dealt with later.
                }
                break;
            case LocomotionVariant.STANDARD:
                //ConstrainProjectionToRail(false);
                ArmSwinger.canReverse = true;
                playerLocomotion.overrideMoveTransform = null;
                playerLocomotion.useRotationOverride = false;
                ArmSwinger.SetSpeedMultiplier(1f);//Greg: This magic number abomination must be dealt with later.
                break;
        }
       // Debug.Log("<color=red>Setup Locomotion " + locomotionType_saved+"</color>");

    }

    public void RefreshLocomotionType()
    {
        StartCoroutine(WaitForFadeToRefreshRoutine());
    }

    IEnumerator WaitForFadeToRefreshRoutine()
    {
        while (fadedOut)
        {
            yield return null;
        }
        SetupForLocomotionType(locomotionType_saved);
    }

    public static void ToggleLocomotionType()
    {
        bool wasOn = playerLocomotion && playerLocomotion.armSwingNavigation;
        if(wasOn)
            SetArmLocomotionEnabled(false);
        //Debug.Log("<color=red>Refresh Locomotion " + wasOn + "</color>");

        if (locomotionType_saved == LocomotionVariant.STANDARD) locomotionType_saved = LocomotionVariant.PROJECTION;
        else if (locomotionType_saved == LocomotionVariant.PROJECTION) locomotionType_saved = LocomotionVariant.STANDARD;

        if (wasOn)
        {
            Instance.StartCoroutine("TurnOnLocoAfterFame");
        }
        Debug.Log("Toggle Locomotion " + locomotionType_saved);
    }

    IEnumerator TurnOnLocoAfterFame()
    {
        yield return null;
            SetArmLocomotionEnabled(true);  
    }

    protected void OnBeginLocomotion()
    {
        //Debug.Log("BEGIN LOCO");

        if (locomotionType_current == LocomotionVariant.PROJECTION)
        {
            if (currentlyProjectingTeleportLocation) return;

            projectionPuppet.gameObject.SetActive(true);
            if (!projectOnRail)
            {
                projectionPuppet.transform.SetPositionAndRotation(playerLocomotion.transform.position, playerLocomotion.transform.rotation);
            }
            currentlyProjectingTeleportLocation = true;

            if (currentlyTetheredPlayer != null)
                currentlyTetheredPlayer.SetPosessedBodyFollow(projectionPuppet.transform);
        }
    }

    protected void OnEndLocomotion()
    {
//        Debug.Log("END LOCO");
        if (locomotionType_current == LocomotionVariant.PROJECTION)
        {
            if (!currentlyProjectingTeleportLocation) return;

                projectionPuppet.gameObject.SetActive(false);
            if (!projectOnRail)
                playerLocomotion.transform.position = projectionPuppet.transform.position;
            else
            {
                Vector3 delta = Camera.main.transform.localPosition; delta.y = 0;
                playerLocomotion.transform.position = projectionPuppet.transform.position - delta;

            }
            currentlyProjectingTeleportLocation = false;
            if (currentlyTetheredPlayer != null)
            {
                currentlyTetheredPlayer.SetPosessedBodyFollow(null);
            }

        }
    }

    public static void ReleasePosessedPlayer(AIFieldPlayer player)
    {
        if (currentlyTetheredPlayer != null && currentlyTetheredPlayer == player) ReleasePosessedPlayer();
    }

    public static void ReleasePosessedPlayer()
    {
        //Debug.Log("ReleasePlayer()");
        if (currentlyTetheredPlayer == null) return;
        Debug.Log("<color=cyan>****************************** RELEASE " + currentlyTetheredPlayer.name + "</color>");
        _ReleasePosessedPlayer();
    }

    private static void _ReleasePosessedPlayer()
    {
        if (currentlyTetheredPlayer == null) return;
        //Debug.Log("_RELEASE POSESSION --------- " + currentlyTetheredPlayer.name);

        currentlyTetheredPlayer.SetHumanPosession(false);
        AIFieldPlayer last = currentlyTetheredPlayer;
        currentlyTetheredPlayer = null;
        if(Instance)Instance.OnEndLocomotion();
        if (OnPlayerUnposessed != null) OnPlayerUnposessed.Invoke(last);
    }


    public static void PosessPlayer(AIFieldPlayer toPosess,
                                    bool cancelIfAlreadyTethered = true,
                                    bool resetPlayer = false,
                                    bool resetPlayArea = false,
                                    bool slowMo = false,
                                    bool fade = false,
                                    bool turnAround = false,
                                    bool matchScale = false)
    {
        
        //Debug.Log("PosessPlayer() " + toPosess);
        if (toPosess == null) return;

        //there's already a coroutine to posess this player, assume the first one takes priority
        if (toPosess == waitingToPosess)
        {
            //Debug.Log("THIS POSSESSION IS ALREADY HAPPENING!");
            //return;
        }

        //bool needsToResetPlayArea = resetPlayer && !PlayAreaIsAtInitialPositionRotation();
        if (toPosess == currentlyTetheredPlayer && cancelIfAlreadyTethered)// && !needsToResetPlayArea && !resetPlayer)
        {
            //Debug.Log("CANCEL BECAUSE ALREADY TETHERED");
            return;
        }
        Debug.Log("<color=cyan>****************************** POSESS " + toPosess.name + "</color>");


        if (Instance)
        {
            if (currentPossesRoutine != null)
            {
                //Debug.Log("<color=blue>****************************** Stopping Old Routine </color> ");
                Instance.StopCoroutine(currentPossesRoutine);
            }
            Debug.Log("Starting possession routine and passing fade == " + fade.ToString());
            currentPossesRoutine = Instance.StartCoroutine(PosessionRoutine(toPosess, cancelIfAlreadyTethered, resetPlayer, resetPlayArea, slowMo, fade, turnAround, matchScale));
        }
        else
        {
            Debug.Log("Calling transfer possession immediate");
            TransferPosession_Immediate(toPosess);
        }
    }

    private static void _PosessPlayer(AIFieldPlayer toPosess)
    {
        if (toPosess == null) return;
        if (toPosess == currentlyTetheredPlayer) return;
        //Debug.Log("_START POSESSION --------- " + toPosess.name);

        Vector3 playAreaPos = playArea.position + (toPosess.transform.position - Camera.main.transform.position);
        playAreaPos.y = toPosess.transform.position.y;
        playArea.position = playAreaPos;

        toPosess.SetHumanPosession(true);
        currentlyTetheredPlayer = toPosess;
        waitingToPosess = null;
        if (playerLocomotion)
            playerLocomotion.CleanHistory();
        if (OnPlayerPosessed != null) OnPlayerPosessed.Invoke(currentlyTetheredPlayer);
    }

    private static IEnumerator PosessionRoutine(AIFieldPlayer nextPosess,
                                    bool cancelIfAlreadyTethered,
                                    bool resetPlayer,
                                    bool resetPlayArea,
                                    bool slowMo,
                                    bool fade,
                                    bool turnAround,
                                    bool matchScale)
    {

        if (cancelIfAlreadyTethered && nextPosess.isTetheredToCamera)//Greg: not sure if this is necessary.
        {
            yield break;
        }


        transferRoutineInProgress = true;

        //Debug.Log("start posession routine " + nextPosess.name);
        waitingToPosess = nextPosess;
        if (slowMo) FieldTransitionManager.Instance.EnterSlowMotion();
        if (fade)
        {
            Debug.Log("DOING THE FADE");
            fadedOut = true;
            FieldTransitionManager.FadeOut(.5f);
            yield return new WaitForSecondsRealtime(.6f);
        }
        if (resetPlayer /*&& !nextPosess.isTetheredToCamera*/)
        {
            nextPosess.InstantReset();
        }

        if (CameraScaler.instance)
        {

            Vector3 pivotPos = Vector3.zero;// nextPosess.transform.position; //pivotPos.y = 0;
            CameraScaler.instance.SetPivotPosition(pivotPos, adjustForCamOffset: false);

            if (matchScale)
            {
                CameraScaler.instance.scale = nextPosess.transform.localScale.y * normalizedScale;
            }
            else
            {
                CameraScaler.instance.scale = normalizedScale;
            }
        }

        TransferPosession_Immediate(nextPosess);

        if (turnAround)
        {
            //Debug.Log("TURN AROUND++++++++++++++++++++++++++++++++++++++++++++++++XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
            playArea.RotateAround(Camera.main.transform.position, Vector3.up, 180f);
        }
        else
        {
            //this must mean that it's coming from the bat.
            // if (FieldManager.Instance && FieldManager.Instance.defense && FieldManager.Instance.defense.catcher == nextPosess)
            if (FieldManager.Instance && FieldManager.Instance.defense && nextPosess == FieldManager.Instance.defense.catcher)
            {
                //Debug.Log("it's the catcher so turn around!");
                HumanPosessionManager.playArea.Rotate(Vector3.up * 180f);
            }


        }
        if (resetPlayArea)
        {
            //Greg: There is no time when reseting the play area will want you to still have locomotion.
            //MARC: there is, when putting the player into Giant spectator mode
            //SetArmLocomotionEnabled(false);
            ResetPlayAreaToInitialPositionRotation();
            if (FieldManager.Instance.offense.humanControlled)
            {
                GameController.isAtBat = true;
            }
        }

        //HumanGearManager.instance.RefreshHatStatus();

        yield return null;
        if (fadedOut)
        {
            FieldTransitionManager.FadeIn(.5f);
            fadedOut = false;
        }

        transferRoutineInProgress = false;
        //Debug.Log("end posession routine ");
    }



    private static void TransferPosession_Immediate(AIFieldPlayer nextPosess)
    {
        if (nextPosess.isTetheredToCamera && FieldManager.Instance.defense.runningPlay != FieldPlayType.STANDARD)
        {
            return;
        }

        _ReleasePosessedPlayer();
        if (nextPosess != null)
        {
            _PosessPlayer(nextPosess);
        }

        if(HandManager.currentLeftCustomHand)
        {
            HandManager.currentLeftCustomHand.RefreshHandState();
            Debug.DrawRay(HandManager.currentLeftCustomHand.transform.position, Vector3.up * 5f, Color.magenta, 100f);
        }

        if(HandManager.currentRightCustomHand)
        {
            HandManager.currentRightCustomHand.RefreshHandState();
            Debug.DrawRay(HandManager.currentRightCustomHand.transform.position, Vector3.up * 5f, Color.magenta, 100f);
        }
    }

    public static void RefreshJersey()
    {
        //Debug.Log("refresh Posession " + currentlyTetheredPlayer);
        if(currentlyTetheredPlayer != null)
        {
            HumanGearManager.instance.SetTorsoMaterials(currentlyTetheredPlayer.GetJerseyMaterials());
        }
    }
}

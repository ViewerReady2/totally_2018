﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldTransitionManager : MonoBehaviour {


    public static FieldTransitionManager Instance;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    #region Fade
    public static void FadeOut(float seconds)
    {
#if STEAM_VR
        SteamVR_Fade.Start(Color.black, seconds);
#elif RIFT
        if(OVRScreenFade.instance)
        {
            OVRScreenFade.instance.fadeTime = seconds;
            OVRScreenFade.instance.FadeOut();
        }
#endif
    }

    public static void FadeIn(float seconds)
    {
        Debug.Log("FADE IN SCREEN!");
#if STEAM_VR
        SteamVR_Fade.Start(Color.clear, seconds);
#elif RIFT
        if (OVRScreenFade.instance)
        {
            OVRScreenFade.instance.fadeTime = seconds;
            OVRScreenFade.instance.FadeIn();
        }
#endif
    }

    private static Coroutine currentFade;
    public void FadeInOut(float inOutDuration, float waitDuration)
    {
        if (currentFade != null)
        {
            StopCoroutine(currentFade);
        }
        currentFade = StartCoroutine(FadeRoutine(inOutDuration, waitDuration));
    }

    protected IEnumerator FadeRoutine(float inoutDuration, float waitDuration)
    {
        FadeOut(inoutDuration);
        yield return new WaitForSeconds(waitDuration + inoutDuration);
        FadeIn(inoutDuration);
    }
#endregion

#region Time
    public AnimationCurve sloMoCurve;

    private static Coroutine currentSlowMo;

    public void EnterSlowMotion()
    {
        Debug.Log("SLOW MO");
        if (currentSlowMo != null)
        {
            StopCoroutine(currentSlowMo);
        }
        currentSlowMo = StartCoroutine(SlowMotionRoutine(3f));
    }

    const float initialFixedDeltaTime = .0167f;
    IEnumerator SlowMotionRoutine(float duration)
    {
        


        float t = 0;
        while (t < 1f)
        {
            Time.timeScale = sloMoCurve.Evaluate(t);
            Time.fixedDeltaTime = Time.timeScale * initialFixedDeltaTime;

            t += Time.unscaledDeltaTime / duration;
            yield return null;
        }


        Time.fixedDeltaTime = initialFixedDeltaTime;
    }
#endregion

}

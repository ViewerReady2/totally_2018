﻿using UnityEngine;
using System.Collections;
using Valve.VR.InteractionSystem;


public class HumanGloveController : MonoBehaviour
{

    // Use this for initialization
    //private GameObject ball;
    //private VRHand hand;
    //private VRHand hand2;
    private CustomHand attachedHand;
    private CatchSoundController sound;
    public bool attachToHandAutomatically;
    public bool attached;
    private bool inPlace;
    //[HideInInspector]
    public bool isOnRightHand;
    private bool attachedToFullBat = false;
    //[HideInInspector]
    private hittable heldBall
    {
        get { return _heldBall; }
        set { _heldBall = value;
//            Debug.Log("held ball " + value);
        }
    }
    private hittable _heldBall;
    //[HideInInspector]
    public bool shouldBeOnLeft;

    public bool isBotOwned = false;
    public bool requireGrip = false;//Greg: When this is true, you MUST TOUCH THE BALL WITH YOUR ACTIVE GLOVE WHILE HOLDING THE TRIGGER.

    public static BallEvent onHumanBallCatch = new BallEvent();
    public static BallEvent onHumanBallRelease = new BallEvent();
    public static BallEvent onHumanBallReleaseIntoHand = new BallEvent();


    public ParticleSystem catchParticles;
    private bool hasWaitedToAttach = false;
    public Animator gloveAnimator;

    public bool hasBall
    {
        get { return heldBall != null; }
    }

#if STEAM_VR
    //private SteamVR_Controller.Device controller;

#endif

    private Rigidbody rb;
    //private Player thisPlayer;
    //private Vector3 typicalScaling;
    public Transform gloveBody;
    public Transform gloveRoot;
    [SerializeField]
    public Transform catchPosition;
    //public Vector3 oculusOffset;
    public Vector3 gearVROffset;
    public Transform oculusMitPosition;

    public bool isGripping;

    private Vector3 initialGloveBodyScale;

    private hittable ballInCatchWindow;

    void Awake()
    {
        //hand = GameObject.FindGameObjectWithTag("hands").GetComponent<VRHand>();
        sound = GetComponent<CatchSoundController>();
        rb = GetComponent<Rigidbody>();

        rb.isKinematic = true;
        //typicalScaling = transform.localScale;
        if (gloveBody)
        {
            initialGloveBodyScale = gloveBody.localScale;
        }

#if RIFT
        if(oculusMitPosition)
        {
            gloveRoot.localPosition = oculusMitPosition.localPosition;
            gloveRoot.localRotation = oculusMitPosition.localRotation;
            gloveRoot.localScale = oculusMitPosition.localScale;
        }
#endif
    }

    void Start()
    {
        if (attachToHandAutomatically)
        {
            StartCoroutine(AutomaticAttach());
        }
    }

    IEnumerator AutomaticAttach()
    {
        Debug.Log("------------------------------- begin automatic attach");
        while(HandManager.IsHandValid(HandManager.HandType.left)==false && HandManager.IsHandValid(HandManager.HandType.right)==false)
        {
            yield return null;
        }

        yield return new WaitForSeconds(1f);
        Debug.Log("------------------------------- after delay");
        if (HandManager.dominantHand == HandManager.HandType.left)
        {
            Debug.Log("------------------------------- should be on left is false");
            shouldBeOnLeft = false;
        }
        else
        {
            Debug.Log("------------------------------- should be on left is true");
            shouldBeOnLeft = true;
        }

        hasWaitedToAttach = true;

        //while (attached == false || attachedHand==null)
        {
            SetAttached(true);
            //yield return new WaitForSeconds(1f);
        }
    }

  


    public bool GetAttached()
    {
        return attached;
    }

    public CustomHand GetAttachedHand()
    {
        if(!attached)
        {
            return null;
        }

        return attachedHand;
    }

    public bool GetIsOnRightHand()
    {
        return isOnRightHand;
    }

    public void OpenCatchWindow(hittable ball)
    {
  //      Debug.Log("OpenCatchWindow " + ball);
        ballInCatchWindow = ball;
        if(attachedHand != null)
        {
            attachedHand.Vibrate(3999);
        }
        //Time.timeScale = 0f;
    }

    public void CloseCatchWindow()
    {
//        Debug.Log("CloseCatchWindow " + ballInCatchWindow);

        ballInCatchWindow = null;
    }

   

    public void CatchBall(hittable ball)
    {
        if (ball == null) return;
        CloseCatchWindow();


        Debug.Log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ON BALLCAUGHT "+ball.name);
        if (!this.enabled)
        {
            return;
        }
        if (ball.visualAssist)
        {
            ball.visualAssist.SetActive(false);
        }

       // Debug.Log("<color=red>on ball caught " + traj.name + "</color>");
        //attachedHand.SetTrajBall(traj);
        heldBall = ball;
        ball.SetHeldByPlayer(true);
        //sound.PlaySound();
        if (ball.GetRigidbody().isKinematic == false)
        {
            float volume = Mathf.Max(Vector3.Distance(rb.velocity, ball.GetRigidbody().velocity), 1f);//this was .2f
                                                                                                      //Debug.Log("IT'S NOT KINEMATIC SO MAKE A SOUND!");
            //catchParticles.Play();
            sound.PlaySound(volume);
        }

        if(onHumanBallCatch != null) onHumanBallCatch.Invoke(ball);
        ball.OnHandGrabbed += ReleaseBallIntoHand;

        CenterHeldBallInGlove();

        RaycastHit hit;
        if (Physics.Raycast(ball.transform.position + (Vector3.up * 0.01f), Vector3.down, out hit, 0.3f, LayerMask.GetMask("Ground")))
        {
            DarkTonic.MasterAudio.MasterAudio.PlaySound3DAtVector3("ball_scoop", transform.position);
        }

        HandManager.VibrateController(.1f, 1, attachedHand);
        /*
        BallTracker tracker = ball.GetComponent<BallTracker>();
        //Debug.Log("<color=red>Tracker = " + tracker + "</color>");
        if (tracker)
        {
            tracker.SetIsHeld(true, attachedHand.transform == MultiplayerManager.instance.myPlayer.rightHand);
        }
        */

        //if (ball.OnCaughtByDefender != null) ball.OnCaughtByDefender.Invoke(ball);

    }

    public void ReleaseBallIntoHand(hittable ball)
    {
        if (heldBall == ball)
        {
            /*
            if (FieldManager.Instance)
            {
                FieldManager.Instance.defense.GetPlayerWithBallControl().glove.ReleaseBall(true);
            }
            */
            ReleaseBall(true);
            /*
            DefensePlayer possessedDefender = 
            if(HumanPosessionManager.currentlyTetheredPlayer)
            {
                HumanPosessionManager.currentlyTetheredPlayer.
            }
            */
        }
    }

    public hittable ReleaseBall(bool releaseIntoHand = false)
    {
        if (heldBall == null) return null;
        hittable release = heldBall;

        heldBall.OnHandGrabbed -= ReleaseBallIntoHand;
        BallCastAhead cast  = heldBall.GetComponent<BallCastAhead>();
        if (cast && cast.isActiveAndEnabled) cast.IgnoreGloveForSeconds(this, .5f);
        heldBall = null;
        //Debug.Log("Release ball");

        //if this ball was grabbed by the player's other hand, don't bother with these
        if (releaseIntoHand)
        {
            if (onHumanBallReleaseIntoHand != null) onHumanBallReleaseIntoHand.Invoke(release);
        }

        else
        {
            release.SetHeldByPlayer(false);
            release.GetRigidbody().isKinematic = false;
            release.transform.SetParent(null);
            if (onHumanBallRelease != null) onHumanBallRelease.Invoke(release);

        }


        return release;
    }

    public void CenterHeldBallInGlove()
    {
        if (heldBall)
        {
            //Greg: parenting it straight to the catch position causes scaling issues.
            //heldBall.transform.SetParent(catchPosition);
            //heldBall.transform.localPosition = Vector3.zero;

            heldBall.transform.SetParent(transform);
            heldBall.transform.position = catchPosition.position;

            heldBall.GetRigidbody().isKinematic = true;
        }
    }
    public hittable GetCurrentBall()
    {
        return heldBall;
    }


    protected void Attach_Delayed()
    {
        StartCoroutine(AutomaticAttach());
    }

    public void SetAttached(bool a, CustomHand handToUse)
    {
        Debug.Log("set attached " + a + " " + handToUse + " current: " + attachedHand);
        if (a && !hasWaitedToAttach)
        {
            Attach_Delayed();
            return;
        }

        //if (!thisPlayer)
        //{
        //    thisPlayer = FindObjectOfType<Player>();
        //}
        if (GameController.fullBatterMode)
        {
            //if (HandManager.genericTracker && HandManager.genericTracker.IsValid())
            {
                attached = a;
                gameObject.SetActive(a);
                attachedToFullBat = true;
                return;
            }
        }

        attachedToFullBat = false;
        if(handToUse != attachedHand)
        {
            Detatch();
        }


        attachedHand = handToUse;
        if (attachedHand)
        {
            attachedHand.showRenderModel = !a;
            attachedHand.isOccupied = true;
//#if RIFT
            attachedHand.MakeHandInvisible();
//#endif
            if (a)
            {
                attachedHand.HoverLock(this.GetComponent<Interactable>());
                if (shouldBeOnLeft == false)
                //if(attachedHand==HandManager.currentRightCustomHand)
                {
//                    Debug.Log("shouldnt be on left.");
                    if (isOnRightHand == false)
                    {
//                        Debug.Log("is now on right for first time.");
                        isOnRightHand = true;
                        foreach (Collider c in GetComponentsInChildren<Collider>())
                        {
                            c.transform.localScale *= -1f;
                        }
                    }
                }
                else
                {
//                    Debug.Log("should be on left");
                    if (isOnRightHand)
                    {
//                        Debug.Log("is now on left hand for first time.");
                        isOnRightHand = false;
                        foreach (Collider c in GetComponentsInChildren<Collider>())
                        {
                            c.transform.localScale *= -1f;
                        }
                        //Debug.Log("glove's on the left hand!");
                    }
                }


            }
            else
            {
                Detatch();
            }

#if STEAM_VR
            //controller = attachedHand.actualHand.controller;
#endif
        }

        attached = a;
        gameObject.SetActive(a);
    }

    protected void Detatch()
    {
        Debug.Log("detatch");
        if (!attachedHand) return;
        attachedHand.HoverUnlock(this.GetComponent<Interactable>());
        attachedHand.isOccupied = false;
        attachedHand.RefreshHandState();
    }

    public void SetAttached(bool a)
    {
       
        if (a && !hasWaitedToAttach)
        {
            Attach_Delayed();
            return;
        }

        if (GameController.fullBatterMode)
        {
            attached = a;
            attachedToFullBat = true;
            gameObject.SetActive(a);
            return;
        }
        CustomHand attachMeTo = attachedHand;
        attachedToFullBat = false;

        if (a)
        {
            if (HandManager.dominantHand == HandManager.HandType.left)
            {
                Debug.Log("attach glove to right");
                attachMeTo = HandManager.currentRightCustomHand;
                if (!attachMeTo)
                {
                    Debug.LogError("nevermind. have to go with the left");
                    attachMeTo = HandManager.currentLeftCustomHand;
                }
            }
            else
            {
                Debug.Log("attach glove to the left.");
                attachMeTo = HandManager.currentLeftCustomHand;
                if (!attachMeTo)
                {
                    Debug.LogError("nevermind have to go with the right.");
                    attachMeTo = HandManager.currentRightCustomHand;
                }
            }
        }
        SetAttached(a, attachMeTo);
    }

    void Update()
    {
        if (attached)
        {
            if (attachedHand)
            {
                isGripping = attachedHand.GetUsing();
            }
            else
            {
                isGripping = false;
            }

            if (gloveBody)
            {
                gloveAnimator.SetFloat("closeVal", attachedHand.GripValue);

                if (isGripping)
                {
                    //gloveBody.localScale = initialGloveBodyScale * 1.1f;//if the ball is parented to catch position this causes scaling issues.
                }
                else
                {
                   // gloveBody.localScale = initialGloveBodyScale;
                }

                if (heldBall)
                {
                    heldBall.transform.position = catchPosition.position;
                }

            }
#if RIFT || GEAR_VR
            if (!inPlace)
            {
                //inPlace = true;//turned this off so it never moves in the physics update.

                if (isOnRightHand)
                {
                    //Debug.Log("it's on the right hand!");
                    transform.rotation = Quaternion.AngleAxis(-90f, attachedHand.transform.forward) * (Quaternion.AngleAxis(165f, attachedHand.transform.right) * attachedHand.transform.rotation);
                }
                else
                {
                    //Debug.Log("it's on the leftt hand!");
                    transform.rotation = Quaternion.AngleAxis(90f, attachedHand.transform.forward) * Quaternion.AngleAxis(-15f, attachedHand.transform.right) * attachedHand.transform.rotation;
                }

                /*
                if (isOnRightHand)
                {
                    //Debug.Log("it's on the right hand!");
                    transform.rotation = Quaternion.AngleAxis(135f, attachedHand.transform.right) * attachedHand.transform.rotation;
                }
                else
                {
                    //Debug.Log("it's on the leftt hand!");
                    transform.rotation = Quaternion.AngleAxis(-45f, attachedHand.transform.right) * attachedHand.transform.rotation;
                }
                */
#if GEAR_VR
                transform.position = attachedHand.transform.position + transform.TransformDirection(gearVROffset);
                if (trajToWatch)
                {
                    transform.LookAt(trajToWatch.transform);
                }
#elif RIFT
                
                if (isOnRightHand)
                {
                    transform.position = attachedHand.transform.position;
                }
                else
                {
                    transform.position = attachedHand.transform.position;
                }
                
#endif
            }
            else
            {

#if GEAR_VR
                
                rb.MovePosition(attachedHand.transform.position + (attachedHand.transform.TransformDirection(gearVROffset)));
                if (!trajToWatch.hittableComponent.hitGround && !trajToWatch.isCaught)
                {
                    rb.MoveRotation(Quaternion.AngleAxis((isOnRightHand ? 135f : -45f), attachedHand.transform.right) * Quaternion.LookRotation(trajToWatch.transform.position - transform.position));
                }
                else
                {
                    Quaternion targetRotation = attachedHand.transform.rotation;
                    if (isOnRightHand)
                    {
                        targetRotation = Quaternion.AngleAxis(135f, attachedHand.transform.right) * targetRotation;
                        //Debug.Log("it's on the right hand!");
                    }
                    else
                    {
                        //Debug.Log("it's on the leftt hand!");
                        targetRotation = Quaternion.AngleAxis(-45f, attachedHand.transform.right) * targetRotation;
                    }

                    if(trajToWatch.isCaught)
                    {
                        targetRotation = Quaternion.AngleAxis(180f,attachedHand.transform.forward) * targetRotation;
                    }

                    rb.MoveRotation(Quaternion.Lerp(transform.rotation,targetRotation, Time.deltaTime*5f));
                }
#elif RIFT
                if (isOnRightHand)
                {
                    //Debug.Log("it's on the right hand!");
                                       
                    rb.MoveRotation(Quaternion.AngleAxis(-90f, attachedHand.transform.forward) * (Quaternion.AngleAxis(165f, attachedHand.transform.right) * attachedHand.transform.rotation));
                    rb.MovePosition(attachedHand.transform.position);
                }
                else
                {
                    //Debug.Log("it's on the leftt hand!");

                    rb.MoveRotation(Quaternion.AngleAxis(90f, attachedHand.transform.forward) * Quaternion.AngleAxis(-15f, attachedHand.transform.right) * attachedHand.transform.rotation);
                    rb.MovePosition(attachedHand.transform.position);
                }
                
#endif
            }

#elif STEAM_VR
            if (attachedToFullBat)
            //if (GameController.fullBatterMode && HandManager.genericTracker && HandManager.genericTracker.IsValid())
            {
                MatchToGenerictracker();
                return;
            }
            /*
            if (attachedHand == null)
            {
                if (HandManager.genericTracker && HandManager.genericTracker.IsValid())
                {
                    MatchToGenerictracker();
                }
                return;
            }
            */

            //this is something that's been added for the new hand finding means.
            if (attachedHand)
            {
                if (shouldBeOnLeft)
                {
                    if (HandManager.currentLeftCustomHand && attachedHand != HandManager.currentLeftCustomHand)
                    {
                        //attachedHand.showRenderModel = true;
                        attachedHand.RefreshHandState();
                        attachedHand = HandManager.currentLeftCustomHand;
                    }
                }
                else
                {
                    if (HandManager.currentRightCustomHand && attachedHand != HandManager.currentRightCustomHand)
                    {
                        //attachedHand.showRenderModel = true;
                        attachedHand.RefreshHandState();
                        attachedHand = HandManager.currentRightCustomHand;
                    }
                }
            }
            if (!inPlace)
            {
                //inPlace = true;//Greg: I commented this out because it was causing the glove to not perfectly align with the AI glove when posessing someone. Maybe this was never necessary to begin with.
                if (attachedHand)
                {
                    if (isOnRightHand)
                    {
                        transform.rotation = Quaternion.AngleAxis(180f, attachedHand.transform.right) * attachedHand.transform.rotation;
                    }
                    else
                    {
                        transform.rotation = attachedHand.transform.rotation;
                    }

                    transform.position = attachedHand.transform.position;
                }
            }
            else
            {
                if (rb && attachedHand)
                {
                    if (isOnRightHand)
                    {
                        rb.MoveRotation(Quaternion.AngleAxis(180f, attachedHand.transform.right) * attachedHand.transform.rotation);
                    }
                    else
                    {
                        rb.MoveRotation(attachedHand.transform.rotation);
                    }
                    rb.MovePosition(attachedHand.transform.position);
                }
            }
#endif
        }

        if(ballInCatchWindow != null)
        {
            if (attachedHand != null)
            {
                attachedHand.Vibrate(2000);
                if (attachedHand.GetUseDown() || attachedHand.GetSqueezeDown())
                {
                    //Debug.Log("Catch ball in catch Window A");
                    CatchBall(ballInCatchWindow);
                }
            }
           //catch
        }
    }

    private void MatchToGenerictracker()
    {
        if (!HandManager.genericTracker)
        {
            return;
        }

        Transform gripTransform = null;
        switch (GameController.batType)
        {
            case GameController.FullBatType.HTCRacquet:
                gripTransform = HandManager.genericTracker.gloveGripRacket;
                break;
            case GameController.FullBatType.TrackerAtBase:
                gripTransform = HandManager.genericTracker.gloveGripBase;
                break;
            case GameController.FullBatType.TrackerAtTip:
                gripTransform = HandManager.genericTracker.gloveGripTip;
                break;
        }

        if (!inPlace)
        {
            if (gripTransform)
            {
                //racket
                transform.rotation = gripTransform.rotation;
                transform.position = gripTransform.position;
            }
            else
            {
                //tracker at base
                transform.rotation = HandManager.genericTracker.transform.rotation;
                transform.position = HandManager.genericTracker.transform.position;
            }
        }
        else
        {
            if (gripTransform)
            {
                //racket
                rb.MovePosition(gripTransform.position);
                rb.MoveRotation(gripTransform.rotation);
            }
            else
            {
                //tracker at base
                rb.MovePosition(HandManager.genericTracker.transform.position);
                rb.MoveRotation(HandManager.genericTracker.transform.rotation);
            }
        }
    }

}



﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugTeamLoader : MonoBehaviour {

    public enum HumanPlayerMode
    {
        NONE,
        FULL,
        DEFENSE_ONLY,
        OFFENSE_ONLY,
        BROADCAST,
        TUTORIAL,
    }

    private bool forceSettings = true;
    public TeamInfo homeTeam;
    public TeamInfo awayTeam;
    public int numInnings = 6;
    public HumanPlayerMode mode;

    void Awake()
    {
        if (forceSettings)
        {
            FieldManager.SetHomeTeam(homeTeam);
            FieldManager.SetAwayTeam(awayTeam);
            FieldManager.SetNumInnings(numInnings);
            if (FieldManager.currentHumanPlayerRole == FieldManager.HumanPlayerRole.NONE)
            {
                switch (mode)
                {
                    case HumanPlayerMode.FULL:
                        FieldManager.PrepForFullGame();
                        break;
                    case HumanPlayerMode.OFFENSE_ONLY:
                        FieldManager.PrepForOffenseOnly();
                        break;
                    case HumanPlayerMode.DEFENSE_ONLY:
                        FieldManager.PrepForDefenseOnly();
                        break;
                    case HumanPlayerMode.BROADCAST:
                        FieldManager.PrepForBroadcast();
                        break;
                    case HumanPlayerMode.TUTORIAL:
                        FieldManager.PrepForTutorial();
                        break;
                }
            }
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using System.Linq;

/// <summary>
/// This class contains information about the field that AI coordinators can reference
/// </summary>
[RequireComponent(typeof(CalloutManager))]
public class FieldManager : MonoBehaviour
{
    public static TeamInfo homeTeam
    {
        get; private set;
    }
    public static TeamInfo awayTeam
    {
        get; private set;
    }

    public static TeamInfo otherTeam(TeamInfo mine)
    {
        if (mine == homeTeam) return awayTeam;
        if (mine == awayTeam) return homeTeam;
        return null;
    }
       

    public bool canSelectPlayers
    {
        get { return HumanPosessionManager.currentlyTetheredPlayer == null; }
    }

    private static ColorPalette colorPalette
    {
        get
        {
            if (_colorPalette == null)
            {
                return ColorPalette.defaultPalette;
            }
            return _colorPalette;
        }
        set
        {
            _colorPalette = value;
        }
    }
    private static ColorPalette _colorPalette;
    public ColorPalette palette;

    public static Color ballColor
    {
        get { return colorPalette.color_1; }
    }

    public static Color baseColor
    {
        get { return colorPalette.color_4; }
    }

    public enum UmpireCall
    {
        STRIKE,
        BALL,
        WILD,
        OUT,
        FOUL,
        HOMERUN,
        FAIR,
    }

    public OffenseCoordinator offense;
    public DefenseCoordinator defense;
    public BroadcastCoordinator broadcast;
    public DugoutCoordinator dugout;
    float maxAnnouncerBoothTime = 17f;

    //[HideInInspector]
    public LocalRoot worldScaler;
    [SerializeField]
    protected FieldPitchersMound pitchersMound;
    //public FieldTransitionEffectManager transition;
    [SerializeField]
    protected BallGate beginStrikeZoneGate_Large;
    [SerializeField]
    protected BallGate beginStrikeZoneGate_Small;
    public BallGate strikeGate
    {
        get
        {
            if (offense.humanControlled) return beginStrikeZoneGate_Small; //ai pitcher should bat to small gate for human batter
            if (defense.humanControlled) return beginStrikeZoneGate_Large; //ai batter should use generous strike gate for human pitcher
            return beginStrikeZoneGate_Small; //ai should use small gate against ai
        }
    }
    public void ShowHideStrikeGate(bool show) {
        beginStrikeZoneGate_Large.ShowHide_Box(false);
        beginStrikeZoneGate_Small.ShowHide_Box(false);
        strikeGate.ShowHide_Box(show);
    }
    public void ShowHideStrikeMarker(bool show)
    {
        beginStrikeZoneGate_Large.ShowHide_Marker(false);
        beginStrikeZoneGate_Small.ShowHide_Marker(false);
        strikeGate.ShowHide_Marker(show);
    }

    //public BallGate endStrikeZoneGate;

    protected static bool drawDebugLines = true;
    protected static CalloutManager callout;
    public FieldBase basePrefab;
    public Transform[] basePlacements;
    public int numBases
    {
        get
        {
            if (basePlacements == null) return 0;
            return basePlacements.Length;
        }
    }

    public Transform homeDugout;
    public Transform awayDugout;

    public const int maxOutsPerFrame = 3;
    private const int maxStrikesPerOut = 3;
    private const int maxFoulsPerOut = 4;
    public const int maxBallsPerWalk = 4;

    private const int numTeams = 2;
    public static int numInningsPerGame
    {
        get
        {
            if (_numInningsPerGame == 0) return 6;
            return _numInningsPerGame;
        }
        private set
        {
            _numInningsPerGame = value;
        }
    }
    private static int _numInningsPerGame;

    public enum InningFrame { Top, Middle, Bottom };
    public static InningFrame currentInningFrame;
    public static InningFrame previousInningFrame = InningFrame.Middle;


    public static TeamInfo currentDefensiveTeam
    {
        get { switch (currentInningFrame) { case InningFrame.Top: return homeTeam; case InningFrame.Bottom: return awayTeam; } return homeTeam; }
    }
    public static TeamInfo currentOffensiveTeam
    {
        get { switch (currentInningFrame) { case InningFrame.Top: return awayTeam; case InningFrame.Bottom: return homeTeam; } return awayTeam; }
    }

    public Color homeTeamColor { get { if (!homeTeam) return Color.white; return homeTeam.homeColor; } }
    public Color awayTeamColor { get { if (!awayTeam) return Color.white; return awayTeam.awayColor; } }


    public Color defenseColor
    {
        get
        {
            if (currentInningFrame == InningFrame.Top) return homeTeamColor;
            if (currentInningFrame == InningFrame.Bottom) return awayTeamColor;
            return Color.white;
        }
    }
    public Color offenseColor
    {
        get
        {
            if (currentInningFrame == InningFrame.Bottom) return homeTeamColor;
            if (currentInningFrame == InningFrame.Top) return awayTeamColor;
            return Color.white;
        }
    }

    public static int homeTeamRuns
    {
        get { return m_homeTeamRuns; }
        private set
        {
            m_homeTeamRuns = value;
        }
    }
    private static int m_homeTeamRuns = 0;

    public static int awayTeamRuns
    {
        get { return m_awayTeamRuns; }
        private set
        {
            m_awayTeamRuns = value;
            if (currentInning >= (numInningsPerGame - 1) && homeTeamRuns < awayTeamRuns && currentInningFrame == InningFrame.Bottom)
            {
                CallEndOfGame();
            }
        }
    }
    private static int m_awayTeamRuns = 0;

    public static int currentInning
    {
        get { return m_inning; }
        private set
        {
            Debug.Log("SEET INNINGGGG " + value);
            m_inning = value;
            if (m_inning >= numInningsPerGame)
            {
                CallEndOfGame();
            }
        }
    }
    private static int m_inning = 0;

    public static int teamAtBat
    {
        get { return m_teamAtBat; }
        private set
        {
            m_teamAtBat = value;
            if (m_teamAtBat >= numTeams)
            {
                m_teamAtBat = 0;
                currentInning++;
                Debug.Log("INNING IS NOW: " + currentInning);
            }
        }
    }
    private static int m_teamAtBat = 0;

    public static int runsThisFrame
    {
        get { return m_runsThisFrame; }
        private set
        {
            if (value > m_runsThisFrame)
            {
                if (teamAtBat % 2 == 0) awayTeamRuns++;
                else homeTeamRuns++;
            }
            m_runsThisFrame = value;
            if (OnScoreChange != null) OnScoreChange(teamAtBat, currentInning, m_runsThisFrame);
        }
    }
    private static int m_runsThisFrame;

    public static int currentStrikes
    {
        get { return m_strikes; }
        private set
        {
            m_strikes = value;
            if (OnStrikesChange != null) OnStrikesChange.Invoke(m_strikes);
            if (m_strikes >= maxStrikesPerOut)
            {
                if (Instance && Instance.offense)
                {
                    //FieldManager.Instance.announcerMinimap.DestroyMiniBatter();
                    Instance.offense.ReactToBattingOut(OutType.STRIKEOUT);
                }
                CalloutOut(null, OutType.STRIKEOUT);
            }
        }
    }
    private static int m_strikes = 0;
    public static int currentFouls
    {
        get { return m_fouls; }
        private set
        {
            m_fouls = value; if (OnFoulsChange != null) OnFoulsChange.Invoke(m_fouls);
            if (m_fouls == 0) return;
            if (currentStrikes < (maxStrikesPerOut - 1))
            {
                currentStrikes++;
            }
            if (currentFouls >= maxFoulsPerOut)
            {
                if (Instance && Instance.offense)
                    Instance.offense.ReactToBattingOut(OutType.FOULS);
                CalloutOut(null, OutType.FOULS);
            }
        }
    }
    private static int m_fouls = 0;
    public static int currentBalls
    {
        get { return m_balls; }
        private set
        {
            m_balls = value;
            if (OnBallsChange != null) OnBallsChange.Invoke(m_balls);
            if (m_balls >= maxBallsPerWalk) CalloutWalk();
        }
    }
    private static int m_balls = 0;
    public static int currentOuts
    {
        get { return m_outs; }
        private set
        {
            m_outs = value; if (OnOutsChange != null) OnOutsChange.Invoke(m_outs);
            if (m_outs >= maxOutsPerFrame)
            {
                CallSwitchSides();
            }
        }
    }
    private static int m_outs = 0;

    public static Action<int> OnStrikesChange;
    public static Action<int> OnFoulsChange;
    public static Action<int> OnBallsChange;
    public static Action<int> OnOutsChange;
    public static Action<int, int, int> OnScoreChange;

    public static Action<Transform, PlayerInfo> OnShowBatter;
    public static Action OnHideBatter;
    public static Action<OffensePlayer> OnRegisterRunner;
    public static Action<OffensePlayer> OnUnRegisterRunner;
    public static Action<DefensePlayer> OnRegisterDefensePlayer;
    public static Action<DefensePlayer> OnUnRegisterDefensePlayer;
    public static Action<Transform> OnBallCreated;
    public static Action OnBallDestroyed;
    public static Action<Queue<PlayerInfo>> OnBattingOrderUpdated;

    public static Action OnRoleChanged;

    protected FieldBase[] baseObjects;

    private static FieldManager m_Instance;
    public static FieldManager Instance
    {
        get
        {
            if (m_Instance == null)
            {
                m_Instance = FindObjectOfType<FieldManager>();
            }
            return m_Instance;
        }

        private set
        {
            m_Instance = value;
        }
    }

    public struct RankedBase
    {
        public int baseNo;
        public float value;
        public RankedBase(int b, float v)
        {
            baseNo = b; value = v;
        }
    }

    public struct RankedPosition
    {
        public Vector3 position;
        public int index;
        public float value;
        public RankedPosition(Vector3 p, float v, int i = 0)
        {
            position = p; value = v; index = i;
        }
    }


    void Awake()
    {
        Debug.Log("FIELD MANAGER AWAKE");
        if (Instance == null || Instance == this)
        {
            Instance = this;
            Debug.Log("FIELD MANAGER INSTANCE SET");
            if (palette != null)
            {
                colorPalette = palette;
            }
            worldScaler = GetComponentInParent<LocalRoot>();
        }
        else
        {
            Debug.Log("FIELD MANAGER INSTANCE WAS ALREADY SET");
            GameObject.Destroy(this);
        }
    }

    void OnEnable()
    {
        BatController.onBatHit.AddListener(BallHit);
        //OnFieldEvent += PrintFieldEvent;
    }

    void OnDisable()
    {
        BatController.onBatHit.RemoveListener(BallHit);
        OnFieldEvent -= PrintFieldEvent;
    }

    void Start()
    {
        this.transform.localPosition = Vector3.zero;
        callout = GetComponent<CalloutManager>();

        Application.SetStackTraceLogType(LogType.Log, StackTraceLogType.ScriptOnly);
        Application.SetStackTraceLogType(LogType.Error, StackTraceLogType.ScriptOnly);

        if (baseObjects == null) CreateFieldBases();
        InitializeGameStats();

        if (offense) offense.Initialize();
        if (defense) defense.Initialize();
        if (broadcast) broadcast.Initialize();
        if (dugout) dugout.Initialize();

        homeTeam.InitializeBattingQueue();
        awayTeam.InitializeBattingQueue();

        //use static role by default, then instance role

        StartOpenningCeremony();
    }

    protected virtual void Update()
    {

        if (Input.GetKeyDown(KeyCode.R))
        {
            InstantResetAll();
        }

        //if a ball manages to get below the ground, destroy it
        if (ballInPlay)
        {
            if ((WorldToLocalPosition(ballInPlay.transform.position).y < -1 && ballInPlay.isHeld == false)
                || (ballInPlay.transform.position.magnitude > 500))
            {

                DestroyBallInPlay();
            }
        }
    }

    void LateUpdate()
    {
        UpdateStateModel();
    }

#region State Model Management
    [System.Serializable]
    public struct FieldState
    {
        public FieldState(float[] runners, bool[] basesOcc, bool[] basesDef, bool defReady, bool offready)
        {
            runnerTimeToBase = runners;
            baseOccupied = basesOcc;
            baseDefended = basesDef;
            defenseReady = defReady;
            offenseReady = offready;
        }
        public float[] runnerTimeToBase { get; private set; }
        public bool[] baseOccupied { get; private set; }
        public bool[] baseDefended { get; private set; }


        public bool offenseReady;
        public bool defenseReady;
    }
    public FieldState fieldState { get; protected set; }


    public void InstantResetAll()
    {
        //remove all runners, hide the batter
        offense.InstantReset();

        defense.InstantReset();

        //make sure players are dressed for defense team. Drop at pitch positions
        foreach (AIFieldPlayer p in defensivePlayers)
        {
            p.InstantReset();
        }
        
        //MARC: shouldn't be any runners left, this is redundant but I'm leaving it in just in case
        foreach (AIFieldPlayer p in offensivePlayers)
        {
            p.InstantReset();
        }

        if (ballInPlay != null)
        {
            DestroyBallInPlay();
        }

        //will apply proper jersey and gear
        //HumanPosessionManager.RefreshJersey(); //Is this Necessary???? Bradley 9-20-19

        if (OnFieldEvent != null) OnFieldEvent.Invoke(null, FieldPlayType.NONE);
        
    }

    void UpdateStateModel()
    {
        bool[] baseOccupation = new bool[numBases];
        float[] runnerTimes = new float[numBases];
        bool[] baseDefense = new bool[numBases];
        for (int i = 0; i < numBases; i++)
        {
            baseOccupation[i] = GetBase(i).occupiedByRunner;
            runnerTimes[i] = -1;
        }
        foreach (OffensePlayer runner in offensivePlayers)
        {
            if (runner.state != AIFieldPlayer.FieldPlayerAction.RUN) continue;

            int advanceBase = runner.baseLastTouched + 1;
            float t_Advance = runner.GetTimeToRunToBase(advanceBase);

            if (runnerTimes[advanceBase] < 0 || runnerTimes[advanceBase] > t_Advance)
                runnerTimes[advanceBase] = t_Advance;
            if (!runner.forcedRun)
            {
                int retreatBase = runner.baseLastTouched;
                float t_Retreat = runner.GetTimeToRunToBase(retreatBase);
                if (runnerTimes[retreatBase] < 0 || runnerTimes[retreatBase] > t_Retreat)
                    runnerTimes[retreatBase] = t_Retreat;
            }
        }

        foreach (FieldBase fBase in baseObjects)
        {
            if (fBase.defendedWithBall)
            {
                baseDefense[fBase.baseNum] = true;
            }
        }

        fieldState = new FieldState(
            runnerTimes,
            baseOccupation,
            baseDefense,
            AllDefendersAtPitchPosition(),
            AllRunnersOnBase()
            );

    }
#endregion

#region WorldScaling
    public static Vector3 LocalToWorldPosition(Vector3 localPos)
    {
        if (Instance == null || Instance.worldScaler == null) return localPos;
        return Instance.worldScaler.LocalToWorldPosition(localPos);

    }

    public static Vector3 LocalToWorldDirection(Vector3 localDir)
    {
        if (Instance == null || Instance.worldScaler == null) return localDir;

        return Instance.worldScaler.LocalToWorldDirection(localDir);
    }

    public static Vector3 WorldToLocalPosition(Vector3 worldPos)
    {
        if (Instance == null || Instance.worldScaler == null) return worldPos;

        return Instance.worldScaler.WorldToLocalPosition(worldPos);
    }

    public static Vector3 WorldToLocalDirection(Vector3 worldDir)
    {
        if (Instance == null || Instance.worldScaler == null) return worldDir;

        return Instance.worldScaler.WorldToLocalDirection(worldDir);
    }

    public static void DrawDebugLine(Vector3 localPoint_a, Vector3 localPoint_b, Color color, float t = -1)
    {
        if (t < 0) t = Time.deltaTime;
        Debug.DrawLine(
            LocalToWorldPosition(localPoint_a),
            LocalToWorldPosition(localPoint_b),
            color,
            t);
    }
#endregion

#region Base management
    /// <summary>
    /// Gets a base position in local space.
    /// </summary>
    /// <param name="baseNo">The name of the base</param>
    /// <returns>Local-space position for the given base</returns>
    public Vector3 GetBasePosition(int baseNo)
    {
        if (baseNo >= basePlacements.Length || baseNo < 0) baseNo = numBases - 1;

        if (basePlacements[baseNo] == null)
        {
            Debug.LogWarning("Could not find base " + baseNo);
            return Vector3.zero;
        }
        //Debug.Log("get base position for base "+baseNo+" at "+basePlacements[baseNo].position);
        return basePlacements[baseNo].localPosition;
    }

    public Vector3 GetBaseDefensePosition(int baseNo)
    {
        if (baseNo >= basePlacements.Length || baseNo < 0) baseNo = numBases - 1;

        if (basePlacements[baseNo] == null)
        {
            Debug.LogWarning("Could not find base " + baseNo);
            return Vector3.zero;
        }
        //Debug.Log("get base position for base "+baseNo+" at "+basePlacements[baseNo].position);
        Transform baseTransform = basePlacements[baseNo];
        Vector3 offset = Vector3.Lerp(baseTransform.forward, -baseTransform.right, .5f) * 1.5f;

        if (baseNo == numBases - 1) offset = Vector3.back * 1.5f;

        return baseTransform.localPosition + offset;
    }

    public Vector3 GetHomePlatePosition()
    {
        return GetBasePosition(numBases - 1);
    }

    public FieldBase GetHomePlate()
    {
        return GetBase(numBases - 1);
    }

    public FieldBase GetBase(int i)
    {
        if (baseObjects == null) CreateFieldBases();
        if (i < 0 || i >= numBases) return null;
        return baseObjects[i];
    }

    public FieldBase[] GetBases()
    {
        return baseObjects;
    }

    public Transform[] GetBaseTransforms()
    {
        return baseObjectTransforms;
    }

    Transform[] baseDefenseTransforms;
    public Transform[] GetBaseDefensePositions()
    {
        if (baseDefenseTransforms == null)
        {
            baseDefenseTransforms = new Transform[numBases];
            for (int i = 0; i < numBases; i++)
            {
                GameObject def = new GameObject("Base_" + i + "_defense");
                def.transform.SetParent(GetBase(i).transform);
                def.transform.position = GetBaseDefensePosition(i);
                baseDefenseTransforms[i] = def.transform;
            }
        }
        return baseDefenseTransforms;
    }

    Vector3[] basePositions;
    public Vector3[] GetBasePositions()
    {
        if (basePositions == null)
        {
            basePositions = new Vector3[numBases];
            for (int i = 0; i < numBases; i++)
            {
                basePositions[i] = GetBasePosition(i);
            }
        }
        return basePositions;
    }

    public Vector3 GetPitchersMoundPosition()
    {
        return pitchersMound.transform.localPosition;
    }

    private Transform[] baseObjectTransforms;
    /// <summary>
    /// Instantiates FieldBase objects where base markers were placed
    /// </summary>
    void CreateFieldBases()
    {
        baseObjects = new FieldBase[numBases];
        baseObjectTransforms = new Transform[numBases];
        for (int i = 0; i < numBases; i++)
        {
            if (basePlacements[i] != null)
            {
                FieldBase newBase = GameObject.Instantiate(basePrefab, basePlacements[i].position, basePlacements[i].rotation, this.transform.parent);


                //newBase.GetComponent<Collider> ().enabled = true;
                newBase.SetBaseNumber(i, (i == numBases - 1));
                newBase.OnRunnerTouchBase += RecordRunnerOnBase;
                baseObjects[i] = newBase;
                baseObjectTransforms[i] = newBase.transform;
            }
        }
    }

    void RecordRunnerOnBase(int baseNo, FieldBase fBase, OffensePlayer runner)
    {
        if (fBase.defendedWithBall) runner.NotifyOfOut(OutType.DEFENDED);
        else if (baseNo == Instance.numBases - 1 && runner.baseTarget_Immediate == baseNo) runner.NotifyOfRunCompletion();
    }

    /// <summary>
    /// If all runners were to run to their current target base, then continue forward, what is the soonest a runner would arrive at this base?
    /// if there are no runners, returns float.MaxValue
    /// </summary>
    /// <param name="baseNo"></param>
    /// <returns></returns>
    public float GetTimeToRunnerOnBase(int baseNo, out bool forced)
    {
        forced = false;
        float minTime = float.MaxValue;
        // Debug.Log("GET TIME TO RUNNER ON BASE "+offensivePlayers.Count);
        foreach (OffensePlayer runner in offensivePlayers)
        {
            //if isBeyondBase, assume they will never run back to it
            bool isBeyondBase = (runner.forcedRun ? runner.baseTarget_Eventual : runner.baseTarget_Immediate) > baseNo;
            if (isBeyondBase) continue;

            bool isForced = false;

            float estRunTime = 0;


            //get the time it will take them to get to their current target

            //if this player is being forced, take time to get to forced base into account
            if (runner.forcedRun)
            {
                estRunTime += runner.GetTimeToRunToPosition(GetBasePosition(runner.baseTarget_Immediate));
                estRunTime += runner.GetTimeToRunBetweenBases(runner.baseTarget_Immediate, runner.baseTarget_Eventual);
                estRunTime += runner.GetTimeToRunBetweenBases(runner.baseTarget_Eventual, baseNo);
                isForced = true;
            }

            //if the base in question is farther than the runners target
            else if (baseNo > runner.baseTarget_Immediate)
            {
                estRunTime += runner.GetTimeToRunToPosition(GetBasePosition(runner.baseTarget_Immediate));
                estRunTime += runner.GetTimeToRunBetweenBases(runner.baseTarget_Immediate, baseNo);

            }

            //if the base in question is the runner's target
            else if (baseNo == runner.baseTarget_Immediate)
            {
                estRunTime += runner.GetTimeToRunToPosition(GetBasePosition(runner.baseTarget_Immediate));

            }


            if (estRunTime < minTime)
            {
                minTime = estRunTime;
                forced = isForced;
            }
        }
        //Debug.Log("GOT TIME TO RUNNER ON BASE " + minTime);

        return minTime;
    }



#endregion

#region Ball Management
    public static Action<hittable, FieldPlayType> OnFieldEvent;

    public hittable ballInPlay;

    void BallHit(hittable ball)
    {
        AnnounceBallHit(ball);
    }


    /// <summary>
    /// Waits before announcing new ball to AI Coordinators to give ball velocity a few frames to settle.
    /// Also checks for home runs and foul balls and informs AI Coordinators
    /// </summary>
    /// <param name="ball"> new hittable in play</param>
    /// <param name="delay"> how long to wait before firing OnBallHit event</param>
    /// <returns></returns>
    IEnumerator AnnounceBallAfterFixedUpdate(hittable ball)
    {
        //Debug.Log("========================================= announce ball after delay.");
        yield return new WaitForFixedUpdate();
        ClearCallouts();
        ball.hasEverHitGround = false;

        bool ballIsJudged = false;
        UmpireCall ballJudgement = UmpireCall.FAIR;
        float timeAtHit = Time.time;


        //if the ball is obviously going to be foul, call it out
        TrajectoryInfo trajectory = new TrajectoryInfo(ball.GetComponent<Rigidbody>());
        Vector3 landingPos = trajectory.groundHitPosition;
        if (HomeRunLevelController.instance.isPointFoulBall(landingPos))
        {
            ballJudgement = UmpireCall.FOUL;
            ballIsJudged = true;
        }

        //if the ball wasn't immediately judged to be a foul, declare a fair play
        if (!ballIsJudged)
        {
            if (OnFieldEvent != null) OnFieldEvent.Invoke(ball, FieldPlayType.STANDARD);

            CalloutFairBall();
        }

        while (ball != null && !ballIsJudged)
        {
            /*if (HomeRunLevelController.instance.isPointFoulBall(ball.transform.position))
                ballJudgement = UmpireCall.FOUL;
            else*/
            //The above was making it so that if a ball lands in the bounds but then rolls out, it's counted as foul. This is not good.
            ballJudgement = UmpireCall.FAIR;

            if (ball.hasTouchedDefender)
            {
                ballIsJudged = true;
            }

            //the ball has touched ground, it is is in the outfield a judgement can be made immediately, otherwise it could still go either way
            if (ball.hitGround)
            {
                //ball is past 1st and 3rd base, judge it immediately
                Vector3 ballPosFlat = ball.transform.position;
                ballPosFlat.y = 0;
                if (Vector3.Distance(GetHomePlatePosition(), ballPosFlat) > 40)
                {
                    ballIsJudged = true;
                }
                //ball is in infield, don't judge it conclusively yet (unless it's foul, which isn't the official rule)
                else
                {
                    if (ballJudgement == UmpireCall.FOUL)
                    {
                        ballIsJudged = true;
                    }
                }
            }
            //the ball has not touched ground, but a call can still be made if it has been caught
            //if the ball has been caught without touching the ground, it can be judged conclusively
            if (ball.hasBeenCaught)
            {
                ballJudgement = UmpireCall.FAIR;
                ballIsJudged = true;
            }


            if (HomeRunLevelController.instance.isHomeRun(ball.transform.position))
            {
                ballJudgement = UmpireCall.HOMERUN;
                ballIsJudged = true;
            }
            //            Debug.Log("Judging ball " + ballJudgement + " isJudged " + ballIsJudged + " " + ball.hasBeenCaught);

            yield return null;
            if (Time.time - timeAtHit > 20)
            {
                Debug.LogWarning("Ball is taking a long time to be judged, breaking loop");
                break;
            }
        }

        yield return null;

        switch (ballJudgement)
        {
            case UmpireCall.FAIR:
                break;
            case UmpireCall.FOUL:
                CalloutFoul();
                //ExplodeBall(1);
                if (OnFieldEvent != null) OnFieldEvent.Invoke(ball, FieldPlayType.FOUL);
                break;
            case UmpireCall.HOMERUN:
                CalloutHomeRun();
                if (OnFieldEvent != null) OnFieldEvent.Invoke(ball, FieldPlayType.HOMERUN);
                break;
        }
    }

    public hittable SpawnFreshBall(bool destroyExisting = true)
    {
//        Debug.Log("Spawn Fresh Ball.");
        GameObject ballShootPrefab = FindObjectOfType<ballController>().ballPrefab;
        GameObject ballObj = GameObject.Instantiate(ballShootPrefab);
        hittable ball = ballObj.GetComponent<hittable>();
        if(destroyExisting) DestroyBallInPlay();
        ballInPlay = ball;
        ball.OnBallDestroy += RemoveBallFromPlay;
        ball.GetComponent<SphereCollider>().enabled = true;
        if (OnBallCreated != null) OnBallCreated(ball.transform);
        return ball;
    }

    public void AnnouncePitch()
    {
        //Debug.Log("PITCH!!!");
        ballInPlay.ResetFlags();
        if (beginStrikeZoneGate_Large)
        {
            beginStrikeZoneGate_Large.Reset();
        }
        if (beginStrikeZoneGate_Small)
        {
            beginStrikeZoneGate_Small.Reset();
        }

        //when do we need to evaluate the pitch?
        //caught by catcher
        //hit ground
        //left field
        //explodes
        //hit by bat


        ballInPlay.OnHitGround +=           JudgePitch;
        ballInPlay.OnCaughtByDefender +=    JudgePitch;
        ballInPlay.OnBatHit +=              JudgePitch;
        ballInPlay.OnBallDestroy +=         JudgePitch;

        OnFieldEvent(ballInPlay, FieldPlayType.PITCH);

        trailParticleManager tempTrail = ballInPlay.GetComponent<trailParticleManager>();
        if (tempTrail)
        {
            tempTrail.OnHit();
        }
    }

    void PrintFieldEvent(hittable ball, FieldPlayType play)
    {
#if UNITY_EDITOR
        Debug.Log("<color=yellow>FIELD PLAY  : " + play.ToString() + "</color>");
#endif
    }

    void JudgePitch(hittable pitch)
    {

        bool strike = false;
        bool ball = false;

        //Debug.Log("CHECK FOR STRIKE " + defense.humanControlled + " " + strikeGate.IsPassed + "  " + strikeGate.strikeDetected);

        if (pitch.GetWasHitByBat())
        {
            // the ball was hit, no need to judge the pitch
        }
        else if (strikeGate.strikeDetected)
        {
            //ball was not hit, and passed through the strike gate, this is a strike
            strike = true;
        }
        else if (offense.SwingDetected())
        {
            //ball was not hit and did not pass through strike gate, but a swing was detected, this is a strike
            strike = true;
        }
        else
        {
            // the ball did not pass the gate, was not hit, and the batter did not swing, this is a ball
            ball = true;
        }

        if (strike)
        {
            CalloutStrike(ballInPlay);

        }
        else if (ball)
        {
            CalloutBall();
        }

        pitch.OnHitGround -= JudgePitch;
        pitch.OnCaughtByDefender -= JudgePitch;
        pitch.OnBatHit -= JudgePitch;
        pitch.OnBallDestroy -= JudgePitch;
    }

    void AnnounceBallHit(hittable ball)
    {
        Debug.Log("BALL HIT " + ball);
        if (ball == null) return;
        ballInPlay = ball;
        ball.OnBallDestroy += RemoveBallFromPlay;
        StartCoroutine(AnnounceBallAfterFixedUpdate(ball));

    }
    void RemoveBallFromPlay(hittable ball)
    {
        if (ball == null) return;
        ball.OnBallDestroy -= RemoveBallFromPlay;
        if (ballInPlay == ball) ballInPlay = null;
        if(OnBallDestroyed != null) OnBallDestroyed();
    }

    void DestroyBallInPlay()
    {
        if (ballInPlay != null)
        {
            if (OnBallDestroyed != null) OnBallDestroyed();
            GameObject.Destroy(ballInPlay.gameObject);
        }
    }

    public void ForceDestroyBallInPlay()
    {
        //Debug.Log("ForceDestroyBallInPlay");
        DestroyBallInPlay();
    }

    public void ExplodeBall(float delay = 0)
    {
       // Debug.Log("Explode Ball.");
        if (delay > 0)
            StartCoroutine(DetonateBallRoutine(delay, true));
        else
            ExplodeBallImmediate();
    }

    private void ExplodeBallImmediate()
    {
        //Debug.Log("ExplodeBallImmediate.");
        if (ballInPlay != null)
        {
            OneShotEffectManager.SpawnEffect("explosion", ballInPlay.transform.position, Quaternion.identity);
            ForceDestroyBallInPlay();
        }
    }

    private IEnumerator DetonateBallRoutine(float delay, bool cancelIfCaught = false)
    {
        Debug.Log("DetonateBall Routine.");
        yield return new WaitForSeconds(delay);
        if (cancelIfCaught && ballInPlay != null && (ballInPlay.hasBeenCaught || ballInPlay.isHeld)) { }//do not explode
        else ExplodeBallImmediate();
    }

    public bool ReadyForNewBall()
    {
        if (offense && offense.runningPlay == FieldPlayType.HOMERUN)
        {
            return false;
        }

        if (defense && defense.runningPlay == FieldPlayType.HOMERUN)
        {
            return false;
        }

        //if (offense && offense.runningPlay == FieldPlayType.FOUL)
        //{
        //    return false;
        //}
        return ballInPlay == null;
    }

    public bool PitcherOnMoundWithBall()
    {
        if (pitchersMound == null)
        {
            return false;
        }
        return pitchersMound.PitcherOnMoundWithBall;

    }

    public bool PitcherOnMound()
    {
        if (pitchersMound == null)
        {
            return false;
        }
        return pitchersMound.PitcherOnMound;

    }

    public bool BallIsHeld()
    {
        if (!ballInPlay) return false;
        return ballInPlay.isHeld;
    }

    #endregion

    #region Game Settings
    public static void SetHomeTeam(TeamInfo home)
    {
        if(homeTeam != null)
        {
            homeTeam.OnBattingOrderUpdated -= BattingOrderChanged;
        }
        homeTeam = home;
        if (homeTeam != null)
        {
            homeTeam.OnBattingOrderUpdated += BattingOrderChanged;
        }
    }

    public static void SetAwayTeam(TeamInfo away)
    {
        if (awayTeam != null)
        {
            awayTeam.OnBattingOrderUpdated -= BattingOrderChanged;
        }
        awayTeam = away;
        if (awayTeam != null)
        {
            awayTeam.OnBattingOrderUpdated += BattingOrderChanged;
        }
    }

    public static void SetNumInnings(int innings)
    {
        numInningsPerGame = innings;
    }
    #endregion

    #region Team Events
    private static void BattingOrderChanged(TeamInfo info)
    {
        if (info == currentOffensiveTeam)
        {
            if (OnBattingOrderUpdated != null) OnBattingOrderUpdated.Invoke(info.battingQueue);
        }
    }

    #endregion

    #region AI Player Tracking

    public static LinkedList<OffensePlayer> offensivePlayers
    {
        get
        {
            if (m_offensivePlayers == null) m_offensivePlayers = new LinkedList<OffensePlayer>();
            return m_offensivePlayers;
        }
        protected set
        {
            m_offensivePlayers = value;
        }
    }
    private static LinkedList<OffensePlayer> m_offensivePlayers;

    public static List<DefensePlayer> defensivePlayers
    {
        get
        {
            if (m_defensivePlayers == null) m_defensivePlayers = new List<DefensePlayer>();
            return m_defensivePlayers;
        }
        protected set
        {
            m_defensivePlayers = value;
        }
    }
    private static List<DefensePlayer> m_defensivePlayers;


    public static void RegisterRunner(OffensePlayer fielder, int startBase = -1)
    {
        if (!offensivePlayers.Contains(fielder))
        {
            fielder.OnOut += CalloutOut;
            fielder.OnHome += CalloutCompletedRun;

            bool foundSpot = false;
            if (offensivePlayers.Count == 0)
            {
                offensivePlayers.AddFirst(fielder);
                foundSpot = true;
                if (OnRegisterRunner != null) OnRegisterRunner(fielder);
                return;
            }

            LinkedListNode<OffensePlayer> cur = offensivePlayers.First;
            if (cur.Value.baseLastTouched >= startBase)
            {
                offensivePlayers.AddFirst(fielder);
                foundSpot = true;
                if (OnRegisterRunner != null) OnRegisterRunner(fielder);
                return;
            }
            while (cur.Next != null && !foundSpot)
            {
                int curBase = cur.Value.baseLastTouched;
                int nextBase = cur.Next.Value.baseLastTouched;
                if (startBase >= curBase && startBase <= nextBase)
                {
                    offensivePlayers.AddAfter(cur, fielder);
                    foundSpot = true;
                    if (OnRegisterRunner != null) OnRegisterRunner(fielder);
                }
                cur = cur.Next;
            }
            if (!foundSpot)
            {
                offensivePlayers.AddLast(fielder);
                if (OnRegisterRunner != null) OnRegisterRunner(fielder);
            }
        }
    }

    public static void UnRegisterRunner(OffensePlayer fielder)
    {
        if (offensivePlayers.Contains(fielder))
        {
            if(OnUnRegisterRunner != null) OnUnRegisterRunner(fielder);
            fielder.OnOut -= CalloutOut;
            fielder.OnHome -= CalloutCompletedRun;
            offensivePlayers.Remove(fielder);
        }
    }

    public static void RegisterDefensePlayer(DefensePlayer fielder)
    {
        if (!defensivePlayers.Contains(fielder))
        {
            defensivePlayers.Add(fielder);
            fielder.OnFumble += CalloutFumble;
            if (OnRegisterDefensePlayer != null) OnRegisterDefensePlayer(fielder);
        }
    }

    public static void UnRegisterDefensePlayer(DefensePlayer fielder)
    {
        if (defensivePlayers.Contains(fielder))
        {
            defensivePlayers.Remove(fielder);
            fielder.OnFumble -= CalloutFumble;
            if(OnUnRegisterDefensePlayer != null) OnUnRegisterDefensePlayer(fielder);
        }
    }

    #endregion

    public bool PitchInProgress()
    {
        return defense.runningPlay == FieldPlayType.PITCH;

    }

    public bool DefenseReadyForNewBatter()
    {
        return defense._state == FieldPlayType.NONE || (defense._state == FieldPlayType.RETURN && PitcherOnMoundWithBall());
    }

    public bool BothTeamsReadyForPitch()
    {
        return AllRunnersOnBase() && PitcherOnMoundWithBall() && offense.isBatterReady;
        //return (offense.runningPlay == FieldPlayType.NONE) && defense.runningPlay == FieldPlayType.NONE;
    }

    public static OffensePlayer RunnerAheadOf(OffensePlayer runner)
    {
        LinkedListNode<OffensePlayer> node = offensivePlayers.Find(runner);
        if (node == null) return null;
        if (node.Next == null) return null;
        return node.Next.Value;
    }

    public static OffensePlayer RunnerBehind(OffensePlayer runner)
    {
        LinkedListNode<OffensePlayer> node = offensivePlayers.Find(runner);
        if (node == null) return null;
        if (node.Previous == null) return null;
        return node.Previous.Value;
    }

    public bool AllDefendersAtPitchPosition()
    {
        foreach (DefensePlayer fielder in defensivePlayers)
        {
            if (!fielder.atPitchPosition && !fielder.isTetheredToCamera)
            {
                return false;
            }
        }
        return true;
    }

    public bool AllRunnersOnBase()
    {
        foreach (OffensePlayer runner in offensivePlayers)
        {
            if (runner.state == AIFieldPlayer.FieldPlayerAction.OUT) continue; //Don't count players that are out
            if (runner.state != AIFieldPlayer.FieldPlayerAction.BASE)
            {
                return false;
            }
        }
        return true;
    }

    public bool RunnerIsTargetingBase(int baseNo)
    {
        foreach (OffensePlayer runner in offensivePlayers)
        {
            if (runner.state == AIFieldPlayer.FieldPlayerAction.OUT) continue;
            if (runner.baseTarget_Immediate == baseNo && runner.state == AIFieldPlayer.FieldPlayerAction.RUN)
            {
                return true;
            }
        }
        return false;
    }

#region Callouts

    private static void InitializeGameStats()
    {
        teamAtBat = 0;
        currentInning = 0;
        runsThisFrame = 0;
        homeTeamRuns = 0;
        awayTeamRuns = 0;

        currentOuts = 0;
        ResetStatsForNewBatter();
    }



    private static void ResetStatsForNewBatter()
    {
        currentStrikes = 0;
        currentBalls = 0;
        currentFouls = 0;
    }


    private static void CalloutPlayBall()
    {

    }

    private static void CalloutWalk()
    {
        callout.SpawnAnimatedMessage(UmpireCall.FAIR);
        ResetStatsForNewBatter();
        OnFieldEvent(null, FieldPlayType.WALK);

    }

    private static void CalloutFairBall()
    {
        Debug.Log("-----------------------------------------------------------------CALLOUT FAIR BALL");
        ResetStatsForNewBatter();

        callout.SpawnAnimatedMessage(UmpireCall.FAIR);
        /*
        if(HandManager.currentLeftCustomHand)
        {
            HandManager.currentLeftCustomHand.StartVibration();
        }

        if (HandManager.currentRightCustomHand)
        {
            HandManager.currentRightCustomHand.StartVibration();
        }
        */
    }

    private void CalloutHomeRun()
    {
        /*Debug.Log("Calling out home run");
        DarkTonic.MasterAudio.MasterAudio.PlaySound("reaction_homerun");
        if (homerunScoreboard != null) homerunScoreboard.PlayHomerunAnimation();
        callout.SpawnAnimatedMessage(UmpireCall.HOMERUN, true);
        if(fireworksSpawnPositions.Length > 0)
        {
            Debug.Log("There was at least one thingy in the list");
            for(int i = 0; i < fireworksSpawnPositions.Length; i++)
            {
                Debug.Log("Spawning in fireworks to " + fireworksSpawnPositions[i].gameObject.name);
                Instantiate(fireworksPrefab, fireworksSpawnPositions[i]);
            }
            for (int i = 0; i < 9; i++)
            {
                Debug.Log("Spawning fireworks noise in " + (0.5f * (float)i) + "s");
                DarkTonic.MasterAudio.MasterAudio.PlaySound("Fireworks" + (i % 3).ToString(), 1f, null, (0.5f * (float)i));
            }
        }*/
        callout.CalloutHomeRun();
    }

    public void ClearCallouts()
    {
        callout.ClearAnimatedMessages();
    }

    private static void CalloutCompletedRun(OffensePlayer runner)
    {
        //spawn a fun message over home plate
        runsThisFrame++;
    }

    public void CalloutStrike(hittable pitch)
    {
        currentStrikes++;
        callout.SpawnAnimatedMessage(UmpireCall.STRIKE);
    }

    private void CalloutFoul()
    {
        currentFouls++;
        callout.SpawnAnimatedMessage(UmpireCall.FOUL);

        //less than two strikes - foul counts as strike
    }

    public static void CalloutBall()//OffensePlayer batter)
    {
        currentBalls++;
        callout.SpawnAnimatedMessage(UmpireCall.BALL);

        if (currentBalls >= maxBallsPerWalk)
        {
            //the batter and all runners get to walk one base
        }
    }

    private static void CalloutFumble(DefensePlayer defender)
    {
        //callout.SpawnBillboardMessage("FUMBLE", defender.transform.position + Vector3.up);// * WorldScaler.worldScale);
    }

    private static void CalloutOut(AIFieldPlayer runner, OutType outType)
    {
        if (outType == OutType.INNING_OVER || outType == OutType.HIT_FOUL) return;

        
        if (runner)
        {
            //runner will spawn their own explosion effect, just need the sound
            DarkTonic.MasterAudio.MasterAudio.PlaySound3DAtTransform("reaction_happy", runner.transform);
            callout.PlaySoundForCall(UmpireCall.OUT);
        }
        else
        {
            callout.SpawnAnimatedMessage(UmpireCall.OUT);
        }

        ResetStatsForNewBatter();
        //DarkTonic.MasterAudio.MasterAudio.PlaySound3DAtTransform("reaction_happy", runner.transform);
        currentOuts++;

    }

    private static void CallSwitchSides()
    {
        Debug.Log("SWITCH SIDES");
        currentOuts = 0;

        teamAtBat++;
        //game may be determined to be over at this point


        if (Instance && currentInning <= (numInningsPerGame-1))
        {
            if(currentInning >= (numInningsPerGame - 1) && homeTeamRuns < awayTeamRuns && currentInningFrame == InningFrame.Top)
            {
                CallEndOfGame();
            }
            else
            {
                Debug.Log("Start Changeover Routine");
                runsThisFrame = 0;
                Instance.StartCoroutine(Instance.ChangeOverRoutine());
            }
        }
    }


    public static bool skipChangeover = false;

    IEnumerator ChangeOverRoutine()
    {
        skipChangeover = false;
        previousInningFrame = currentInningFrame;

        if (doInningChangeOver)
        {
            currentInningFrame = InningFrame.Middle;

            HumanPlayerRole targetRole = currentHumanPlayerRole;
            if (currentHumanPlayerRole == HumanPlayerRole.DEFENSE)
            {
                targetRole = (HumanPlayerRole.OFFENSE);
                OnRoleChanged.Invoke();
                ChangePlayerRole(HumanPlayerRole.BROADCAST);
            }
            else if (currentHumanPlayerRole == HumanPlayerRole.OFFENSE)
            {
                targetRole = (HumanPlayerRole.DEFENSE);
                OnRoleChanged.Invoke();
                ChangePlayerRole(HumanPlayerRole.BROADCAST);
            }
            yield return null;

            //wait for posession routine to finish
            while (HumanPosessionManager.transferRoutineInProgress)
            {
                yield return null;
            }
            DestroyBallInPlay();
            yield return null; //wait for ball to respawn in pitcher's glove
            OnFieldEvent(null, FieldPlayType.SWITCH);
            callout.PlaySoundForFieldPlayType(FieldPlayType.SWITCH);

            yield return new WaitForFixedUpdate(); //wait for the pitcher with the ball to be removed from the mound trigger

            //while (CalloutManager.currentVOClip.ActingVariation.IsPlaying)
            while (CalloutManager.isVOPlaying)
            {
                yield return null;
            }
            float timeInBooth = 0f;
            yield return new WaitForSeconds(1f);
            timeInBooth += 1f;
            DarkTonic.MasterAudio.PlaySoundResult currentSound;
            Debug.Log("About to play sound");
            if (previousInningFrame == InningFrame.Top) currentSound = DarkTonic.MasterAudio.MasterAudio.PlaySound("Mid_Inning");
            else currentSound = DarkTonic.MasterAudio.MasterAudio.PlaySound("End_Inning");
            if(currentSound == null)
            {
                Debug.Log("Current sound is null");
            }
            else if (currentSound.ActingVariation == null)
            {
                Debug.Log("Acting variation is null");
            }
            else
            {
                CalloutManager.currentVOClip = currentSound;
                while (currentSound.ActingVariation.IsPlaying)
                {
                    timeInBooth += Time.deltaTime;
                    yield return null;
                }
                yield return new WaitForSeconds(1f);
                timeInBooth += 1f;
                currentSound = DarkTonic.MasterAudio.MasterAudio.PlaySound("Sponsor");
                CalloutManager.currentVOClip = currentSound;
                while (currentSound.ActingVariation.IsPlaying)
                {
                    timeInBooth += Time.deltaTime;
                    yield return null;
                }
                yield return new WaitForSeconds(1f);
                timeInBooth += 1f;
                currentSound = DarkTonic.MasterAudio.MasterAudio.PlaySound("Back_to_game");
                CalloutManager.currentVOClip = currentSound;
                while (currentSound.ActingVariation.IsPlaying)
                {
                    timeInBooth += Time.deltaTime;
                    yield return null;
                }
            }

            //while ((!PitcherOnMoundWithBall() || timeInBooth < maxAnnouncerBoothTime) && !skipChangeover)
            //while (!PitcherOnMoundWithBall() && !skipChangeover)
            while ((!PitcherOnMoundWithBall() || timeInBooth < maxAnnouncerBoothTime) && !skipChangeover)
            {
                timeInBooth += Time.deltaTime;
                //Debug.Log("ChangeOver Wait " + PitcherOnMoundWithBall() + " " + skipChangeover);
                yield return null;
            }
            yield return new WaitForSeconds(1);
            if (previousInningFrame == InningFrame.Top)
            {
                currentInningFrame = InningFrame.Bottom;
            }
            else
            {
                currentInningFrame = InningFrame.Top;
            }
            previousInningFrame = InningFrame.Middle;
            OnRoleChanged.Invoke();
            if (currentHumanPlayerRole != targetRole) ChangePlayerRole(targetRole);
            while (!AllDefendersAtPitchPosition()
                && !skipChangeover)
            {
                //            Debug.Log("Waiting to reach positions " + AllDefendersAtPitchPosition());
                yield return null;
            }
        }
        /*if (previousInningFrame == InningFrame.Top)
        {
            currentInningFrame = InningFrame.Bottom;
        }
        else
        {
            currentInningFrame = InningFrame.Top;
        }
        previousInningFrame = InningFrame.Middle;*/
        InstantResetAll();
    }

    private static void StartOpenningCeremony()
    {
        if (Instance)
        {
            Instance.StartCoroutine(Instance.OpenningCeremonyRoutine());
        }
    }

    IEnumerator OpenningCeremonyRoutine()
    {
        HumanPlayerRole startingRole;
        startingRole = currentHumanPlayerRole;


        if (doOpenningCeremony)
        {
            callout.PlaySoundForFieldPlayType(FieldPlayType.OPENNING);
            if (OnFieldEvent != null) OnFieldEvent.Invoke(null, FieldPlayType.OPENNING);


           if(startingRole != HumanPlayerRole.NONE)
            {
                OnRoleChanged.Invoke();
                ChangePlayerRole(HumanPlayerRole.BROADCAST, true);
            }

            yield return new WaitForSeconds(15f);
        }
        OnRoleChanged.Invoke();
        ChangePlayerRole(startingRole, true);
        InstantResetAll();
        yield return null;
    }

    private static void StartClosingCeremony()
    {
        if (Instance)
        {
            Instance.StartCoroutine(Instance.ClosingCeremonyRoutine());
        }
    }

    IEnumerator ClosingCeremonyRoutine()
    {
        if (doClosingCeremony)
        {
            if (OnFieldEvent != null) OnFieldEvent.Invoke(null, FieldPlayType.CLOSING);
            if (currentHumanPlayerRole != HumanPlayerRole.NONE)
            {
                Instance.ChangePlayerRole(HumanPlayerRole.BROADCAST);
            }

            callout.PlaySoundForFieldPlayType(FieldPlayType.CLOSING);
            yield return new WaitForSeconds(15);
        }
    }


    public Vector3 GetDugoutForTeam(TeamInfo team)
    {
        if (team == homeTeam) return homeDugout.position;
        if (team == awayTeam) return awayDugout.position;

        return Vector3.zero;
    }

    private static void CallEndOfGame()
    {
        Debug.Log("END OF GAME");
        StartClosingCeremony();
    }

#endregion


#region Bases
    public int MaxForcedBase()
    {
        int maxForcedBase = 0;
        for (int i = 0; i < fieldState.baseOccupied.Length; i++)
        {
            if (fieldState.baseOccupied[i])
            {
                maxForcedBase = i + 1;
            }
            else
            {
                break;
            }
        }
        return maxForcedBase;
    }

    public bool BaseIsDefended(int baseNo)
    {
        if (baseNo < 0 || baseNo >= fieldState.baseDefended.Length) return false;
        return fieldState.baseDefended[baseNo];
    }

    public bool RunnerIsOnBase(OffensePlayer runner, out int baseNo)
    {
        baseNo = -1;
        foreach (FieldBase fBase in baseObjects)
        {
            if (fBase.offensivePlayers.Contains(runner))
            {
                baseNo = fBase.baseNum;
                return true;
            }

        }
        return false;
    }

    public bool RunnerIsOnBase(OffensePlayer runner)
    {
        foreach (FieldBase fBase in baseObjects)
        {
            if (fBase.offensivePlayers.Contains(runner))
            {
                return true;
            }
        }
        return false;
    }

    public bool BaseIsOccupiedByRunner(int baseNo)
    {
        return GetBase(baseNo).occupiedByRunner;
    }

    public bool DefenderIsOnBase(DefensePlayer player, out int baseNo)
    {
        baseNo = -1;
        foreach (FieldBase fBase in baseObjects)
        {
            if (fBase.defensivePlayers.Contains(player))
            {
                baseNo = fBase.baseNum;
                return true;
            }

        }
        return false;
    }
#endregion

    private static bool FindTimesToIntercept(Vector3 targetPosition, Vector3 targetVelocity, Vector3 interceptorPosition, float interceptorSpeed, out float t1, out float t2, float targetRadius = 0, float interceptorRadius = 0)
    {
        // |a|^2 = |b|^2 + |c|^2 -2|b||c| cosA
        //where
        //a is interceptor to interceptionPoint
        //b is target to interceptionPiont
        //c is target to interceptor
        //A is angle between b and c
        /*
                  interception
                /\
               /  \
        a     /    \  b
             /      \
            /_______A\ target
                 c
        */

        targetPosition.y = 0;// WorldScaler.elevation;
        Vector3 targetToInterceptor = interceptorPosition - targetPosition;
        targetToInterceptor.y = 0;// WorldScaler.elevation;

        //Debug.DrawLine(outfielderPosition, ballPosGround, Color.yellow, 5);
        targetVelocity.y = 0;// WorldScaler.elevation;

        float targetSpeed = targetVelocity.magnitude;
        float A = Vector3.Angle(targetToInterceptor, targetVelocity);

        //q is -2 * c * cosA
        //p is c ^2

        float targetToInterceptorMag_adjusted = targetToInterceptor.magnitude - targetRadius - interceptorRadius;

        float q = -2f * targetToInterceptor.magnitude * Mathf.Cos(Mathf.Deg2Rad * A);
        float p = targetToInterceptorMag_adjusted * targetToInterceptorMag_adjusted;

        //replace b with t * targetSpeed v
        //replace a with t * interceptorSpeed s

        //0 = (v ^ 2 - s ^2) t ^2 + (q x v)t + p
        //where
        //v is magnitude of targetVelocity
        //s is interceptor speed
        //t is time to intercept

        float a = (targetSpeed * targetSpeed - interceptorSpeed * interceptorSpeed);
        float b = q * targetSpeed;
        float c = p;

        t1 = 0;
        t2 = 0;

        //Debug.Log(square + " a: " + a + " b: " + b + " c: " +c + " ballSpeed: " + ballSpeed);
        if (MathUtility.SolveQuadratic(a, b, c, out t1, out t2))
        {
            return true;
        }
        return false;
    }

    public static AIFieldCoordinator<DefensePlayer>.RankedFieldPlayer FindPassingInterceptionPoint(DefensePlayer from, DefensePlayer to)
    {
        AIFieldCoordinator<DefensePlayer>.RankedFieldPlayer interception = new AIFieldCoordinator<DefensePlayer>.RankedFieldPlayer();
        interception.fieldPlayer = to;

        Vector3 targetVelocity = (to.moveTargetPosition - to.position).normalized * to.runSpeed;
        targetVelocity.y = 0;


        float t1, t2;
        //Debug.Log(square + " a: " + a + " b: " + b + " c: " +c + " ballSpeed: " + ballSpeed);
        if (FindTimesToIntercept(to.position, targetVelocity, from.position, from.throwSpeed, out t1, out t2))
        {
            if (t1 < 0) t1 = float.MaxValue;
            if (t2 < 0) t2 = float.MaxValue;

            float t_firstPathIntercept = Mathf.Min(t1, t2);
            interception.value = t_firstPathIntercept;
            interception.position = to.position + targetVelocity * t_firstPathIntercept;

        }

        else
        {
            interception.value = float.MaxValue;
        }

        return interception;
    }

    public static List<AIFieldCoordinator<OffensePlayer>.RankedFieldPlayer> RunnerInterceptionEstimates(DefensePlayer ballHolder)
    {
        List<AIFieldCoordinator<OffensePlayer>.RankedFieldPlayer> interceptionEstimates = new List<AIFieldCoordinator<OffensePlayer>.RankedFieldPlayer>();

        foreach (OffensePlayer runner in FieldManager.offensivePlayers)
        {
            AIFieldCoordinator<OffensePlayer>.RankedFieldPlayer interception = EstimatedInterceptionOfRunner(ballHolder, runner);
            //Debug.Log("interception estimate " + runner.name + " " + interception.value);

            //interception.value += tolerance;
            if (interception.value >= 0)
            {
                interceptionEstimates.Add(interception);
            }
        }

        interceptionEstimates = interceptionEstimates.OrderBy(x => x.value).ToList();

        return interceptionEstimates;
    }


    /// <summary>
    /// Calculate expected interception of a Runner by a Defender
    /// if interception is not possible, estimation value will be float.Max
    /// </summary>
    /// <param name="defender"></param>
    /// <param name="runner"></param>
    /// <returns></returns>
    public static AIFieldCoordinator<OffensePlayer>.RankedFieldPlayer EstimatedInterceptionOfRunner(DefensePlayer defender, OffensePlayer runner)
    {
        AIFieldCoordinator<OffensePlayer>.RankedFieldPlayer interception = new AIFieldCoordinator<OffensePlayer>.RankedFieldPlayer();
        interception.fieldPlayer = runner;

        Vector3 runnerVelocity = (Instance.GetBasePosition(runner.baseTarget_Immediate) - runner.position).normalized * runner.runSpeed;
        runnerVelocity.y = 0;// WorldScaler.elevation;

        float t1, t2;
        //Debug.Log(square + " a: " + a + " b: " + b + " c: " +c + " ballSpeed: " + ballSpeed);
        if (FindTimesToIntercept(runner.position, runnerVelocity, defender.position, defender.runSpeed, out t1, out t2))
        {
            if (t1 < 0) t1 = float.MaxValue;
            if (t2 < 0) t2 = float.MaxValue;

            float t_firstPathIntercept = Mathf.Min(t1, t2);

            float timeToBase = runner.GetTimeToRunToPosition(Instance.GetBasePosition(runner.baseTarget_Immediate));

            //will not be able to intercept the runner before they reach their target base
            if (t_firstPathIntercept > timeToBase)
            {
                interception.value = float.MaxValue;
            }
            else
            {
                interception.value = t_firstPathIntercept;
                interception.position = runner.position + runnerVelocity * t_firstPathIntercept;
            }

        }
        else
        {
            interception.value = float.MaxValue;
        }
        return interception;
    }

    public static List<AIFieldCoordinator<DefensePlayer>.RankedFieldPlayer> GroundInterceptionEstimates(hittable ball)
    {
        List<AIFieldCoordinator<DefensePlayer>.RankedFieldPlayer> fastestGroundChasers = new List<AIFieldCoordinator<DefensePlayer>.RankedFieldPlayer>();
        if (ball == null)
        {
            Debug.Log("no ball to judge groundInterception estimates with...");
            return fastestGroundChasers;
        }

        Rigidbody ballBody = ball.GetRigidbody();
        TrajectoryInfo info = new TrajectoryInfo(ballBody);

        foreach (DefensePlayer outfielder in FieldManager.defensivePlayers)
        {
            float t = EstimatedTimeToInterceptGroundBall(info, outfielder);
            Vector3 pos_at_t = info.Position_at_T(t, Space.Self);
            pos_at_t.y = 0;// WorldScaler.elevation;
            fastestGroundChasers.Add(new AIFieldCoordinator<DefensePlayer>.RankedFieldPlayer(outfielder, t, pos_at_t));
        }
        fastestGroundChasers = fastestGroundChasers.OrderBy(x => x.value).ToList();
        /*
        Debug.Log("=================================Finished Estimating ground chasers");
        foreach(AIFieldCoordinator<DefensePlayer>.RankedFieldPlayer r in fastestGroundChasers)
        {
            Debug.Log("player:"+r.fieldPlayer.name+" "+r.value);

        }
        */
        return fastestGroundChasers;
    }

    /// <summary>
    /// Calculates an estimated time to intercept a ground ball
    /// </summary>
    /// <param name="trajectory"></param>
    /// <param name="outfielder"></param>
    /// <returns></returns>
    public static float EstimatedTimeToInterceptGroundBall(TrajectoryInfo trajectory, DefensePlayer outfielder)
    {
        Vector3 targetPosition = EstimatedGroundInterceptionPoint(trajectory.Position_at_T(0, Space.Self), trajectory.Velocity_at_T(0, Space.Self), outfielder);
        float t_intercept = outfielder.GetTimeToRunToPosition(targetPosition);
        outfielder.SetDebugText(t_intercept.ToString("0.0"));// t_outfielderAtRendezVous.ToString("0.0") + " + " + t_ballAtRendezVous.ToString("0.0"));

        return t_intercept;



    }


    /// <summary>
    /// Gets estimated intercept point in local space
    /// </summary>
    /// <param name="targetPosition">target position in local space</param>
    /// <param name="targetVelocity">target velocity in local space</param>
    /// <param name="defender"></param>
    /// <returns></returns>
    public static Vector3 EstimatedGroundInterceptionPoint(Vector3 targetPosition, Vector3 targetVelocity, DefensePlayer defender)
    {
        Vector3 targetPositionFlat = targetPosition;
        Vector3 targetVelocityFlat = targetVelocity;


        TrajectoryInfo info = new TrajectoryInfo();
        info.SimulatePosition(targetPosition, Space.Self);
        info.SimulateVelocity(targetVelocity, Space.Self);

        if (info.T_EnterDescendingCatchZone(defender.maxVerticalReach) > 0)
        {
            targetPositionFlat = info.groundHitPosition;
            //            Debug.Log("-0----" +targetPositionFlat + "  " + targetVelocityFlat);
            ///targetPositionFlat is getting set to the wrong value in this block

        }

        targetPositionFlat.y = 0;// WorldScaler.elevation;
        targetVelocityFlat.y = 0;// WorldScaler.elevation;
        float distanceToTarget = Vector3.Distance(targetPositionFlat, defender.position);
        Vector3 closestPointOnTargetPath = Vector3.Dot(defender.position - targetPositionFlat, targetVelocityFlat.normalized) * targetVelocityFlat.normalized + targetPositionFlat;
        //DrawDebugLine(defender.position, closestPointOnTargetPath, Color.yellow, Time.deltaTime);

        DrawDebugLine(targetPositionFlat, targetPositionFlat + Vector3.up * 10, Color.red);
        Vector3 combinedVelocities = targetVelocity + (targetPosition - defender.position).normalized * defender.runSpeed;
        float combo = combinedVelocities.magnitude;

        float T = distanceToTarget / Mathf.Abs(targetVelocityFlat.magnitude + defender.runSpeed);


        Vector3 goalPosition = targetPositionFlat + targetVelocityFlat * T;


        goalPosition.y = 0;// WorldScaler.elevation;
        return goalPosition;
    }

    public static List<AIFieldCoordinator<DefensePlayer>.RankedFieldPlayer> AirInterceptionEstimates(hittable ball)
    {
        List<AIFieldCoordinator<DefensePlayer>.RankedFieldPlayer> fastestCatchers = new List<AIFieldCoordinator<DefensePlayer>.RankedFieldPlayer>();
        if (ball == null) return fastestCatchers;

        Rigidbody ballBody = ball.GetRigidbody();
        //DrawCatchableAreaForBall(ballBody);
        TrajectoryInfo info = new TrajectoryInfo(ballBody);


        foreach (DefensePlayer outfielder in FieldManager.defensivePlayers)
        {
            AIFieldCoordinator<DefensePlayer>.RankedFieldPlayer t_airIntercept;
            if (CanCatchInAir(info, outfielder, out t_airIntercept))
            {
                /*
                Vector3 catchPos = info.Position_at_T(t_airIntercept);
                catchPos.y = 0;// WorldScaler.elevation;
				float t_toCatchPosition = Vector3.Distance(outfielder.position, catchPos) / outfielder.runSpeed;
                fastestCatchers.Add(new AIFieldCoordinator<DefensePlayer>.RankedFieldPlayer(outfielder, t_toCatchPosition, catchPos));
                */
                t_airIntercept.value *= (1 / t_airIntercept.fieldPlayer.GetWeightInDirection(info.currentPosition, t_airIntercept.position));
                fastestCatchers.Add(t_airIntercept);
            }
        }
        fastestCatchers = fastestCatchers
            .OrderBy(x => x.value)
            .ThenBy(x => Vector3.Distance(x.fieldPlayer.position, x.position))
            .ToList();

        foreach (AIFieldCoordinator<DefensePlayer>.RankedFieldPlayer rank in fastestCatchers)
        {
            //            Debug.Log(rank.fieldPlayer + " " + rank.value);
        }
        return fastestCatchers;
    }

    /// <summary>
    /// Will this player be able to catch the ball before it reaches the ground
    /// </summary>
    /// <param name="trajectory"></param>
    /// <param name="fieldPlayer"></param>
    /// <param name="timeToIntercept"></param>
    /// <returns>Outfielder is able to enter ball's path before it touches the ground</returns>
    public static bool CanCatchInAir(TrajectoryInfo trajectory, DefensePlayer fieldPlayer, out AIFieldCoordinator<DefensePlayer>.RankedFieldPlayer interception)
    {

        Vector3 ballPosGround = trajectory.currentPosition;
        ballPosGround.y = 0;

        Vector3 ballVelocity = trajectory.currentVelocity;
        ballVelocity.y = 0;

        bool canCatch = false;
        float t1, t2;
        float timeToIntercept = 0;

        Vector3 catchPosGround = trajectory.currentPosition;
        float t_toCatchPosition = float.MaxValue;

        //Debug.Log(square + " a: " + a + " b: " + b + " c: " +c + " ballSpeed: " + ballSpeed);
        if (FindTimesToIntercept(ballPosGround, ballVelocity, fieldPlayer.position, fieldPlayer.runSpeed, out t1, out t2))
        {
            //I think t2 will always be the answer but whatever. 
            //This will check both halves of the quadratic equation.



            //if the ball is slower than the outfielder, one of the solutions will be negative because the ball would never pass the outfielder 
            if (t1 < 0) t1 = float.MaxValue;
            if (t2 < 0) t2 = float.MaxValue;

            float t_firstPathIntercept = Mathf.Min(t1, t2);
            float t_lastPathIntercept = Mathf.Max(t1, t2);

            Vector3 earlyCatchPoint = trajectory.Position_at_T(t_firstPathIntercept);
            earlyCatchPoint.y = 0;// WorldScaler.elevation;
            Vector3 lastCatchPoint = trajectory.Position_at_T(t_lastPathIntercept);
            lastCatchPoint.y = 0;// WorldScaler.elevation;
            //Debug.DrawLine(outfielderPosition, earlyCatchPoint, Color.magenta, 5);
            //Debug.DrawLine(outfielderPosition, lastCatchPoint, Color.magenta, 5);

            float t_comingDown = trajectory.T_EnterDescendingCatchZone(fieldPlayer.maxVerticalReach);
            float t_goingUp = trajectory.T_ExitAscendingCatchZone(fieldPlayer.maxVerticalReach);
            float t_ground = trajectory.T_Ground;

            //Debug.Log(t_goingUp + "  " + t_comingDown + "  " + t_ground);
            //if it will be possible to catch the ball before it has risen above catchable altitude
            if (t_firstPathIntercept > 0 && t_firstPathIntercept <= t_goingUp)
            {
                //Debug.Log("here1");
                timeToIntercept = t_firstPathIntercept;
                canCatch = true;
            }


            //if ball is too high to catch when this outfielder would be able to intercept its path of travel
            else if (t_firstPathIntercept > t_goingUp && t_lastPathIntercept < t_comingDown)
            {
                //Debug.Log("here2");

                timeToIntercept = 0;
                canCatch = false;
            }



            //if it will be possible to catch the ball coming down, before it hits the ground
            else if (t_firstPathIntercept > t_comingDown && t_firstPathIntercept < t_ground)
            {
                //Debug.Log("here3");

                timeToIntercept = t_firstPathIntercept;
                canCatch = true;
            }

            //if it will be possible to catch the ball the moment it enters catchable altitude
            else if (t_firstPathIntercept < t_comingDown && t_lastPathIntercept > t_comingDown)
            {
                //Debug.Log("here4");

                timeToIntercept = t_comingDown;
                canCatch = true;
            }

            else
            {
                //Debug.Log("here5");

                canCatch = false;

            }

            catchPosGround = trajectory.Position_at_T(timeToIntercept);
            catchPosGround.y = 0;// WorldScaler.elevation;
            t_toCatchPosition = fieldPlayer.GetTimeToRunToPosition(catchPosGround);// Vector3.Distance(outfielder.position, catchPos) / fieldp.runSpeed;

        }

        //won't be in the path of the ball, but may be able to catch it with an outstretched arm
        else
        {
            catchPosGround = MathUtility.FindClosestPointOnLineToPoint(new Ray(ballPosGround, ballVelocity), fieldPlayer.position);
            catchPosGround.y = 0;// WorldScaler.elevation;

            if (Vector3.Dot(ballVelocity, catchPosGround - ballPosGround) < 0)
            {
                canCatch = false;
            }

            else
            {
                t_toCatchPosition = Vector3.Distance(catchPosGround, fieldPlayer.position) / fieldPlayer.runSpeed;
                float t_ballPassingPlayer = Vector3.Distance(ballPosGround, catchPosGround) / ballVelocity.magnitude;
                Vector3 playerChestPosWhenBallPasses = fieldPlayer.position + (catchPosGround - fieldPlayer.position).normalized * t_ballPassingPlayer + Vector3.up * fieldPlayer.chestHeight;

                canCatch = (Vector3.Distance(playerChestPosWhenBallPasses, trajectory.Position_at_T(t_ballPassingPlayer)) <= fieldPlayer.maxHorizontalReach);
            }
        }



        interception = new AIFieldCoordinator<DefensePlayer>.RankedFieldPlayer(fieldPlayer, t_toCatchPosition, catchPosGround);
        //Debug.Log(interception.ToString());

        return canCatch;

    }

    public static bool CanCatchInAir(hittable ball, DefensePlayer fieldPlayer, out AIFieldCoordinator<DefensePlayer>.RankedFieldPlayer interception)
    {
        return CanCatchInAir(new TrajectoryInfo(ball.GetRigidbody()), fieldPlayer, out interception);
    }

#region HumanPlayerRole

    private static bool doInningChangeOver = true;
    private static bool doOpenningCeremony = false;
    private static bool doClosingCeremony = false;


    public enum HumanPlayerRole
    {
        NONE,
        DEFENSE,
        OFFENSE,
        BROADCAST,
        TUTORIAL
    }
    public static HumanPlayerRole currentHumanPlayerRole { get; private set; }
    public static HumanPlayerRole previousHumanPlayerRole { get; private set; }



    public void ChangePlayerRole_Defense() { ChangePlayerRole(HumanPlayerRole.DEFENSE); }
    public void ChangePlayerRole_Offense() { ChangePlayerRole(HumanPlayerRole.OFFENSE); }
    public void ChangePlayerRole_Broadcast() { ChangePlayerRole(HumanPlayerRole.BROADCAST); }
    public void ChangePlayerRole_None() { ChangePlayerRole(HumanPlayerRole.NONE); }
    public void ChangePlayerRole_Tutorial() { ChangePlayerRole(HumanPlayerRole.TUTORIAL); }



    public static void PrepForFullGame(bool startBatting = false)
    {
        if (startBatting)
        {
            currentHumanPlayerRole = HumanPlayerRole.OFFENSE;
        }
        else
        {
            currentHumanPlayerRole = HumanPlayerRole.DEFENSE;
        }
        doOpenningCeremony = true;
        doClosingCeremony = true;
        doInningChangeOver = true;
    }

    public static void PrepForOffenseOnly()
    {
        currentHumanPlayerRole = HumanPlayerRole.OFFENSE;
        doOpenningCeremony = false;
        doClosingCeremony = false;
        doInningChangeOver = false;
    }

    public static void PrepForDefenseOnly()
    {
        currentHumanPlayerRole = HumanPlayerRole.DEFENSE;
        doOpenningCeremony = false;
        doClosingCeremony = false;
        doInningChangeOver = false;
    }

    public static void PrepForBroadcast()
    {
        currentHumanPlayerRole = HumanPlayerRole.BROADCAST;
        doOpenningCeremony = true;
        doClosingCeremony = true;
        doInningChangeOver = true;
    }


    public static void PrepForTutorial()
    {
        currentHumanPlayerRole = HumanPlayerRole.TUTORIAL;
        doOpenningCeremony = false;
        doClosingCeremony = false;
        doInningChangeOver = false;
    }

    public void ChangePlayerRole(HumanPlayerRole role, bool refresh = false)
    {
#if UNITY_EDITOR
        Debug.Log("<color=cyan>CHANGE PLAYER ROLE   " + currentHumanPlayerRole + "   to   " + role + "</color>");
#endif
        
        if (currentHumanPlayerRole == role && !refresh) return;

        switch (currentHumanPlayerRole)
        {
            case HumanPlayerRole.NONE:
                break;
            case HumanPlayerRole.DEFENSE:
                defense.SetHumanControlled(false);
                break;
            case HumanPlayerRole.OFFENSE:
                offense.SetHumanControlled(false);
                break;
            case HumanPlayerRole.BROADCAST:
                broadcast.SetHumanControlled(false);
                break;
            case HumanPlayerRole.TUTORIAL:
                defense.SetResponsiveToFieldEvents(true);
                offense.SetResponsiveToFieldEvents(true);
                broadcast.SetResponsiveToFieldEvents(true);

                defense.enabled = true;
                offense.enabled = true;
                //spectator.enabled = false;
                dugout.enabled = true;
                break;
        }

        previousHumanPlayerRole = currentHumanPlayerRole;
        currentHumanPlayerRole = role;

        switch (role)
        {
            case HumanPlayerRole.NONE:
                HumanPosessionManager.SetArmLocomotionEnabled(true);
                break;
            case HumanPlayerRole.DEFENSE:
                defense.SetHumanControlled(true);
                break;
            case HumanPlayerRole.OFFENSE:
                offense.SetHumanControlled(true);
                break;
            case HumanPlayerRole.BROADCAST:
                broadcast.SetHumanControlled(true);
                break;
            case HumanPlayerRole.TUTORIAL:
                defense.SetResponsiveToFieldEvents(false);
                offense.SetResponsiveToFieldEvents(false);
                broadcast.SetResponsiveToFieldEvents(false);
                defense.enabled = false;
                offense.enabled = false;
                //spectator.enabled = false;
                dugout.enabled = false;
                break;
        }
        if (HumanPosessionManager.Instance)
        {
            HumanPosessionManager.Instance.RefreshLocomotionType();
        }
        if (HumanGearManager.instance)
        {
            HumanGearManager.instance.RefreshColliderStatus();
        }
    }
#endregion
}

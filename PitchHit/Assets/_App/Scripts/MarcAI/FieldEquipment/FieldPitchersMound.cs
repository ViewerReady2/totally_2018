﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class FieldPitchersMound : MonoBehaviour, ISelectable {

    public List<DefensePlayer> defensivePlayers { get; private set; }
    protected new SphereCollider collider;
    public SpriteRenderer ringLight;

    public bool PitcherOnMoundWithBall{
        get
        {
            return defensivePlayers.Any(x => (x.inPosessionOfBall && x.fieldPosition == DefenseFieldPosition.PITCHER));
        }
    }

    public bool PitcherOnMound
    {
        get
        {
            return defensivePlayers.Any(x => x.fieldPosition == DefenseFieldPosition.PITCHER);
        }
    }

    public void Awake()
    {
        defensivePlayers = new List<DefensePlayer>();
        collider = GetComponent<SphereCollider>();
        collider.enabled = false;
    }
    void Start()
    {
        FieldSelectManager.RegisterSelectable(this);
        //highlight.color = FieldManager.baseColor;
        collider.enabled = true;
        //ringLight.color = FieldManager.ballColor;
        ringLight.enabled = false;
        //collider.enabled = true;
    }

    void OnTriggerEnter(Collider other)
    {

//		Debug.Log ("xyyyyyyyyyyyyyyyyyyyyyy OnTriggerEnter"+ other.name);
        DefensePlayer d = other.GetComponent<DefensePlayer>();
        if (d != null)
        {
            AddDefensePlayer(d);
        }
    }

    void OnTriggerExit(Collider other)
    {
        DefensePlayer d = other.GetComponent<DefensePlayer>();
        if (d != null)
        {

            RemoveDefensePlayer(d);
        }
    }

    void AddDefensePlayer(DefensePlayer d)
    {
//		Debug.Log ("..xxxxxxxxxxxxxxxxxxxxxxx Add Defense player "+d.name);
        if (!defensivePlayers.Contains(d))
        {
            defensivePlayers.Add(d);
            d.OnBallPosession += OnBallEvent;
            d.OnThrow += OnBallEvent;
        }
        UpdateRingLight();
    }

    void RemoveDefensePlayer(DefensePlayer d)
    {
        if (defensivePlayers.Contains(d))
        {
            d.OnBallPosession -= OnBallEvent;
            d.OnThrow -= OnBallEvent;
            defensivePlayers.Remove(d);
        }
        UpdateRingLight();
    }



    void OnBallEvent(DefensePlayer d)
    {
        UpdateRingLight();
    }

    void UpdateRingLight()
    {
        ringLight.enabled = PitcherOnMoundWithBall;
    }

    #region ISelectable Implementation
    public PlayerHighlight highlight;

    public Vector3 GetSelectablePosition()
    {
        return transform.position;

    }

    public virtual bool CanBePrimarySelection()
    {
        return false;
    }
    public bool CanBeDirectedToSelectable(ISelectable other)
    {
        return false;
    }
    public void DirectTo(ISelectable other)
    {

    }

    public void DirectTo(Vector3 position)
    {

    }

    public void DragToOtherSelectable(ISelectable other, PathLine line) { }
    public void DragToPoint(Vector3 point, PathLine line) { }

    public void SetSelectableState(SelectableState state)
    {
        if (highlight != null) highlight.UpdateForSelectableState(state);
    }
    #endregion
}

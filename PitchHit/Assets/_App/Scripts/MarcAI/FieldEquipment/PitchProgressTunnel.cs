﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PitchProgressTunnel : MonoBehaviour {

    protected PitchProgressCheckpoint[] checkpoints;


	// Use this for initialization
	void Start () {
        checkpoints = GetComponentsInChildren<PitchProgressCheckpoint>();
        ResetAll();
	}


    void ResetAll()
    {
        foreach (PitchProgressCheckpoint check in checkpoints)
        {
            check.Reset();
        }
    }
}

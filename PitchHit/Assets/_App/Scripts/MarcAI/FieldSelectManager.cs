﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
#if LUMIN
using UnityEngine.XR.MagicLeap;
#endif
#if UNITY_EDITOR
using UnityEditor;
#endif

public class FieldSelectManager : MonoBehaviour {

    public static ISelectable pressSelection;
    public static ISelectable hoverSelection;

    public GameObject Controller;
    public float castRadius = 10;


    private bool _triggerDown = false;
    private Vector3 _hitPoint = Vector3.zero;
    private bool _bumper = false;


    public LineRenderer selectLine;


    public static List<ISelectable> allSelectables
    {
        get
        {
            if (_allSelectables == null) _allSelectables = new List<ISelectable>();
            return _allSelectables;
        }
        private set
        {
            _allSelectables = value;
        }
    }
    private static List<ISelectable> _allSelectables;

    private static int numAvailableSelectables = 0;

    public PathLine dragLine;

#if LUMIN
    private MLInputController _controller;

    void Awake()
    {
        if (MLInput.IsStarted)
        {
            _controller = MLInput.GetController(MLInput.Hand.Left);
            //MLInput.OnControllerButtonDown += OnButtonDown;
            //MLInput.OnControllerButtonUp += OnButtonUp;
        }

    }

    /// <summary>
    /// Handles the event for button down and cycles the raycast mode.
    /// </summary>
    /// <param name="controller_id">The id of the controller.</param>
    /// <param name="button">The button that is being pressed.</param>
    private void OnButtonDown(byte controller_id, MLInputControllerButton button)
    {
        if (button == MLInputControllerButton.Bumper)
        {
            _bumper = true;
            SetPrimarySelection(null);
            //transition.ZoomIn();
        }
    }

    /// <summary>
    /// Handles the event for button down and cycles the raycast mode.
    /// </summary>
    /// <param name="controller_id">The id of the controller.</param>
    /// <param name="button">The button that is being pressed.</param>
    private void OnButtonUp(byte controller_id, MLInputControllerButton button)
    {
        if (button == MLInputControllerButton.Bumper)
        {
            _bumper = false;
            //transition.ZoomOut();
        }
    }

    void CheckControl()
    {
        if ((_controller != null && _controller.TriggerValue > 0.2f) && !_triggerDown
            || Input.GetMouseButtonDown(0)
            )
        {
            _triggerDown = true;
            OnTriggerPull();
        }
        else if ((_controller != null && _controller.TriggerValue <= .2f) && _triggerDown
            || Input.GetMouseButtonUp(0)
            )
        {
            _triggerDown = false;
            OnTriggerRelease();
        }

    }
    void Update()
    {
        UpdateSelection();
        CheckControl();

        ShowHide_SelectLine((numAvailableSelectables>0));
    }
#endif



    public static void RegisterSelectable(ISelectable sel)
    {
        if (!allSelectables.Contains(sel)) allSelectables.Add(sel);
        RefreshSelectableAvailability();
    }

    public static void UnRegisterSelectable(ISelectable sel)
    {
        allSelectables.Remove(sel);
        RefreshSelectableAvailability();
    }

#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        if (_triggerDown)
        {
            Color c = Color.yellow;
            c.a = .1f;

            Handles.color = c;

            Handles.DrawSolidDisc(_hitPoint, Vector3.up, castRadius);
            Handles.color = Color.black;
            Handles.DrawLine(_hitPoint, _hitPoint + Vector3.up * 3);
        }
    }
#endif

    

    


    void OnTriggerPull()
    {
        

        if (hoverSelection != null)
        {
            if (pressSelection == null) {
                if(hoverSelection.CanBePrimarySelection())
                    SetPrimarySelection(hoverSelection);
            }
            else
            {

            }
        }
        if (pressSelection != null)
        {
            dragLine.ShowHide(true);
        }
    }

    void OnTriggerRelease()
    {
        if (pressSelection != null)
        {
            if (hoverSelection != null && pressSelection.CanBeDirectedToSelectable(hoverSelection))
            {
                if (hoverSelection == pressSelection)
                {
                    //hold onto press selection
                }
                else
                {
                    pressSelection.DirectTo(hoverSelection);
                    SetPrimarySelection(null);

                }

            }
            else
            {
                pressSelection.DirectTo(_hitPoint);
                SetPrimarySelection(null);

            }

        }

        if (pressSelection == null)
        {
            dragLine.ShowHide(false);
        }
        else
        {
            dragLine.ShowHide(true);
        }

    }

    public void ShowHide_SelectLine(bool isShowing)
    {
        selectLine.enabled = isShowing;
    }

    void UpdateSelection()
    {
        if (!FieldManager.Instance) return;
        if (!FieldManager.Instance.canSelectPlayers) return;

        RaycastHit hit;
        Ray castRay = new Ray(Controller.transform.position, Controller.transform.forward);
#if UNITY_EDITOR
        /*
        castRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        Debug.DrawRay(castRay.origin, castRay.direction * 1000);
        */
#endif
        if (Physics.Raycast(castRay, out hit, 1000, LayerMask.GetMask("Ground"))){
            Vector3 center = hit.point;


            _hitPoint = hit.point;
            List<ISelectable> hitSelectables = SelectablesInSphere(center, castRadius);
            if (hitSelectables.Count > 0)
            {
                SetHoverSelectable(hitSelectables[0]);
            }
            else if (hoverSelection != null)
            {
                SetHoverSelectable(null);
            }
        }
        else if (hoverSelection != null)
        {
            SetHoverSelectable(null);
        }

        if(pressSelection != null)
        {
            if(hoverSelection == null)
            {
                pressSelection.DragToPoint(hit.point, dragLine);
            }
            else
            {
                pressSelection.DragToOtherSelectable(hoverSelection, dragLine);
            }

        }
    }
    
    List<ISelectable> SelectablesInSphere(Vector3 center, float radius)
    {
        List<ISelectable> hits = new List<ISelectable>();
        Collider[] overlaps = Physics.OverlapSphere(center, radius);
        foreach(Collider col in overlaps)
        {
            ISelectable select = col.GetComponent<ISelectable>();
            if(select != null)
            {
                if(pressSelection == null) {
                    if (select.CanBePrimarySelection())
                    {
                        hits.Add(select);
                    }
                }
                else if (pressSelection.CanBeDirectedToSelectable(select))
                {
                    hits.Add(select);
                }
                Debug.DrawLine(center, select.GetSelectablePosition(), Color.black);

            }
        }
        hits = hits.OrderBy(x => (Vector3.Distance(center, x.GetSelectablePosition()))).ToList();
        return hits;
    }

    void SetHoverSelectable(ISelectable selectable)
    {
        if (hoverSelection != null)
        {
            if (hoverSelection == selectable) return;
            //highlightSelection.OnUnhighlight();
        }
        hoverSelection = selectable;
        if (hoverSelection != null) hoverSelection.SetSelectableState(SelectableState.HOVER);

        RefreshSelectableAvailability();
    }

    void SetPrimarySelection(ISelectable selection)
    {
//        Debug.Log("SET PRIMARY SELECTION");
        if (pressSelection != null)
        {
            if (pressSelection == selection) return;
            //primarySelection.OnUnselect();
        }
        pressSelection = selection;
        if (pressSelection != null)
        {
            selection.SetSelectableState(SelectableState.PRESS);
        }

        RefreshSelectableAvailability();
    }


    public static void RefreshSelectableAvailability()
    {
        if(FieldManager.Instance == null)
        {
            return;
        }

        numAvailableSelectables = 0;
        if (!FieldManager.Instance.canSelectPlayers)
        {
            pressSelection = null;
            hoverSelection = null;

            foreach (ISelectable sel in allSelectables)
            {

                sel.SetSelectableState(SelectableState.UNAVAILABLE);
            }

            return;
        }


        bool primary = pressSelection != null;
        bool highlight = hoverSelection != null;



        foreach(ISelectable sel in allSelectables)
        {

            if (primary && sel == pressSelection)
            {
                numAvailableSelectables++;
                continue;
            }
            if (highlight && sel == hoverSelection)
            {
                numAvailableSelectables++;
                continue;
            }
            if (
            (primary && pressSelection.CanBeDirectedToSelectable(sel))
            || (!primary && sel.CanBePrimarySelection()))
            {
                numAvailableSelectables++;
                sel.SetSelectableState(SelectableState.AVAILABLE);
            continue;
            }
                
                sel.SetSelectableState(SelectableState.UNAVAILABLE);
                
        }
        
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SelectableState
{
    AVAILABLE, //can be selected
    UNAVAILABLE, //cannot be selected
    HOVER, //cusor hoving over
    PRESS, //cursor over and button pressed
}
public interface ISelectable {


    Vector3 GetSelectablePosition();

    void SetSelectableState(SelectableState state);


    bool CanBePrimarySelection();
    bool CanBeDirectedToSelectable(ISelectable other);

    void DirectTo(ISelectable other);
    void DirectTo(Vector3 position);

    void DragToOtherSelectable(ISelectable other, PathLine line);
    void DragToPoint(Vector3 point, PathLine line);

}

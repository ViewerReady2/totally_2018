﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MonsterLove.StateMachine;

public class OffensePlayer : AIFieldPlayer
{

    public Action<int, OffensePlayer> OnBaseReached;
    public Action<int, OffensePlayer> OnBaseLeft;
    public Action<OffensePlayer> OnDestroyRunner;
    public Action<OffensePlayer, OutType> OnOut;
    public Action<OffensePlayer> OnHome;

    public int baseAtPitch
    {
        get; protected set;
    }

    //[HideInInspector]
    public int baseLastTouched
    {
        get { return _baseLastTouched; }
        protected set { //Debug.Log("touch base " + value);
            _baseLastTouched = value; }
    }

    private int _baseLastTouched;

    public bool forcedRun
    {
        get; protected set;
    }

    public int baseTarget_Eventual { get; protected set; }
    public int baseTarget_Immediate { get; protected set; }
    /*
	private WorldScaler worldScale;

	public void SetWorldScaler(WorldScaler world)
	{
		worldScale = world;
	}
*/
    public override void InstantReset()
    {
        StayOnBase(baseLastTouched);
        position = FieldManager.Instance.GetBasePosition(baseLastTouched);
        RemoveForce();
    }

    public override void Initialize()
    {
        Initialize(-1);

    }

    public void Initialize(int baseNo)
    {
        if (isInitialized) return;
        baseLastTouched = baseNo;
        FieldManager.RegisterRunner(this, baseNo);
        base.Initialize();
        bodyControl.SetAnimatorEnabled(true);

    }

    protected override void GetDressed()
    {
        base.GetDressed();
        bodyControl.EquipGear(FieldPlayerGear.HELMET, true);

    }

    public override void SetPlayerInfo(TeamInfo tInfo, PlayerInfo pInfo)
    {
        base.SetPlayerInfo(tInfo, pInfo);
        runSpeed = pInfo.runSpeedOffense;
    }


    protected override void OnDestroy()
    {
        base.OnDestroy();
        FieldManager.UnRegisterRunner(this);
        if (OnDestroyRunner != null) OnDestroyRunner.Invoke(this);
    }

    #region AIFieldPlayer Extension

    public override void SetHumanPosession(bool isPosessed)
    {
        if (isTetheredToCamera == isPosessed)
        {
            return;
        }
        base.SetHumanPosession(isPosessed);
        if (isPosessed)
        {
            HumanGearManager.instance.ShowBatterGear();
        }
    }

    protected override void SetPlayerAction(FieldPlayerAction action)
    {
        if (state == FieldPlayerAction.OUT) return; // MARC Fail-Safe: Under no circumstances should a runner who is OUT change state
        base.SetPlayerAction(action);
    }

    #endregion

    public override void RunToBase(int baseNo)
    {
        //        Debug.Log("Run to base " + baseNo);
        if (!isInitialized) Initialize();

        //if(FieldManager.Instance.defense.runningPlay == FieldPlayType.RETURN)
        //{
        //    return;
        //}

        baseTarget_Eventual = baseNo;
        baseTarget_Immediate = GetNextImmediateBaseTarget();

        //Greg: I'm not sure what to do if either of these return false.
        FieldManager.Instance.GetBases()[baseTarget_Eventual].AttemptToTakeControl(this);
        FieldManager.Instance.GetBases()[baseTarget_Immediate].AttemptToTakeControl(this);

        base.RunToBase(baseTarget_Immediate);
    }

    public void RunToBaseForced(int baseNo)
    {
        forcedRun = true;
        RunToBase(baseNo);
    }

    /// <summary>
    /// If a human player has reached a base, the runner's state should be BASE if they are safe, RUN if they are forced.
    /// It will be changed to RUN when they leave the base
    /// </summary>
    /// <param name="b"></param>
    public void OnHumanBaseEntered(FieldBase b)
    {

        //Debug.Log("Human on base " + b.baseNum);
        if(b.baseNum>baseLastTouched+1 || b.baseNum<baseLastTouched)
        {
            return;//skipped a base? or touched homebase as leaving it?
        }
        // retreated to last base
        if (b.baseNum == baseLastTouched)
        {
            SetPlayerAction(FieldPlayerAction.BASE);
        }

        baseLastTouched = b.baseNum;

        if (baseLastTouched != baseTarget_Eventual)
        {
            //Debug.Log("++++++++++++++++++++++++++ " + gameObject.name + " RUN_UPDATE RAN INCOMPLETE COMPLETE");
            baseTarget_Immediate = GetNextImmediateBaseTarget();
            //SetMoveTargetPosition(FieldManager.Instance.GetBasePosition(baseTarget_Immediate));
            //SetLookTargetPosition(FieldManager.Instance.GetBasePosition(baseTarget_Immediate));
        }
     
        else//running forward
        {
            //Debug.Log("++++++++++++++++++++++++++ " + gameObject.name + " RUN_UPDATE RAN COMPLETE");
            if (forcedRun) RemoveForce();
            SetPlayerAction(FieldPlayerAction.BASE);
            baseTarget_Eventual = baseLastTouched + 1;
            baseTarget_Immediate = GetNextImmediateBaseTarget();
            if (OnBaseReached != null) OnBaseReached.Invoke(baseLastTouched, this);
        }

        HumanPosessionManager.SetProjectionRailPoints(FieldManager.Instance.GetBasePosition(baseLastTouched), FieldManager.Instance.GetBasePosition(baseTarget_Immediate));

    }

    public void OnHumanBaseExited(FieldBase b)
    {
        if(state != FieldPlayerAction.OUT)
            SetPlayerAction(FieldPlayerAction.RUN);

    }

    public float GetTimeToRunBetweenBases(int startBase, int endBase)
    {
        startBase = Mathf.Clamp(startBase, 0, FieldManager.Instance.numBases);
        endBase = Mathf.Clamp(endBase, 0, FieldManager.Instance.numBases);
        float total = 0;
        int direction = startBase < endBase ? 1 : -1;
        for (int baseNo = startBase; baseNo != endBase; baseNo += direction)
        {
            total += Vector3.Distance(FieldManager.Instance.GetBasePosition(baseNo), FieldManager.Instance.GetBasePosition(baseNo + direction)) / runSpeed;
        }
        //        Debug.Log("Time to Base " + startBase + " " + endBase + " " + total);
        return total;
    }

    protected int GetNextImmediateBaseTarget()
    {
        int eventualGoal = baseTarget_Eventual;
        int lastTagged = baseLastTouched;
        if (eventualGoal == lastTagged) return eventualGoal;
        int direction = (baseTarget_Eventual > lastTagged ? 1 : -1);
        return (lastTagged + direction);
    }

    public void StayOnBase(int baseNo)
    {
        baseTarget_Eventual = baseNo +1;
        baseTarget_Immediate = baseNo +1;
        baseLastTouched = baseNo;
        SetPlayerAction(FieldPlayerAction.BASE);
    }

    public void EjectFromBase(FieldBase b)
    {
        Debug.Log("----------------------------EJECT "+name+" FROM BASE "+b.name+"????????????");
        if((state == FieldPlayerAction.BASE && b.baseNum != baseLastTouched) || (state == FieldPlayerAction.RUN && b.baseNum != baseTarget_Eventual))
        {

            Debug.Log(state+" NO NEVERMIND THAT'S NOT MY BASE "+b.baseNum+"  "+baseTarget_Eventual+" "+baseTarget_Immediate+" "+baseLastTouched);
            return;//must be some sort of mistake. I'm not even going to that base.
        }

        if(!forcedRun)//that's ok. I can just keep running.
        {
            if(b.baseNum == baseLastTouched)
            {
                RunToBaseForced(baseLastTouched + 1);
            }
            else
            {
                RunToBase(baseLastTouched);//Greg: This isn't forced because I don't want players to accidentally get their own runners out simply by motioning to the next base.
            }
        }
    }

    public void RemoveForce()
    {
        forcedRun = false;
    }

    #region FieldManager Handles
    public void NotifyOfOut(OutType outCall)
    {
        if (state == FieldPlayerAction.OUT) return; //ALREADY OUT
                                                    //        Debug.Log(this.name + " OUT " + outCall);
        FieldManager.currentOffensiveTeam.AddToBattingQueue(info);
        if (OnOut != null) OnOut.Invoke(this, outCall);
        SetPlayerAction(FieldPlayerAction.OUT);
    }

    public void NotifyOfRunCompletion()
    {
        Debug.Log(this.name + " COMPLETED RUN");

        OneShotEffectManager.SpawnEffect("run_score", FieldManager.Instance.GetHomePlate().transform.position, Quaternion.identity, spectatorOnly: isTetheredToCamera);
        //FieldManager.Instance.callou
        FieldManager.currentOffensiveTeam.AddToBattingQueue(info);
        if (OnHome != null) OnHome.Invoke(this);
        if (!gameObject.activeSelf) Destroy(this.gameObject);
        else
        StartCoroutine(DestroyNextFrame());
    }

    public void RecordBaseAtPitch()
    {
        baseAtPitch = baseLastTouched;

    }
    #endregion

    public void AttemptTagOut(OutfielderGlove glove)
    {
        if (!FieldManager.Instance.RunnerIsOnBase(this) && glove.heldBall != null && FieldManager.Instance.offense.runningPlay != FieldPlayType.WALK)
        {
            NotifyOfOut(OutType.TAG);
        }
    }

    #region StateMachine Logic
    void RESET_Enter()
    {
        SetPlayerAction(FieldPlayerAction.IDLE);
    }

    void RUN_Enter()
    {
        SetLookMode(LookMode.FORWARD);
        SetMoveTargetPosition(FieldManager.Instance.GetBasePosition(baseTarget_Immediate));
        SetLookTargetPosition(FieldManager.Instance.GetBasePosition(baseTarget_Immediate));
    }

    void RUN_FixedUpdate()
    {
        MoveTowardsTargetPosition();
    }

    [HideInInspector]
    float distanceToNextBase = float.MaxValue;
    [HideInInspector]
    float lastDistanceToNextBase = 0;

    void RUN_Update()
    {
        //Debug.Log("++++++++++++++++++++++++++ "+gameObject.name+" RUN_UPDATE");
        Debug.DrawLine(position + (Vector3.up * 2f), FieldManager.Instance.GetBasePosition(baseTarget_Immediate) + (Vector3.up * 2f), Color.cyan);

        //if this runner is player controlled, detect which base they're running towards and 
        if(isTetheredToCamera)
        {
            if (!forcedRun)
            {
                int lastBase = baseLastTouched;
                int nextBase = baseLastTouched + 1;

                distanceToNextBase = Vector3.Distance(position, FieldManager.Instance.GetBasePosition(nextBase));


                bool runningToNextBase = lastDistanceToNextBase > distanceToNextBase;
                //                Debug.Log(lastDistanceToNextBase + "   " + distanceToNextBase);

                int previous_baseTarget_Immediate = baseTarget_Immediate;
                baseTarget_Immediate = (runningToNextBase ? nextBase : lastBase);

                //Greg: There's been a change of heart!
                if(baseTarget_Immediate != previous_baseTarget_Immediate)
                {
                    //Greg: Uh Oh! you changed your mind to a base you cannot take!
                    if (FieldManager.Instance.GetBases()[baseTarget_Immediate].AttemptToTakeControl(this) == false)
                    {
                        if (baseTarget_Immediate == lastBase)
                        {
                            if(FieldManager.Instance.GetBases()[baseTarget_Immediate].offensivePlayers.Count>0)
                            {
                                //GREG: You REALLY can't go back because someone else is on tha base now!
                                baseTarget_Immediate = previous_baseTarget_Immediate;
                                forcedRun = true;
                            }
                        }
                    }
                }

                //only save this distance if its at least .25f different from last
                if (Mathf.Abs(distanceToNextBase - lastDistanceToNextBase) > .2f)
                {
                    lastDistanceToNextBase = distanceToNextBase;
                }
            }

            return;
        }

        //GREG
        //if you're running forward and forced and the base behind you becomes available
            //you are no longer forced and can turn back if you like. 
        if(forcedRun)
        {
            if (baseLastTouched != -1)
            {
                if (FieldManager.Instance.GetBases()[baseLastTouched].runnerWithControl == null && baseTarget_Eventual != baseAtPitch)
                {
                    RemoveForce();
                }
            }
        }

        if (Vector3.Distance(position, FieldManager.Instance.GetBasePosition(baseTarget_Immediate)) < (.1f))
        {
            baseLastTouched = baseTarget_Immediate;
            if (baseLastTouched != baseTarget_Eventual)
            {
//                Debug.Log("++++++++++++++++++++++++++ " + gameObject.name + " RUN_UPDATE RAN TO END COMPLETE");
                baseTarget_Immediate = GetNextImmediateBaseTarget();
                SetMoveTargetPosition(FieldManager.Instance.GetBasePosition(baseTarget_Immediate));

            }
            else//running forward
            {
//                Debug.Log("++++++++++++++++++++++++++ " + gameObject.name + " RUN_UPDATE RAN INCREMENT COMPLETE");
                if (forcedRun) RemoveForce();
                SetPlayerAction(FieldPlayerAction.BASE);
                if (OnBaseReached != null) OnBaseReached.Invoke(baseTarget_Immediate, this);
            }
        }
    }

    void RUN_Exit()
    {

    }

    void BASE_Enter()
    {
        if (isTetheredToCamera)
        {
            HumanPosessionManager.SetReverseRailLocomotionEnabled(false);
        }
        SetLookTargetPosition(Vector3.zero);
        SetLookMode(LookMode.TARGET);

    }

    void BASE_Update()
    {
        //GREG
        //if the base you're on is targetted by another runner behind you
            //you are forced to run to the next base.



        //if(FieldManager.Instance.GetBases()[baseLastTouched])
    }


    void BASE_Exit()
    {
        if (isTetheredToCamera)
        {
            HumanPosessionManager.SetReverseRailLocomotionEnabled(true);
        }
        if (OnBaseLeft != null) OnBaseLeft.Invoke(baseAtPitch, this);

    }

    void OUT_Enter()
    {
        OneShotEffectManager.SpawnEffect("player_out", this.transform.position, Quaternion.identity, 3, spectatorOnly: isTetheredToCamera);
    }

    void OUT_Update()
    {
        if(!isTetheredToCamera)
        GameObject.Destroy(this.gameObject);

    }
    #endregion

    #region ISelectable 
    public override bool CanBePrimarySelection()
    {
        //false for demo version
        return false;
        return FieldManager.Instance.offense.humanControlled;// && FieldManager.Instance.offense.runningPlay == FieldPlayType.STANDARD;
    }

    public override bool CanBeDirectedToSelectable(ISelectable other)
    {

        if (other is FieldBase)
        {
            FieldBase fBase = other as FieldBase;
            int baseNum = fBase.baseNum;

            bool cannotTargetBase = false;

            if (fBase.offensivePlayers.Contains(this)
                || (forcedRun && baseNum != baseTarget_Eventual)
                || (baseNum < baseLastTouched))
            {
                cannotTargetBase = true;
            }

            OffensePlayer ahead = FieldManager.RunnerAheadOf(this);
            OffensePlayer behind = FieldManager.RunnerBehind(this);

            bool clearAhead = (ahead == null || (baseNum < ahead.baseTarget_Immediate));
            bool clearBehind = (behind == null || (baseNum > behind.baseTarget_Immediate));

            if (!clearAhead || !clearBehind)
            {
                cannotTargetBase = true;
            }

            return !cannotTargetBase;

        }
        else if (other == this as ISelectable)
        {
            return true;
        }

        return false;
    }

    public override void DirectTo(ISelectable other)
    {
        if (other is FieldBase)
        {
            int baseNum = (other as FieldBase).baseNum;
            RunToBase(baseNum);
            AnnounceHumanInstruction(new PlayerActionContainer(FieldPlayerAction.BASE, _baseNo: baseNum));
        }
        else if (other == this as ISelectable)
        {
            Debug.Log("SELECTED ON BASE 1");

            //if on a base, run to the next
            //if running to next base, run back to lastTaggedBase
            if (state == FieldPlayerAction.BASE)
            {
                Debug.Log("SELECTED ON BASE");
                int nextBaseNum = baseLastTouched + 1;
                RunToBase(nextBaseNum);
                AnnounceHumanInstruction(new PlayerActionContainer(FieldPlayerAction.BASE, _baseNo: nextBaseNum));
            }
            else
            {
                int nextBaseNum = baseLastTouched;
                RunToBase(nextBaseNum);
                AnnounceHumanInstruction(new PlayerActionContainer(FieldPlayerAction.BASE, _baseNo: nextBaseNum));
            }
        }
    }

    public override void DirectTo(Vector3 directPos)
    {
        /*
        Vector3 localDirectPos = WorldScaler.WorldToLocalPosition(directPos);

        RunToPosition(localDirectPos);
        AnnounceHumanInstruction(new PlayerActionContainer(FieldPlayerAction.RUN, pos: localDirectPos));
        */
    }

    public override void DragToOtherSelectable(ISelectable other, PathLine line)
    {
        if (other is FieldBase)
        {
            int selectedBaseNum = (other as FieldBase).baseNum;
            //line.SetColor(FieldManager.offenseColor);
            List<Vector3> points = new List<Vector3>();
            points.Add(GetSelectablePosition());

            int direction = 1;
            if (selectedBaseNum <= baseLastTouched)
            {
                direction = -1;
                points.Add(FieldManager.Instance.GetBase(baseLastTouched).GetSelectablePosition());

            }
            if (selectedBaseNum != baseLastTouched)
            {
                for (int i = baseLastTouched + direction; i != selectedBaseNum; i += direction)
                {
                    points.Add(FieldManager.Instance.GetBase(i).GetSelectablePosition());

                }
                points.Add(other.GetSelectablePosition());

            }
            Vector3[] pointsArray = points.ToArray();
            line.SetPositions(pointsArray);

            destinationPointer.transform.position = other.GetSelectablePosition();
        }
        else
        {
            DragToPoint(other.GetSelectablePosition(), line);
        }

    }
    public override void DragToPoint(Vector3 point, PathLine line)
    {
        /*
        line.startColor = FieldManager.offenseColor;
        line.endColor = FieldManager.offenseColor;
        base.DrawLineToPoint(point, line);
        destinationPointer.enabled = true;
        */
        if (destinationPointer != null)
            destinationPointer.enabled = false;

    }
    #endregion

}

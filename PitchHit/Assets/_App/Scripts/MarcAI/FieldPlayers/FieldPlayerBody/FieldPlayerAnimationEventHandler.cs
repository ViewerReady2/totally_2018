﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldPlayerAnimationEventHandler : MonoBehaviour {

    DefensePlayer def;
    OffensePlayer off;

    void Start()
    {
        def = GetComponentInParent<DefensePlayer>();
        off = GetComponentInParent<OffensePlayer>();
    }

    public void PitchBall()
    {
        if (def != null) def.PitchBall();
    }

    public void ThrowBall()
    {
        if (def != null) def.ThrowBall_AnimDelayed();
    }

    public void ParentBallToRightHand()
    {
        if (def != null) def.AIMoveBallFromGloveToHand();
    }

    public void BatterBecomesReady()
    {
        if(FieldManager.Instance && FieldManager.Instance.offense)
        {
            FieldManager.Instance.offense.isBatterReady = true;
        }
    }
}

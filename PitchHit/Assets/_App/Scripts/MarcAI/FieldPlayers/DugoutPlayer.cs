﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DugoutPlayer : AIFieldPlayer {

    new void Start()
    { 
        base.Start();
    }

    protected override void GetDressed()
    {
        base.GetDressed();
        bodyControl.EquipGear(FieldPlayerGear.CAP, UnityEngine.Random.Range(0f,1f) > .3f);
    }

    public override void InstantReset()
    {

    }


    public void SitOnBenchSeat(Transform seat, bool isLeft)
    {
        position = seat.position;
        transform.rotation = seat.rotation;
        bodyControl.SetAnimationInt("random", UnityEngine.Random.Range(0, 3));
        bodyControl.SetAnimationTrigger(isLeft? "sit_left" : "sit_right");


        
    }

    public void PaceBetweenPoints(Transform left, Transform right)
    {
        position = Vector3.Lerp(left.position, right.position, UnityEngine.Random.Range(0f, 1f));
        transform.rotation = left.rotation;
        bodyControl.SetAnimationInt("random", UnityEngine.Random.Range(0, 3));
        bodyControl.SetAnimationTrigger("stand");
    }
}

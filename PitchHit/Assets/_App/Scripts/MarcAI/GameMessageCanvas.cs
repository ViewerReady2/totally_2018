﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Canvas))]
public class GameMessageCanvas : MonoBehaviour {

    private static GameMessageCanvas Instance;
    protected Canvas canvas;
    public RectTransform messageContainer;
    public Text messageText;

    Text[] allText;

	// Use this for initialization
	void Awake () {
        if (Instance == null) Instance = this;
        else
        {
            GameObject.Destroy(this);
            return;
        }
        canvas = GetComponent<Canvas>();
	}

    void Start()
    {
        allText = GetComponentsInChildren<Text>();
        ForceRenderAllCharacters();
        Show_Hide(false);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    protected static void Show_Hide(bool isShowing)
    {
        if (!Instance) return;
        Instance.Show_Hide_Inst(isShowing);
    }

    protected void Show_Hide_Inst(bool isShowing)
    {
       canvas.enabled = isShowing;
    }



    public static void ShowMessage(string message, float duration = -1)
    {
        if (!Instance) return;
        Show_Hide(true);
        Instance.ShowMessage_Inst(message, duration);

    }

    public static void CloseMessage()
    {
        if (!Instance) return;
        Instance.CloseMessageContainer();

    }

    protected void ShowMessage_Inst(string message, float duration = -1)
    {
        float length = CalculateLengthOfMessage(message) + 400;
//        Debug.Log("Message length " + length);
        messageText.text = message;

        messageContainer.sizeDelta = new Vector2(length, messageContainer.sizeDelta.y);

        StopAllCoroutines();
        StartCoroutine(OpenMessageContainerRoutine(length));
        if(duration > 0)
            StartCoroutine(CloseMessageAfterSeconds(duration));
    }

    public void CloseMessageContainer()
    {
        StopAllCoroutines();
        StartCoroutine(CloseMessageContainerRoutine());
    }

    int CalculateLengthOfMessage(string message)
    {
        int totalLength = 0;

        Font myFont = messageText.font;  //chatText is my Text component
        CharacterInfo characterInfo = new CharacterInfo();

        char[] arr = message.ToCharArray();

        foreach (char c in arr)
        {
            myFont.GetCharacterInfo(c, out characterInfo, messageText.fontSize);
            totalLength += characterInfo.advance;
        }

        return totalLength;
    }

    IEnumerator OpenMessageContainerRoutine(float maxSize)
    {
        float t = 0;
        while (maxSize-t >.1f)
        {
            t = Mathf.Lerp(t, maxSize, Time.unscaledDeltaTime * 3);
            messageContainer.sizeDelta = new Vector2(t, messageContainer.sizeDelta.y);
            SetTextAlpha(t / maxSize);
            yield return null;

        }
        messageContainer.sizeDelta = new Vector2(maxSize, messageContainer.sizeDelta.y);
    }

    IEnumerator CloseMessageContainerRoutine()
    {
        float startSize = messageContainer.sizeDelta.x;
        float t = startSize;
        while (t > 10f)
        {
            t = Mathf.Lerp(t, 0, Time.unscaledDeltaTime * 6);
            messageContainer.sizeDelta = new Vector2(t, messageContainer.sizeDelta.y);
            SetTextAlpha(t / startSize);

            yield return null;

        }
        messageContainer.sizeDelta = new Vector2(0, messageContainer.sizeDelta.y);
        Show_Hide_Inst(false);
    }

    IEnumerator CloseMessageAfterSeconds(float seconds) {
        yield return new WaitForSeconds(seconds);
        CloseMessageContainer();
    }

    void ForceRenderAllCharacters()
    {
        messageText.text = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz";
    }

    void SetTextAlpha(float alpha)
    {
        foreach(Text text in allText)
        {
            Color c = text.color;
            c.a = alpha;
            text.color = c;
        }
    }


    
}

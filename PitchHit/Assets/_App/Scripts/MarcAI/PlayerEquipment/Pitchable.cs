﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System;
//using Valve.VR.InteractionSystem;

public class Pitchable : MonoBehaviour {
    public hittable hitComponent;
    public Rigidbody rb;
    public trailParticleManager trail;
    public CustomThrowable customThrowableComponent;
    public bool cantBeThrown;
    public GameObject cantBeThrownPuffPrefab;
    public UnityEvent OnPitchComplete;

    public static Action<hittable> OnBallThrown;
    private Valve.VR.InteractionSystem.VelocityEstimator velocityEstimator;

   

    //private CustomHand currentCustomHand;
    private bool isHeld;

	// Use this for initialization
	void Start () {
        //rb = GetComponent<Rigidbody>();
        if (rb)
        {
            rb.maxAngularVelocity = 21f;//Mathf.Infinity;
        }//Debug.Log("max ang: "+rb.maxAngularVelocity);

        //hitComponent = GetComponent<hittable>();
        velocityEstimator = GetComponent<Valve.VR.InteractionSystem.VelocityEstimator>();
    }

    public void OnGrabbed()
    {
        isHeld = true;
#if RIFT
        velocityEstimator.BeginEstimatingVelocity();
        rb.angularVelocity = Vector3.zero;
        rb.velocity = Vector3.zero;
#endif
    }

    public void OnReleased()
    {
        isHeld = false;
#if RIFT
        //Logic copied from Throwable, which will not be executed without Steam_VR
        rb.isKinematic = false;

        velocityEstimator.FinishEstimatingVelocity();
            Vector3 angularVelocity = velocityEstimator.GetAngularVelocityEstimate();
            angularVelocity = velocityEstimator.GetAngularVelocityEstimate();
            Vector3 velocity = velocityEstimator.GetVelocityEstimate();
			Vector3 position = velocityEstimator.transform.position;
            Vector3 r = transform.TransformPoint( rb.centerOfMass ) - position;
            rb.velocity = velocity + (Vector3.Cross( angularVelocity, r ) * Pitchable.flickDirectionFactor);
            rb.angularVelocity = angularVelocity;

			// Make the object travel at the release velocity for the amount
			// of time it will take until the next fixed update, at which
			// point Unity physics will take over
			float timeUntilFixedUpdate = ( Time.fixedDeltaTime + Time.fixedTime ) - Time.time;
			transform.position += timeUntilFixedUpdate * velocity;
			float angle = Mathf.Rad2Deg * angularVelocity.magnitude;
			Vector3 axis = angularVelocity.normalized;
			transform.rotation *= Quaternion.AngleAxis( angle * timeUntilFixedUpdate, axis );
#endif
        OnPitched(customThrowableComponent.GetAttachedHand());
    }
	
    public void OnPitched(CustomHand throwingHand)
    {
#if RIFT || STEAM_VR
        Debug.Log("Pitchable On Pitched");
        throwingHand.OnPitched(GetComponent<Rigidbody>());
        StartCoroutine(ThrowingRoutine(throwingHand));
        hitComponent.spotThrownFrom = transform.position;

        DarkTonic.MasterAudio.MasterAudio.PlaySound3DAtTransform("player_throw_ball", transform);
        
        //StartCoroutine(MagnusRoutine());
        if(trail)
        {
            trail.OnHit();
        }

        if(cantBeThrown)
        {
            StartCoroutine(InvalidPitchRoutine());
        }

#endif
    }
    

    IEnumerator InvalidPitchRoutine()
    {
        Vector3 initialPosition = transform.position;
        while(Vector3.Distance(transform.position,initialPosition)<3f)
        {
            if(hitComponent.GetWasHitByBat() || isHeld || (hitComponent.hitGround && rb.velocity.magnitude<1f))
            {
                yield break;
            }

            yield return null;
        }

        Instantiate(cantBeThrownPuffPrefab, transform.position, transform.rotation, null);
        Destroy(gameObject);
    }


#if STEAM_VR
    public static float flickDirectionFactor = 0;//3.0f;
    public static float throwSpeedMultiplier = .005f;//.005f;
#elif RIFT
    public static float throwSpeedMultiplier = .007f;
    public static float flickDirectionFactor = 1f;
#elif GEAR_VR
    public static float flickDirectionFactor = 1f;
    public static float throwSpeedMultiplier = .007f;
#else
    public static float flickDirectionFactor = 3.0f;
    public static float throwSpeedMultiplier = .005f;
#endif
#if RIFT || STEAM_VR

    public Renderer debugRenderer;


    IEnumerator ThrowingRoutine_MARC(CustomHand throwingHand)
    {
        //use grip value to determine influence after ball leaves hand
        //use velocity estimator to determine direction of influence
        //hand's sphere of influence is based on:
            // 1. ball speed relative to hand, fast hand has high influence on slow ball, slow hand has low influence on fast ball
            // 2. ball direction relative to hand direction. hand moving in same direction has high influence, hand moving perpendicular has no influence

        //NOTE: the ball's velocity will ALWAYS be weak on throw, needs to be PUSEHD

        int loopBreak = 0;
        
        float handFriction = 1;//0-1, 1 means ball moves with hand 1-1, 0 means hand has no influence on ball
        float distanceFromHand = 0;
        float ballSpeed = rb.velocity.magnitude;
        float gripValue = throwingHand.GripValue;
        float handSpeed = throwingHand.GetVelocity().magnitude;
        float ballSpeedRelativeToHand = 0;
        while (handFriction > 0)
        {

            gripValue = throwingHand.GripValue;
            distanceFromHand = Vector3.Distance(throwingHand.grippedBallLocation.position, this.transform.position);

            handSpeed = throwingHand.GetVelocity().magnitude;
            ballSpeed = rb.velocity.magnitude;

            ballSpeedRelativeToHand = ballSpeed - handSpeed;

            ballSpeed = rb.velocity.magnitude;


            //CALCULATE handFriction

            handFriction = throwingHand.GripValue;


            //CALCULATE handFriction direction


            //APPLY handFriction
            //blend handVelocity and ballVelocity
            rb.velocity = Vector3.Lerp(rb.velocity, throwingHand.GetVelocity(), handFriction);



            yield return new WaitForFixedUpdate();


            loopBreak++;
            if (loopBreak > 100)
            {
                Debug.LogWarning("Runaway While Loop?");
                break;
            }

        }



        yield return new WaitForFixedUpdate();

        StrikeZoneController.OnBallPitched(hitComponent);
        OnPitchComplete.Invoke();
        if (OnBallThrown != null) OnBallThrown.Invoke(hitComponent);


    }


    /// <summary>
    /// Greg Tomargo's throwing algorithm. Uses wrist rotation
    /// Runs in parallel with CustomHand.ThrowingRoutine
    /// </summary>
    /// <param name="throwingHand"></param>
    /// <returns></returns>
    IEnumerator ThrowingRoutine(CustomHand throwingHand)
    {
        //Color initialColor = Color.white;
        //if (debugRenderer)
        //{
        //   initialColor = debugRenderer.material.color;
        //    debugRenderer.material.color = Color.green;
        //}
        //Debug.Log("start throwing routine.");
        bool stillPushing = true;
#if RIFT
        float remainingVelocityAverages = throwingHand.GetVelocityFrameCount()/2f;
#else
        float remainingVelocityAverages = throwingHand.GetVelocityFrameCount()/2f;
#endif
        float initialRemainingVelocityAverages = remainingVelocityAverages;
        //Debug.Log("adittion averages vel:"+remainingVelocityAverages+" ang:"+remainingAngularAverages);

        while (stillPushing || remainingVelocityAverages>0)
        {
            yield return new WaitForFixedUpdate();
            //yield return null;

            Debug.Log("-------------------------------- PITCHABLE IS STILL BEING PITCHED!!");

            if (remainingVelocityAverages > 0)
            {
                //Debug.Log("throwing with remaining velocity averages "+ (remainingVelocityAverages / initialRemainingVelocityAverages));
                //Quaternion temp = Quaternion.FromToRotation(rb.velocity, throwingHand.GetVelocity());
                //rb.velocity = Vector3.Lerp(rb.velocity, temp * rb.velocity, .5f);

                Vector3 velocityTiltAxis = Vector3.Cross(rb.velocity, Vector3.up);

                Vector3 handVelocity = throwingHand.GetVelocity();
                //Vector3 handVelocity = Quaternion.AngleAxis(throwingHand.GetAngularVelocity().magnitude * 20f * Time.fixedDeltaTime, throwingHand.GetAngularVelocity()) * throwingHand.GetVelocity();


                float temp = GameController.AngleAroundAxis(rb.velocity, handVelocity, velocityTiltAxis);

                //Debug.Log("additional velocity averaging."+Time.time);

                //Lerp the direction of the ball's velocity to the direction the hand would now want it to go (but only vertically.)
                //rb.velocity = Vector3.Lerp(retargetedVelocity, retargetedVelocity.normalized * throwingHand.GetVelocity().magnitude, (remainingVelocityAverages / initialRemainingVelocityAverages));
#if STEAM_VR
                rb.velocity = Vector3.Lerp(rb.velocity, (Quaternion.AngleAxis(temp, velocityTiltAxis) * rb.velocity),remainingVelocityAverages / initialRemainingVelocityAverages);
#elif RIFT
                //rb.velocity = Vector3.Lerp(rb.velocity, (Quaternion.AngleAxis(temp, velocityTiltAxis) * rb.velocity), (1f - (remainingVelocityAverages / initialRemainingVelocityAverages))*.2f);
                rb.velocity = Vector3.Lerp(rb.velocity, (Quaternion.AngleAxis(temp, velocityTiltAxis) * rb.velocity), (remainingVelocityAverages / initialRemainingVelocityAverages));
#endif
                remainingVelocityAverages--;
            }

            if (stillPushing)
            {
                Vector3 gripVelocity = throwingHand.GetVelocity() * (1f + (throwSpeedMultiplier * Mathf.Pow(throwingHand.GetFlickDeltaEstimate(),2) ) );

                if (Vector3.Angle(gripVelocity, rb.velocity) < 90)
                {
                    if (gripVelocity.sqrMagnitude < rb.velocity.sqrMagnitude)
                    {
                        Debug.Log("too slow. not pushing anymore "+ gripVelocity.sqrMagnitude +" < "+rb.velocity.sqrMagnitude);
                        stillPushing = false;
                    }
                    else
                    {
                        rb.velocity = rb.velocity.normalized * gripVelocity.magnitude;

                        //if(gripAngularVelocity.sqrMagnitude>rb.angularVelocity.sqrMagnitude)
                        {
                            rb.angularVelocity = -1f * throwingHand.GetAngularVelocity();
                        }
                    }
                }
                else
                {
                    Debug.Log("going the wrong direction. not pushing anymore. " );
                    stillPushing = false;
                }
            }
            

        }
        //if (debugRenderer)
        //{
        //    debugRenderer.material.color = initialColor;
        //}
        //rb.angularVelocity = 20000f * Vector3.one;
        //yield return new WaitForFixedUpdate();

        ApplyAssist(rb, true);


        yield return new WaitForFixedUpdate();


        StrikeZoneController.OnBallPitched(hitComponent);
        OnPitchComplete.Invoke();
        if (OnBallThrown != null) OnBallThrown.Invoke(hitComponent);

    }

    public void DebugColor(bool on)
    {
        if (debugRenderer)
        {
            debugRenderer.material.color = on ? Color.red : Color.white;
        }
    }


    public static float assistWeight
    {
        get { return _assistWeight; }
        set { _assistWeight = Mathf.Clamp01(value); }
    }
    private static float _assistWeight;
    public static Vector3 assistTarget = Vector3.up;


    //if the throw is more than this many degrees off, no assist is given
    private const float maxAssistRange = 45f;
    /// <summary>
    /// 
    /// </summary>
    /// <param name="body"></param>
    /// <param name="targetHit">position in space pitch would hit ideally</param>
    /// <param name="assist">0 is no assist, 1 is direct to target</param>
    /// <returns></returns>
    void ApplyAssist(Rigidbody body, bool drawLine = false)
    {
        if (drawLine) {
            TrajectoryInfo debugInfo = new TrajectoryInfo(body);
            debugInfo.DebugDrawTrajectory(0, 3, Color.white, 10);
        }

        float assist = Mathf.Clamp01(assistWeight);
        float travelTime = Vector3.Distance(body.position, assistTarget) / body.velocity.magnitude;
        Vector3 idealVelocity = Trajectory.InitialVelocityNeededToHitPoint(body, assistTarget, travelTime);

        //if the ball would have to move faster to reach the target, it isn't fast enough
        if (idealVelocity.magnitude > body.velocity.magnitude) return;
        //if the throw is way off, don't do anything
        if (Vector3.Angle(body.velocity, assistTarget - body.position) > maxAssistRange) return;

        float naturalAccuracy = (maxAssistRange-Vector3.Angle(body.velocity, idealVelocity))/maxAssistRange;
        naturalAccuracy = Mathf.Clamp01(naturalAccuracy);
        //assist gets stronger the more accurate the throw is
        assist *= (naturalAccuracy * naturalAccuracy);

        //assist = 1;
        Debug.Log("Assist " + assist);
        if(assist>0)
            body.velocity = Vector3.Lerp(body.velocity, idealVelocity, assist);

        if (assist > 0 && drawLine)
        {
            TrajectoryInfo debugInfo = new TrajectoryInfo(body);
            debugInfo.DebugDrawTrajectory(0, 3, Color.yellow, 10);
        }
    }



#endif
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MonsterLove.StateMachine;
#if STEAM_VR
using Valve.VR.InteractionSystem;
#endif

public enum FieldPlayType
{
    NONE, //setting up for next pitch
    STANDARD, //get runs, get outs, the usual
    HOMERUN, //home run
    FOUL, //foul ball
    RETURN, //return the ball to the pitcher
    PITCH, //ball is travelling from the pitcher to the batter
    SWITCH, //teams are switching sides
    WALK, // 4 balls = walk for batter
    OPENNING, //openning ceremony
    CLOSING, //closing ceremony
    DISABLED, //do nothing
}
public abstract class AIFieldCoordinator<T> : MonoBehaviour where T : AIFieldPlayer
{
    public struct RankedFieldPlayer
    {
        public RankedFieldPlayer(T o, float v, Vector3 p)
        {
            fieldPlayer = o; value = v; position = p;
        }
        public T fieldPlayer;
        public float value;
        public Vector3 position;
        public override string ToString()
        {
            return "{PLAYER: " + fieldPlayer.ToString() + " VAL: " + value + " POS: " + position + "}";
        }
    }

    public FieldPlayType _state;
    public FieldPlayType runningPlay
    {
        get
        {
            if (fsm !=null)
            {
                return fsm.State;
            }
            else
            {
                return FieldPlayType.NONE;
            }
        }
    }
    private StateMachine<FieldPlayType> fsm;
    protected bool isInitialized = false;

    [Space(10, order = 0)]
    public T FieldPlayerPrefab;
    protected delegate void Instructions(T fieldPlayer);
    protected delegate void DirectedInstructions(T fieldPlayer, Transform transform);
    protected delegate bool Condition();

    protected delegate List<FieldManager.RankedPosition> RankedPositionSort(List<FieldManager.RankedPosition> positionList);
    protected delegate float PositionEvaluation(List<RankedFieldPlayer> candidates, Vector3 position, int baseNo = -1);
    protected delegate float PlayerEvaluation(AIFieldPlayer candidate, Vector3 position, int baseNo = -1);



    //protected T[] fieldPlayers;
    public bool debugCheats = false;
    public bool humanControlled { get; protected set; }
    private bool isResponsiveToFieldEvents = true;


    public virtual void Initialize()
    {
        if (isInitialized) return;
        fsm = StateMachine<FieldPlayType>.Initialize(this, FieldPlayType.DISABLED);
        fsm.Changed += SetEditorVisibleState;
        FieldManager.OnFieldEvent += RespondToFieldEvent;
        isInitialized = true;
    }


    protected virtual void Start()
    {
        if (!isInitialized) Initialize();
    }

    void OnDestroy()
    {
        FieldManager.OnFieldEvent -= RespondToFieldEvent;
    }

    private void RespondToFieldEvent(hittable ball, FieldPlayType play)
    {
        if (isResponsiveToFieldEvents) ExecutePlayWithBall(ball, play);   
    }

    protected abstract void ExecutePlayWithBall(hittable ball, FieldPlayType play);

    protected void SetPlayType(FieldPlayType play)
    {
        Debug.Log("<color=white>++++++++++++++++++++++++++++++++++++++++++++++++++++++++" + this.GetType().ToString() + " : " + play + "</color>");
        fsm.ChangeState(play);
    }


    private void SetEditorVisibleState(FieldPlayType state)
    {
        _state = state;
    }

    public virtual void SetHumanControlled(bool isHuman)
    {
        if (!isInitialized) Initialize();
        humanControlled = isHuman;
    }

    public void SetResponsiveToFieldEvents(bool isResponsive)
    {
        isResponsiveToFieldEvents = isResponsive;
    }

    public virtual void InstantReset()
    {

    }



}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public enum OutType
{
    STRIKEOUT,
    FOULS,
    CAUGHT,
    FORCED,
    DEFENDED,
    TAG,
    INNING_OVER,
    HIT_FOUL,
}

public class OffenseCoordinator : AIFieldCoordinator<OffensePlayer>
{
    public bool humanRunning;

    //[HideInInspector]
    private BatController humanBat
    {
        get
        {
            return HandManager.instance.bat;
        }
    }

    public BatterPlayer batterPrefab;
    private BatterPlayer batter
    {
        get {
            if (_batter == null)
            {
                _batter = Instantiate(batterPrefab, this.transform.parent);
                _batter.transform.localPosition = Vector3.left;
                _batter.transform.localRotation = Quaternion.identity;
            }
            return _batter;
        }
    }
    private BatterPlayer _batter;
    private OffensePlayer lastRunner;
    private int maxForcedBaseThisPlay = 0;
    private int runnerSerial = 0;

    private bool waitingForCatch = false;

    public bool isBatterReady;
    public Transform onDeckTransform;
    public Transform leftBatterBoxTransform;
    public Transform rightBatterBoxTransform;

    protected Condition PlayOverCondition;

    protected int batterIndex = 0;

    public static bool repeatLastLaunch
    {
        get
        {
            return PlayerPrefs.GetInt("repeatSavedLaunch") == 1;
        }
        protected set
        {
            PlayerPrefs.SetInt("repeatSavedLaunch", value ? 1 : 0);
        }
    }

    private static Vector3 m_savedLaunch;
    public static Vector3 savedLaunch
    {
        get
        {
            if (m_savedLaunch == Vector3.zero)
            {
                m_savedLaunch = new Vector3(
                    PlayerPrefs.GetFloat("savedLaunch_x"),
                    PlayerPrefs.GetFloat("savedLaunch_y"),
                    PlayerPrefs.GetFloat("savedLaunch_z")
                );
            }
            return m_savedLaunch;
        }
        set
        {
            m_savedLaunch = value;
            PlayerPrefs.SetFloat("savedLaunch_x", value.x);
            PlayerPrefs.SetFloat("savedLaunch_y", value.y);
            PlayerPrefs.SetFloat("savedLaunch_z", value.z);
        }
    }

    private static Vector3 m_savedLaunchOrigin;
    public static Vector3 savedLaunchOrigin
    {
        get
        {
            if (m_savedLaunchOrigin == Vector3.zero)
            {
                m_savedLaunchOrigin = new Vector3(
                    PlayerPrefs.GetFloat("savedLaunchOrigin_x"),
                    PlayerPrefs.GetFloat("savedLaunchOrigin_y"),
                    PlayerPrefs.GetFloat("savedLaunchOrigin_z")
                );
                
            }
            return m_savedLaunchOrigin;
        }
        set
        {
            m_savedLaunchOrigin = value;
            PlayerPrefs.SetFloat("savedLaunchOrigin_x", value.x);
            PlayerPrefs.SetFloat("savedLaunchOrigin_y", value.y);
            PlayerPrefs.SetFloat("savedLaunchOrigin_z", value.z);
        }
    }

    //public WorldScaler worldScale;

    /// <summary>
    /// Sets up critical fields, called in AIFieldPlayer.Start()
    /// </summary>
    public override void Initialize()
    {
        if (isInitialized) return;
        //PrepareBatter();

        base.Initialize();
    }


    void Awake()
    {

    }

    void Update()
    {
        CheckForCheatInput();

        if (FieldManager.Instance.DefenseReadyForNewBatter() && !batter.isActiveAndEnabled)
        {
            PrepareBatter();
        }
    }

#if UNITY_EDITOR
    
    void OnDrawGizmos()
    {
        if (repeatLastLaunch)
        {
            Handles.color = Color.magenta;
            Handles.DrawRectangle(0, Vector3.zero, Quaternion.LookRotation(Vector3.up), 2);

            TrajectoryInfo traj = new TrajectoryInfo();
            traj.SimulatePosition(new Vector3(0, 1, 1));

            traj.SimulateVelocity(savedLaunch);
            traj.SimulatePosition(savedLaunchOrigin);
            traj.DebugDrawTrajectory_Handles();
        }

    }
#endif

    public override void InstantReset()
    {
        base.InstantReset();
        batterIndex = 0;
        RemoveAllRunners();
        if(batter)
        batter.gameObject.SetActive(false);
    }

    public override void SetHumanControlled(bool isHuman)
    {
        base.SetHumanControlled(isHuman);

        GameController.isAtBat = isHuman;
        if (isHuman)
        {
            HumanPosessionManager.SetInitialPositionRotation(FieldManager.Instance.GetHomePlatePosition(), Quaternion.LookRotation(Vector3.forward, Vector3.up));
            HumanPosessionManager.PosessPlayer(batter, resetPlayer: true, resetPlayArea: true, fade: true, slowMo: false, cancelIfAlreadyTethered: false);
            HumanPosessionManager.ConstrainProjectionToRail(true);
        }
        else
        {
        }
        
        if (runningPlay == FieldPlayType.NONE)
        {
            NONE_Enter();
        }
        else SetPlayType(FieldPlayType.NONE);
        
        //RefreshState();
    }


    void CheckForCheatInput()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            ForceLoadBase(0);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            ForceLoadBase(1);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            ForceLoadBase(2);
        }
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            repeatLastLaunch = !repeatLastLaunch;
            Debug.Log("Repeat Launch " + repeatLastLaunch + " " + savedLaunch);

        }
    }


    /// <summary>
    /// Immediately place a runner onto a base
    /// </summary>
    /// <param name="baseNo">Base to put a runner on</param>
    void ForceLoadBase(int baseNo)
    {
        if (baseNo >= 0 && baseNo < FieldManager.Instance.numBases)
        {
            OffensePlayer newRunner = SpawnRunner(baseNo);
            newRunner.position = FieldManager.Instance.GetBasePosition(baseNo);

            newRunner.StayOnBase(baseNo);
        }
    }

    protected override void ExecutePlayWithBall(hittable hitBall, FieldPlayType playType)
    {
        if (hitBall != null)
        {
            hitBall.OnCaughtByDefender += CheckForOutOnCatch;
            waitingForCatch = FieldManager.AirInterceptionEstimates(hitBall).Count > 0;
        }
        SetPlayType(playType);
    }

    void SendOutNewRunner()
    {
        int maxForcedBase = FieldManager.Instance.MaxForcedBase();
        //        Debug.Log("MAX FORCED BASE  " + maxForcedBase);

        foreach (OffensePlayer runner in FieldManager.offensivePlayers)
        {
            int nextBase = runner.baseAtPitch + 1;
            if (nextBase <= maxForcedBase)
            {
                runner.RunToBaseForced(nextBase);
            }
            //if not forced, runners will make decisions about running in UpdateRunners()                
        }

        lastRunner = SpawnRunner();

        lastRunner.RunToBaseForced(0);

        if (humanControlled && humanRunning)
        {
            //Debug.Log("PLAYER HAS TO RUN WITH ARMS!");
            GameController.isAtBat = false;
            HumanPosessionManager.PosessPlayer(lastRunner, fade: false, slowMo: false, resetPlayer: false, resetPlayArea: false);
            HumanPosessionManager.SetArmLocomotionEnabled(true, 1f);
            HumanPosessionManager.SetProjectionRailPoints(FieldManager.Instance.GetHomePlatePosition(), FieldManager.Instance.GetBasePosition(0));
            HumanPosessionManager.SetReverseRailLocomotionEnabled(false);
        }

        if (!humanControlled)
            switcherooRoutine = StartCoroutine(BatterRunnerSwitcheroo(batter, lastRunner));
    }

    void CheckForForcedOuts()
    {
        foreach (OffensePlayer runner in FieldManager.offensivePlayers)
        {
            
            //if the runner has no choice, check to see if they're out by force
            if (runner.forcedRun)
            {
                //if their forced base is defended
                if (FieldManager.Instance.BaseIsDefended(runner.baseTarget_Eventual))
                {
                    //if they're currently on their forced base, continue to the next runner
                    int onBase;
                    if (FieldManager.Instance.RunnerIsOnBase(runner, out onBase))
                    {
                        if (onBase == runner.baseTarget_Eventual)
                            continue;
                    }
                    runner.NotifyOfOut(OutType.FORCED);
                }

                
                
            }
        }
    }

    public void ReactToBattingOut(OutType outType)
    {
        Debug.Log("batter " + batter);
        Debug.Log("find batter " + FindObjectOfType<BatterPlayer>());
        OneShotEffectManager.SpawnEffect("player_out", batter.transform.position, batter.transform.rotation, lifetime: 3f, spectatorOnly: batter.isTetheredToCamera);
        batter.gameObject.SetActive(false);
        //batter will reappear in none_enter 
        
    }

    /// <summary>
    /// Iterate through list of runners and make decisions about whether to run forward, run back, or stay put
    /// </summary>
    void UpdateRunnerTargetBases()
    {

        AIFieldCoordinator<DefensePlayer>.RankedFieldPlayer posessionEstimate = new AIFieldCoordinator<DefensePlayer>.RankedFieldPlayer();

        List<AIFieldCoordinator<DefensePlayer>.RankedFieldPlayer> airInterception = FieldManager.AirInterceptionEstimates(FieldManager.Instance.ballInPlay);
        if (airInterception.Count > 0) posessionEstimate = airInterception[0];
        else
        {
            List<AIFieldCoordinator<DefensePlayer>.RankedFieldPlayer> groundInterception = FieldManager.GroundInterceptionEstimates(FieldManager.Instance.ballInPlay);
            if (groundInterception.Count > 0) posessionEstimate = groundInterception[0];
        }

        foreach (OffensePlayer runner in FieldManager.offensivePlayers)
        {
            //Debug.Log("runner " + runner.name + " " + runner.state);
            //if this player is out, don't give them any instructions
            if (runner.state == AIFieldPlayer.FieldPlayerAction.OUT)
            {

                continue;
            }

            //if the runner has no choice, no need to make calculations. If the runner is tethered, do not change it's state
            if (!runner.forcedRun && !runner.isTetheredToCamera)
            {
                int baseLastTagged = runner.baseLastTouched;
                int nextBase = baseLastTagged + 1;

                if (ShouldRunToNextBase(runner, baseLastTagged, nextBase, posessionEstimate))
                {
//                    Debug.Log(runner.name + " run to next base " + nextBase);
                    //if the runner isn't already running to the next base, tell it to
                    //if (runner.baseTarget_Immediate != nextBase)
                    runner.RunToBase(nextBase);
                }

                else
                {
                    //runner is already on base, no need to do anything
                    if (Vector3.Distance(runner.position, FieldManager.Instance.GetBasePosition(baseLastTagged)) < .1f)
                    {
                        runner.StayOnBase(baseLastTagged);
                    }

                    //if the runner isn't already running back to baseLastTouched, tell it to
                    else if (runner.baseTarget_Eventual != baseLastTagged)
                    {
                        runner.RunToBase(baseLastTagged);
                    }
                }
            }
        }


    }

    /// <summary>
    /// Give directions to runner who has just reached a base.
    /// <param name="baseNo"></param>
    /// <param name="runner"></param>
    void GiveDirectionsToRunnerOnBase(int baseNo, OffensePlayer runner)
    {
        //if runner is currently being controlled, 
        if (runner.isTetheredToCamera)
        {

        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="runner"></param>
    /// <param name="lastBase"></param>
    /// <param name="nextBase"></param>
    /// <returns>true means run to next base, false means run to last base</returns>
    bool ShouldRunToNextBase(OffensePlayer runner, int lastBase, int nextBase, AIFieldCoordinator<DefensePlayer>.RankedFieldPlayer posession)
    {
        if (runningPlay == FieldPlayType.HOMERUN) return true;
        if (runningPlay == FieldPlayType.FOUL) return false;
        if (runningPlay == FieldPlayType.WALK) return false;


        if (waitingForCatch) return false;

        //if there's no estimation of who will have the ball next, the ball might be destroyed. so go for it.
        if (posession.fieldPlayer == null) return true;

        FieldManager.FieldState state = FieldManager.Instance.fieldState;
        OffensePlayer ahead = FieldManager.RunnerAheadOf(runner);
        if (ahead != null && ahead.baseTarget_Immediate == nextBase) return false;
        //if(state.baseOccupied[nextBase]) return false;

        Vector3 pos_nextBase = FieldManager.Instance.GetBasePosition(nextBase);
        Vector3 pos_lastBase = FieldManager.Instance.GetBasePosition(lastBase);
        float time_runToNextBase = runner.GetTimeToRunToPosition(pos_nextBase);// Vector3.Distance(runner.transform.position, pos_nextBase) / runner.runSpeed;
        float time_runToLastBase = runner.GetTimeToRunToPosition(pos_lastBase);// Vector3.Distance(runner.transform.position, pos_lastBase) / runner.runSpeed;
        float time_ballToNextBase = posession.value + Vector3.Distance(posession.position, pos_nextBase) / posession.fieldPlayer.throwSpeed;
        float time_ballToLastBase = posession.value + Vector3.Distance(posession.position, pos_lastBase) / posession.fieldPlayer.throwSpeed;

        bool safeToNext = time_runToNextBase < time_ballToNextBase;
        bool safeToLast = time_runToLastBase < time_ballToLastBase;

        runner.SetDebugText(time_runToNextBase.ToString("0.0") + " " + time_ballToNextBase.ToString("0.0"));

        //        Debug.Log("Should Run To Next " + lastBase + " " + nextBase + " " + time_runToNextBase + " " + time_ballToNextBase + " " + posession + " " + Vector3.Distance(posession.position, pos_nextBase) / posession.fieldPlayer.throwSpeed);
        if (safeToNext) return true;
        else if (safeToLast) return false;
        else return true;
    }

    /// <summary>
    /// Called whenever the playBall is caught. Checks to see if the ball ever touched the ground and gets the last runner out if not.
    /// </summary>
    /// <param name="ball"></param>
    /// <param name="catcher"></param>
    void CheckForOutOnCatch(hittable ball)
    {
        if (runningPlay == FieldPlayType.PITCH
            || runningPlay == FieldPlayType.HOMERUN
            || runningPlay == FieldPlayType.WALK)
        {
            //do nothing
        }

        else if (!ball.hasEverHitGround)
        {
            if (lastRunner)
            {
                lastRunner.NotifyOfOut(OutType.CAUGHT);
            }
            ForceReturnRunnersToBaseAtPitch();
        }

        ball.OnCaughtByDefender -= CheckForOutOnCatch;
        waitingForCatch = false;
    }


    void ForceReturnRunnersToBaseAtPitch()
    {
        foreach (OffensePlayer runner in FieldManager.offensivePlayers)
        {
            //players that are out need to stay out
            if (runner.state == AIFieldPlayer.FieldPlayerAction.OUT) continue;
            if (runner == lastRunner) continue;
            runner.RunToBaseForced(runner.baseAtPitch);
        }
    }

    OffensePlayer SpawnRunner(int baseNo = -1)
    {
        OffensePlayer runner = Instantiate(FieldPlayerPrefab, this.transform.parent, false) as OffensePlayer;
        runner.Initialize(baseNo);
        //Debug.Log("spawn runner at base:"+baseNo+" at "+ FieldManager.Instance.GetBasePosition(baseNo));

        if (baseNo == -1)
        {
            if (humanControlled)//Greg: Sometimes the player was jerking to the batter's forced position. So I skipped the middle man.
            {
                Vector3 playerPosition = Camera.main.transform.position;
                playerPosition.y = 0f;
                runner.position = playerPosition;
            }
            else
            {
                runner.position = batter.position;
            }
        }
        else
        {
            runner.position = FieldManager.Instance.GetBasePosition(baseNo);
        }
        //			runner.SetWorldScaler (worldScale);

        TeamInfo tInfo;
        if (FieldManager.currentInningFrame == FieldManager.InningFrame.Top)
        {
            tInfo = FieldManager.awayTeam;
        }
        else
        {
            tInfo = FieldManager.homeTeam;
        }

        PlayerInfo pInfo;
        if (batter) pInfo = batter.info;
        else pInfo = tInfo.GetRandomBatter();

        runner.SetPlayerInfo(tInfo, pInfo);

        runner.runSpeed = pInfo.runSpeedOffense;

        //runner.DressLikeOtherPlayer(batter);

        Debug.DrawRay(runner.position, Vector3.up * 3f, Color.red, 100f);

        CapsuleCollider colid = runner.GetComponent<CapsuleCollider>();
        if (colid)
        {
            colid.radius *= .5f;
            colid.height *= .5f;
        }

        runner.OnOut += ReactToPlayerOut;
        runner.OnBaseReached += GiveDirectionsToRunnerOnBase;
        runner.OnFieldPlayerStateChange += ReactToPlayerStateChange;
        runner.OnHumanInstruction += ReactToPlayerReceivingHumanInstructions;
        runner.OnHumanPosession += ReactToHumanPosession;
        runner.OnHumanDeposession += ReactToHumanDeposession;
        runner.name = "Runner_" + runnerSerial;
        runnerSerial++;
        return runner;
    }

    private Coroutine switcherooRoutine;
    IEnumerator BatterRunnerSwitcheroo(BatterPlayer bat, OffensePlayer run)
    {
        run.gameObject.SetActive(false);
        yield return new WaitForSeconds(.5f);
        if(run != null) run.gameObject.SetActive(true);
        if(bat != null) bat.gameObject.SetActive(false);
        isBatterReady = false;
        if (FieldManager.OnHideBatter != null) FieldManager.OnHideBatter();
        //FieldManager.Instance.UnRegisterBatterFromMinimap();
        //yield return new WaitForSeconds(2);
    }

    Coroutine newBatterWalkupRoutine;
    protected void PrepareBatter(float delay = 0, bool startAtPlate = false)
    {
        if(batter && !batter.isActiveAndEnabled)
        {
                StartCoroutine(SendOutBatterRoutine(delay, startAtPlate));
        }

        /*
        Debug.Log("PrepareBatter()");
        if (FieldManager.currentInningFrame == FieldManager.InningFrame.Middle)
        {
            return;
        }

        if (batter && batter.isActiveAndEnabled)
        {
            if (newBatterWalkupRoutine != null)
            {
                return;
            }

            isBatterReady = true;
            batter.PlaySwingSetupAnimation();
            return;
        }

        Debug.Log("Batter walk up");
        newBatterWalkupRoutine = StartCoroutine(SendOutBatterRoutine(delay));
        */
    }

    IEnumerator SendOutBatterRoutine(float delay, bool startAtPlate)
    {
        batter.gameObject.SetActive(true);
        batter.DitherFade(0, 0, 0);

        if (delay > 0)
            yield return new WaitForSeconds(delay);

        TeamInfo team = FieldManager.currentOffensiveTeam;
        if (FieldManager.OnBattingOrderUpdated != null) FieldManager.OnBattingOrderUpdated(team.battingQueue);
        PlayerInfo newBatterInfo = team.GetNextBatter();
        batter.SetPlayerInfo(team, newBatterInfo);

        if (startAtPlate)
        {
            batter.transform.localPosition = Vector3.left;
            batter.transform.localRotation = Quaternion.identity;
        }
        else
        {
            batter.position = onDeckTransform.position;
            batter.transform.rotation = onDeckTransform.rotation;
        }
        batter.gameObject.SetActive(true);
        batter.DitherFade(0, 1, .5f);
        if (FieldManager.OnShowBatter != null) FieldManager.OnShowBatter(batter.transform, batter.info);

        batter.StartWalkingAnimation();
        batter.RunToTarget(leftBatterBoxTransform);
    }

    void RemoveAllRunners()
    {
        foreach (OffensePlayer runner in FieldManager.offensivePlayers)
        {
            if (runner.state != AIFieldPlayer.FieldPlayerAction.OUT)
                runner.NotifyOfOut(OutType.INNING_OVER);
        }
    }

    public void EndPlay()
    {
//        Debug.Log("EndPlay for Offense");
        if (_state == FieldPlayType.NONE)//Greg: In case we have the EndPlay functions calling each other we don't want a stack overflow.
        {
            return;
        }


        SetPlayType(FieldPlayType.NONE);

        //        Debug.Log("..............................OFFENSE COORDINATOR STANDAR EXIT.");
        if (humanControlled)
        {

            /*
            Debug.Log("EndPlay human controlled");
            if (HumanPosessionManager.currentlyTetheredPlayer)
            {
                Debug.Log("EndPlay untether current player.");
                HumanPosessionManager.ReleasePosessedPlayer();
                //FieldManager.Instance.currentlyTetheredPlayer.SetHumanPosession(false);
            }

            HumanPosessionManager.ResetPlayAreaToInitialPositionRotation();

            HumanPosessionManager.SetArmLocomotionEnabled(false);
            */

        }
    }

    void ReactToHumanPosession(AIFieldPlayer player)
    {

    }

    void ReactToHumanDeposession(AIFieldPlayer player)
    {
        //if the runner is on a base, make sure they center themself on that base
        if (player.state == AIFieldPlayer.FieldPlayerAction.BASE) player.InstantReset();
    }


    public bool SwingDetected()
    {
        if (humanControlled) return humanBat.SwingWasDetected();
        return batter.AutoSwingDetected();
    }

    #region Conditions

    bool NoActiveRunners()
    {
        return (FieldManager.offensivePlayers.Count == 0);

    }

    bool AllRunnersOnBase()
    {
        return FieldManager.Instance.AllRunnersOnBase();
    }

    bool BallIsNoLongerAirborne()
    {
        if (FieldManager.Instance.ballInPlay.hitGround
                || FieldManager.Instance.ballInPlay.hasBeenCaught
                || FieldManager.WorldToLocalPosition(FieldManager.Instance.ballInPlay.transform.position).y < -1)
        {
            return true;
        }
        return false;
    }


    #endregion


    #region PlayerState_Machine


    void ReactToPlayerOut(AIFieldPlayer player, OutType outType)
    {
        Debug.Log("react to plyaer out " + outType + " " + player.isTetheredToCamera);
        //anytime a posessed runner gets tagged out, make sure posession transfers to the batter

        //NOTE: We don't always want to posess the batter when a runner gets out. if it's the end of the inning
        //No guarantee FieldManager will update state 
        if (player.isTetheredToCamera)
        {
            StartCoroutine("PosessBatterIfHumanControlledAfterFrame");
        }

    }

    IEnumerator PosessBatterIfHumanControlledAfterFrame()
    {
        yield return null;
        if(humanControlled)
            HumanPosessionManager.PosessPlayer(batter, fade: true, resetPlayArea: true, resetPlayer: true, cancelIfAlreadyTethered: true);

    }
    void ReactToPlayerStateChange(AIFieldPlayer player, AIFieldPlayer.FieldPlayerAction newState)
    {

    }

    void ReactToPlayerReceivingHumanInstructions(AIFieldPlayer instructedPlayer, AIFieldPlayer.PlayerActionContainer instructedAction)
    {

        OffensePlayer instructedDefender = instructedPlayer as OffensePlayer;
        if (instructedDefender == null) return;

        switch (instructedAction.action)
        {

            case AIFieldPlayer.FieldPlayerAction.BASE:
                break;
            case AIFieldPlayer.FieldPlayerAction.RUN:
                break;

        }
    }
    #endregion

    #region STATE_MACHINE
    void NONE_Enter()
    {
        if (humanControlled)
        {
            bool isStartOfGame = false;

            HumanPosessionManager.PosessPlayer(batter, resetPlayArea: true, fade: !isStartOfGame, cancelIfAlreadyTethered: true);
            isBatterReady = true;
        }
        else
        {
            //Debug.Log("PrepareBatter after outs " + FieldManager.currentOuts);
            PrepareBatter();
        }
    }

    void NONE_Exit()
    {

    }

    void PITCH_Enter()
    {
        PlayOverCondition = BallIsNoLongerAirborne;
        lastRunner = null;

        foreach (OffensePlayer runner in FieldManager.offensivePlayers)
        {
            runner.RecordBaseAtPitch();
        }

        if (!humanControlled)
        {
            batter.PrepareForAutoBat(FieldManager.Instance.strikeGate, FieldManager.Instance.ballInPlay);
        }


        if (humanControlled)
        {
            humanBat.ResetSwingFlag();
            if (humanControlled)
            {
                humanBat.SetColliderActive(true);
            }
            else
            {
                humanBat.SetColliderActive(false);
            }
        }
    }

    void PITCH_Update()
    {
        if (PlayOverCondition())
        {
            EndPlay();
        }
    }

    void PITCH_Exit()
    {
        if (humanControlled)
        {
            humanBat.SetColliderActive(false);
        }

    }

    void STANDARD_Enter()
    {
        PlayOverCondition = NoActiveRunners;
        SendOutNewRunner();

        //hide the batter so he doesn't appear in the player's face suddenly when not posessed
        if (humanControlled)
        {
            batter.gameObject.SetActive(false);
        }
    }

    void STANDARD_Update()
    {
        if (waitingForCatch && FieldManager.Instance.ballInPlay.hasEverHitGround) waitingForCatch = false;
        CheckForForcedOuts();
        UpdateRunnerTargetBases();


        if (humanControlled)
        {
            //MARC: this may be causing player to get kicked out of a runner prematurely
            // Debug.Log(FieldManager.Instance.PitcherOnMoundWithBall() + "   " + FieldManager.Instance.AllRunnersOnBase());

            if (FieldManager.Instance.PitcherOnMoundWithBall() && FieldManager.Instance.AllRunnersOnBase())
            {
                EndPlay();
            }

        }
        if (PlayOverCondition()) EndPlay();


    }
    void STANDARD_Exit()
    {
        // StartCoroutine(BatterWalkUpRoutine());
    }

    void HOMERUN_Enter()
    {
        PlayOverCondition = NoActiveRunners;
        AbridgeHomeRun();
    }
    void HOMERUN_Update()
    {
        UpdateRunnerTargetBases();
        if (PlayOverCondition()) EndPlay();

    }

    void AbridgeHomeRun()
    {
        StartCoroutine("HOMERUN_Abridge");
    }

    IEnumerator HOMERUN_Abridge()
    {
        while (FieldManager.offensivePlayers.Count > 0)
        {
            LinkedListNode<OffensePlayer> run = FieldManager.offensivePlayers.Last;
            while (run != null)
            {
                if (run.Value.state != AIFieldPlayer.FieldPlayerAction.OUT)
                {
                    run.Value.NotifyOfRunCompletion();
                    break;
                }
                run = run.Previous;
            }

            yield return new WaitForSeconds(1);
        }
    }

    void HOMERUN_Exit()
    {
        StopCoroutine("HOMERUN_Abridge");
    }

    void FOUL_Enter()
    {
        FieldManager.Instance.ExplodeBall(1);

        PlayOverCondition = AllRunnersOnBase;

        //reset last batter
        Debug.Log("FOUL  offense ");

        if (switcherooRoutine != null)
        {
            StopCoroutine(switcherooRoutine);
            PrepareBatter(startAtPlate: true);
            //batter.gameObject.SetActive(true);
        }

        if (lastRunner != null)
        {
            Debug.Log("deal with hit foul");
            lastRunner.gameObject.SetActive(true);
            lastRunner.NotifyOfOut(OutType.HIT_FOUL);
            //GameObject.Destroy(lastRunner.gameObject);
        }



        ForceReturnRunnersToBaseAtPitch();

    }

    void FOUL_Update()
    {
        UpdateRunnerTargetBases();
        if (PlayOverCondition()) EndPlay();

    }

    void FOUL_Exit()
    {

    }

    void RETURN_Enter()
    {
        PlayOverCondition = NoActiveRunners;
    }

    void RETURN_Update()
    {
        if (PlayOverCondition()) EndPlay();
    }

    void RETURN_Exit() { }

    void SWITCH_Enter()
    {
        InstantReset();
    }

    void SWITCH_Update()
    {

    }
    void SWITCH_Exit()
    {


    }

    void WALK_Enter()
    {
        PlayOverCondition = AllRunnersOnBase;
        SendOutNewRunner();
    }

    void WALK_Update()
    {
        UpdateRunnerTargetBases();
        if (PlayOverCondition()) EndPlay();
    }

    void WALK_Exit()
    {

    }

    void OPENNING_Enter()
    {

    }

    void OPENNING_Update()
    {

    }

    void OPENNING_Exit()
    {
    }

    void CLOSING_Enter()
    {
        RemoveAllRunners();
        batter.gameObject.SetActive(false);

    }

    void CLOSING_Update()
    {
    }

    void CLOSING_Exit()
    {
    }

    void DISABLED_Enter()
    {

    }

    void DISABLED_Exit()
    {

    }


    #endregion

    public void RefreshState()
    {
        switch (_state)
        {
            case FieldPlayType.STANDARD:
                STANDARD_Enter();
                break;
            case FieldPlayType.FOUL:
                FOUL_Enter();
                break;
            case FieldPlayType.HOMERUN:
                HOMERUN_Enter();
                break;
            case FieldPlayType.NONE:
                NONE_Enter();
                break;
            case FieldPlayType.PITCH:
                PITCH_Enter();
                break;
            case FieldPlayType.RETURN:
                RETURN_Enter();
                break;
        }
    }
}

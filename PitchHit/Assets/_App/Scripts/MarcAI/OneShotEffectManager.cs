﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


[Serializable] public class StringToObjectDictionary : SerializableDictionary<string, GameObject> { }


public class OneShotEffectManager : MonoBehaviour {

    private static Dictionary<string, GameObject> effectDictionary_static
    {
        get
        {
            if(_effectDictionary_static == null) _effectDictionary_static = new Dictionary<string, GameObject>();
            return _effectDictionary_static;
        }
        set
        {
            _effectDictionary_static = value;
        }
    }
    private static Dictionary<string, GameObject> _effectDictionary_static;
    public StringToObjectDictionary effects;


    void Start()
    {
        AddEffectsToStaticDictionary();
    }

    void AddEffectsToStaticDictionary()
    {
        foreach(string key in effects.Keys)
        {
            if(!effectDictionary_static.ContainsKey(key))
            effectDictionary_static.Add(key, effects[key]);
        }
    }


    public static void SpawnEffect(string effectKey, Vector3 position, Quaternion orientation, float lifetime = -1, bool spectatorOnly = false)
    {
        if (effectDictionary_static == null) return;
        GameObject prefab;
        if(effectDictionary_static.TryGetValue(effectKey, out prefab)){
            GameObject effect = GameObject.Instantiate(prefab, position, orientation);
            if (spectatorOnly)
            {
                GameController.SetLayerRecursively(effect, LayerMask.NameToLayer("SpectatorOnly"));
            }
            if (lifetime > 0)
            {
                Destroy(effect, lifetime);
            }
        }
    }
}

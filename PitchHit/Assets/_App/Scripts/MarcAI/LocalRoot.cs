﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalRoot : MonoBehaviour {

    
    //private static WorldScaler Instance;
    /*
	public static float worldScale 
	{
		get { return _worldScale; }
		set {_worldScale = value;
			Physics.gravity = Vector3.up * initialGravity * _worldScale;
            //Physics.defaultContactOffset = initialDefaultContactOffset * _worldScale* _worldScale * _worldScale;
        }
	}
    */
    //public static float elevation;
	//public float scaleToStartWith = .1f;
	//private static float _worldScale = 1f;
	//public static float initialGravity = Physics.gravity.magnitude * -1f;
    //private static float initialDefaultContactOffset = .01f;
	// Use this for initialization
    /*
	void Awake () {
        if (Instance == null) Instance = this;
        else
        {
            GameObject.Destroy(this.gameObject);
            return;
        }
        //initialDefaultContactOffset = Physics.defaultContactOffset;
        //worldScale = scaleToStartWith;
        Physics.defaultContactOffset = .001f;

        //transform.localScale = Vector3.one * _worldScale;
        //elevation = transform.position.y;
        //Debug.Log(elevation+" is the elevation of the world scaler.");
        Debug.Log("physics grav "+Physics.gravity.y);
    }
    */

    public Vector3 LocalToWorldPosition(Vector3 localPos)
    {
        return transform.TransformPoint(localPos);
    }

    public Vector3 LocalToWorldDirection(Vector3 localDir)
    {
        return transform.TransformDirection(localDir);
    }

    public Vector3 WorldToLocalPosition(Vector3 worldPos)
    {
        return transform.InverseTransformPoint(worldPos);
    }

    public Vector3 WorldToLocalDirection(Vector3 worldDir)
    {
        return transform.InverseTransformDirection(worldDir);
    }

    public void DrawDebugLine(Vector3 localPoint_a, Vector3 localPoint_b, Color color, float t = -1 )
    {
        if (t < 0) t = Time.deltaTime;
        Debug.DrawLine(
            LocalToWorldPosition(localPoint_a),
            LocalToWorldPosition(localPoint_b),
            color,
            t);
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMarkerHandler : MonoBehaviour
{

    public BallLandingSpotMarker groundedMarker;

    void Awake()
    {
        HideMarker();
        FieldManager.OnFieldEvent += DropMarkerForBall;
    }

    void OnDestroy()
    {
        FieldManager.OnFieldEvent -= DropMarkerForBall;
    }

    // Use this for initialization
    void DropMarkerForBall(hittable ball, FieldPlayType play)
    {
        if (ball == null || (play != FieldPlayType.STANDARD)) return;
        TrajectoryInfo trajectory = new TrajectoryInfo(ball.GetRigidbody());
        groundedMarker.ShowTrajectory(ball, trajectory);
        Vector3 groundPos = trajectory.groundHitPosition;
        groundedMarker.transform.localPosition = groundPos;
        AddListeners(ball);
    }

    void BallCaught(hittable ball)
    {
        RemoveListeners(ball);
    }

    void BallHitGround(hittable ball)
    {

    }

    void AddListeners(hittable ball)
    {
        ball.OnCaughtByDefender += BallCaught;
        //ball.OnHitGround += BallHitGround;
        ball.OnBallDestroy += RemoveListeners;
    }

    void RemoveListeners(hittable ball)
    {
        HideMarker();

        ball.OnCaughtByDefender -= BallCaught;
        //ball.OnHitGround -= BallHitGround;
        ball.OnBallDestroy -= RemoveListeners;
    }

    // Update is called once per frame
    void HideMarker()
    {
        groundedMarker.ShowHide(false);
    }


}

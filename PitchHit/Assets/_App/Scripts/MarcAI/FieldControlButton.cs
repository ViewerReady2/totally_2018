﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class FieldControlButton : MonoBehaviour, ISelectable {

    public SpriteRenderer icon;

	public Color activeColor = Color.green;
    public Color litColor;
    protected Color unlitColor;

    protected bool _highlit;
    protected Material buttonRenderer;
	protected bool isActive;

    // Use this for initialization
    protected void Start()
    {
        if (buttonRenderer == null)
        {
            buttonRenderer = GetComponent<MeshRenderer>().material;
        }
        unlitColor = GetDesaturatedColor(litColor);
        SetSelectableState(SelectableState.AVAILABLE);
    }

    protected Color GetDesaturatedColor(Color original)
    {
        float h; float s; float v;
        Color.RGBToHSV(original, out h, out s, out v);
        return Color.HSVToRGB(h, s / 2f, v);
    }

    protected void SetColor(Color c)
    {
        if(buttonRenderer==null)
        {
            buttonRenderer = GetComponent<MeshRenderer>().material;
        }

      buttonRenderer.color = c;
    }

    protected abstract void OnButtonPress();
    

    public Vector3 GetSelectablePosition()
    {
        return transform.position;
    }

    public virtual bool CanBePrimarySelection()
    {
        return true;
    }
    public virtual bool CanBeDirectedToSelectable(ISelectable other)
    {
        return false;
    }

    public virtual void DirectTo(ISelectable other)
    {

    }

    public virtual void DirectTo(Vector3 position)
    {
    }

    public void DragToOtherSelectable(ISelectable other, PathLine line) { }
    public void DragToPoint(Vector3 point, PathLine line) { }
    public void SetSelectableState(SelectableState state)
    {
        switch (state)
        {
            case SelectableState.AVAILABLE:
                _highlit = false;
                SetColor(isActive? activeColor:unlitColor);
                break;
            case SelectableState.UNAVAILABLE:
                _highlit = false;
				SetColor(isActive? activeColor:unlitColor);
                break;
            case SelectableState.HOVER:
                _highlit = true;
                SetColor(isActive? activeColor:litColor);
                break;
            case SelectableState.PRESS:
                OnButtonPress();
                _highlit = true;
                SetColor(isActive? activeColor:litColor);
                break;
        }
    }
}

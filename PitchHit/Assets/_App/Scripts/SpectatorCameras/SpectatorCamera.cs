﻿using UnityEngine;
using System.Collections;
using Valve.VR.InteractionSystem;

[RequireComponent(typeof(Camera))]
public class SpectatorCamera : MonoBehaviour
{
    public bool shouldOrbit;
    public bool shouldLookAt;
    public bool shouldLookWithParent;
    public bool isGrabbable;

    public Transform target;
    public Vector3 targetOffset;
    public float orbitSpeed = 10f;

    private bool isActivated;
    private Camera cam;
    private CustomThrowable throwableComponent;
    public Canvas myCanvas;
    public Transform keepInFrontOf;
    public bool maintainInitialWorldAngle;

    private Vector3 initialPositionDelta;
    private Quaternion initialRotation;

    void Awake()
    {
        if (myCanvas)
        {
            myCanvas.gameObject.SetActive(false);
        }

        initialRotation = transform.rotation;

        if (maintainInitialWorldAngle)
        {
            if (target)
            {
                initialPositionDelta = transform.position - target.position;

            }
            else
            {
                if (transform.parent)
                {
                    initialPositionDelta = transform.position - transform.parent.position;
                }
                else
                {
                    initialPositionDelta = transform.position;
                }

            }
        }
    }

    // Use this for initialization
    void Start()
    {
        cam = GetComponent<Camera>();
        throwableComponent = GetComponent<CustomThrowable>();


#if RIFT
        bool shouldKeepInFrontOf = true;
#elif STEAM_VR
        //bool shouldKeepInFrontOf = GameController.isOculusInSteam;
        bool shouldKeepInFrontOf = true;
#else 
        bool shouldKeepInFrontOf = false;
#endif

        if (shouldKeepInFrontOf && keepInFrontOf)
        {
            float inFrontBuffer = 0f;
            Vector3 relativePos = transform.InverseTransformPoint(keepInFrontOf.position);
            if (relativePos.z > inFrontBuffer)
            {

                transform.position += transform.forward * (relativePos.z - inFrontBuffer);
            }
        }
    }

    public void OnEnable()
    {
        if (myCanvas)
        {
            myCanvas.gameObject.SetActive(true);
        }
        PerformOrientation();
    }

    public void OnDisable()
    {
        if (myCanvas)
        {
            myCanvas.gameObject.SetActive(false);
        }

        if (isGrabbable && throwableComponent)
        {
            CustomHand holdingHand = throwableComponent.GetAttachedHand();
            if (holdingHand)
            {
                holdingHand.DetachObject(gameObject);
                holdingHand.HoverUnlock(null);
            }
        }
    }

    public void ActivateGrabbable()
    {
        isActivated = true;
        //VRThrowable interact = GetComponentInChildren<VRThrowable>();
        //if(interact)
        //{
        //    interact.
        //}
    }

    // Update is called once per frame
    void Update()
    {
        if (cam.enabled)
        {
            PerformOrientation();
        }
    }

    private void PerformOrientation()
    {
        if (isGrabbable)
        {

        }
        else
        {

            if (target)
            {
                if (maintainInitialWorldAngle)
                {
                    transform.position = target.position + initialPositionDelta;
                    transform.rotation = initialRotation;
                }
                if (shouldLookAt)
                {
                    if (transform.parent)
                    {
                        if (shouldLookWithParent)
                        {
                            if (transform.parent.parent)
                            {
                                transform.parent.LookAt(target.position + targetOffset, transform.parent.parent.up);
                            }
                            else
                            {
                                transform.parent.LookAt(target.position + targetOffset, Vector3.up);
                            }
                        }
                        else
                        {
                            transform.LookAt(target.position + targetOffset, transform.parent.up);
                        }
                    }
                    else
                    {
                        transform.LookAt(target.position + targetOffset, Vector3.up);
                    }
                }
                if (shouldOrbit)
                {
                    transform.RotateAround(target.position, Vector3.up, orbitSpeed * Time.deltaTime);
                }
            }
            else
            {
                if (maintainInitialWorldAngle)
                {
                    transform.rotation = initialRotation;
                    if(target)
                    {
                        transform.position = target.position + initialPositionDelta;
                    }
                    else
                    {
                        if (transform.parent)
                        {
                            transform.position = transform.parent.position + initialPositionDelta;
                        }
                        else
                        {
                            transform.position = initialPositionDelta;
                        }
                    }
                }
               
            }
        }
    }
}

﻿using UnityEngine;
using Cinemachine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

public enum CameraShot
{
    None,
    Home,
    First,
    Pitcher,
    Ball,
    Runner,
    ThirdTripod,
    Booth,
    Custom,
    VR
}

public class SpectatorCameraController : MonoBehaviour
{
    public CinemachineBrain cinemachineBrain;

    public CinemachineVirtualCamera ballCam;
    public CinemachineVirtualCamera runnerCam;
    public CinemachineVirtualCamera homePlateCam;
    public CinemachineVirtualCamera thirdBaseCam;
    public CinemachineVirtualCamera pitcherMoundCam;
    public CinemachineVirtualCamera boothCam;
    public CinemachineVirtualCamera customCam;
    public CinemachineVirtualCamera VRCam;

    public GameObject RenderScreen; //This is the quad in 3D space that we're slapping the render texture on.

    public CinemachineVirtualCamera currentCam;
    public GameObject instructionsPrefab;
    protected bool shouldParentToCameraRig = false;

    public Transform ballFollower;
    public Transform runnerFollower;
    public float nextShotChangeTime;
    private float lastShotChangeTime;

    private static bool hasTaughtCameras;
    private bool allowHDR = false;
    // Use this for initialization

    private List<CameraShot> cameraPool = new List<CameraShot>();
    private Dictionary<CameraShot, CinemachineVirtualCamera> cameraDict = new Dictionary<CameraShot, CinemachineVirtualCamera>();
    private CinematicProfile currentProfile;
    [SerializeField]
    private CinematicProfile defaultProfile;

    //used for randomly picking from a weighted pool
    private float randomTotalRange = 0;

    private static SpectatorCameraController Instance;

    void Awake()
    {
        if (Instance == null) Instance = this;

        cameraDict = new Dictionary<CameraShot, CinemachineVirtualCamera>
        {
            {CameraShot.Pitcher, pitcherMoundCam },
            {CameraShot.Home, homePlateCam },
            {CameraShot.Ball, ballCam },
            {CameraShot.Runner, runnerCam },
            {CameraShot.ThirdTripod, thirdBaseCam },
            {CameraShot.Booth, boothCam },
            {CameraShot.Custom, customCam },
            {CameraShot.VR, VRCam }
        };

        Camera.main.allowHDR = allowHDR;

        foreach (CinemachineVirtualCamera cam in cameraDict.Values)
        {
            cam.enabled = false;
        }
    }

    void Start()
    {
        if (shouldParentToCameraRig)
        {
            transform.SetParent(TButt.TBCameraRig.instance.GetActiveCameraObject().transform);
        }
        if(ballFollower)
        {
            ballFollower.SetParent(null);
        }
        if(runnerFollower)
        {
            runnerFollower.SetParent(null);
        }

        LoadCinematicProfile(defaultProfile);
    }


    public static void LoadCinematicProfile_Static(CinematicProfile profile)
    {
        if (Instance)
        {
            Instance.LoadCinematicProfile(profile);
        }
    }

    public void LoadCinematicProfile(CinematicProfile profile)
    {
        if (currentProfile == profile) return;

        cameraPool.Clear();
        CameraShot[] shots = (CameraShot[])Enum.GetValues(typeof(CameraShot));
        randomTotalRange = 0;
        foreach(CameraShot shot in shots)
        {
            if (profile.HasShot(shot))
            {
                cameraPool.Add(shot);
                randomTotalRange += profile.ShotWeight(shot);
            }
        }
        currentProfile = profile;
        if (profile.delay == 0)
        {
            ChangeCameraShot();
        }
        else
        {
            nextShotChangeTime = Time.time + profile.delay;
        }
    }

    public void ChangeCameraShot()
    {
        //We don't want to override the Director's camera decisions.
        if (FieldManager.currentHumanPlayerRole == FieldManager.HumanPlayerRole.BROADCAST)
        {
            return;
        }

        CameraShot newShot = CameraShot.None;
        if (currentProfile != null)
        {
            float rand = UnityEngine.Random.Range(0, randomTotalRange);
            float runningTotal = 0;
            foreach (CameraShot shotOption in cameraPool)
            {
                float currentWeight = currentProfile.ShotWeight(shotOption);
                if (rand >= runningTotal && rand <= (runningTotal + currentWeight))
                {
                    newShot = shotOption;
                    break;
                }
                runningTotal += currentWeight;
            }
        }
        SetCameraShot(newShot);
    }

    private void SetCameraShot(CameraShot shot)
    {
        if (shot == CameraShot.None) return;

        //RenderScreen.SetActive(shot != CameraShot.VR);
        CinemachineVirtualCamera nextCam;
        if (cameraDict.TryGetValue(shot, out nextCam))
        {
            if (currentCam)
            {
                if (currentCam == nextCam) return;
                currentCam.enabled = false;
            }
            nextCam.enabled = true;
            Debug.Log("Enabled Camera " + nextCam.gameObject.name);

            currentCam = nextCam;
        }
    }

    public void SetCinemachineEnabled(bool enabled)
    {
        Debug.Log("Cinemachine Enabled " + enabled);
        cinemachineBrain.gameObject.SetActive(enabled);
    }

    public void SetCamToBall()
    {
        SetCameraShot(CameraShot.Ball);
    }

    public void SetCamToBooth()
    {
        Debug.Log("Setting cam to booth");
        SetCameraShot(CameraShot.Booth);
    }

    public void SetCamToFirst()
    {
        SetCameraShot(CameraShot.First);
    }

    public void SetCamToHome()
    {
        SetCameraShot(CameraShot.Home);
    }

    public void SetCamToPitcher()
    {
        SetCameraShot(CameraShot.Pitcher);
    }

    public void SetCamToRunner()
    {
        SetCameraShot(CameraShot.Runner);
    }

    public void SetCamToThirdTripod()
    {
        SetCameraShot(CameraShot.ThirdTripod);
    }

    public void SetCamToCustom()
    {
        SetCameraShot(CameraShot.Custom);
    }

    public void SetCamToVR()
    {
        SetCameraShot(CameraShot.VR);
    }

    void Update()
    {
        if (Time.time >= nextShotChangeTime)
        {
            lastShotChangeTime = Time.time;
            nextShotChangeTime = lastShotChangeTime + UnityEngine.Random.Range(4f, 5f);
            ChangeCameraShot();
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            ChangeCameraShot();
        }

        if (ballFollower)
        {
            if (FieldManager.Instance && FieldManager.Instance.ballInPlay)
            {
                ballFollower.position = FieldManager.Instance.ballInPlay.transform.position;
            }
            else
            {
                if (ballController.currentlyActiveBall)
                {
                    ballFollower.position = ballController.currentlyActiveBall.transform.position;
                }
            }
        }

        if (runnerFollower)
        {
            if (FieldManager.Instance && FieldManager.offensivePlayers.Count > 0)
            {
                runnerFollower.position = FieldManager.offensivePlayers.First.Value.transform.position;
            }
        }
    }
}

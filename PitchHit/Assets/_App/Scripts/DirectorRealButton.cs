﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.UI;

public class DirectorRealButton : MonoBehaviour
{
    public Transform theButton;
    public Transform theBase;
    public DirectorRealButton[] otherDirectorButtons;
    public Material buttonMaterialUp;
    public Material buttonMaterialDown;
    public AudioClip soundOnActivate;
    public AudioClip soundOnDeactivate;
    public float buttonPushDepth = .07f;
    [Range(.1f, .9f)]
    public float buttonSensitivity = .5f;

    public bool buttonDown;
    public UnityEvent onButtonActivated;
    public UnityEvent onButtonDeactivated;
    private const float lookAtAngle = 45f;
    private Vector3 deepestButtonPosition;

    public Renderer buttonSkin;
    public Renderer baseSkin;

    private static LayerMask batLayer = -1;
    private static LayerMask pusherLayer = -1;
    private Vector3 initialPosition;
    private Vector3 initialButtonLocalPosition;
    private bool buttonIsTouched;
    private bool hasBeenPressed;
    private float pressedDepth;

    void Awake()
    {
        if (batLayer < 0)
        {
            batLayer = LayerMask.NameToLayer("Bat");
            pusherLayer = LayerMask.NameToLayer("ButtonPusher");
        }
        initialButtonLocalPosition = theButton.localPosition;
        SetupButton();
    }

    private void SetupButton()
    {
        theButton.localPosition = initialButtonLocalPosition;
        initialPosition = transform.position;
        deepestButtonPosition = theButton.position;
        if (buttonMaterialUp != null) buttonSkin.material = buttonMaterialUp;
    }

    void Update()
    {
        if (transform.position != initialPosition)
        {
            SetupButton();
        }

        if (!buttonDown)
        {
            if (Camera.main && Vector3.Angle(Camera.main.transform.forward, transform.position - Camera.main.transform.position) < lookAtAngle)
            {
                RaycastHit hit = new RaycastHit();
                buttonIsTouched = false;
                Vector3 bounds = theBase.GetComponent<BoxCollider>().bounds.extents;
                if (Physics.OverlapBox(deepestButtonPosition, bounds * 0.5f, theButton.rotation, (1 << batLayer) | (1 << pusherLayer)).Length > 0)
                {
                    buttonIsTouched = true;
                    hit.distance = 0f;
                }
                else
                {
                    buttonIsTouched = Physics.BoxCast(deepestButtonPosition, bounds * 0.5f, theBase.forward, out hit, theButton.rotation, buttonPushDepth, (1 << batLayer) | (1 << pusherLayer));
                }

                if (buttonIsTouched)
                {
                    theButton.position = deepestButtonPosition + (theButton.forward * hit.distance);
                    if (hit.distance < buttonPushDepth * buttonSensitivity)
                    {
                        //Button Activate
                        buttonDown = true;
                        theButton.position = deepestButtonPosition;
                        if (buttonMaterialDown != null) buttonSkin.material = buttonMaterialDown;
                        onButtonActivated.Invoke();
                        foreach(DirectorRealButton rb in otherDirectorButtons)
                        {
                            rb.RadioDeactivate();
                        }
                    }
                }
                else
                {
                    OnNotTouched();
                }
            }
            else
            {
                OnNotTouched();
            }
        }
    }

    public float GetPressedDepth()
    {
        return pressedDepth;
    }

    public void RadioDeactivate()
    {
        buttonDown = false;
        theButton.position = deepestButtonPosition + (theButton.forward * buttonPushDepth);
        if (buttonMaterialUp != null) buttonSkin.material = buttonMaterialUp;
        onButtonDeactivated.Invoke();
    }

    public void OnNotTouched()
    {
        if (!buttonDown)
        {
            buttonIsTouched = false;
            theButton.position = deepestButtonPosition + (theButton.forward * buttonPushDepth);
            pressedDepth = 0;
        }
    }

    void OnTriggerExit(Collider c)
    {
        if (c.gameObject.layer == batLayer)
        {
            OnNotTouched();
        }
    }
    void OnDestroy()
    {

    }

    void OnDisable()
    {

    }
}

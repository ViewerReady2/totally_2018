﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using Valve.VR.InteractionSystem;

public class CustomThrowable : MonoBehaviour {
    public UnityEvent onHandHoverBegin;
    public UnityEvent onHandHoverEnd;
    public UnityEvent onAttachedToHand;
    public UnityEvent onDetachedFromHand;

    private CustomHand currentAttachedCustomHand;

#if RIFT
    public void OnGrabbed(Grabber grabber)
    {
        currentAttachedCustomHand = grabber.customHand;
    }

    public void OnUngrabbed(Grabber grabber)
    {
        currentAttachedCustomHand = grabber.customHand;

        onDetachedFromHand.Invoke();
    }
#elif STEAM_VR
    public void OnAttachedToHand(Hand hand)
    {

        Debug.Log("on attached to Hand");
        currentAttachedCustomHand = hand.GetComponent<CustomHand>();

        onAttachedToHand.Invoke();
    }


    public void OnDetachedFromHand(Hand hand)
    {
        onDetachedFromHand.Invoke();

        currentAttachedCustomHand = null;

    }
#endif

    public CustomHand GetAttachedHand()
    {
        return currentAttachedCustomHand;
    }
}

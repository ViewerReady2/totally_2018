﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformVolume : MonoBehaviour {
    public float viveVolume = 1f;
    public float oculusVolume = .5f;
	// Use this for initialization
	void Start () {
        AudioSource sound = GetComponent<AudioSource>();

#if STEAM_VR
        sound.volume = viveVolume;
#elif RIFT
        sound.volume = oculusVolume;
#endif   
    }
	
}

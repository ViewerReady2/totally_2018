﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;

public class MakeTeamInfo
{
    [MenuItem("Assets/Create/TeamInfo")]
    public static void CreateMyAsset()
    {
        TeamInfo asset = ScriptableObject.CreateInstance<TeamInfo>();

        AssetDatabase.CreateAsset(asset, "Assets/_App/Scriptable Objects/NewTeamInfo.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
}
#endif

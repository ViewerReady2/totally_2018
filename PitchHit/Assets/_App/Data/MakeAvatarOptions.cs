﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;

public class MakeAvatarOptions
{
    [MenuItem("Assets/Create/AvatarOptions")]
    public static void CreateMyAsset()
    {
        AvatarMeshFeature asset = ScriptableObject.CreateInstance<AvatarMeshFeature>();

        AssetDatabase.CreateAsset(asset, "Assets/_App/Data/NewAvatarOptions.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
}
#endif

﻿Shader "Particles/Additive (No Batching)"{
	Properties {
 	_TintColor ("Tint Color", Color) = (0.5,0.5,0.5,0.5)
 	_MainTex ("Particle Texture", 2D) = "white" { }
	_InvFade ("Soft Particles Factor", Range(0.01,3)) = 1
}
	SubShader { 
		Tags { "QUEUE"="Transparent" "IGNOREPROJECTOR"="true" "RenderType"="Transparent" "DisableBatching"="True" }
		Cull Back
		Blend SrcAlpha OneMinusSrcAlpha
		ColorMask RGB




		Pass
		{
			

		}

	}

	fallback "Particles/Additive"
}

﻿using UnityEngine;
using System.Collections;

public class LookAtScript : MonoBehaviour {
    public Transform oculusCamera;
	// Use this for initialization
	//void Start () {
	
	//}
	
	// Update is called once per frame
	void Update () {
        if(GameController.isTrueOculus && oculusCamera)
        {
            transform.LookAt(oculusCamera);
        }

        if (Camera.main && Camera.main.transform)
        {
            this.transform.LookAt(Camera.main.transform.position);
        }
	}
}

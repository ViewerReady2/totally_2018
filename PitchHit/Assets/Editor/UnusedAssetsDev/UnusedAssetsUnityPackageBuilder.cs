﻿using System;
using System.Linq;
using UnityEditor;

namespace UnusedAssets.Editor
{
    public static class UnusedAssetsUnityPackageBuilder
    {
        /// <summary> 
        /// Export the plugin to a .unitypackage
        /// </summary>
        [MenuItem("Tools/Unused Assets Dev/Create Plugin Package", false, 103)]
        public static void CreatePackage()
        {
            var packageFilename = string.Format("Unused Assets Finder {0:yyyy-dd-MMM HH-mm-ss}", DateTime.Now);
            var exportPath = EditorUtility.SaveFilePanel("Save Plugin Package To...", "", packageFilename, "unitypackage");

            if (string.IsNullOrEmpty(exportPath)) return;

            var assets = AssetDatabase.GetAllAssetPaths().ToList();

            assets.RemoveAll(asset => !asset.StartsWith("Assets/UnusedAssets/")); //Remove all assets that are not in a known unused assets folder
            assets.RemoveAll(asset => asset.Contains("/Resources/")); //Remove Resources
            assets.RemoveAll(asset => asset.EndsWith(".asset")); //Remove All Scriptable Objects, these should get auto generated

            //scripts
            assets.RemoveAll(asset => asset.Contains("UnusedAssetsUnityPackageBuilder")); //Strings.GetNameOf doesn't work for classes
            assets.RemoveAll(asset => asset.Contains("ScreenshotHelper"));

            AssetDatabase.ExportPackage(assets.ToArray(), exportPath, ExportPackageOptions.Interactive);
        }

        /// <summary> 
        /// Export the plugin to a .unitypackage with the demo
        /// </summary>
        [MenuItem("Tools/Unused Assets Dev/Create Plugin Package with Demo", false, 104)]
        public static void CreatePackageWithDemo()
        {
            var packageFilename = string.Format("Unused Assets Finder {0:yyyy-dd-MMM HH-mm-ss}", DateTime.Now);
            var exportPath = EditorUtility.SaveFilePanel("Save Plugin Package To...", "", packageFilename, "unitypackage");

            if (string.IsNullOrEmpty(exportPath)) return;

            var assets = AssetDatabase.GetAllAssetPaths().ToList();

            assets.RemoveAll(asset => !asset.StartsWith("Assets/UnusedAssets/") && !asset.StartsWith("Assets/UnusedAssets Demos/")); //Remove all assets that are not in a known unused assets folder
            assets.RemoveAll(asset => asset.Contains("/Resources/")); //Remove Resources
            assets.RemoveAll(asset => asset.EndsWith(".asset")); //Remove All Scriptable Objects, these should get auto generated

            //scripts
            assets.RemoveAll(asset => asset.Contains("UnusedAssetsUnityPackageBuilder")); //Strings.GetNameOf doesn't work for classes
            assets.RemoveAll(asset => asset.Contains("ScreenshotHelper"));

            AssetDatabase.ExportPackage(assets.ToArray(), exportPath, ExportPackageOptions.Interactive);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.Events;

[RequireComponent(typeof(CinemachineVirtualCamera))]
public class CinemachineCameraLiveListener : MonoBehaviour {
    private CinemachineVirtualCamera _camera
    {
        get {
            if (m_camera == null) m_camera = GetComponent<CinemachineVirtualCamera>();
            return m_camera;
        }
    }
    private CinemachineVirtualCamera m_camera;

    private bool IsLive
    {
        get { return CinemachineCore.Instance.IsLive(_camera); }
    }
    private bool _liveLastFrame = false;
    public UnityEvent OnLive;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if(IsLive && !_liveLastFrame)
        {
            OnLive.Invoke();
        }
        _liveLastFrame = IsLive;	
	}
}
